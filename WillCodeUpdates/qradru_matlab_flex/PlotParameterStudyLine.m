function PlotParameterStudyLine(filename,linespec,COTlimit)


load(['ParameterStudies/',filename],'configurations','types','indices','grids');
figure(3);
hold on
box on
grid on
x = [];
y = [];
for i = 1:length(configurations)
     if(configurations{i}.nFailed < 5)
         if(configurations{i}.finalDatStruct.p(4) > 0.1 && (configurations{i}.costValue < COTlimit));
             x = [x,grids{1}(configurations{i}.indexVector(1))];
             y = [y,configurations{i}.costValue];
         end
     end
end
z = [x;y];
[tmp ind] = sort(z(1,:));
x = z(1,ind);
y = z(2,ind);
plot(x,y,linespec); %actuated spine

end
