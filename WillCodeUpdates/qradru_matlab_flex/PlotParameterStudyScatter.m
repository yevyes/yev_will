function PlotParameterStudyScatter(filename,linespec,gaitType)

load(['ParameterStudies/',filename],'configurations','types','indices','grids');
figure(3);
hold on
box on
grid on

for i = 1:length(configurations)
    %for j = [8,10]
    for j = [6,7,9,11]
        if(configurations{i}.nFailed < 5)
            if(configurations{i}.finalDatStruct.p(4) > 0.1 && (configurations{i}.costValue < 0.4));
                 plot(grids{1}(configurations{i}.indexVector(1)),configurations{i}.finalDatStruct.p(j),'*','Color',[1-((j-6)*0.2),j*0.05,(j-6)/5]);
                 %plot(grids{1}(configurations{i}.indexVector(1)),configurations{i}.finalDatStruct.p(10),linespec);
            end
        end
      
    end
end
legend('kalpha','du max alpha','du max l','du max theta');
title(gaitType);
%legend('kl','ktheta');
%legend('kl Trotting',['kl ', gaitType]);

end