function PlayMuscodResults_ParamStudyPoint(ParamStudyName, Velocity, plotflag)
    %% Init:
    % Reset the MATLAB search path to its default value:
    path(pathdef);
    % Set the path to include all library functions:
    path(path,'/home/yevyes/Yev_Will/qradru_matlab_flex/playqra2');
    load(ParamStudyName)

    %% Read file
    [t, y, u, yName, uName] = ReadPLTfile('BaseData/Trot_SH_SL_BASE_01',45,9);

    %% Read file
   %[t, y, u, yName, uName] = ReadPLTfile('BaseData/WALK_PH_PL_BASE_00',25,4);

   tol = 0.001; % Tolerance to find velocity
   
   for i = 1:length(configurations)
       if configurations{i}.nFailed < 5
           if (abs(configurations{i}.finalDatStruct.p(4) - Velocity) < tol)
               ind1 = configurations{i}.indexVector(1);
               %ind2 = configurations{i}.indexVector(2);
               p = configurations{i}.finalDatStruct.p;
               CostFuncVal = configurations{i}.costValue;
               disp(['i value is: ' num2str(i)])
               disp(['cost value: ' num2str(CostFuncVal)])
               %sd_fix = configurations{i}.finalDatStruct.sd_fix
               %sd_fix_init = configurations{i}.finalDatStruct.sd_fix_init
           end
       end%             u(exctStateIndices.ulL) = ulLVec(j);
%             u(exctStateIndices.ulR) = ulRVec(j); 
   end
   
   clear t y u
   
   t = time{ind1};
   totaltime = t(end)
   u = forces{ind1};
   y = states{ind1};
   h = hval{ind1}
   
   if (plotflag)
%     forcesParam = u;
%     statesParam = y;
%     timeParam = t;
%     hParam = h;

% Plot motions
   
     %stateIndices = 1:45;
     stateIndices = [7 9 11 19 21];
    
     figure(3)
     clf
     hold on
     grid on
     box on
     c_lim_alpha = 0.07*0.5^-0.37; 
     c_lim_l     = 0.07*0.5^-0.37;
     P_max_alpha = 24*0.5^1.35;
     P_max_l     = 24*0.5^1.35;
     plot(t,u(:,[1,6]))
     % Show limits on actuator torques:
     plot([t(1),t(end)],-[c_lim_alpha*P_max_alpha/p(7),c_lim_alpha*P_max_alpha/p(7)],'b:');
     plot([t(1),t(end)],-[c_lim_l*P_max_l/p(9),c_lim_l*P_max_l/p(9)],'r:');
     plot([t(1),t(end)],+[c_lim_alpha*P_max_alpha/p(7),c_lim_alpha*P_max_alpha/p(7)],'b:');
     plot([t(1),t(end)],+[c_lim_l*P_max_l/p(9),c_lim_l*P_max_l/p(9)],'r:');     
     % Show limits on actuator torques:
     legend(uName([1,6]));
     
     figure(4)
     clf
     hold on
     grid on
     box on
     plot(t,y(:,stateIndices))
     legend(yName{stateIndices});

   end
%     
%     
%     figure(3)
   
    %% Extract trajectories
    tVec       = [t(1:end-1,1);t(1:end-1,1)+t(end,1)];
    xVec       = [y(1:end-1,1);y(1:end-1,1)+y(end,1)];
    yVec       = [y(1:end-1,3);y(1:end-1,3)];
    phiVec     = [y(1:end-1,5);y(1:end-1,5)];
    thetaVec     = [y(1:end-1,7);y(1:end-1,7)];
    alphaLHVec  = [y(1:end-1,11);y(1:end-1,23)];
    ualphaLHVec = [y(1:end-1,13);y(1:end-1,25)];
    alphaLFVec  = [y(1:end-1,15);y(1:end-1,19)];
    ualphaLFVec = [y(1:end-1,17);y(1:end-1,21)];
    alphaRFVec  = [y(1:end-1,19);y(1:end-1,15)];
    ualphaRFVec = [y(1:end-1,21);y(1:end-1,17)];
    alphaRHVec  = [y(1:end-1,23);y(1:end-1,11)];
    ualphaRHVec = [y(1:end-1,25);y(1:end-1,13)];
    lLHVec      = [y(1:end-1,27);y(1:end-1,39)];
    ulLHVec     = [y(1:end-1,29);y(1:end-1,41)];
    lLFVec      = [y(1:end-1,31);y(1:end-1,35)];
    ulLFVec     = [y(1:end-1,33);y(1:end-1,37)];
    lRFVec      = [y(1:end-1,35);y(1:end-1,31)];
    ulRFVec     = [y(1:end-1,37);y(1:end-1,33)];
    lRHVec      = [y(1:end-1,39);y(1:end-1,27)];
    ulRHVec     = [y(1:end-1,41);y(1:end-1,29)];
    % Resample
    method = 'linear';
    n = 50;      % # of frames per step
    % to create destinct values:
    tOld       = tVec+linspace(0,1e-10,length(tVec))';
    tVec       = linspace(0,2*t(end,1),n);
    tVec       = tVec(1:end-1);
    xVec       = interp1(tOld, xVec,tVec, method);
    yVec       = interp1(tOld, yVec,tVec, method);
    phiVec     = interp1(tOld, phiVec,tVec, method);
    thetaVec = interp1(tOld, thetaVec,tVec, method);
    alphaLHVec  = interp1(tOld, alphaLHVec,tVec, method);
    ualphaLHVec = interp1(tOld, ualphaLHVec,tVec, method);
    alphaLFVec  = interp1(tOld, alphaLFVec,tVec, method);
    ualphaLFVec = interp1(tOld, ualphaLFVec,tVec, method);
    alphaRFVec  = interp1(tOld, alphaRFVec,tVec, method);
    ualphaRFVec = interp1(tOld, ualphaRFVec,tVec, method);
    alphaRHVec  = interp1(tOld, alphaRHVec,tVec, method);
    ualphaRHVec = interp1(tOld, ualphaRHVec,tVec, method);
    lLHVec      = interp1(tOld, lLHVec,tVec, method);
    ulLHVec     = interp1(tOld, ulLHVec,tVec, method);
    lLFVec      = interp1(tOld, lLFVec,tVec, method);
    ulLFVec     = interp1(tOld, ulLFVec,tVec, method);
    lRFVec      = interp1(tOld, lRFVec,tVec, method);
    ulRFVec     = interp1(tOld, ulRFVec,tVec, method);
    lRHVec      = interp1(tOld, lRHVec,tVec, method);
    ulRHVec     = interp1(tOld, ulRHVec,tVec, method);
%     % Plot
    if(plotflag)
     figure(3)
     %clf
     
%      plot(tVec,alphaLHVec,'y');
     %{
     plot(tVec,ualphaLHVec,'y:');
     plot(tVec,alphaRHVec,'c');
     plot(tVec,ualphaRHVec,'c:');
     plot(tVec,alphaLFVec,'y');
     plot(tVec,ualphaLFVec,'y:');
     plot(tVec,alphaRFVec,'c');
     plot(tVec,ualphaRFVec,'c:');
     plot(tVec,lLHVec,'k');
     plot(tVec,ulLHVec,'k:');
     plot(tVec,lRHVec,'m');
     plot(tVec,ulRHVec,'m:');
     plot(tVec,lLFVec,'k');
     plot(tVec,ulLFVec,'k:');
     plot(tVec,lRFVec,'m');
     plot(tVec,ulRFVec,'m:');
     %}
    end
    %% Animate:
	% Set up the graphical output:
    graph3DOUTPUT = Quadruped3DCLASS(false);
    xOffset = 0; % This shifts the model forward from step to step
    % If desired, every iteration a rendered picture is saved to disc.  This
    % can later be used to create a animation of the monopod.
    % (un)comment the following lines, if you (don't) want to save the
    % individual frames to disc:
    %frameCount = 0; 
    %mkdir MovieFramesPrismaticBipedRunWalk;
    [y, ~, contStateIndices] = ContStateDefinition();
    z = DiscStateDefinition();
    [u, ~, exctStateIndices] = ExctStateDefinition();
    %  For each step
    for i = 1:2 % Movie is composed of 2*2 strides
        for j = 1:n-1
            y(contStateIndices.x) = xVec(j) + xOffset;
            y(contStateIndices.y) = yVec(j);
            y(contStateIndices.phi) = phiVec(j);
            y(contStateIndices.theta) = thetaVec(j);
            y(contStateIndices.alphaLH) = alphaLHVec(j);
            y(contStateIndices.alphaLF) = alphaLFVec(j);
            y(contStateIndices.alphaRF) = alphaRFVec(j);
            y(contStateIndices.alphaRH) = alphaRHVec(j);
            y(contStateIndices.lLH) = lLHVec(j);
            y(contStateIndices.lLF) = lLFVec(j);
            y(contStateIndices.lRF) = lRFVec(j);
            y(contStateIndices.lRH) = lRHVec(j);
            u(exctStateIndices.ualphaLH) = ualphaLHVec(j); 
            u(exctStateIndices.ualphaLF) = ualphaLFVec(j); 
            u(exctStateIndices.ualphaRF) = ualphaRFVec(j); 
            u(exctStateIndices.ualphaRH) = ualphaRHVec(j); 
            u(exctStateIndices.ulLH) = ulLHVec(j); 
            u(exctStateIndices.ulLF) = ulLFVec(j); 
            u(exctStateIndices.ulRF) = ulRFVec(j); 
            u(exctStateIndices.ulRH) = ulRHVec(j); 
            graph3DOUTPUT.update(y,z,[],u);
            % (un)comment the following line, if you (don't) want to save the
            % individual frames to disc:
           % fig = gcf; print(fig,'-r600','-djpeg',['MovieFramesPrismaticBipedRunWalk/Frame',num2str(frameCount,'%04d.jpg')],'-zbuffer'); frameCount = frameCount + 1;
        end
        % After each step the model is moved forward:
        xOffset = xOffset + xVec(end);
    end     
    
    %%
    cd(PathToMatlab);
end

