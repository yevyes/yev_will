% Runs a single instance of Muscod for a specified gait and hip/leg type.
% All arguments are to be passed as strings


function [filename, cmdout] = RunSingleBaseDataFile(datStruct, gaitType, hipJointType, legJointType, iteration, folderlocations)

load(folderlocations);

cd(PathToMatlab)
switch hipJointType
        case 'PEA'
            p(1) = 1;
            hipString = 'PH';
        case 'SEA'
            p(1) = 0;
            hipString = 'SH';
        otherwise
            error('Unknown hip joint type')
end
switch legJointType
    case 'PEA'
            p(2) = 1;
            legString = 'PL';
    case 'SEA'
            p(2) = 0;
            legString = 'SL';
    otherwise
            error('Unknown hip joint type')
end

filename = strcat(gaitType,'_',hipString,'_',legString,'_BASE_',iteration);

% Make all changes to the datStruct above this line
save(strcat('BaseData/',filename,'.mat'), 'datStruct');
disp(strcat('BaseData saved under: ', filename));
CreateDatFile(strcat('BaseData/',filename),datStruct); %Save datStruct to be run in Muscod

% Copy to project directory to process in Muscod
currentPath = pwd;
system(['cp ',pwd,['/BaseData/',gaitType,'_*_BASE_',iteration,'.dat '], PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_11(1), cmdout] = system([PathToMuscod,[' ',filename]]);
disp(MuscodResults_11(1))

cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,['RES/',gaitType,'_*_BASE_',iteration,'.* '],pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);


end