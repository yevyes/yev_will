%% Computer dependent settings:
% Possibility one: Lab Computer
PathToMuscod  = '/home/yevyes/MUSCOD-II/MC2/Release/bin/muscod';
PathTo2Dqrad   = '/home/yevyes/Yev_Will/qradru_model_flex/';
PathToMatlab = '/home/yevyes/Yev_Will/qradru_matlab_flex/';

save('folderlocations','PathToMuscod','PathTo2Dqrad','PathToMatlab');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Trotting
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Step 1
%Create intial datStruct with no constraints to converge on a
%gait
cd(PathToMatlab);
datStruct = CreateDatStruct('Trot','SEA','SEA');
% Make all changes to the datStruct directly below this line

datStruct.p(4) = 0.4; %enforce average velocity
datStruct.sd_fix(9) = 1;
datStruct.sd_fix_init(9) = 1;
datStruct.sd(9) = 0;

[filename, cmdout] = RunSingleBaseDataFile(datStruct,'Trot','SEA','SEA','00','folderlocations');
%%
PlotMuscodResults('BaseData/Trot_SH_SL_BASE_007',[7,9]);
%%
PlayMuscodResults('BaseData/Trot_SH_SL_BASE_007');
%% STEP 2
cd(PathToMatlab);
datStruct = UpdateDatStruct(datStruct,'BaseData/Trot_SH_SL_BASE_00', 30,45,9);

datStruct.p(5) = 1; %enforce ground clearance constraints

[filename, cmdout] = RunSingleBaseDataFile(datStruct,'Trot','SEA','SEA','01','folderlocations');
%% STEP 3
cd(PathToMatlab);
datStruct = UpdateDatStruct(datStruct,'BaseData/Trot_SH_SL_BASE_01', 30,45,9);
        
datStruct.p(5) = 7; %enforce ground clearance and motor torque constraints
datStruct.p(4) = 0.7;
datStruct.sd_fix(9) = 1;

[filename, cmdout] = RunSingleBaseDataFile(datStruct,'Trot','SEA','SEA','02','folderlocations');
%% STEP 4 Electrical Work Cost Function
cd(PathToMatlab);
datStruct = UpdateDatStruct(datStruct,'BaseData/Trot_SH_SL_BASE_02', 30,45,9);
        
datStruct.p(3) = 2;
datStruct.p(4) = 1.75;

[filename, cmdout] = RunSingleBaseDataFile(datStruct,'Trot','SEA','SEA','03','folderlocations');
%% STEP 1.0 % Try again with Actuated Spine
cd(PathToMatlab);
datStruct = CreateDatStruct('Trot','SEA','SEA');
        
datStruct.p(5) = 7; %enforce ground clearance and motor torque constraints
datStruct.p(4) = 0.7;
datStruct.p(13) = 1;

[filename, cmdout] = RunSingleBaseDataFile(datStruct,'Trot','SEA','SEA','002','folderlocations');
%% STEP 2.0 Trotting with a lower force limit
cd(PathToMatlab);
datStruct = UpdateDatStruct(datStruct,'BaseData/Trot_SH_SL_BASE_02', 30,45,9);
        
datStruct.p(9) = 1.0; %add a lower force limit
datStruct.sd_fix(9) = 1;

[filename, cmdout] = RunSingleBaseDataFile(datStruct,'Trot','SEA','SEA','001','folderlocations');
%% STEP 3.0 Free Parameters
cd(PathToMatlab);
datStruct = UpdateDatStruct(datStruct,'BaseData/Trot_SH_SL_BASE_02', 30,45,9);
        
datStruct.p_fix(6:11) = 0;
datStruct.sd_fix(9) = 1;

[filename, cmdout] = RunSingleBaseDataFile(datStruct,'Trot','SEA','SEA','003','folderlocations');

%% STEP 4.0 Trotting with active spine and free parameters
cd(PathToMatlab);
datStruct = UpdateDatStruct(datStruct,'BaseData/Trot_SH_SL_BASE_02', 30,45,9);
        
datStruct.p(13) = 1; %Active Spine
datStruct.p_fix(6:11) = 0;
datStruct.sd_fix(9) = 0;
datStruct.sd_fix_init(9) = 0;

[filename, cmdout] = RunSingleBaseDataFile(datStruct,'Trot','SEA','SEA','004','folderlocations');
%% STEP 5.0 Trotting with active spine and lower force limit
cd(PathToMatlab);
datStruct = UpdateDatStruct(datStruct,'BaseData/Trot_SH_SL_BASE_001', 30,45,9);
        
datStruct.p(13) = 1; %Active Spine
datStruct.sd_fix(9) = 0;
datStruct.sd_fix_init(9) = 0;


[filename, cmdout] = RunSingleBaseDataFile(datStruct,'Trot','SEA','SEA','005','folderlocations');
%% STEP 6.0 Trotting with active spine and lower hip torque limit
cd(PathToMatlab);
datStruct = UpdateDatStruct(datStruct,'BaseData/Trot_SH_SL_BASE_02', 30,45,9);
        
datStruct.p(13) = 1; %Active Spine
datStruct.sd_fix(9) = 0;
datStruct.sd_fix_init(9) = 0;
datStruct.p(7) = 10;

[filename, cmdout] = RunSingleBaseDataFile(datStruct,'Trot','SEA','SEA','006','folderlocations');
%% STEP 6.1 Trotting with active spine and lower hip torque limit
cd(PathToMatlab);
datStruct = UpdateDatStruct(datStruct,'BaseData/Trot_SH_SL_BASE_02', 30,45,9);
        
datStruct.p(13) = 1; %Active Spine
datStruct.sd_fix(9) = 0;
datStruct.sd_fix_init(9) = 0;
datStruct.p(7) = 10;
datStruct.p(4) = 1.4;

[filename, cmdout] = RunSingleBaseDataFile(datStruct,'Trot','SEA','SEA','007','folderlocations');

%% Parameter Study for Trotting
cd(PathToMatlab)
load('BaseData/Trot_SH_SL_BASE_006.mat','datStruct');

minVel = 0.025;
maxVel = 3.0;
deltaVel = 0.025;
VelVec = minVel:deltaVel:maxVel;

CreateParameterStudy('ParameterStudies/Trot_SH_SL_BASE_006',datStruct,'p',4,VelVec);
[CostMatval,forces, states, time, hval] = ProcessParameterStudy_ANNOTATED_Copy2('ParameterStudies/Trot_SH_SL_BASE_006', PathTo2Dqrad, PathToMuscod);

load('ParameterStudies/Trot_SH_SL_BASE_006');
save('Saved States/TrotFlexWithStates_Active_LowerHipTorqueLimit');
%% Parameter Study for Trotting
cd(PathToMatlab)
load('BaseData/Trot_SH_SL_BASE_005.mat','datStruct');

minVel = 0.025;
maxVel = 3.0;
deltaVel = 0.025;
VelVec = minVel:deltaVel:maxVel;

CreateParameterStudy('ParameterStudies/Trot_SH_SL_BASE_005',datStruct,'p',4,VelVec);
[CostMatval,forces, states, time, hval] = ProcessParameterStudy_ANNOTATED('ParameterStudies/Trot_SH_SL_BASE_005', PathTo2Dqrad, PathToMuscod);

load('ParameterStudies/Trot_SH_SL_BASE_005');
save('Saved States/TrotFlexWithStates_Active_LowerForceLimit');
%% Plot Parameter Study For Trotting

PlotParameterStudyLine('Trot_SH_SL_BASE_02','-r',0.4)
legend('Gallop','Trotting')
xlabel('Average Velocity');
ylabel('COT');

%% Plot Parameter Study For Trotting (free parameters)

PlotParameterStudyScatter('Trot_SH_SL_BASE_004','b*',' Trotting');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Walking
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%STEP 1
%Create intial datStruct with no constraints to converge on a
%gait
datStruct = CreateDatStruct('Walk','SEA','SEA');

datStruct.sd_fix_init(9) = 1;
datStruct.sd_fix(9) = 1;
datStruct.sd(9) = 0;


[filename, cmdout] = RunSingleBaseDataFile(datStruct,'Walk','SEA','SEA','00','folderlocations');
%%
PlotMuscodResults('BaseData/Walk_SH_SL_BASE_02',[7,9]);
%%
PlayMuscodResults('BaseData/Walk_SH_SL_BASE_02');
%% STEP 2
cd(PathToMatlab);
datStruct = UpdateDatStruct(datStruct,'BaseData/Walk_SH_SL_BASE_00', 30,45,9);
        
datStruct.p(5) = 1; %enforce ground clearance constraints

[filename, cmdout] = RunSingleBaseDataFile(datStruct,'Walk','SEA','SEA','01','folderlocations');
%% STEP 3
cd(PathToMatlab);
datStruct = UpdateDatStruct(datStruct,'BaseData/Walk_SH_SL_BASE_01', 30,45,9);
        
datStruct.p(5) = 7; %enforce ground clearance and motor torque constraints

[filename, cmdout] = RunSingleBaseDataFile(datStruct,'Walk','SEA','SEA','02','folderlocations');
%% STEP 1.0 Walking with an Actuated Spine
cd(PathToMatlab)
datStruct = UpdateDatStruct(datStruct,'BaseData/Walk_SH_SL_BASE_02', 30,45,9);

datStruct.p(13) = 1;
datStruct.sd_fix(9) = 0;
datStruct.sd_fix_init(9) = 0;
datStruct.p_fix(6:11) = 0;


[filename, cmdout] = RunSingleBaseDataFile(datStruct,'Walk','SEA','SEA','004','folderlocations');
%% Parameter Study for Walking (finished)
cd(PathToMatlab)
load('BaseData/Walk_SH_SL_BASE_02.mat','datStruct');

minVel = 0.0;
maxVel = 3.0;
deltaVel = 0.025;
VelVec = minVel:deltaVel:maxVel;

CreateParameterStudy('ParameterStudies/Walk_SH_SL_BASE_02',datStruct,'p',4,VelVec);
[CostMatval,forces, states, time, hval] = ProcessParameterStudy_ANNOTATED('ParameterStudies/Walk_SH_SL_BASE_02', PathTo2Dqrad, PathToMuscod);

load('ParameterStudies/Walk_SH_SL_BASE_02');
save('Saved States/WalkFlexWithStates');
%% Plot Parameter Study Walking

PlotParameterStudyLine('Walk_SH_SL_BASE_004','-c',0.4);

%% Plot Parameter Study Walking

PlotParameterStudyScatter('Walk_SH_SL_BASE_004','*k',' Walking');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Galloping
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%STEP 1
%Create intial datStruct with no constraints to converge on a
%gait
datStruct = CreateDatStruct('Gallop','SEA','SEA');

datStruct.p(4) = 1.0; %Set Average Speed
datStruct.p(5) = 7;

datStruct.sd_fix_init(9) = 1; %fix utheta
datStruct.sd_fix(9) = 1;
datStruct.sd(9) = 0;

[filename, cmdout] = RunSingleBaseDataFile(datStruct,'Gallop','SEA','SEA','00','folderlocations');
%% STEP 2 
cd(PathToMatlab);
datStruct = UpdateDatStruct(datStruct,'BaseData/Gallop_SH_SL_BASE_00', 30,45,9);

datStruct.p(9) = 1.0;

[filename, cmdout] = RunSingleBaseDataFile(datStruct,'Gallop','SEA','SEA','01','folderlocations');
%% STEP 3 Elec Losses Cost Function
cd(PathToMatlab);
datStruct = UpdateDatStruct(datStruct,'BaseData/Gallop_SH_SL_BASE_01', 30,45,9);

datStruct.p(3) = 2; %Try Elec Work Cost Function

[filename, cmdout] = RunSingleBaseDataFile(datStruct,'Gallop','SEA','SEA','002','folderlocations');
%% STEP 1.0 Test Galloping with actuated spine
datStruct = UpdateDatStruct(datStruct,'BaseData/Gallop_SH_SL_BASE_00', 30,45,9);

datStruct.p(13) = 1; % Actuated Spine
datStruct.sd_fix(9) = 0;
datStruct.sd_fix_init(9) = 0;
datStruct.p(4) = 1.0;

[filename, cmdout] = RunSingleBaseDataFile(datStruct,'Gallop','SEA','SEA','001','folderlocations');
%% STEP 2.0 Lower Force Limit with Actuated Spine
cd(PathToMatlab);
datStruct = UpdateDatStruct(datStruct,'BaseData/Gallop_SH_SL_BASE_00', 30,45,9);

datStruct.p(9) = 1.0;
datStruct.sd_fix(9) = 0;
datStruct.sd_fix_init(9) = 0;
datStruct.p(13) = 1;

[filename, cmdout] = RunSingleBaseDataFile(datStruct,'Gallop','SEA','SEA','005','folderlocations');
%% STEP 3.0 Free Parameters
cd(PathToMatlab);
datStruct = UpdateDatStruct(datStruct,'BaseData/Gallop_SH_SL_BASE_00', 30,45,9);

datStruct.p(4) = 1.2;
datStruct.p_fix(6:11) = 0;
datStruct.sd_fix(9) = 1;
datStruct.sd(9) = 0;

[filename, cmdout] = RunSingleBaseDataFile(datStruct,'Gallop','SEA','SEA','003','folderlocations');

%% STEP 4.0 Active Free Parameter
cd(PathToMatlab);
datStruct = UpdateDatStruct(datStruct,'BaseData/Gallop_SH_SL_BASE_003', 30,45,9);

datStruct.p(13) = 1;
datStruct.sd_fix_init(9) = 0;
datStruct.sd_fix(9) = 0;

[filename, cmdout] = RunSingleBaseDataFile(datStruct,'Gallop','SEA','SEA','004','folderlocations');
%% STEP 03 Passive New Parameters
cd(PathToMatlab);
datStruct = UpdateDatStruct(datStruct,'BaseData/Gallop_SH_SL_BASE_00', 30,45,9);

datStruct.p(6) = 6.5;
datStruct.p(10) = 35;
datStruct.p(11) = 25;
datStruct.sd_fix(9) = 1;
datStruct.sd_fix_init(9) = 1;
datStruct.p(4) = 1.6;

[filename, cmdout] = RunSingleBaseDataFile(datStruct,'Gallop','SEA','SEA','03','folderlocations');

%%
PlotMuscodResults('BaseData/Gallop_SH_SL_BASE_03',[7,9]);
%%
PlayMuscodResults('BaseData/Gallop_SH_SL_BASE_03');
%% Parameter Study for Galloping (finished)
cd(PathToMatlab)
load('BaseData/Gallop_SH_SL_BASE_004.mat','datStruct');

minVel = 0.025;
maxVel = 4.0;
deltaVel = 0.025;
VelVec = minVel:deltaVel:maxVel;

CreateParameterStudy('ParameterStudies/Gallop_SH_SL_BASE_004',datStruct,'p',4,VelVec);
[CostMatval,forces, states, time, hval] = ProcessParameterStudy_ANNOTATED('ParameterStudies/Gallop_SH_SL_BASE_004', PathTo2Dqrad, PathToMuscod);

load('ParameterStudies/Gallop_SH_SL_BASE_004');
save('Saved States/GallopFlexWithStates_Active_FreeParameters');
%% Plot Parameter Study Galloping

PlotParameterStudyLine('Gallop_SH_SL_BASE_00','-k',0.4);
%legend('Trotting (Passive)','Trotting (Active)','Galloping (Passive)','Galloping (Active)');

%% Plot Parameter Study Galloping Free Parameters

PlotParameterStudyScatter('Gallop_SH_SL_BASE_004','*',' Galloping');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Bounding
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

datStruct = CreateDatStruct('Bound','SEA','SEA');

datStruct.sd(9) = 0;
datStruct.sd_fix(9) = 1;
datStruct.sd_fix_init(9) = 1;

[filename, cmdout] = RunSingleBaseDataFile(datStruct,'Bound','SEA','SEA','00','folderlocations');
%% STEP 2
datStruct = UpdateDatStruct(datStruct,'BaseData/Bound_SH_SL_BASE_00',30,45,9);

datStruct.p(5) = 1; 

[filename, cmdout] = RunSingleBaseDataFile(datStruct,'Bound','SEA','SEA','01','folderlocations');
%% STEP 3
datStruct = UpdateDatStruct(datStruct,'BaseData/Bound_SH_SL_BASE_01',30,45,9);

datStruct.p(5) = 7;

[filename, cmdout] = RunSingleBaseDataFile(datStruct,'Bound','SEA','SEA','02','folderlocations');
%% STEP 4
datStruct = UpdateDatStruct(datStruct,'BaseData/Bound_SH_SL_BASE_02',30,45,9);

datStruct.p(4) = 1.4; %high speed to emphasize the effect of the spine
datStruct.sd_fix(9) = 1;

[filename, cmdout] = RunSingleBaseDataFile(datStruct,'Bound','SEA','SEA','03','folderlocations');
%% Plot
PlotMuscodResults('BaseData/Bound_SH_SL_BASE_03',[7,9]);
%% Play Muscod Results
PlayMuscodResults('BaseData/Bound_SH_SL_BASE_03');
%% Parameter Study for Bounding (Finished)
cd(PathToMatlab)
load('BaseData/Bound_SH_SL_BASE_02.mat','datStruct');

minVel = 0.0;
maxVel = 3.0;
deltaVel = 0.025;
VelVec = minVel:deltaVel:maxVel;

CreateParameterStudy('ParameterStudies/Bound_SH_SL_BASE_02',datStruct,'p',4,VelVec);
[CostMatval,forces, states, time, hval] = ProcessParameterStudy_ANNOTATED_Copy('ParameterStudies/Bound_SH_SL_BASE_02', PathTo2Dqrad, PathToMuscod);

load('ParameterStudies/Bound_SH_SL_BASE_02');
save('BoundFlexWithStates');
%% Plot Parameter Study Bounding

PlotParameterStudyLine('Bound_SH_SL_BASE_02',':r',0.4);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Gallop Bounding
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Step 1
datStruct = CreateDatStruct('GallopBound','SEA','SEA');

datStruct.sd(9) = 0;
datStruct.sd_fix(9) = 1;
datStruct.sd_fix_init(9) = 1;

datStruct.p(5) = 7;

[filename, cmdout] = RunSingleBaseDataFile(datStruct,'GallopBound','SEA','SEA','00','folderlocations');
%% STEP 2
datStruct = UpdateDatStruct(datStruct,'BaseData/GallopBound_SH_SL_BASE_00',30,45,9);

datStruct.p(4) = 2.0;

[filename, cmdout] = RunSingleBaseDataFile(datStruct,'GallopBound','SEA','SEA','01','folderlocations');

%% Plot
PlotMuscodResults('BaseData/GallopBound_SH_SL_BASE_01',[11,15,19,23]);
%% Play Muscod Results
PlayMuscodResults('BaseData/GallopBound_SH_SL_BASE_00');
%% Parameter Study for Bounding (Finished)
cd(PathToMatlab)
load('BaseData/GallopBound_SH_SL_BASE_00.mat','datStruct');

minVel = 0.0;
maxVel = 3.0;
deltaVel = 0.025;
VelVec = minVel:deltaVel:maxVel;

CreateParameterStudy('ParameterStudies/GallopBound_SH_SL_BASE_00',datStruct,'p',4,VelVec);
[CostMatval,forces, states, time, hval] = ProcessParameterStudy_ANNOTATED_Copy('ParameterStudies/GallopBound_SH_SL_BASE_00', PathTo2Dqrad, PathToMuscod);

load('ParameterStudies/GallopBound_SH_SL_BASE_00');
save('GallopBoundFlexWithStates');

%% Plot Parameter Study Bounding

PlotParameterStudyLine('GallopBound_SH_SL_BASE_00','-g',0.4);

%% Play Muscod Result at Parameter Study Point
Veloc = 0.5;
shouldplot = 1;
SavedStatesFileName = 'Saved States/WalkFlexWithStates_Active_FreeParameters.mat';
PlayMuscodResults_ParamStudyPoint(SavedStatesFileName, Veloc, shouldplot)

%% Play Muscod Result at Parameter Study Point
Veloc = 0.525;
shouldplot = 0;
SavedStatesFileName = 'Saved States/TrotFlexWithStates_Active_LowerHipTorqueLimit.mat';
PlayMuscodResults_ParamStudyPoint(SavedStatesFileName, Veloc, shouldplot)

%% Play Muscod Result at Parameter Study Point
Veloc = 2.4;
shouldplot = 0;
SavedStatesFileName = 'Saved States/GallopBoundFlexWithStates.mat';
PlayMuscodResults_ParamStudyPoint(SavedStatesFileName, Veloc, shouldplot)

%% Play Muscod Result at Parameter Study Point
Veloc = .7;
shouldplot = 1;
SavedStatesFileName = 'Saved States/TrotFlexWithStates_Active_LowerForceLimit.mat';
PlayMuscodResults_ParamStudyPoint(SavedStatesFileName, Veloc, shouldplot)

%% Plot Parameter Study
%load('ParameterStudies/Gallop_SH_SL_BASE_002_Velocity','configurations','types','indices','grids');
count = zeros(length(grids{1}),1);
%clf
figure(3)
hold on
box on
grid on
x = [];
y = [];
for i = 1:length(configurations)
    for j = 6:11
        if(configurations{i}.nFailed < 5)
            if(configurations{i}.finalDatStruct.p(4) > 0.2 && (configurations{i}.costValue < 0.4))
                count(configurations{i}.indexVector(1))=count(configurations{i}.indexVector(1))+1;
                x = [x,grids{1}(configurations{i}.indexVector(1))];
                y = [y,configurations{i}.costValue];
                %plot(grids{1}(configurations{i}.indexVector(1)),configurations{i}.finalDatStruct.p(j),'*','Color',[1-(j*0.05),j*0.05,(j-6)/5]);
            end
        end
    end
end
z = [x;y];
[tmp ind] = sort(z(1,:));
x = z(1,ind);
y = z(2,ind);
plot(x,y,'--','Color',[0.4,1,0.4]);