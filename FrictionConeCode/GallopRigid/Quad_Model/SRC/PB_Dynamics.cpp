/*
 * PB_Dynamics.cpp
 *
 * Common Dynamic code for a Prismatic Biped.
 *
 *  Created on: April 13, 2013
 *      Author: cdremy@umich.edu
 */

#include "PB_Dynamics.h"
#include <algorithm>

PB_Dynamics::PB_Dynamics() {}  // Constructor (Empty)
PB_Dynamics::~PB_Dynamics() {} // Destructor (Empty)

/* Hybrid Dynamic Equations */
/* The FlowMap computes the right hand side of the governing differential
 * equations.  Prior to it's call, the values for continuous states y,
 * discrete states z, excitation states u, and system parameters p must
 * be set to the newest values.  The function updates the dynamics (i.e.,
 * computes mass matrix, differentiable forces, etc. automatically.  It
 * returns an array (size NY) of the right hand side.
 */

VectorXd PB_Dynamics::ComputeContact(){
	updateDynamics();

	VectorXd lambda(nContact*2, 1);
	if (nContact>0)
	{  // check if some ground contact is present
		MatrixXd projM(nContact*2, nContact*2);
		VectorXd projF(nContact*2, 1);
		projM = J * Mdecomp.solve(J.transpose());
		projF = J * Mdecomp.solve(tau + h);
		lambda = projM.partialPivLu().solve(-projF - dJdtTimesdqdt);
	} else {
		// Do nothing, lambda is an empty vector anyway
	}

	return lambda;
}

void PB_Dynamics::FlowMap(double* dydtPtr){
	updateDynamics();

	VectorYd dydt;

	/**********************
	 * Actuator dynamics:
	 **********************/
	// Map velocities to position derivatives:
	dydt(ualphaLH)  = yVec(dualphaLH);
	dydt(ualphaLF)  = yVec(dualphaLF);
	dydt(ualphaRF)  = yVec(dualphaRF);
	dydt(ualphaRH)  = yVec(dualphaRH);
	dydt(ulLH)      = yVec(dulLH);
	dydt(ulLF)      = yVec(dulLF);
	dydt(ulRF)      = yVec(dulRF);
	dydt(ulRH)      = yVec(dulRH);
	if (pVec(hip_jnt_type) == PEA){ // No actuator dynamics in the parallel actuated joints
		dydt(dualphaLH) = 0;
		dydt(dualphaLF) = 0;
		dydt(dualphaRF) = 0;
		dydt(dualphaRH) = 0;
	} else { // Actuators are driven by the difference between motor and joint force
		dydt(dualphaLH) = (uVec(TalphaLH) - tau(qalphaLH))/j_mot_alpha;
		dydt(dualphaLF) = (uVec(TalphaLF) - tau(qalphaLF))/j_mot_alpha;
		dydt(dualphaRF) = (uVec(TalphaRF) - tau(qalphaRF))/j_mot_alpha;
		dydt(dualphaRH) = (uVec(TalphaRH) - tau(qalphaRH))/j_mot_alpha;
	}
	if (pVec(leg_jnt_type) == PEA){ // No actuator dynamics in the parallel actuated joints
		dydt(dulLH)     = 0;
		dydt(dulLF)     = 0;
		dydt(dulRF)     = 0;
		dydt(dulRH)     = 0;
	} else { // Actuators are driven by the difference between motor and joint force
		dydt(dulLH)     = (uVec(FlLH) - tau(qlLH))/j_mot_l;
		dydt(dulLF)     = (uVec(FlLF) - tau(qlLF))/j_mot_l;
		dydt(dulRF)     = (uVec(FlRF) - tau(qlRF))/j_mot_l;
		dydt(dulRH)     = (uVec(FlRH) - tau(qlRH))/j_mot_l;
	}

	/**********************
	 * Mechanical dynamics:
	 **********************/
	// Map velocities to position derivatives:
	dydt(x)      = yVec(dx);
	dydt(y)      = yVec(dy);
	dydt(phi)    = yVec(dphi);
	dydt(alphaLH) = yVec(dalphaLH);
	dydt(alphaLF) = yVec(dalphaLF);
	dydt(alphaRF) = yVec(dalphaRF);
	dydt(alphaRH) = yVec(dalphaRH);
	dydt(lLH)     = yVec(dlLH);
	dydt(lLF)     = yVec(dlLF);
	dydt(lRF)     = yVec(dlRF);
	dydt(lRH)     = yVec(dlRH);

	// Compute contact forces:
	VectorQd Jlambda;
	if (nContact>0) // check if some ground contact is present
	{
		MatrixXd projM(nContact*2, nContact*2);
		VectorXd projF(nContact*2, 1);
		VectorXd lambda(nContact*2, 1);
		projM = J * Mdecomp.solve(J.transpose());
		projF = J * Mdecomp.solve(tau + h);
		lambda = projM.partialPivLu().solve(-projF - dJdtTimesdqdt);
		// Project these forces back into the generalized coordinate space
		Jlambda = J.transpose()*lambda;
	} else {
		Jlambda = VectorQd::Zero();
	}

	// Evaluate the EQM:
	VectorQd dd_q;
	dd_q = Mdecomp.solve(tau + h + Jlambda);

	// Map the generalized accelerations back into continuous state derivatives:
	dydt(dx)      = dd_q(qx);
	dydt(dy)      = dd_q(qy);
	dydt(dphi)    = dd_q(qphi);
	dydt(dalphaLH) = dd_q(qalphaLH);
	dydt(dalphaLF) = dd_q(qalphaLF);
	dydt(dalphaRF) = dd_q(qalphaRF);
	dydt(dalphaRH) = dd_q(qalphaRH);
	dydt(dlLH)     = dd_q(qlLH);
	dydt(dlLF)     = dd_q(qlLF);
	dydt(dlRF)     = dd_q(qlRF);
	dydt(dlRH)     = dd_q(qlRH);

	/*****************
	 * Cost Functions:
	 *****************/
	// Compute the mechanical power performed by the actuator (that is actuator velocity times actuator torque/force):
	double P_act_alphaLH, P_act_alphaLF, P_act_alphaRF, P_act_alphaRH, P_act_lLH, P_act_lLF, P_act_lRF, P_act_lRH;
	if (pVec(hip_jnt_type) == PEA){ // Actuator velocity is joint velocity
		P_act_alphaLH = yVec(dalphaLH)*uVec(TalphaLH);
		P_act_alphaLF = yVec(dalphaLF)*uVec(TalphaLF);
		P_act_alphaRF = yVec(dalphaRF)*uVec(TalphaRF);
		P_act_alphaRH = yVec(dalphaRH)*uVec(TalphaRH);
	} else { // Actuator velocity is different from joint velocity
		P_act_alphaLH = yVec(dualphaLH)*uVec(TalphaLH);
		P_act_alphaLF = yVec(dualphaLF)*uVec(TalphaLF);
		P_act_alphaRF = yVec(dualphaRF)*uVec(TalphaRF);
		P_act_alphaRH = yVec(dualphaRH)*uVec(TalphaRH);
	}
	if (pVec(leg_jnt_type) == PEA){ // Actuator velocity is joint velocity
		P_act_lLH = yVec(dlLH)*uVec(FlLH);
		P_act_lLF = yVec(dlLF)*uVec(FlLF);
		P_act_lRF = yVec(dlRF)*uVec(FlRF);
		P_act_lRH = yVec(dlRH)*uVec(FlRH);
	} else { // Actuator velocity is different from joint velocity
		P_act_lLH = yVec(dulLH)*uVec(FlLH);
		P_act_lLF = yVec(dulLF)*uVec(FlLF);
		P_act_lRF = yVec(dulRF)*uVec(FlRF);
		P_act_lRH = yVec(dulRH)*uVec(FlRH);
	}
	// Compute the electrical losses:
	double P_loss_el_alphaLH = uVec(TalphaLH)*uVec(TalphaLH) * pVec(du_max_alpha)*pVec(du_max_alpha) / P_max_alpha;
	double P_loss_el_alphaLF = uVec(TalphaLF)*uVec(TalphaLF) * pVec(du_max_alpha)*pVec(du_max_alpha) / P_max_alpha;
	double P_loss_el_alphaRF = uVec(TalphaRF)*uVec(TalphaRF) * pVec(du_max_alpha)*pVec(du_max_alpha) / P_max_alpha;
	double P_loss_el_alphaRH = uVec(TalphaRH)*uVec(TalphaRH) * pVec(du_max_alpha)*pVec(du_max_alpha) / P_max_alpha;
	double P_loss_el_lLH = uVec(FlLH)*uVec(FlLH) * pVec(du_max_l)*pVec(du_max_l) / P_max_l;
	double P_loss_el_lLF = uVec(FlLF)*uVec(FlLF) * pVec(du_max_l)*pVec(du_max_l) / P_max_l;
	double P_loss_el_lRF = uVec(FlRF)*uVec(FlRF) * pVec(du_max_l)*pVec(du_max_l) / P_max_l;
	double P_loss_el_lRH = uVec(FlRH)*uVec(FlRH) * pVec(du_max_l)*pVec(du_max_l) / P_max_l;

	// Compute the accumulated positive actuator work (assuming that negative work at EACH actuator can not be recovered):
	dydt(posActWork)  = Logistic(P_act_alphaLH, pVec(sigma)) +
			Logistic(P_act_alphaLF, pVec(sigma)) +
                        Logistic(P_act_alphaRF, pVec(sigma)) +
                        Logistic(P_act_alphaRH, pVec(sigma)) +
                        Logistic(P_act_lLH, pVec(sigma)) +
                        Logistic(P_act_lLF, pVec(sigma)) +
                        Logistic(P_act_lRF, pVec(sigma)) +
                        Logistic(P_act_lRH, pVec(sigma));
    // Compute the total electrical Losses
	dydt(totElLoss)  = P_loss_el_alphaLH + P_loss_el_alphaLF + P_loss_el_alphaRF + P_loss_el_alphaRH + P_loss_el_lLH + P_loss_el_lLF + P_loss_el_lRF + P_loss_el_lRH;
	// Compute the positive electrical work (which allows using negative work in one actuator to perform positive work in another):
    dydt(posElWork)  = max(0.0, P_act_alphaLH + P_act_alphaLF + P_act_alphaRF + P_act_alphaRH + P_act_lLH + P_act_lLF + P_act_lRF + P_act_lRH + double(dydt(totElLoss)));


	/*******************
	 * Write to pointer:
	 *******************/
    for (int i = 0; i<NY; i++)
    	dydtPtr[i] = dydt(i);
}
/* The JumpMap computes the changes in velocities, that are necessary to
 * bring the contact points to a velocity of zero.  Prior to it's call,
 * the values for continuous states y, discrete states z, excitation
 * states u, and system parameters p must be set to the newest values.
 * **NOTE** the values of z need to represent the phases AFTER the
 * collision.  The function updates the dynamics (i.e., computes mass
 * matrix, differentiable forces, etc. automatically.  It returns an
 * array (size NY) of the state after the collision
 */
void PB_Dynamics::JumpMap(double* yPlusPtr){
	updateDynamics();

	// As most things remain unchanged we copy the current states
	for (int i = 0; i<NY; i++)
		yPlusPtr[i] = yVec(i);

	MatrixXd projM(nContact*2, nContact*2);
	MatrixQd projMinv;
	VectorQd dqMINUS;
	VectorQd dqPLUS;
	// At touchdown, the contact point comes to a complete rest, a fully plastic collision is computed:
	// Velocities before collision:
	dqMINUS(qx)      = yVec(dx);
	dqMINUS(qy)      = yVec(dy);
	dqMINUS(qphi)    = yVec(dphi);
	dqMINUS(qalphaLH) = yVec(dalphaLH);
	dqMINUS(qalphaLF) = yVec(dalphaLF);
	dqMINUS(qalphaRF) = yVec(dalphaRF);
	dqMINUS(qalphaRH) = yVec(dalphaRH);
	dqMINUS(qlLH)     = yVec(dlLH);
	dqMINUS(qlLF)     = yVec(dlLF);
	dqMINUS(qlRF)     = yVec(dlRF);
	dqMINUS(qlRH)     = yVec(dlRH);
	// Project EoM into the contact space:
	// Rest after contact: J*qPlus = 0
	// with: M*(qPlus-qMinus) = I_cont_q % with the contact impulse I_cont
	// qPlus = inv(M)*I_cont_q + qMinus
	// -> J*inv(M)*I_cont_q + qMinus = 0
	// -> J*inv(M)*J'*I_cont_x + qMinus = 0
	// -> I_cont_x = -inv(J*inv(M)*J')*qMinus
	// -> I_cont_q = -J'*inv(J*inv(M)*J')*J*qMinus
	// qPlus = -inv(M)*J'*inv(J*inv(M)*J')*J*qMinus + qMinus
	projM = J * Mdecomp.solve(J.transpose());
	projMinv = J.transpose() * projM.partialPivLu().solve(J);
	// Velocities after
	dqPLUS = Mdecomp.solve(M*dqMINUS - projMinv*dqMINUS);
	yPlusPtr[dx]       = dqPLUS(qx);
	yPlusPtr[dy]       = dqPLUS(qy);
	yPlusPtr[dphi]     = dqPLUS(qphi);
	yPlusPtr[dalphaLH]  = dqPLUS(qalphaLH);
	yPlusPtr[dalphaLF]  = dqPLUS(qalphaLF);
	yPlusPtr[dalphaRF]  = dqPLUS(qalphaRF);
	yPlusPtr[dalphaRH]  = dqPLUS(qalphaRH);
	yPlusPtr[dlLH]      = dqPLUS(qlLH);
	yPlusPtr[dlLF]      = dqPLUS(qlLF);
	yPlusPtr[dlRF]      = dqPLUS(qlRF);
	yPlusPtr[dlRH]      = dqPLUS(qlRH);
}
/* The JumpSet tracks the various events that can happen throughout a
 * stride. Each event value will have a zero crossing in positive
 * direction, when the corresponding event happens  Prior to it's call,
 * the values for continuous states y, discrete states z, excitation
 * states u, and system parameters p must be set to the newest values.
 * **NOTE** the values of z need to represent the phases BEFORE the
 * event.  If one seeks to identify multiple events simultaneously,
 * all involved event values must be zero.  They are returned in an
 * array (size NEV).
 */
void PB_Dynamics::JumpSet(double* eventValPtr){
	updateDynamics();

	// Compute the contact forces:
	VectorXd lambda(nContact*2, 1);
	if (nContact>0)
	{  // check if some ground contact is present
		MatrixXd projM(nContact*2, nContact*2);
		VectorXd projF(nContact*2, 1);
		projM = J * Mdecomp.solve(J.transpose());
		projF = J * Mdecomp.solve(tau + h);
		lambda = projM.partialPivLu().solve(-projF - dJdtTimesdqdt);
	} else {
		// Do nothing, lambda is an empty vector anyway
	}
	int counter = 0;
	if (zVec(phaseLH) == stance) {	//  Event is detected if the vertical contact force of the left leg becomes negative.
		eventValPtr[liftoffLH] = -lambda (counter+1);
		counter = counter + 2;
	} else { // But only in flight
		eventValPtr[liftoffLH] = -1;
	}
	if (zVec(phaseLF) == stance) {	//  Event is detected if the vertical contact force of the left leg becomes negative.
		eventValPtr[liftoffLF] = -lambda (counter+1);
		counter = counter + 2;
	} else { // But only in flight
		eventValPtr[liftoffLF] = -1;
	}
	if (zVec(phaseRF) == stance) {	//  Event is detected if the vertical contact force of the right leg becomes negative.
		eventValPtr[liftoffRF] = -lambda (counter+1);
		counter = counter + 2;
	} else { // But only in flight
		eventValPtr[liftoffRF] = -1;
	}
	if (zVec(phaseRH) == stance) {	//  Event is detected if the vertical contact force of the right leg becomes negative.
		eventValPtr[liftoffRH] = -lambda (counter+1);
		counter = counter + 2;
	} else { // But only in flight
		eventValPtr[liftoffRH] = -1;
	}
	if (zVec(phaseLH) == flight) {	// Event is detected if the left foot goes below the ground during flight
		eventValPtr[touchdownLH] = -posLH(1);
	} else { // But only in flight
		eventValPtr[touchdownLH] = -1;
	}
	if (zVec(phaseLF) == flight) {	// Event is detected if the left foot goes below the ground during flight
		eventValPtr[touchdownLF] = -posLF(1);
	} else { // But only in flight
		eventValPtr[touchdownLF] = -1;
	}
	if (zVec(phaseRF) == flight) {	// Event is detected if the right foot goes below the ground during flight
		eventValPtr[touchdownRF] = -posRF(1);
	} else { // But only in flight
		eventValPtr[touchdownRF] = -1;
	}
	if (zVec(phaseRH) == flight) {	// Event is detected if the right foot goes below the ground during flight
		eventValPtr[touchdownRH] = -posRH(1);
	} else { // But only in flight
		eventValPtr[touchdownRH] = -1;
	}
	// Detect apex transit
	eventValPtr[apexTransit] = yVec(dy);
}

/* Compute a number of constraint values for
 * a) The position of the left and right foot above the ground
 * b) The velocity du of each actuator below du_max*(1-c_lim)
 * c) The force Fu of each actuator below F_max = c_lim*P_max/du_max
 * I.e., each constraint is met as soon it is larger than 0.
 * the vectors for the actuator speeds and velocities are given in the order:
 * [alphaL, alphaR, lL, lR]
 */
void PB_Dynamics::Constraints(double* gcLH, double* gcLF, double* gcRF, double* gcRH, double* du_max, double* F_max, double* Fric_max){
	updateDynamics();
	VectorXd lambdaTemp(nContact*2, 1);
	lambdaTemp = ComputeContact();

	// Ground clearance:
	if (zVec(phaseLH) == flight) {
		*gcLH = posLH(1);
	} else {
		*gcLH = 1; // if the foot is on the ground, the constraint is always met
	}

	if (zVec(phaseLF) == flight) {
		*gcLF = posLF(1);
	} else {
		*gcLF = 1; // if the foot is on the ground, the constraint is always met
	}

	if (zVec(phaseRF) == flight) {
		*gcRF = posRF(1);
	} else {
		*gcRF = 1; // If the foot is on the ground, the constraint is always met
	}

	if (zVec(phaseRH) == flight) {
		*gcRH = posRH(1);
	} else {
		*gcRH = 1; // If the foot is on the ground, the constraint is always met
	}

	if (pVec(hip_jnt_type) == PEA){ // Actuator velocity is joint velocity
		// Compute how much the actual joint velocity is below its upper limit:
		du_max[0] = pVec(du_max_alpha)*(1-c_lim_alpha)-yVec(dalphaLH);
		du_max[1] = pVec(du_max_alpha)*(1-c_lim_alpha)-yVec(dalphaLF);
		du_max[2] = pVec(du_max_alpha)*(1-c_lim_alpha)-yVec(dalphaRF);
		du_max[3] = pVec(du_max_alpha)*(1-c_lim_alpha)-yVec(dalphaRH);
		// Compute how much the actual joint velocity is above its lower limit:
		du_max[8] = pVec(du_max_alpha)*(1-c_lim_alpha)+yVec(dalphaLH);
		du_max[9] = pVec(du_max_alpha)*(1-c_lim_alpha)+yVec(dalphaLF);
		du_max[10] = pVec(du_max_alpha)*(1-c_lim_alpha)+yVec(dalphaRF);
		du_max[11] = pVec(du_max_alpha)*(1-c_lim_alpha)+yVec(dalphaRH);
	} else { // Actuator velocity is different from joint velocity
		// Compute how much the actual actuator velocity is below its upper limit:
		du_max[0] = pVec(du_max_alpha)*(1-c_lim_alpha)-yVec(dualphaLH);
		du_max[1] = pVec(du_max_alpha)*(1-c_lim_alpha)-yVec(dualphaLF);
		du_max[2] = pVec(du_max_alpha)*(1-c_lim_alpha)-yVec(dualphaRF);
		du_max[3] = pVec(du_max_alpha)*(1-c_lim_alpha)-yVec(dualphaRH);
		// Compute how much the actual actuator velocity is above its lower limit:
		du_max[8] = pVec(du_max_alpha)*(1-c_lim_alpha)+yVec(dualphaLH);
		du_max[9] = pVec(du_max_alpha)*(1-c_lim_alpha)+yVec(dualphaLF);
		du_max[10] = pVec(du_max_alpha)*(1-c_lim_alpha)+yVec(dualphaRF);
		du_max[11] = pVec(du_max_alpha)*(1-c_lim_alpha)+yVec(dualphaRF);
	}
	if (pVec(leg_jnt_type) == PEA){ // Actuator velocity is joint velocity
		// Compute how much the actual joint velocity is below its upper limit:
		du_max[4] = pVec(du_max_l)*(1-c_lim_l)-yVec(dlLH);
		du_max[5] = pVec(du_max_l)*(1-c_lim_l)-yVec(dlLF);
		du_max[6] = pVec(du_max_l)*(1-c_lim_l)-yVec(dlRF);
		du_max[7] = pVec(du_max_l)*(1-c_lim_l)-yVec(dlRH);
		// Compute how much the actual joint velocity is above its lower limit:
		du_max[12] = pVec(du_max_l)*(1-c_lim_l)+yVec(dlLH);
		du_max[13] = pVec(du_max_l)*(1-c_lim_l)+yVec(dlLF);
		du_max[14] = pVec(du_max_l)*(1-c_lim_l)+yVec(dlRF);
		du_max[15] = pVec(du_max_l)*(1-c_lim_l)+yVec(dlRH);
	} else { // Actuator velocity is different from joint velocity
		// Compute how much the actual actuator velocity is below its upper limit:
		du_max[4] = pVec(du_max_l)*(1-c_lim_l)-yVec(dulLH);
		du_max[5] = pVec(du_max_l)*(1-c_lim_l)-yVec(dulLF);
		du_max[6] = pVec(du_max_l)*(1-c_lim_l)-yVec(dulRF);
		du_max[7] = pVec(du_max_l)*(1-c_lim_l)-yVec(dulRH);
		// Compute how much the actual actuator velocity is above its lower limit:
		du_max[12] = pVec(du_max_l)*(1-c_lim_l)+yVec(dulLH);
		du_max[13] = pVec(du_max_l)*(1-c_lim_l)+yVec(dulLF);
		du_max[14] = pVec(du_max_l)*(1-c_lim_l)+yVec(dulRF);
		du_max[15] = pVec(du_max_l)*(1-c_lim_l)+yVec(dulRH);
	}

	// Compute how much the actual actuator torque is below its upper limit:
	F_max[0] = c_lim_alpha*P_max_alpha/pVec(du_max_alpha) - uVec(TalphaLH);
	F_max[1] = c_lim_alpha*P_max_alpha/pVec(du_max_alpha) - uVec(TalphaLF);
	F_max[2] = c_lim_alpha*P_max_alpha/pVec(du_max_alpha) - uVec(TalphaRF);
	F_max[3] = c_lim_alpha*P_max_alpha/pVec(du_max_alpha) - uVec(TalphaRH);
	F_max[4] = c_lim_l*P_max_l/pVec(du_max_l) - uVec(FlLH);
	F_max[5] = c_lim_l*P_max_l/pVec(du_max_l) - uVec(FlLF);
	F_max[6] = c_lim_l*P_max_l/pVec(du_max_l) - uVec(FlRF);
	F_max[7] = c_lim_l*P_max_l/pVec(du_max_l) - uVec(FlRH);
	// Compute how much the actual actuator torque is above its lower limit:
	F_max[8] = c_lim_alpha*P_max_alpha/pVec(du_max_alpha) + uVec(TalphaLH);
	F_max[9] = c_lim_alpha*P_max_alpha/pVec(du_max_alpha) + uVec(TalphaLF);
	F_max[10] = c_lim_alpha*P_max_alpha/pVec(du_max_alpha) + uVec(TalphaRF);
	F_max[11] = c_lim_alpha*P_max_alpha/pVec(du_max_alpha) + uVec(TalphaRH);
	F_max[12] = c_lim_l*P_max_l/pVec(du_max_l) + uVec(FlLH);
	F_max[13] = c_lim_l*P_max_l/pVec(du_max_l) + uVec(FlLF);
	F_max[14] = c_lim_l*P_max_l/pVec(du_max_l) + uVec(FlRF);
	F_max[15] = c_lim_l*P_max_l/pVec(du_max_l) + uVec(FlRH);

	int j = -2;   // counting variable
	
	for (int i = 0; i<4; i++){
		//cout << "i " << i << endl;
		if (i<nContact){
			j = j+2;
			Fric_max[i] = pVec(mu)*abs(lambdaTemp[j+1])-abs(lambdaTemp[j]);
			//Fric_max[i] = 1 -abs(lambdaTemp[j]);		
			//cout << "Fric_max " << (pVec(mu)*abs(lambdaTemp[j+1])-abs(lambdaTemp[j])) << endl;
		}
		else {
			Fric_max[i] = 1;
			//cout << "Fric_max " << 1 << endl;
		}

	}

}

// Update current states
void PB_Dynamics::setContState(double* yPtr){yVec = Map<VectorYd>(yPtr);}
void PB_Dynamics::setDiscState(double* zPtr){zVec = Map<VectorZd>(zPtr);}
void PB_Dynamics::setSystParam(double* pPtr){pVec = Map<VectorPd>(pPtr);}
void PB_Dynamics::setExctState(double* uPtr){uVec = Map<VectorUd>(uPtr);}
// Retrieve the current states
void PB_Dynamics::getContState(double* yPtr){
	for (int i = 0; i<NY; i++)
		yPtr[i] = yVec(i);
}
void PB_Dynamics::getDiscState(double* zPtr){
	for (int i = 0; i<NZ; i++)
		zPtr[i] = zVec(i);
}
void PB_Dynamics::getSystParam(double* pPtr){
	for (int i = 0; i<NP; i++)
		pPtr[i] = pVec(i);
}
void PB_Dynamics::getExctState(double* uPtr){
	for (int i = 0; i<NU; i++)
		uPtr[i] = uVec(i);
}

// Prepare all the components for the equations of motion:
void PB_Dynamics::updateDynamics(){
	// Before this function is called, we assume that all states and parameters have been updated.
	// Update all parameters (inertia values, damping coefficients, ...)
	ComputeDependentParameters();
	// We compute new values for all the 'dynamics'-components:
	ComputeMassMatrix();  // M & Mdecomp
	ComputeDiffForces();  // h
	ComputeJointForces(); // f (this also computes T_spring_alpha and F_spring_l)
	nContact = 0;
	// And for the contact processing:
	if (zVec(phaseLH) == flight) {
		ComputeContactPointLH();     // posL
	} else {
		nContact++;
		ComputeContactJacobianLH();  //JL
		ComputeContactJacobianDtTIMESdqdtLH();  // dJLdtTimesdqdt
	}
	if (zVec(phaseLF) == flight) {
		ComputeContactPointLF();     // posL
	} else {
		nContact++;
		ComputeContactJacobianLF();  //JL
		ComputeContactJacobianDtTIMESdqdtLF();  // dJLdtTimesdqdt
	}
	if (zVec(phaseRF) == flight) {
		ComputeContactPointRF();     // posL
	} else {
		nContact++;
		ComputeContactJacobianRF();  //JR
		ComputeContactJacobianDtTIMESdqdtRF();  // dJRdtTimesdqdt
	}
	if (zVec(phaseRH) == flight) {
		ComputeContactPointRH();     // posL
	} else {
		nContact++;
		ComputeContactJacobianRH();  //JR
		ComputeContactJacobianDtTIMESdqdtRH();  // dJRdtTimesdqdt
	}
	//cout << "updateDynamics"  << nContact << endl;
	// And build the compound Jacobian (which starts with L, then R)
	J.resize(nContact*2, NQ);
	dJdtTimesdqdt.resize(nContact*2, 1);
	int counter = 0;
	if (zVec(phaseLH) == stance) {
		J.row(counter)   = JLH.row(0);
		J.row(counter+1) = JLH.row(1);
		dJdtTimesdqdt(counter)   = dJLHdtdqdt(0);
		dJdtTimesdqdt(counter+1) = dJLHdtdqdt(1);
		counter = counter + 2;
	}
	if (zVec(phaseLF) == stance) {
		J.row(counter)   = JLF.row(0);
		J.row(counter+1) = JLF.row(1);
		dJdtTimesdqdt(counter)   = dJLFdtdqdt(0);
		dJdtTimesdqdt(counter+1) = dJLFdtdqdt(1);
		counter = counter + 2;
	}
	if (zVec(phaseRF) == stance) {
		J.row(counter)   = JRF.row(0);
		J.row(counter+1) = JRF.row(1);
		dJdtTimesdqdt(counter)   = dJRFdtdqdt(0);
		dJdtTimesdqdt(counter+1) = dJRFdtdqdt(1);
		counter = counter + 2;
	}
	if (zVec(phaseRH) == stance) {
		J.row(counter)   = JRH.row(0);
		J.row(counter+1) = JRH.row(1);
		dJdtTimesdqdt(counter)   = dJRHdtdqdt(0);
		dJdtTimesdqdt(counter+1) = dJRHdtdqdt(1);
		counter = counter + 2;
	}
}

// Update dependent parameters
void PB_Dynamics::ComputeDependentParameters(){
	// Compute maximal power and the unscaled actuator inertia according to the scaling laws and
	// the values provided for rho_alpha and rho_l:
	P_max_alpha  = pVec(P_max)  * pow(double(pVec(rho_alpha)), +1.35);
	P_max_l      = pVec(P_max)  * pow(double(pVec(rho_l)),     +1.35);
	// Percentage of the maximal power that can be used
	c_lim_alpha  = pVec(c_lim)  * pow(double(pVec(rho_alpha)), -0.37);
	c_lim_l      = pVec(c_lim)  * pow(double(pVec(rho_l)),     -0.37);
	// Compute the actual actuator inertias as a function of the gear ratios (maximal speeds)
	double j_unsc_alpha = pVec(j_unsc) * pow(double(pVec(rho_alpha)), +1.26);
	double j_unsc_l     = pVec(j_unsc) * pow(double(pVec(rho_l)),     +1.26);
	j_mot_alpha = j_unsc_alpha/(pVec(du_max_alpha)*pVec(du_max_alpha));
	j_mot_l     = j_unsc_l/(pVec(du_max_l)*pVec(du_max_l));
	// Compute the viscous damping coefficient of the springs, according to the desired damping ratio:
	double j_leg   = pVec(j3) + pow(double(pVec(l_0) - pVec(l3)),2)*pVec(m3) + pVec(j2) + pow(double(pVec(l2)),2)*pVec(m2); // total leg inertia wrt the hip
	b_alpha  = pVec(balphaRat)*2*sqrt(double(pVec(kalpha)*j_leg));
	b_l      = pVec(blRat)*2*sqrt(double(pVec(kl)*pVec(m3)));
	// To prevent instability damping values are forced to be non-negative:
	b_alpha = max(0.0, b_alpha);
	b_l     = max(0.0, b_l);
}

// Components for the Equations of Motion
void PB_Dynamics::ComputeMassMatrix(){
	M = MatrixQd::Zero();
	double t2 = yVec(alphaLF)+yVec(phi);
	double t3 = cos(t2);
	double t4 = yVec(alphaLH)+yVec(phi);
	double t5 = cos(t4);
	double t6 = yVec(alphaRF)+yVec(phi);
	double t7 = cos(t6);
	double t8 = yVec(alphaRH)+yVec(phi);
	double t9 = cos(t8);
	double t10 = pVec(l2)*pVec(m2)*t5;
	double t11 = yVec(lLH)*pVec(m3)*t5;
	double t12 = pVec(l2)*pVec(m2)*t3;
	double t13 = yVec(lLF)*pVec(m3)*t3;
	double t14 = pVec(l2)*pVec(m2)*t7;
	double t15 = yVec(lRF)*pVec(m3)*t7;
	double t16 = pVec(l2)*pVec(m2)*t9;
	double t17 = yVec(lRH)*pVec(m3)*t9;
	double t18 = pVec(m2)*4.0;
	double t19 = pVec(m3)*4.0;
	double t20 = pVec(m1)+t18+t19;
	double t21 = sin(t2);
	double t22 = sin(t4);
	double t23 = sin(t6);
	double t24 = sin(t8);
	double t25 = pVec(l2)*pVec(m2)*t22;
	double t26 = yVec(lLH)*pVec(m3)*t22;
	double t27 = pVec(l2)*pVec(m2)*t21;
	double t28 = yVec(lLF)*pVec(m3)*t21;
	double t29 = pVec(l2)*pVec(m2)*t23;
	double t30 = yVec(lRF)*pVec(m3)*t23;
	double t31 = pVec(l2)*pVec(m2)*t24;
	double t32 = yVec(lRH)*pVec(m3)*t24;
	double t52 = pVec(l3)*pVec(m3)*t5;
	double t60 = pVec(l3)*pVec(m3)*t3;
	double t68 = pVec(l3)*pVec(m3)*t7;
	double t76 = pVec(l3)*pVec(m3)*t9;
	double t33 = t10+t11+t12+t13+t14+t15+t16+t17-t52-t60-t68-t76;
	double t53 = pVec(l3)*pVec(m3)*t22;
	double t61 = pVec(l3)*pVec(m3)*t21;
	double t69 = pVec(l3)*pVec(m3)*t23;
	double t77 = pVec(l3)*pVec(m3)*t24;
	double t34 = t25+t26+t27+t28+t29+t30+t31+t32-t53-t61-t69-t77;
	double t35 = pVec(l1)*pVec(l1);
	double t36 = sin(yVec(alphaLF));
	double t37 = sin(yVec(alphaLH));
	double t38 = sin(yVec(alphaRF));
	double t39 = sin(yVec(alphaRH));
	double t40 = pVec(l2)*pVec(l2);
	double t41 = pVec(l3)*pVec(l3);
	double t42 = yVec(lLH)*yVec(lLH);
	double t43 = pVec(m3)*t42;
	double t44 = pVec(m2)*t40;
	double t45 = pVec(m3)*t41;
	double t46 = yVec(lLF)*yVec(lLF);
	double t47 = pVec(m3)*t46;
	double t48 = yVec(lRF)*yVec(lRF);
	double t49 = pVec(m3)*t48;
	double t50 = yVec(lRH)*yVec(lRH);
	double t51 = pVec(m3)*t50;
	double t54 = pVec(l1)*pVec(l3)*pVec(m3)*t37;
	double t56 = pVec(l3)*yVec(lLH)*pVec(m3)*2.0;
	double t55 = pVec(j2)+pVec(j3)+t43+t44+t45+t54-t56-pVec(l1)*pVec(l2)*pVec(m2)*t37-pVec(l1)*yVec(lLH)*pVec(m3)*t37;
	double t57 = pVec(m3)*t22;
	double t58 = cos(yVec(alphaLH));
	double t59 = pVec(l1)*pVec(m3)*t58;
	double t62 = pVec(l1)*pVec(l2)*pVec(m2)*t36;
	double t63 = pVec(l1)*yVec(lLF)*pVec(m3)*t36;
	double t65 = pVec(l3)*yVec(lLF)*pVec(m3)*2.0;
	double t64 = pVec(j2)+pVec(j3)+t44+t45+t47+t62+t63-t65-pVec(l1)*pVec(l3)*pVec(m3)*t36;
	double t66 = pVec(m3)*t21;
	double t67 = cos(yVec(alphaLF));
	double t70 = pVec(l1)*pVec(l2)*pVec(m2)*t38;
	double t71 = pVec(l1)*yVec(lRF)*pVec(m3)*t38;
	double t73 = pVec(l3)*yVec(lRF)*pVec(m3)*2.0;
	double t72 = pVec(j2)+pVec(j3)+t44+t45+t49+t70+t71-t73-pVec(l1)*pVec(l3)*pVec(m3)*t38;
	double t74 = pVec(m3)*t23;
	double t75 = cos(yVec(alphaRF));
	double t78 = pVec(l1)*pVec(l3)*pVec(m3)*t39;
	double t80 = pVec(l3)*yVec(lRH)*pVec(m3)*2.0;
	double t79 = pVec(j2)+pVec(j3)+t44+t45+t51+t78-t80-pVec(l1)*pVec(l2)*pVec(m2)*t39-pVec(l1)*yVec(lRH)*pVec(m3)*t39;
	double t81 = pVec(m3)*t24;
	double t82 = cos(yVec(alphaRH));
	double t83 = pVec(l1)*pVec(m3)*t82;
	M(0,0) = t20;
	M(0,2) = t33;
	M(0,3) = t10+t11-pVec(l3)*pVec(m3)*t5;
	M(0,4) = t57;
	M(0,5) = t12+t13-pVec(l3)*pVec(m3)*t3;
	M(0,6) = t66;
	M(0,7) = t14+t15-pVec(l3)*pVec(m3)*t7;
	M(0,8) = t74;
	M(0,9) = t16+t17-pVec(l3)*pVec(m3)*t9;
	M(0,10) = t81;
	M(1,1) = t20;
	M(1,2) = t34;
	M(1,3) = t25+t26-pVec(l3)*pVec(m3)*t22;
	M(1,4) = -pVec(m3)*t5;
	M(1,5) = t27+t28-pVec(l3)*pVec(m3)*t21;
	M(1,6) = -pVec(m3)*t3;
	M(1,7) = t29+t30-pVec(l3)*pVec(m3)*t23;
	M(1,8) = -pVec(m3)*t7;
	M(1,9) = t31+t32-pVec(l3)*pVec(m3)*t24;
	M(1,10) = -pVec(m3)*t9;
	M(2,0) = t33;
	M(2,1) = t34;
	M(2,2) = pVec(j1_)+pVec(j2)*4.0+pVec(j3)*4.0+t43+t47+t49+t51+pVec(m2)*t35*4.0+pVec(m3)*t35*4.0+pVec(m2)*t40*4.0+pVec(m3)*t41*4.0-pVec(l3)*yVec(lLF)*pVec(m3)*2.0-pVec(l3)*yVec(lLH)*pVec(m3)*2.0-pVec(l3)*yVec(lRF)*pVec(m3)*2.0-pVec(l3)*yVec(lRH)*pVec(m3)*2.0+pVec(l1)*pVec(l2)*pVec(m2)*t36*2.0-pVec(l1)*pVec(l2)*pVec(m2)*t37*2.0+pVec(l1)*pVec(l2)*pVec(m2)*t38*2.0-pVec(l1)*pVec(l3)*pVec(m3)*t36*2.0-pVec(l1)*pVec(l2)*pVec(m2)*t39*2.0+pVec(l1)*pVec(l3)*pVec(m3)*t37*2.0-pVec(l1)*pVec(l3)*pVec(m3)*t38*2.0+pVec(l1)*pVec(l3)*pVec(m3)*t39*2.0+pVec(l1)*yVec(lLF)*pVec(m3)*t36*2.0-pVec(l1)*yVec(lLH)*pVec(m3)*t37*2.0+pVec(l1)*yVec(lRF)*pVec(m3)*t38*2.0-pVec(l1)*yVec(lRH)*pVec(m3)*t39*2.0;
	M(2,3) = t55;
	M(2,4) = t59;
	M(2,5) = t64;
	M(2,6) = -pVec(l1)*pVec(m3)*t67;
	M(2,7) = t72;
	M(2,8) = -pVec(l1)*pVec(m3)*t75;
	M(2,9) = t79;
	M(2,10) = t83;
	M(3,0) = t10+t11-t52;
	M(3,1) = t25+t26-t53;
	M(3,2) = t55;
	M(3,3) = pVec(j2)+pVec(j3)+t43+t44+t45-t56;
	M(4,0) = t57;
	M(4,1) = -pVec(m3)*t5;
	M(4,2) = t59;
	M(4,4) = pVec(m3);
	M(5,0) = t12+t13-t60;
	M(5,1) = t27+t28-t61;
	M(5,2) = t64;
	M(5,5) = pVec(j2)+pVec(j3)+t44+t45+t47-t65;
	M(6,0) = t66;
	M(6,1) = -pVec(m3)*t3;
	M(6,2) = -pVec(l1)*pVec(m3)*t67;
	M(6,6) = pVec(m3);
	M(7,0) = t14+t15-t68;
	M(7,1) = t29+t30-t69;
	M(7,2) = t72;
	M(7,7) = pVec(j2)+pVec(j3)+t44+t45+t49-t73;
	M(8,0) = t74;
	M(8,1) = -pVec(m3)*t7;
	M(8,2) = -pVec(l1)*pVec(m3)*t75;
	M(8,8) = pVec(m3);
	M(9,0) = t16+t17-t76;
	M(9,1) = t31+t32-t77;
	M(9,2) = t79;
	M(9,9) = pVec(j2)+pVec(j3)+t44+t45+t51-t80;
	M(10,0) = t81;
	M(10,1) = -pVec(m3)*t9;
	M(10,2) = t83;
	M(10,10) = pVec(m3);
	
	// Increase inertia matrix values for DOFs with parallel elastic actuation:
	// HIP
	if (pVec(hip_jnt_type) == PEA){
		M(qalphaLH,qalphaLH) = M(qalphaLH,qalphaLH) + j_mot_alpha;
		M(qalphaLF,qalphaLF) = M(qalphaLF,qalphaLF) + j_mot_alpha;
		M(qalphaRF,qalphaRF) = M(qalphaRF,qalphaRF) + j_mot_alpha;
		M(qalphaRH,qalphaRH) = M(qalphaRF,qalphaRH) + j_mot_alpha;
	}
	// LEG
	if (pVec(leg_jnt_type) == PEA){
		M(qlLH,qlLH) = M(qlLH,qlLH) + j_mot_l;
		M(qlLF,qlLF) = M(qlLF,qlLF) + j_mot_l;
		M(qlRF,qlRF) = M(qlRF,qlRF) + j_mot_l;
		M(qlRF,qlRH) = M(qlRH,qlRH) + j_mot_l;
	}
	// directly decompose the Mass-matrix:
	Mdecomp = M.ldlt();
}
void PB_Dynamics::ComputeDiffForces(){
	h = VectorQd::Zero();
	double t85 = yVec(alphaLF)+yVec(phi);
	double t86 = sin(t85);
	double t87 = yVec(alphaLH)+yVec(phi);
	double t88 = sin(t87);
	double t89 = yVec(alphaRF)+yVec(phi);
	double t90 = sin(t89);
	double t91 = yVec(alphaRH)+yVec(phi);
	double t92 = sin(t91);
	double t93 = cos(t85);
	double t94 = yVec(dalphaLF)*pVec(l2)*pVec(m2)*t86;
	double t95 = yVec(dphi)*pVec(l2)*pVec(m2)*t86;
	double t96 = yVec(dalphaLF)*yVec(lLF)*pVec(m3)*t86;
	double t97 = yVec(dphi)*yVec(lLF)*pVec(m3)*t86;
	double t98 = cos(t87);
	double t99 = yVec(dalphaLH)*pVec(l2)*pVec(m2)*t88;
	double t100 = yVec(dphi)*pVec(l2)*pVec(m2)*t88;
	double t101 = yVec(dalphaLH)*yVec(lLH)*pVec(m3)*t88;
	double t102 = yVec(dphi)*yVec(lLH)*pVec(m3)*t88;
	double t103 = cos(t89);
	double t104 = yVec(dalphaRF)*pVec(l2)*pVec(m2)*t90;
	double t105 = yVec(dphi)*pVec(l2)*pVec(m2)*t90;
	double t106 = yVec(dalphaRF)*yVec(lRF)*pVec(m3)*t90;
	double t107 = yVec(dphi)*yVec(lRF)*pVec(m3)*t90;
	double t108 = cos(t91);
	double t109 = yVec(dalphaRH)*pVec(l2)*pVec(m2)*t92;
	double t110 = yVec(dphi)*pVec(l2)*pVec(m2)*t92;
	double t111 = yVec(dalphaRH)*yVec(lRH)*pVec(m3)*t92;
	double t112 = yVec(dphi)*yVec(lRH)*pVec(m3)*t92;
	double t113 = yVec(dlLF)*pVec(m3)*t86;
	double t114 = yVec(dalphaLF)*pVec(l2)*pVec(m2)*t93;
	double t115 = yVec(dphi)*pVec(l2)*pVec(m2)*t93;
	double t116 = yVec(dalphaLF)*yVec(lLF)*pVec(m3)*t93;
	double t117 = yVec(dphi)*yVec(lLF)*pVec(m3)*t93;
	double t118 = yVec(dlLH)*pVec(m3)*t88;
	double t119 = yVec(dalphaLH)*pVec(l2)*pVec(m2)*t98;
	double t120 = yVec(dphi)*pVec(l2)*pVec(m2)*t98;
	double t121 = yVec(dalphaLH)*yVec(lLH)*pVec(m3)*t98;
	double t122 = yVec(dphi)*yVec(lLH)*pVec(m3)*t98;
	double t123 = yVec(dlRF)*pVec(m3)*t90;
	double t124 = yVec(dalphaRF)*pVec(l2)*pVec(m2)*t103;
	double t125 = yVec(dphi)*pVec(l2)*pVec(m2)*t103;
	double t126 = yVec(dalphaRF)*yVec(lRF)*pVec(m3)*t103;
	double t127 = yVec(dphi)*yVec(lRF)*pVec(m3)*t103;
	double t128 = yVec(dlRH)*pVec(m3)*t92;
	double t129 = yVec(dalphaRH)*pVec(l2)*pVec(m2)*t108;
	double t130 = yVec(dphi)*pVec(l2)*pVec(m2)*t108;
	double t131 = yVec(dalphaRH)*yVec(lRH)*pVec(m3)*t108;
	double t132 = yVec(dphi)*yVec(lRH)*pVec(m3)*t108;
	double t133 = sin(yVec(alphaLF));
	double t134 = sin(yVec(alphaLH));
	double t135 = yVec(dphi)*pVec(l3)*pVec(m3)*2.0;
	double t136 = sin(yVec(alphaRF));
	double t137 = sin(yVec(alphaRH));
	double t138 = yVec(dy)*pVec(l3)*pVec(m3)*t93;
	double t139 = yVec(dx)*pVec(l2)*pVec(m2)*t86;
	double t140 = yVec(dx)*yVec(lLF)*pVec(m3)*t86;
	double t141 = cos(yVec(alphaLF));
	double t142 = yVec(dy)*pVec(l3)*pVec(m3)*t98;
	double t143 = yVec(dx)*pVec(l2)*pVec(m2)*t88;
	double t144 = yVec(dx)*yVec(lLH)*pVec(m3)*t88;
	double t145 = cos(yVec(alphaLH));
	double t146 = yVec(dy)*pVec(l3)*pVec(m3)*t103;
	double t147 = yVec(dx)*pVec(l2)*pVec(m2)*t90;
	double t148 = yVec(dx)*yVec(lRF)*pVec(m3)*t90;
	double t149 = cos(yVec(alphaRF));
	double t150 = yVec(dy)*pVec(l3)*pVec(m3)*t108;
	double t151 = yVec(dx)*pVec(l2)*pVec(m2)*t92;
	double t152 = yVec(dx)*yVec(lRH)*pVec(m3)*t92;
	double t153 = cos(yVec(alphaRH));
	double t154 = yVec(dalphaLH)*pVec(l3)*pVec(m3)*2.0;
	double t155 = pVec(l2)*pVec(m2)*t88;
	double t156 = yVec(lLH)*pVec(m3)*t88;
	double t157 = yVec(dlLH)*yVec(dx)*pVec(m3)*t98;
	double t158 = yVec(dlLH)*yVec(dy)*pVec(m3)*t88;
	double t159 = yVec(dphi)*yVec(dphi);
	double t160 = yVec(dalphaLH)*yVec(dy)*pVec(l2)*pVec(m2)*t98;
	double t161 = yVec(dphi)*yVec(dy)*pVec(l2)*pVec(m2)*t98;
	double t162 = yVec(dalphaLH)*yVec(dy)*yVec(lLH)*pVec(m3)*t98;
	double t163 = yVec(dphi)*yVec(dy)*yVec(lLH)*pVec(m3)*t98;
	double t164 = yVec(dalphaLH)*yVec(dx)*pVec(l3)*pVec(m3)*t88;
	double t165 = yVec(dphi)*yVec(dx)*pVec(l3)*pVec(m3)*t88;
	double t166 = yVec(dx)*pVec(m3)*t98;
	double t167 = yVec(dy)*pVec(m3)*t88;
	double t168 = yVec(dalphaLH)*yVec(dalphaLH);
	double t169 = yVec(dy)*pVec(l2)*pVec(m2)*t93;
	double t170 = yVec(dy)*yVec(lLF)*pVec(m3)*t93;
	double t171 = yVec(dx)*pVec(l3)*pVec(m3)*t86;
	double t172 = yVec(dalphaLF)*yVec(lLF)*pVec(m3)*2.0;
	double t173 = yVec(dphi)*yVec(lLF)*pVec(m3)*2.0;
	double t174 = yVec(dx)*pVec(m3)*t93;
	double t175 = yVec(dy)*pVec(m3)*t86;
	double t176 = pVec(l2)*pVec(m2)*t86;
	double t177 = yVec(lLF)*pVec(m3)*t86;
	double t178 = yVec(dlLF)*yVec(dx)*pVec(m3)*t93;
	double t179 = yVec(dlLF)*yVec(dy)*pVec(m3)*t86;
	double t180 = yVec(dalphaLF)*yVec(dy)*pVec(l2)*pVec(m2)*t93;
	double t181 = yVec(dphi)*yVec(dy)*pVec(l2)*pVec(m2)*t93;
	double t182 = yVec(dalphaLF)*yVec(dy)*yVec(lLF)*pVec(m3)*t93;
	double t183 = yVec(dphi)*yVec(dy)*yVec(lLF)*pVec(m3)*t93;
	double t184 = yVec(dalphaLF)*yVec(dx)*pVec(l3)*pVec(m3)*t86;
	double t185 = yVec(dphi)*yVec(dx)*pVec(l3)*pVec(m3)*t86;
	double t186 = yVec(dphi)*pVec(l1)*pVec(m3)*t133;
	double t187 = yVec(dalphaLF)*yVec(dalphaLF);
	double t188 = yVec(dy)*pVec(l2)*pVec(m2)*t103;
	double t189 = yVec(dy)*yVec(lRF)*pVec(m3)*t103;
	double t190 = yVec(dx)*pVec(l3)*pVec(m3)*t90;
	double t191 = yVec(dalphaRF)*yVec(lRF)*pVec(m3)*2.0;
	double t192 = yVec(dphi)*yVec(lRF)*pVec(m3)*2.0;
	double t193 = yVec(dx)*pVec(m3)*t103;
	double t194 = yVec(dy)*pVec(m3)*t90;
	double t195 = pVec(l2)*pVec(m2)*t90;
	double t196 = yVec(lRF)*pVec(m3)*t90;
	double t197 = yVec(dlRF)*yVec(dx)*pVec(m3)*t103;
	double t198 = yVec(dlRF)*yVec(dy)*pVec(m3)*t90;
	double t199 = yVec(dalphaRF)*yVec(dy)*pVec(l2)*pVec(m2)*t103;
	double t200 = yVec(dphi)*yVec(dy)*pVec(l2)*pVec(m2)*t103;
	double t201 = yVec(dalphaRF)*yVec(dy)*yVec(lRF)*pVec(m3)*t103;
	double t202 = yVec(dphi)*yVec(dy)*yVec(lRF)*pVec(m3)*t103;
	double t203 = yVec(dalphaRF)*yVec(dx)*pVec(l3)*pVec(m3)*t90;
	double t204 = yVec(dphi)*yVec(dx)*pVec(l3)*pVec(m3)*t90;
	double t205 = yVec(dphi)*pVec(l1)*pVec(m3)*t136;
	double t206 = yVec(dalphaRF)*yVec(dalphaRF);
	double t207 = yVec(dalphaRH)*pVec(l3)*pVec(m3)*2.0;
	double t208 = pVec(l2)*pVec(m2)*t92;
	double t209 = yVec(lRH)*pVec(m3)*t92;
	double t210 = yVec(dlRH)*yVec(dx)*pVec(m3)*t108;
	double t211 = yVec(dlRH)*yVec(dy)*pVec(m3)*t92;
	double t212 = yVec(dalphaRH)*yVec(dy)*pVec(l2)*pVec(m2)*t108;
	double t213 = yVec(dphi)*yVec(dy)*pVec(l2)*pVec(m2)*t108;
	double t214 = yVec(dalphaRH)*yVec(dy)*yVec(lRH)*pVec(m3)*t108;
	double t215 = yVec(dphi)*yVec(dy)*yVec(lRH)*pVec(m3)*t108;
	double t216 = yVec(dalphaRH)*yVec(dx)*pVec(l3)*pVec(m3)*t92;
	double t217 = yVec(dphi)*yVec(dx)*pVec(l3)*pVec(m3)*t92;
	double t218 = yVec(dx)*pVec(m3)*t108;
	double t219 = yVec(dy)*pVec(m3)*t92;
	double t220 = yVec(dalphaRH)*yVec(dalphaRH);
	h(0,0) = -yVec(dlLF)*(yVec(dalphaLF)*pVec(m3)*t93+yVec(dphi)*pVec(m3)*t93)-yVec(dlLH)*(yVec(dalphaLH)*pVec(m3)*t98+yVec(dphi)*pVec(m3)*t98)-yVec(dlRF)*(yVec(dalphaRF)*pVec(m3)*t103+yVec(dphi)*pVec(m3)*t103)-yVec(dlRH)*(yVec(dalphaRH)*pVec(m3)*t108+yVec(dphi)*pVec(m3)*t108)+yVec(dphi)*(t94+t95+t96+t97+t99+t100+t101+t102+t104+t105+t106+t107+t109+t110+t111+t112-yVec(dlLF)*pVec(m3)*t93-yVec(dlLH)*pVec(m3)*t98-yVec(dlRF)*pVec(m3)*t103-yVec(dlRH)*pVec(m3)*t108-yVec(dalphaLF)*pVec(l3)*pVec(m3)*t86-yVec(dalphaLH)*pVec(l3)*pVec(m3)*t88-yVec(dalphaRF)*pVec(l3)*pVec(m3)*t90-yVec(dalphaRH)*pVec(l3)*pVec(m3)*t92-yVec(dphi)*pVec(l3)*pVec(m3)*t86-yVec(dphi)*pVec(l3)*pVec(m3)*t88-yVec(dphi)*pVec(l3)*pVec(m3)*t90-yVec(dphi)*pVec(l3)*pVec(m3)*t92)+yVec(dalphaLF)*(t94+t95+t96+t97-yVec(dlLF)*pVec(m3)*t93-yVec(dalphaLF)*pVec(l3)*pVec(m3)*t86-yVec(dphi)*pVec(l3)*pVec(m3)*t86)+yVec(dalphaLH)*(t99+t100+t101+t102-yVec(dlLH)*pVec(m3)*t98-yVec(dalphaLH)*pVec(l3)*pVec(m3)*t88-yVec(dphi)*pVec(l3)*pVec(m3)*t88)+yVec(dalphaRF)*(t104+t105+t106+t107-yVec(dlRF)*pVec(m3)*t103-yVec(dalphaRF)*pVec(l3)*pVec(m3)*t90-yVec(dphi)*pVec(l3)*pVec(m3)*t90)+yVec(dalphaRH)*(t109+t110+t111+t112-yVec(dlRH)*pVec(m3)*t108-yVec(dalphaRH)*pVec(l3)*pVec(m3)*t92-yVec(dphi)*pVec(l3)*pVec(m3)*t92);
	h(1,0) = -pVec(g)*(pVec(m1)+pVec(m2)*4.0+pVec(m3)*4.0)-yVec(dlLF)*(yVec(dalphaLF)*pVec(m3)*t86+yVec(dphi)*pVec(m3)*t86)-yVec(dlLH)*(yVec(dalphaLH)*pVec(m3)*t88+yVec(dphi)*pVec(m3)*t88)-yVec(dlRF)*(yVec(dalphaRF)*pVec(m3)*t90+yVec(dphi)*pVec(m3)*t90)-yVec(dlRH)*(yVec(dalphaRH)*pVec(m3)*t92+yVec(dphi)*pVec(m3)*t92)-yVec(dalphaLF)*(t113+t114+t115+t116+t117-yVec(dalphaLF)*pVec(l3)*pVec(m3)*t93-yVec(dphi)*pVec(l3)*pVec(m3)*t93)-yVec(dalphaLH)*(t118+t119+t120+t121+t122-yVec(dalphaLH)*pVec(l3)*pVec(m3)*t98-yVec(dphi)*pVec(l3)*pVec(m3)*t98)-yVec(dalphaRF)*(t123+t124+t125+t126+t127-yVec(dalphaRF)*pVec(l3)*pVec(m3)*t103-yVec(dphi)*pVec(l3)*pVec(m3)*t103)-yVec(dalphaRH)*(t128+t129+t130+t131+t132-yVec(dalphaRH)*pVec(l3)*pVec(m3)*t108-yVec(dphi)*pVec(l3)*pVec(m3)*t108)-yVec(dphi)*(t113+t114+t115+t116+t117+t118+t119+t120+t121+t122+t123+t124+t125+t126+t127+t128+t129+t130+t131+t132-yVec(dalphaLF)*pVec(l3)*pVec(m3)*t93-yVec(dalphaLH)*pVec(l3)*pVec(m3)*t98-yVec(dalphaRF)*pVec(l3)*pVec(m3)*t103-yVec(dalphaRH)*pVec(l3)*pVec(m3)*t108-yVec(dphi)*pVec(l3)*pVec(m3)*t93-yVec(dphi)*pVec(l3)*pVec(m3)*t98-yVec(dphi)*pVec(l3)*pVec(m3)*t103-yVec(dphi)*pVec(l3)*pVec(m3)*t108);
	h(2,0) = t157+t158+t160+t161+t162+t163+t164+t165+t178+t179+t180+t181+t182+t183+t184+t185+t197+t198+t199+t200+t201+t202+t203+t204+t210+t211+t212+t213+t214+t215+t216+t217-yVec(dlLF)*(t172+t173+t174+t175-yVec(dalphaLF)*pVec(l3)*pVec(m3)*2.0-yVec(dphi)*pVec(l3)*pVec(m3)*2.0+yVec(dalphaLF)*pVec(l1)*pVec(m3)*t133+yVec(dphi)*pVec(l1)*pVec(m3)*t133*2.0)+yVec(dphi)*(t138+t139+t140+t142+t143+t144+t146+t147+t148+t150+t151+t152-yVec(dx)*pVec(l3)*pVec(m3)*t86-yVec(dx)*pVec(l3)*pVec(m3)*t88-yVec(dx)*pVec(l3)*pVec(m3)*t90-yVec(dx)*pVec(l3)*pVec(m3)*t92-yVec(dy)*pVec(l2)*pVec(m2)*t93-yVec(dy)*pVec(l2)*pVec(m2)*t98-yVec(dy)*pVec(l2)*pVec(m2)*t103-yVec(dy)*pVec(l2)*pVec(m2)*t108-yVec(dy)*yVec(lLF)*pVec(m3)*t93-yVec(dy)*yVec(lLH)*pVec(m3)*t98-yVec(dy)*yVec(lRF)*pVec(m3)*t103-yVec(dy)*yVec(lRH)*pVec(m3)*t108)-pVec(g)*(t155+t156+t176+t177+t195+t196+t208+t209-pVec(l3)*pVec(m3)*t86-pVec(l3)*pVec(m3)*t88-pVec(l3)*pVec(m3)*t90-pVec(l3)*pVec(m3)*t92)-yVec(dlRF)*(-t135+t191+t192+t193+t194-yVec(dalphaRF)*pVec(l3)*pVec(m3)*2.0+yVec(dalphaRF)*pVec(l1)*pVec(m3)*t136+yVec(dphi)*pVec(l1)*pVec(m3)*t136*2.0)+yVec(dalphaLH)*(t142+t143+t144+yVec(dlLH)*pVec(l1)*pVec(m3)*t134-yVec(dx)*pVec(l3)*pVec(m3)*t88-yVec(dy)*pVec(l2)*pVec(m2)*t98-yVec(dy)*yVec(lLH)*pVec(m3)*t98+yVec(dalphaLH)*pVec(l1)*pVec(l2)*pVec(m2)*t145-yVec(dalphaLH)*pVec(l1)*pVec(l3)*pVec(m3)*t145+yVec(dphi)*pVec(l1)*pVec(l2)*pVec(m2)*t145*2.0-yVec(dphi)*pVec(l1)*pVec(l3)*pVec(m3)*t145*2.0+yVec(dalphaLH)*pVec(l1)*yVec(lLH)*pVec(m3)*t145+yVec(dphi)*pVec(l1)*yVec(lLH)*pVec(m3)*t145*2.0)+yVec(dalphaRH)*(t150+t151+t152+yVec(dlRH)*pVec(l1)*pVec(m3)*t137-yVec(dx)*pVec(l3)*pVec(m3)*t92-yVec(dy)*pVec(l2)*pVec(m2)*t108-yVec(dy)*yVec(lRH)*pVec(m3)*t108+yVec(dalphaRH)*pVec(l1)*pVec(l2)*pVec(m2)*t153-yVec(dalphaRH)*pVec(l1)*pVec(l3)*pVec(m3)*t153+yVec(dphi)*pVec(l1)*pVec(l2)*pVec(m2)*t153*2.0-yVec(dphi)*pVec(l1)*pVec(l3)*pVec(m3)*t153*2.0+yVec(dalphaRH)*pVec(l1)*yVec(lRH)*pVec(m3)*t153+yVec(dphi)*pVec(l1)*yVec(lRH)*pVec(m3)*t153*2.0)+yVec(dlLH)*(t135+t154-yVec(dalphaLH)*yVec(lLH)*pVec(m3)*2.0-yVec(dphi)*yVec(lLH)*pVec(m3)*2.0-yVec(dx)*pVec(m3)*t98-yVec(dy)*pVec(m3)*t88+yVec(dalphaLH)*pVec(l1)*pVec(m3)*t134+yVec(dphi)*pVec(l1)*pVec(m3)*t134*2.0)+yVec(dlRH)*(t135+t207-yVec(dalphaRH)*yVec(lRH)*pVec(m3)*2.0-yVec(dphi)*yVec(lRH)*pVec(m3)*2.0-yVec(dx)*pVec(m3)*t108-yVec(dy)*pVec(m3)*t92+yVec(dalphaRH)*pVec(l1)*pVec(m3)*t137+yVec(dphi)*pVec(l1)*pVec(m3)*t137*2.0)-yVec(dalphaLF)*(-t138-t139-t140+t169+t170+t171+yVec(dlLF)*pVec(l1)*pVec(m3)*t133+yVec(dalphaLF)*pVec(l1)*pVec(l2)*pVec(m2)*t141-yVec(dalphaLF)*pVec(l1)*pVec(l3)*pVec(m3)*t141+yVec(dphi)*pVec(l1)*pVec(l2)*pVec(m2)*t141*2.0-yVec(dphi)*pVec(l1)*pVec(l3)*pVec(m3)*t141*2.0+yVec(dalphaLF)*pVec(l1)*yVec(lLF)*pVec(m3)*t141+yVec(dphi)*pVec(l1)*yVec(lLF)*pVec(m3)*t141*2.0)-yVec(dalphaRF)*(-t146-t147-t148+t188+t189+t190+yVec(dlRF)*pVec(l1)*pVec(m3)*t136+yVec(dalphaRF)*pVec(l1)*pVec(l2)*pVec(m2)*t149-yVec(dalphaRF)*pVec(l1)*pVec(l3)*pVec(m3)*t149+yVec(dphi)*pVec(l1)*pVec(l2)*pVec(m2)*t149*2.0-yVec(dphi)*pVec(l1)*pVec(l3)*pVec(m3)*t149*2.0+yVec(dalphaRF)*pVec(l1)*yVec(lRF)*pVec(m3)*t149+yVec(dphi)*pVec(l1)*yVec(lRF)*pVec(m3)*t149*2.0)-yVec(dalphaLF)*yVec(dx)*pVec(l2)*pVec(m2)*t86-yVec(dalphaLH)*yVec(dx)*pVec(l2)*pVec(m2)*t88-yVec(dalphaRF)*yVec(dx)*pVec(l2)*pVec(m2)*t90-yVec(dalphaRH)*yVec(dx)*pVec(l2)*pVec(m2)*t92-yVec(dalphaLF)*yVec(dy)*pVec(l3)*pVec(m3)*t93-yVec(dalphaLH)*yVec(dy)*pVec(l3)*pVec(m3)*t98-yVec(dalphaRF)*yVec(dy)*pVec(l3)*pVec(m3)*t103-yVec(dalphaRH)*yVec(dy)*pVec(l3)*pVec(m3)*t108-yVec(dphi)*yVec(dx)*pVec(l2)*pVec(m2)*t86-yVec(dphi)*yVec(dx)*pVec(l2)*pVec(m2)*t88-yVec(dphi)*yVec(dx)*pVec(l2)*pVec(m2)*t90-yVec(dphi)*yVec(dx)*pVec(l2)*pVec(m2)*t92-yVec(dphi)*yVec(dy)*pVec(l3)*pVec(m3)*t93-yVec(dphi)*yVec(dy)*pVec(l3)*pVec(m3)*t98-yVec(dphi)*yVec(dy)*pVec(l3)*pVec(m3)*t103-yVec(dphi)*yVec(dy)*pVec(l3)*pVec(m3)*t108-yVec(dalphaLF)*yVec(dx)*yVec(lLF)*pVec(m3)*t86-yVec(dalphaLH)*yVec(dx)*yVec(lLH)*pVec(m3)*t88-yVec(dalphaRF)*yVec(dx)*yVec(lRF)*pVec(m3)*t90-yVec(dalphaRH)*yVec(dx)*yVec(lRH)*pVec(m3)*t92-yVec(dphi)*yVec(dx)*yVec(lLF)*pVec(m3)*t86-yVec(dphi)*yVec(dx)*yVec(lLH)*pVec(m3)*t88-yVec(dphi)*yVec(dx)*yVec(lRF)*pVec(m3)*t90-yVec(dphi)*yVec(dx)*yVec(lRH)*pVec(m3)*t92;
	h(3,0) = t157+t158+t160+t161+t162+t163+t164+t165+yVec(dalphaLH)*(t142+t143+t144-yVec(dx)*pVec(l3)*pVec(m3)*t88-yVec(dy)*pVec(l2)*pVec(m2)*t98-yVec(dy)*yVec(lLH)*pVec(m3)*t98+yVec(dphi)*pVec(l1)*pVec(l2)*pVec(m2)*t145-yVec(dphi)*pVec(l1)*pVec(l3)*pVec(m3)*t145+yVec(dphi)*pVec(l1)*yVec(lLH)*pVec(m3)*t145)+yVec(dphi)*(t142+t143+t144-yVec(dx)*pVec(l3)*pVec(m3)*t88-yVec(dy)*pVec(l2)*pVec(m2)*t98-yVec(dy)*yVec(lLH)*pVec(m3)*t98)-pVec(g)*(t155+t156-pVec(l3)*pVec(m3)*t88)-yVec(dlLH)*(-t135-t154+t166+t167+yVec(dalphaLH)*yVec(lLH)*pVec(m3)*2.0+yVec(dphi)*yVec(lLH)*pVec(m3)*2.0-yVec(dphi)*pVec(l1)*pVec(m3)*t134)-yVec(dalphaLH)*yVec(dx)*pVec(l2)*pVec(m2)*t88-yVec(dalphaLH)*yVec(dy)*pVec(l3)*pVec(m3)*t98-yVec(dlLH)*yVec(dphi)*pVec(l1)*pVec(m3)*t134-yVec(dphi)*yVec(dx)*pVec(l2)*pVec(m2)*t88-yVec(dphi)*yVec(dy)*pVec(l3)*pVec(m3)*t98-yVec(dalphaLH)*yVec(dx)*yVec(lLH)*pVec(m3)*t88-yVec(dphi)*yVec(dx)*yVec(lLH)*pVec(m3)*t88-pVec(l1)*pVec(l2)*pVec(m2)*t145*t159+pVec(l1)*pVec(l3)*pVec(m3)*t145*t159-pVec(l1)*yVec(lLH)*pVec(m3)*t145*t159-yVec(dalphaLH)*yVec(dphi)*pVec(l1)*pVec(l2)*pVec(m2)*t145+yVec(dalphaLH)*yVec(dphi)*pVec(l1)*pVec(l3)*pVec(m3)*t145-yVec(dalphaLH)*yVec(dphi)*pVec(l1)*yVec(lLH)*pVec(m3)*t145;
	h(4,0) = -yVec(dphi)*(t166+t167)-yVec(dalphaLH)*(t166+t167-yVec(dphi)*pVec(l1)*pVec(m3)*t134)+pVec(g)*pVec(m3)*t98-pVec(l3)*pVec(m3)*t159-pVec(l3)*pVec(m3)*t168+yVec(lLH)*pVec(m3)*t159+yVec(lLH)*pVec(m3)*t168-yVec(dalphaLH)*yVec(dphi)*pVec(l3)*pVec(m3)*2.0+yVec(dalphaLH)*yVec(dphi)*yVec(lLH)*pVec(m3)*2.0+yVec(dalphaLH)*yVec(dx)*pVec(m3)*t98+yVec(dalphaLH)*yVec(dy)*pVec(m3)*t88+yVec(dphi)*yVec(dx)*pVec(m3)*t98+yVec(dphi)*yVec(dy)*pVec(m3)*t88-pVec(l1)*pVec(m3)*t134*t159-yVec(dalphaLH)*yVec(dphi)*pVec(l1)*pVec(m3)*t134;
	h(5,0) = t178+t179+t180+t181+t182+t183+t184+t185-yVec(dalphaLF)*(-t138-t139-t140+t169+t170+t171+yVec(dphi)*pVec(l1)*pVec(l2)*pVec(m2)*t141-yVec(dphi)*pVec(l1)*pVec(l3)*pVec(m3)*t141+yVec(dphi)*pVec(l1)*yVec(lLF)*pVec(m3)*t141)-pVec(g)*(t176+t177-pVec(l3)*pVec(m3)*t86)-yVec(dlLF)*(-t135+t172+t173+t174+t175+t186-yVec(dalphaLF)*pVec(l3)*pVec(m3)*2.0)+yVec(dphi)*(t138+t139+t140-t169-t170-t171)-yVec(dalphaLF)*yVec(dx)*pVec(l2)*pVec(m2)*t86-yVec(dalphaLF)*yVec(dy)*pVec(l3)*pVec(m3)*t93+yVec(dlLF)*yVec(dphi)*pVec(l1)*pVec(m3)*t133-yVec(dphi)*yVec(dx)*pVec(l2)*pVec(m2)*t86-yVec(dphi)*yVec(dy)*pVec(l3)*pVec(m3)*t93-yVec(dalphaLF)*yVec(dx)*yVec(lLF)*pVec(m3)*t86-yVec(dphi)*yVec(dx)*yVec(lLF)*pVec(m3)*t86+pVec(l1)*pVec(l2)*pVec(m2)*t141*t159-pVec(l1)*pVec(l3)*pVec(m3)*t141*t159+pVec(l1)*yVec(lLF)*pVec(m3)*t141*t159+yVec(dalphaLF)*yVec(dphi)*pVec(l1)*pVec(l2)*pVec(m2)*t141-yVec(dalphaLF)*yVec(dphi)*pVec(l1)*pVec(l3)*pVec(m3)*t141+yVec(dalphaLF)*yVec(dphi)*pVec(l1)*yVec(lLF)*pVec(m3)*t141;
	h(6,0) = -yVec(dphi)*(t174+t175)-yVec(dalphaLF)*(t174+t175+t186)+pVec(g)*pVec(m3)*t93-pVec(l3)*pVec(m3)*t159-pVec(l3)*pVec(m3)*t187+yVec(lLF)*pVec(m3)*t159+yVec(lLF)*pVec(m3)*t187-yVec(dalphaLF)*yVec(dphi)*pVec(l3)*pVec(m3)*2.0+yVec(dalphaLF)*yVec(dphi)*yVec(lLF)*pVec(m3)*2.0+yVec(dalphaLF)*yVec(dx)*pVec(m3)*t93+yVec(dalphaLF)*yVec(dy)*pVec(m3)*t86+yVec(dphi)*yVec(dx)*pVec(m3)*t93+yVec(dphi)*yVec(dy)*pVec(m3)*t86+pVec(l1)*pVec(m3)*t133*t159+yVec(dalphaLF)*yVec(dphi)*pVec(l1)*pVec(m3)*t133;
	h(7,0) = t197+t198+t199+t200+t201+t202+t203+t204-yVec(dalphaRF)*(-t146-t147-t148+t188+t189+t190+yVec(dphi)*pVec(l1)*pVec(l2)*pVec(m2)*t149-yVec(dphi)*pVec(l1)*pVec(l3)*pVec(m3)*t149+yVec(dphi)*pVec(l1)*yVec(lRF)*pVec(m3)*t149)-pVec(g)*(t195+t196-pVec(l3)*pVec(m3)*t90)-yVec(dlRF)*(-t135+t191+t192+t193+t194+t205-yVec(dalphaRF)*pVec(l3)*pVec(m3)*2.0)+yVec(dphi)*(t146+t147+t148-t188-t189-t190)-yVec(dalphaRF)*yVec(dx)*pVec(l2)*pVec(m2)*t90-yVec(dalphaRF)*yVec(dy)*pVec(l3)*pVec(m3)*t103+yVec(dlRF)*yVec(dphi)*pVec(l1)*pVec(m3)*t136-yVec(dphi)*yVec(dx)*pVec(l2)*pVec(m2)*t90-yVec(dphi)*yVec(dy)*pVec(l3)*pVec(m3)*t103-yVec(dalphaRF)*yVec(dx)*yVec(lRF)*pVec(m3)*t90-yVec(dphi)*yVec(dx)*yVec(lRF)*pVec(m3)*t90+pVec(l1)*pVec(l2)*pVec(m2)*t149*t159-pVec(l1)*pVec(l3)*pVec(m3)*t149*t159+pVec(l1)*yVec(lRF)*pVec(m3)*t149*t159+yVec(dalphaRF)*yVec(dphi)*pVec(l1)*pVec(l2)*pVec(m2)*t149-yVec(dalphaRF)*yVec(dphi)*pVec(l1)*pVec(l3)*pVec(m3)*t149+yVec(dalphaRF)*yVec(dphi)*pVec(l1)*yVec(lRF)*pVec(m3)*t149;
	h(8,0) = -yVec(dphi)*(t193+t194)-yVec(dalphaRF)*(t193+t194+t205)+pVec(g)*pVec(m3)*t103-pVec(l3)*pVec(m3)*t159-pVec(l3)*pVec(m3)*t206+yVec(lRF)*pVec(m3)*t159+yVec(lRF)*pVec(m3)*t206-yVec(dalphaRF)*yVec(dphi)*pVec(l3)*pVec(m3)*2.0+yVec(dalphaRF)*yVec(dphi)*yVec(lRF)*pVec(m3)*2.0+yVec(dalphaRF)*yVec(dx)*pVec(m3)*t103+yVec(dalphaRF)*yVec(dy)*pVec(m3)*t90+yVec(dphi)*yVec(dx)*pVec(m3)*t103+yVec(dphi)*yVec(dy)*pVec(m3)*t90+pVec(l1)*pVec(m3)*t136*t159+yVec(dalphaRF)*yVec(dphi)*pVec(l1)*pVec(m3)*t136;
	h(9,0) = t210+t211+t212+t213+t214+t215+t216+t217+yVec(dalphaRH)*(t150+t151+t152-yVec(dx)*pVec(l3)*pVec(m3)*t92-yVec(dy)*pVec(l2)*pVec(m2)*t108-yVec(dy)*yVec(lRH)*pVec(m3)*t108+yVec(dphi)*pVec(l1)*pVec(l2)*pVec(m2)*t153-yVec(dphi)*pVec(l1)*pVec(l3)*pVec(m3)*t153+yVec(dphi)*pVec(l1)*yVec(lRH)*pVec(m3)*t153)+yVec(dphi)*(t150+t151+t152-yVec(dx)*pVec(l3)*pVec(m3)*t92-yVec(dy)*pVec(l2)*pVec(m2)*t108-yVec(dy)*yVec(lRH)*pVec(m3)*t108)-pVec(g)*(t208+t209-pVec(l3)*pVec(m3)*t92)-yVec(dlRH)*(-t135-t207+t218+t219+yVec(dalphaRH)*yVec(lRH)*pVec(m3)*2.0+yVec(dphi)*yVec(lRH)*pVec(m3)*2.0-yVec(dphi)*pVec(l1)*pVec(m3)*t137)-yVec(dalphaRH)*yVec(dx)*pVec(l2)*pVec(m2)*t92-yVec(dalphaRH)*yVec(dy)*pVec(l3)*pVec(m3)*t108-yVec(dlRH)*yVec(dphi)*pVec(l1)*pVec(m3)*t137-yVec(dphi)*yVec(dx)*pVec(l2)*pVec(m2)*t92-yVec(dphi)*yVec(dy)*pVec(l3)*pVec(m3)*t108-yVec(dalphaRH)*yVec(dx)*yVec(lRH)*pVec(m3)*t92-yVec(dphi)*yVec(dx)*yVec(lRH)*pVec(m3)*t92-pVec(l1)*pVec(l2)*pVec(m2)*t153*t159+pVec(l1)*pVec(l3)*pVec(m3)*t153*t159-pVec(l1)*yVec(lRH)*pVec(m3)*t153*t159-yVec(dalphaRH)*yVec(dphi)*pVec(l1)*pVec(l2)*pVec(m2)*t153+yVec(dalphaRH)*yVec(dphi)*pVec(l1)*pVec(l3)*pVec(m3)*t153-yVec(dalphaRH)*yVec(dphi)*pVec(l1)*yVec(lRH)*pVec(m3)*t153;
	h(10,0) = -yVec(dphi)*(t218+t219)-yVec(dalphaRH)*(t218+t219-yVec(dphi)*pVec(l1)*pVec(m3)*t137)+pVec(g)*pVec(m3)*t108-pVec(l3)*pVec(m3)*t159-pVec(l3)*pVec(m3)*t220+yVec(lRH)*pVec(m3)*t159+yVec(lRH)*pVec(m3)*t220-yVec(dalphaRH)*yVec(dphi)*pVec(l3)*pVec(m3)*2.0+yVec(dalphaRH)*yVec(dphi)*yVec(lRH)*pVec(m3)*2.0+yVec(dalphaRH)*yVec(dx)*pVec(m3)*t108+yVec(dalphaRH)*yVec(dy)*pVec(m3)*t92+yVec(dphi)*yVec(dx)*pVec(m3)*t108+yVec(dphi)*yVec(dy)*pVec(m3)*t92-pVec(l1)*pVec(m3)*t137*t159-yVec(dalphaRH)*yVec(dphi)*pVec(l1)*pVec(m3)*t137;
	}

void  PB_Dynamics::ComputeContactPointLH(){
	posLH =  Vector2d::Zero();
	double t222 = yVec(alphaLH)+yVec(phi);
	posLH(0,0) = yVec(x)-pVec(l1)*cos(yVec(phi))+yVec(lLH)*sin(t222);
	posLH(1,0) = -pVec(rFoot)+yVec(y)-yVec(lLH)*cos(t222)-pVec(l1)*sin(yVec(phi));
}

void  PB_Dynamics::ComputeContactPointLF(){
	posLF =  Vector2d::Zero();
	double t224 = yVec(alphaLF)+yVec(phi);
	posLF(0,0) = yVec(x)+pVec(l1)*cos(yVec(phi))+yVec(lLF)*sin(t224);
	posLF(1,0) = -pVec(rFoot)+yVec(y)-yVec(lLF)*cos(t224)+pVec(l1)*sin(yVec(phi));
}

void  PB_Dynamics::ComputeContactPointRF(){
	posRF =  Vector2d::Zero();
	double t226 = yVec(alphaRF)+yVec(phi);
	posRF(0,0) = yVec(x)+pVec(l1)*cos(yVec(phi))+yVec(lRF)*sin(t226);
	posRF(1,0) = -pVec(rFoot)+yVec(y)-yVec(lRF)*cos(t226)+pVec(l1)*sin(yVec(phi));
}

void  PB_Dynamics::ComputeContactPointRH(){
	posRH =  Vector2d::Zero();
	double t228 = yVec(alphaRH)+yVec(phi);
	posRH(0,0) = yVec(x)-pVec(l1)*cos(yVec(phi))+yVec(lRH)*sin(t228);
	posRH(1,0) = -pVec(rFoot)+yVec(y)-yVec(lRH)*cos(t228)-pVec(l1)*sin(yVec(phi));
}

void PB_Dynamics::ComputeContactJacobianLH(){
	JLH =  Matrix2Qd::Zero();
	double t230 = yVec(alphaLH)+yVec(phi);
	double t231 = cos(t230);
	double t232 = yVec(lLH)*t231;
	double t233 = sin(t230);
	double t234 = yVec(lLH)*t233;
	JLH(0,0) = 1.0;
	JLH(0,2) = pVec(rFoot)+t232+pVec(l1)*sin(yVec(phi));
	JLH(0,3) = pVec(rFoot)+t232;
	JLH(0,4) = t233;
	JLH(1,1) = 1.0;
	JLH(1,2) = t234-pVec(l1)*cos(yVec(phi));
	JLH(1,3) = t234;
	JLH(1,4) = -t231;
}

void PB_Dynamics::ComputeContactJacobianLF(){
	JLF =  Matrix2Qd::Zero();
	double t236 = yVec(alphaLF)+yVec(phi);
	double t237 = cos(t236);
	double t238 = yVec(lLF)*t237;
	double t239 = sin(t236);
	double t240 = yVec(lLF)*t239;
	JLF(0,0) = 1.0;
	JLF(0,2) = pVec(rFoot)+t238-pVec(l1)*sin(yVec(phi));
	JLF(0,5) = pVec(rFoot)+t238;
	JLF(0,6) = t239;
	JLF(1,1) = 1.0;
	JLF(1,2) = t240+pVec(l1)*cos(yVec(phi));
	JLF(1,5) = t240;
	JLF(1,6) = -t237;
}
void PB_Dynamics::ComputeContactJacobianRF(){
	JRF =  Matrix2Qd::Zero();
	double t242 = yVec(alphaRF)+yVec(phi);
	double t243 = cos(t242);
	double t244 = yVec(lRF)*t243;
	double t245 = sin(t242);
	double t246 = yVec(lRF)*t245;
	JRF(0,0) = 1.0;
	JRF(0,2) = pVec(rFoot)+t244-pVec(l1)*sin(yVec(phi));
	JRF(0,7) = pVec(rFoot)+t244;
	JRF(0,8) = t245;
	JRF(1,1) = 1.0;
	JRF(1,2) = t246+pVec(l1)*cos(yVec(phi));
	JRF(1,7) = t246;
	JRF(1,8) = -t243;
}
void PB_Dynamics::ComputeContactJacobianRH(){
	JRH =  Matrix2Qd::Zero();
	double t248 = yVec(alphaRH)+yVec(phi);
	double t249 = cos(t248);
	double t250 = yVec(lRH)*t249;
	double t251 = sin(t248);
	double t252 = yVec(lRH)*t251;
	JRH(0,0) = 1.0;
	JRH(0,2) = pVec(rFoot)+t250+pVec(l1)*sin(yVec(phi));
	JRH(0,9) = pVec(rFoot)+t250;
	JRH(0,10) = t251;
	JRH(1,1) = 1.0;
	JRH(1,2) = t252-pVec(l1)*cos(yVec(phi));
	JRH(1,9) = t252;
	JRH(1,10) = -t249;
}
void  PB_Dynamics::ComputeContactJacobianDtTIMESdqdtLH(){
	dJLHdtdqdt =  Vector2d::Zero();
	double t254 = yVec(alphaLH)+yVec(phi);
	double t255 = sin(t254);
	double t256 = cos(t254);
	double t257 = yVec(dalphaLH)*yVec(lLH)*t255;
	double t258 = yVec(dlLH)*t255;
	double t259 = yVec(dalphaLH)*yVec(lLH)*t256;
	double t260 = yVec(dalphaLH)+yVec(dphi);
	dJLHdtdqdt(0,0) = -yVec(dalphaLH)*(t257-yVec(dlLH)*t256+yVec(dphi)*yVec(lLH)*t255)-yVec(dphi)*(t257+yVec(dphi)*(yVec(lLH)*t255-pVec(l1)*cos(yVec(phi)))-yVec(dlLH)*t256)+yVec(dlLH)*t256*t260;
	dJLHdtdqdt(1,0) = yVec(dphi)*(t258+t259+yVec(dphi)*(yVec(lLH)*t256+pVec(l1)*sin(yVec(phi))))+yVec(dalphaLH)*(t258+t259+yVec(dphi)*yVec(lLH)*t256)+yVec(dlLH)*t255*t260;
}
void  PB_Dynamics::ComputeContactJacobianDtTIMESdqdtLF(){
	dJLFdtdqdt =  Vector2d::Zero();
	double t262 = yVec(alphaLF)+yVec(phi);
	double t263 = sin(t262);
	double t264 = cos(t262);
	double t265 = yVec(dalphaLF)*yVec(lLF)*t263;
	double t266 = yVec(dlLF)*t263;
	double t267 = yVec(dalphaLF)*yVec(lLF)*t264;
	double t268 = yVec(dalphaLF)+yVec(dphi);
	dJLFdtdqdt(0,0) = -yVec(dalphaLF)*(t265-yVec(dlLF)*t264+yVec(dphi)*yVec(lLF)*t263)-yVec(dphi)*(t265+yVec(dphi)*(yVec(lLF)*t263+pVec(l1)*cos(yVec(phi)))-yVec(dlLF)*t264)+yVec(dlLF)*t264*t268;
	dJLFdtdqdt(1,0) = yVec(dphi)*(t266+t267+yVec(dphi)*(yVec(lLF)*t264-pVec(l1)*sin(yVec(phi))))+yVec(dalphaLF)*(t266+t267+yVec(dphi)*yVec(lLF)*t264)+yVec(dlLF)*t263*t268;
}
void  PB_Dynamics::ComputeContactJacobianDtTIMESdqdtRF(){
	dJRFdtdqdt =  Vector2d::Zero();
	double t270 = yVec(alphaRF)+yVec(phi);
	double t271 = sin(t270);
	double t272 = cos(t270);
	double t273 = yVec(dalphaRF)*yVec(lRF)*t271;
	double t274 = yVec(dlRF)*t271;
	double t275 = yVec(dalphaRF)*yVec(lRF)*t272;
	double t276 = yVec(dalphaRF)+yVec(dphi);
	dJRFdtdqdt(0,0) = -yVec(dalphaRF)*(t273-yVec(dlRF)*t272+yVec(dphi)*yVec(lRF)*t271)-yVec(dphi)*(t273+yVec(dphi)*(yVec(lRF)*t271+pVec(l1)*cos(yVec(phi)))-yVec(dlRF)*t272)+yVec(dlRF)*t272*t276;
	dJRFdtdqdt(1,0) = yVec(dphi)*(t274+t275+yVec(dphi)*(yVec(lRF)*t272-pVec(l1)*sin(yVec(phi))))+yVec(dalphaRF)*(t274+t275+yVec(dphi)*yVec(lRF)*t272)+yVec(dlRF)*t271*t276;
}
void  PB_Dynamics::ComputeContactJacobianDtTIMESdqdtRH(){
	dJRHdtdqdt =  Vector2d::Zero();
	double t278 = yVec(alphaRH)+yVec(phi);
	double t279 = sin(t278);
	double t280 = cos(t278);
	double t281 = yVec(dalphaRH)*yVec(lRH)*t279;
	double t282 = yVec(dlRH)*t279;
	double t283 = yVec(dalphaRH)*yVec(lRH)*t280;
	double t284 = yVec(dalphaRH)+yVec(dphi);
	dJRHdtdqdt(0,0) = -yVec(dalphaRH)*(t281-yVec(dlRH)*t280+yVec(dphi)*yVec(lRH)*t279)-yVec(dphi)*(t281+yVec(dphi)*(yVec(lRH)*t279-pVec(l1)*cos(yVec(phi)))-yVec(dlRH)*t280)+yVec(dlRH)*t280*t284;
	dJRHdtdqdt(1,0) = yVec(dphi)*(t282+t283+yVec(dphi)*(yVec(lRH)*t280+pVec(l1)*sin(yVec(phi))))+yVec(dalphaRH)*(t282+t283+yVec(dphi)*yVec(lRH)*t280)+yVec(dlRH)*t279*t284;
}
void PB_Dynamics::ComputeJointForces(){
	// Joint forces:
	tau = VectorQd::Zero();
	// Compute spring and damping forces:
	double T_spring_alphaLH, T_spring_alphaLF, T_spring_alphaRF, T_spring_alphaRH,
	       F_spring_lLH, F_spring_lLF, F_spring_lRF, F_spring_lRH;
	// HIP
	if (pVec(hip_jnt_type) == PEA){ // Parallel spring in the hip
		T_spring_alphaLH = pVec(kalpha)*(0 - yVec(alphaLH)) +
				              b_alpha *(0 - yVec(dalphaLH));
		T_spring_alphaLF = pVec(kalpha)*(0 - yVec(alphaLF)) +
				              b_alpha *(0 - yVec(dalphaLF));
		T_spring_alphaRF = pVec(kalpha)*(0 - yVec(alphaRF)) +
				              b_alpha *(0 - yVec(dalphaRF));
		T_spring_alphaRH = pVec(kalpha)*(0 - yVec(alphaRH)) +
				              b_alpha *(0 - yVec(dalphaRH));
		tau(qalphaLH) = T_spring_alphaLH + uVec(TalphaLH);
		tau(qalphaLF) = T_spring_alphaLF + uVec(TalphaLF);
		tau(qalphaRF) = T_spring_alphaRF + uVec(TalphaRF);
		tau(qalphaRH) = T_spring_alphaRH + uVec(TalphaRH);
	} else { // Serial spring in the hip
		T_spring_alphaLH = pVec(kalpha)*(yVec(ualphaLH)  - yVec(alphaLH)) +
				              b_alpha *(yVec(dualphaLH) - yVec(dalphaLH));
		T_spring_alphaLF = pVec(kalpha)*(yVec(ualphaLF)  - yVec(alphaLF)) +
				              b_alpha *(yVec(dualphaLF) - yVec(dalphaLF));
		T_spring_alphaRF = pVec(kalpha)*(yVec(ualphaRF)  - yVec(alphaRF)) +
				              b_alpha *(yVec(dualphaRF) - yVec(dalphaRF));
		T_spring_alphaRH = pVec(kalpha)*(yVec(ualphaRH)  - yVec(alphaRH)) +
				              b_alpha *(yVec(dualphaRH) - yVec(dalphaRH));
		tau(qalphaLH) = T_spring_alphaLH + 0;
		tau(qalphaLF) = T_spring_alphaLF + 0;
		tau(qalphaRF) = T_spring_alphaRF + 0;
		tau(qalphaRH) = T_spring_alphaRH + 0;
	}
	// LEG
	if (pVec(leg_jnt_type) == PEA){ // Parallel spring in the leg
		F_spring_lLH = pVec(kl)*(pVec(l_0) + 0 - yVec(lLH)) +
			              b_l *(          + 0 - yVec(dlLH));
		F_spring_lLF = pVec(kl)*(pVec(l_0) + 0 - yVec(lLF)) +
			              b_l *(          + 0 - yVec(dlLF));
		F_spring_lRF = pVec(kl)*(pVec(l_0) + 0 - yVec(lRF)) +
			              b_l *(          + 0 - yVec(dlRF));
		F_spring_lRH = pVec(kl)*(pVec(l_0) + 0 - yVec(lRH)) +
			              b_l *(          + 0 - yVec(dlRH));
		tau(qlLH) = F_spring_lLH + uVec(FlLH);
		tau(qlLF) = F_spring_lLF + uVec(FlLF);
		tau(qlRF) = F_spring_lRF + uVec(FlRF);
		tau(qlRH) = F_spring_lRH + uVec(FlRH);
	} else { // Serial spring in the leg
		F_spring_lLH = pVec(kl)*(pVec(l_0) + yVec(ulLH)  - yVec(lLH)) +
			              b_l *(          + yVec(dulLH) - yVec(dlLH));
		F_spring_lLF = pVec(kl)*(pVec(l_0) + yVec(ulLF)  - yVec(lLF)) +
			              b_l *(          + yVec(dulLF) - yVec(dlLF));
		F_spring_lRF = pVec(kl)*(pVec(l_0) + yVec(ulRF)  - yVec(lRF)) +
			              b_l *(          + yVec(dulRF) - yVec(dlRF));
		F_spring_lRH = pVec(kl)*(pVec(l_0) + yVec(ulRH)  - yVec(lRH)) +
			              b_l *(          + yVec(dulRH) - yVec(dlRH));
		tau(qlLH) = F_spring_lLH + 0;
		tau(qlLF) = F_spring_lLF + 0;
		tau(qlRF) = F_spring_lRF + 0;
		tau(qlRH) = F_spring_lRH + 0;
	}
}

double PB_Dynamics::Logistic(double x, double sigma){
	double y = x/sigma;
	
	if (abs(y) > 50){ // In linear domain
		return max(0.0,x);
	} else { // In logistic domain
		return sigma*log(1+exp(y));
	}
}
