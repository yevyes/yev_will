clear
clc

% Possibility one: Lab Computer
PathToMuscod  = '/home/yevyes/MUSCOD-II/MC2/Release/bin/muscod';
PathTo2Dqrad   = '/home/yevyes/Yev_Will/GallopRigid/Quad_Model/';
PathToMatlab = '/home/yevyes/Yev_Will/GallopRigid/qradru_matlab/';

cd(PathToMatlab)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Gallopting
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Create intial datStruct with no constraints to converge on a
%gait
% datStruct = CreateDatStruct('Gallop','SEA','SEA');
% Make all changes to the datStruct directly below this line
load('datStruct_GallopInit.mat')
datStruct = datStruct_GallopInit;

% datStruct.p(4) = 0.7; %enforce average velocity
datStruct.p(5) = 15; %enforce friction cone

datStruct.p(11) = 1;
datStruct.p_name{11} = 'mu';
datStruct.p_comment{11} = 'friction';
datStruct.p_sca(11) = 1;
datStruct.p_min(11) = 0;
datStruct.p_max(11) = 100;
datStruct.p_fix(11) = 1; 


% Make all changes to the datStruct above this line
save('BaseData/Gallop_SH_SL_BASE_00.mat', 'datStruct');
CreateDatFile('BaseData/Gallop_SH_SL_BASE_00',datStruct); %Save datStruct to be run in Muscod

% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Gallop_*_BASE_00.dat ', PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_11(1), cmdout] = system([PathToMuscod,' Gallop_SH_SL_BASE_00']);
disp(MuscodResults_11(1))

cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Gallop_*_BASE_00.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

%%
% PlayMuscodResults('BaseData/Gallop_SH_SL_BASE_00');

%% Gallopting
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %Create intial datStruct with no constraints to converge on a
% %gait
% cd(PathToMatlab);
% datStruct = UpdateDatStruct(datStruct,'BaseData/Gallop_SH_SL_BASE_00', 30,41,8);
% 
% % datStruct.p(4) = 0.7; %enforce average velocity
% datStruct.p(5) = 15; %enforce friction cone
% % 7 11 15 19
% 
% % Make all changes to the datStruct above this line
% save('BaseData/Gallop_SH_SL_BASE_01.mat', 'datStruct');
% CreateDatFile('BaseData/Gallop_SH_SL_BASE_01',datStruct); %Save datStruct to be run in Muscod
% 
% % Copy to project director to process in Muscod
% currentPath = pwd;
% system(['cp ',pwd,'/BaseData/Gallop_*_BASE_01.dat ', PathTo2Dqrad,'DAT/']);
% 
% % SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
% cd(PathTo2Dqrad);
% [MuscodResults_11(1), cmdout] = system([PathToMuscod,' Gallop_SH_SL_BASE_01']);
% disp(MuscodResults_11(1))
% 
% cd(currentPath);
% % Copy back to BaseData/
% system(['cp ',PathTo2Dqrad,'RES/Gallop_*_BASE_01.* ',pwd,'/BaseData/']);
% % Clean up DAT and RES folders
% system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
% system(['rm ',PathTo2Dqrad,'RES/*.* ']);



%%
% PlotMuscodResults('BaseData/Gallop_SH_SL_BASE_00',[7,9]);
%%
% PlayMuscodResults('BaseData/Gallop_SH_SL_BASE_00');

%% Parameter Study for Gallopting
cd(PathToMatlab)
datStruct = UpdateDatStruct(datStruct,'BaseData/Gallop_SH_SL_BASE_00', 30,41,8);

% Make all changes to the datStruct above this line
% save('BaseData/Gallop_SH_SL_BASE_03.mat', 'datStruct');

minVel = 0.025;
maxVel = 4.0;
deltaVel = 0.025;
VelVec = minVel:deltaVel:maxVel;

CreateParameterStudy('ParameterStudies/Gallop_SH_SL_BASE_RigidFricCone_Velocity',datStruct,'p',4,VelVec);
[CostMatval,forces, states, time, hval] = ProcessParameterStudy_ANNOTATED('ParameterStudies/Gallop_SH_SL_BASE_RigidFricCone_Velocity', PathTo2Dqrad, PathToMuscod);
load('ParameterStudies/Gallop_SH_SL_BASE_RigidFricCone_Velocity');
save('GallopFlexFrictionCone_Rigid');
%% Plot Parameter Study For Gallopting

%load('ParameterStudies/Gallop_SH_SL_BASE_02_Velocity','configurations','types','indices','grids');
% count = zeros(length(grids{1}),1);
% figure(3);
% %clf
% hold on
% box on
% grid on
% x = [];
% y = [];
% for i = 1:length(configurations)
%     count(configurations{i}.indexVector(1))=count(configurations{i}.indexVector(1))+1;
%     if(configurations{i}.nFailed < 5)
%         if(configurations{i}.finalDatStruct.p(4) > 0.2 && (configurations{i}.costValue < 0.4));
%             %plot(grids{1}(configurations{i}.indexVector(1)),configurations{i}.costValue,'*r');
%             x = [x,grids{1}(configurations{i}.indexVector(1))];
%             y = [y,configurations{i}.costValue];
%         end
%       
%     end
% end
% z = [x;y];
% [tmp ind] = sort(z(1,:));
% x = z(1,ind);
% y = z(2,ind);
% plot(x,y,'--k');
% legend('Gallopting');
