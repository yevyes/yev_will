JRF = MatrixXXd::Zero();	double t242 = yVec(alphaRF)+yVec(phi);
	double t243 = cos(t242);
	double t244 = yVec(lRF)*t243;
	double t245 = sin(t242);
	double t246 = yVec(lRF)*t245;
	JRF(0,0) = 1.0;
	JRF(0,2) = pVec(rFoot)+t244-pVec(l1)*sin(yVec(phi));
	JRF(0,7) = pVec(rFoot)+t244;
	JRF(0,8) = t245;
	JRF(1,1) = 1.0;
	JRF(1,2) = t246+pVec(l1)*cos(yVec(phi));
	JRF(1,7) = t246;
	JRF(1,8) = -t243;
	