JRH = MatrixXXd::Zero();	double t248 = yVec(alphaRH)+yVec(phi);
	double t249 = cos(t248);
	double t250 = yVec(lRH)*t249;
	double t251 = sin(t248);
	double t252 = yVec(lRH)*t251;
	JRH(0,0) = 1.0;
	JRH(0,2) = pVec(rFoot)+t250+pVec(l1)*sin(yVec(phi));
	JRH(0,9) = pVec(rFoot)+t250;
	JRH(0,10) = t251;
	JRH(1,1) = 1.0;
	JRH(1,2) = t252-pVec(l1)*cos(yVec(phi));
	JRH(1,9) = t252;
	JRH(1,10) = -t249;
	