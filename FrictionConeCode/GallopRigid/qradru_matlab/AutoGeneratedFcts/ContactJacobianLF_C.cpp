JLF = MatrixXXd::Zero();	double t236 = yVec(alphaLF)+yVec(phi);
	double t237 = cos(t236);
	double t238 = yVec(lLF)*t237;
	double t239 = sin(t236);
	double t240 = yVec(lLF)*t239;
	JLF(0,0) = 1.0;
	JLF(0,2) = pVec(rFoot)+t238-pVec(l1)*sin(yVec(phi));
	JLF(0,5) = pVec(rFoot)+t238;
	JLF(0,6) = t239;
	JLF(1,1) = 1.0;
	JLF(1,2) = t240+pVec(l1)*cos(yVec(phi));
	JLF(1,5) = t240;
	JLF(1,6) = -t237;
	