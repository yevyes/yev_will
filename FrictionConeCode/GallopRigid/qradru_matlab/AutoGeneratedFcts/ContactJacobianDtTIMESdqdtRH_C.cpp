dJRHdtTIMESdqdt = MatrixXXd::Zero();	double t278 = yVec(alphaRH)+yVec(phi);
	double t279 = sin(t278);
	double t280 = cos(t278);
	double t281 = yVec(dalphaRH)*yVec(lRH)*t279;
	double t282 = yVec(dlRH)*t279;
	double t283 = yVec(dalphaRH)*yVec(lRH)*t280;
	double t284 = yVec(dalphaRH)+yVec(dphi);
	dJRHdtTIMESdqdt(0,0) = -yVec(dalphaRH)*(t281-yVec(dlRH)*t280+yVec(dphi)*yVec(lRH)*t279)-yVec(dphi)*(t281+yVec(dphi)*(yVec(lRH)*t279-pVec(l1)*cos(yVec(phi)))-yVec(dlRH)*t280)+yVec(dlRH)*t280*t284;
	dJRHdtTIMESdqdt(1,0) = yVec(dphi)*(t282+t283+yVec(dphi)*(yVec(lRH)*t280+pVec(l1)*sin(yVec(phi))))+yVec(dalphaRH)*(t282+t283+yVec(dphi)*yVec(lRH)*t280)+yVec(dlRH)*t279*t284;
	