%% Computer dependent settings:
% Possibility one: Lab Computer
PathToMuscod  = '/home/yevyes/MUSCOD-II/MC2/Release/bin/muscod';
PathTo2Dqrad   = '/home/yevyes/Yev_Will/Quad_Model/';
PathToMatlab = '/home/yevyes/Yev_Will/qradru_matlab/';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Trotting
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% STEP1
% This first set of datStructs and .dat files are the initial problem
% files. The initial parameters have been selected as simple as possible.
% Yet they will converge to solutions when used in muscod.
%  - No constraints are imposed at this stage.
%  - Transmission values are fixed.
cd(PathToMatlab);
datStruct = CreateDatStruct('Trot','SEA','SEA');
        
%datStruct Parameters: selecting the parameters that will mostly likley
%converge


save('BaseData/Trot_SH_SL_BASE_00.mat', 'datStruct');
CreateDatFile('BaseData/Trot_SH_SL_BASE_00',datStruct);

% Copy to project directory to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Trot_*_BASE_00.dat ', PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_11(1), cmdout] = system([PathToMuscod,' Trot_SH_SL_BASE_00']);
disp(MuscodResults_11);

cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Trot_*_BASE_00.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

%PlotMuscodResults('BaseData/Trot_SH_SL_BASE_00',[1:6]);
%PlotMuscodResults('BaseData/Trot_SH_SL_BASE_00',[10,14,18,22]);
%% STEP 2
% This first set of datStructs and .dat files are the initial problem
% files. The initial parameters have been selected as simple as possible.
% Yet they will converge to solutions when used in muscod.
%  - No constraints are imposed at this stage.
%  - Transmission values are fixed.
cd(PathToMatlab);
datStruct = UpdateDatStruct(datStruct,'BaseData/Trot_SH_SL_BASE_00', 30,41,8);
        
%datStruct Parameters: selecting the parameters that will mostly likley
%converge
datStruct.p(5) = 1;

save('BaseData/Trot_SH_SL_BASE_01.mat', 'datStruct');
CreateDatFile('BaseData/Trot_SH_SL_BASE_01',datStruct);

% Copy to project directory to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Trot_*_BASE_01.dat ', PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Trot_SH_SL_BASE_01']);
disp(MuscodResults_12);

cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Trot_*_BASE_01.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

%% STEP 3
load('BaseData/Trot_SH_SL_BASE_01.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/Trot_SH_SL_BASE_01', 30,41,8);
datStruct.header = {'// Updated from Trot_SH_SL_BASE_01'};

datStruct.p(5) = 7;

save('BaseData/Trot_SH_SL_BASE_02.mat', 'datStruct');
CreateDatFile('BaseData/Trot_SH_SL_BASE_02',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Trot_SH_SL_BASE_02.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_13(1), cmdout] = system([PathToMuscod,' Trot_SH_SL_BASE_02']);
disp(MuscodResults_13);

cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Trot_SH_SL_BASE_02.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

%% STEP 4
load('BaseData/Trot_SH_SL_BASE_03.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/Trot_SH_SL_BASE_03', 30,41,8);
datStruct.header = {'Updated from Trot_SH_SL_BASE02'};

datStruct.options_acc = 1e-6;
datStruct.p(5) = 7;


save('BaseData/Trot_SH_SL_BASE_03.mat', 'datStruct');
CreateDatFile('BaseData/Trot_SH_SL_BASE_03',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Trot_SH_SL_BASE_03.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_14(1), cmdout] = system([PathToMuscod,' Trot_SH_SL_BASE_03']);
disp(MuscodResults_14);

cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Trot_SH_SL_BASE_03.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);
%%
PlotMuscodResults('BaseData/Trot_SH_SL_BASE_03',[7,9]);
%%
PlayMuscodResults('BaseData/Trot_SH_SL_BASE_03');
%% Parameter Study for Trotting


cd(PathToMatlab)
load('BaseData/Trot_SH_SL_BASE_02.mat','datStruct');

minVel = 0.025;
maxVel = 3.0;
deltaVel = 0.025;
VelVec = minVel:deltaVel:maxVel;

CreateParameterStudy('ParameterStudies/Trot_SH_SL_BASE_02_Velocity',datStruct,'p',4,VelVec);
[CostMatval,forces, states, time, hval] = ProcessParameterStudy_ANNOTATED('ParameterStudies/Trot_SH_SL_BASE_02_Velocity', PathTo2Dqrad, PathToMuscod);
load('ParameterStudies/Trot_SH_SL_BASE_02_Velocity');
save('TrotWithStates');
%% Plot Parameter Study

load('ParameterStudies/Trot_SH_SL_BASE_02_Velocity','configurations','types','indices','grids');
count = zeros(length(grids{1}),1);
%clf
figure(3)
hold on
box on
grid on
x = [];
y = [];

for i = 1:length(configurations)
    count(configurations{i}.indexVector(1))=count(configurations{i}.indexVector(1))+1;
    if(configurations{i}.nFailed < 5)
        if(configurations{i}.finalDatStruct.p(4) > 0.2 && (configurations{i}.costValue < 0.4))
            %plot(grids{1}(configurations{i}.indexVector(1)),configurations{i}.costValue,'*r');
            x = [x,grids{1}(configurations{i}.indexVector(1))];
            y = [y,configurations{i}.costValue];
        end
    end
end
%disp(count);
z = [x;y];
[tmp ind] = sort(z(1,:));
x = z(1,ind);
y = z(2,ind);
plot(x,y,'-k');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Walking
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% STEP1
% This first set of datStructs and .dat files are the initial problem
% files. The initial parameters have been selected as simple as possible.
% Yet they will converge to solutions when used in muscod.
%  - No constraints are imposed at this stage.
%  - Transmission values are fixed.
cd(PathToMatlab);
datStruct = CreateDatStruct('Walk','SEA','SEA');
        

%datStruct Parameters: selecting the parameters that will mostly likley
%converge

save('BaseData/Walk_SH_SL_BASE_00.mat', 'datStruct');
CreateDatFile('BaseData/Walk_SH_SL_BASE_00',datStruct);

% Copy to project directory to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Walk_*_BASE_00.dat ', PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_11(1), cmdout] = system([PathToMuscod,' Walk_SH_SL_BASE_00']);
disp(MuscodResults_11);

cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Walk_*_BASE_00.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

%% STEP 2
cd(PathToMatlab);
datStruct = UpdateDatStruct(datStruct, 'BaseData/Walk_SH_SL_BASE_00', 30,41,8);
        
%datStruct Parameters: selecting the parameters that will mostly likley
%converge
datStruct.p(5) = 1;
datStruct.p(10) = 0.001;

save('BaseData/Walk_SH_SL_BASE_01.mat', 'datStruct');
CreateDatFile('BaseData/Walk_SH_SL_BASE_01',datStruct);

% Copy to project directory to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Walk_*_BASE_01.dat ', PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_11(1), cmdout] = system([PathToMuscod,' Walk_SH_SL_BASE_01']);
disp(MuscodResults_11);

cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Walk_*_BASE_01.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

%% STEP 3
cd(PathToMatlab);
datStruct = UpdateDatStruct(datStruct, 'BaseData/Walk_SH_SL_BASE_02', 30,41,8);
        
%datStruct Parameters: selecting the parameters that will mostly likley
%converge
datStruct.p(5) = 7;

save('BaseData/Walk_SH_SL_BASE_02.mat', 'datStruct');
CreateDatFile('BaseData/Walk_SH_SL_BASE_02',datStruct);

% Copy to project directory to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Walk_*_BASE_02.dat ', PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_11(1), cmdout] = system([PathToMuscod,' Walk_SH_SL_BASE_02']);
disp(MuscodResults_11);

cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Walk_*_BASE_02.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);
%%
PlotMuscodResults('BaseData/Walk_SH_SL_BASE_02',[7,9]);
%%
PlayMuscodResults('BaseData/Walk_SH_SL_BASE_02');

%% Parameter Study for Walking
cd(PathToMatlab)
load('BaseData/Walk_SH_SL_BASE_02.mat','datStruct');

minVel = 0.0;
maxVel = 2.0;
deltaVel = 0.025;
VelVec = minVel:deltaVel:maxVel;

CreateParameterStudy('ParameterStudies/Walk_SH_SL_BASE_02_Velocity',datStruct,'p',4,VelVec);
[CostMatval,forces, states, time,hval] = ProcessParameterStudy_ANNOTATED('ParameterStudies/Walk_SH_SL_BASE_02_Velocity', PathTo2Dqrad, PathToMuscod);
load('ParameterStudies/Walk_SH_SL_BASE_02_Velocity');
save('WalkWithStates');
%% Find Configurations
for i = 1:size(configurations)
    if(configurations{i}.finalDatStruct.p(4) == 1.1)
        i;
        nFailed = (configurations{i}.nFailed);
    end
end
%% Plot Parameter Study
load('ParameterStudies/Walk_SH_SL_BASE_02_Velocity','configurations','types','indices','grids');
count = zeros(length(grids{1}),1);
%clf
figure(3)
hold on
box on
grid on

x = [];
y = [];
for i = 1:length(configurations)
    count(configurations{i}.indexVector(1))=count(configurations{i}.indexVector(1))+1;
    if(configurations{i}.nFailed < 5)
        if(configurations{i}.finalDatStruct.p(4) > 0.2 && (configurations{i}.costValue < 0.4))
            x = [x,grids{1}(configurations{i}.indexVector(1))];
            y = [y,configurations{i}.costValue];
            %plot(grids{1}(configurations{i}.indexVector(1)),configurations{i}.costValue,'*b');
        end
    end
end
%disp(count);

z = [x;y];
[tmp ind] = sort(z(1,:));
x = z(1,ind);
y = z(2,ind);
plot(x,y,'-b');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Galloping
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Step 1
% This first set of datStructs and .dat files are the initial problem
% files. The initial parameters have been selected as simple as possible.
% Yet they will converge to solutions when used in muscod.
%  - No constraints are imposed at this stage.**
%  - Transmission values are fixed.
cd(PathToMatlab);
datStruct = CreateDatStruct('Gallop','SEA','SEA');
        
datStruct.p(4) = 1;


%datStruct Parameters: selecting the parameters that will mostly likley
%converge
save('BaseData/Gallop_SH_SL_BASE_00.mat', 'datStruct');
CreateDatFile('BaseData/Gallop_SH_SL_BASE_00',datStruct);

% Copy to project directory to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Gallop_*_BASE_00.dat ', PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_11(1), cmdout] = system([PathToMuscod,' Gallop_SH_SL_BASE_00']);
disp(MuscodResults_11);

cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Gallop_*_BASE_00.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

%% Step 2
cd(PathToMatlab);
datStruct = UpdateDatStruct(datStruct,'BaseData/Gallop_SH_SL_BASE_00',30,41,8);

datStruct.p(5) = 1; %Ground Clearance Constraint        

save('BaseData/Gallop_SH_SL_BASE_01.mat', 'datStruct');
CreateDatFile('BaseData/Gallop_SH_SL_BASE_01',datStruct);

% Copy to project directory to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Gallop_*_BASE_01.dat ', PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Gallop_SH_SL_BASE_01']);
disp(MuscodResults_12);

cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Gallop_*_BASE_01.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

%% Step 3
cd(PathToMatlab);
datStruct = UpdateDatStruct(datStruct,'BaseData/Gallop_SH_SL_BASE_01',30,41,8);

datStruct.p(5) = 7; %Enforce Ground and Motor Torque Constraints
        
save('BaseData/Gallop_SH_SL_BASE_02.mat', 'datStruct');
CreateDatFile('BaseData/Gallop_SH_SL_BASE_02',datStruct);

% Copy to project directory to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Gallop_*_BASE_02.dat ', PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_13(1), cmdout] = system([PathToMuscod,' Gallop_SH_SL_BASE_02']);
disp(MuscodResults_13);

cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Gallop_*_BASE_02.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

%% Plot state variables
PlotMuscodResults('BaseData/Gallop_SH_SL_BASE_02',[27 31]);

%% Play Muscod Results
PlayMuscodResults('BaseData/Gallop_SH_SL_BASE_02');

%% Parameter Study for Galloping
cd(PathToMatlab)
load('BaseData/Gallop_SH_SL_BASE_02.mat','datStruct');

minVel = 0.025;
maxVel = 5.0;
deltaVel = 0.025;
VelVec = minVel:deltaVel:maxVel;

CreateParameterStudy('ParameterStudies/Gallop_SH_SL_BASE_02_Velocity',datStruct,'p',4,VelVec);
[CostMatval,forces, states, time,hval] = ProcessParameterStudy_ANNOTATED('ParameterStudies/Gallop_SH_SL_BASE_02_Velocity', PathTo2Dqrad, PathToMuscod);
load('ParameterStudies/Gallop_SH_SL_BASE_02_Velocity');
save('GallopWithStates');
%% Plot Parameter Study

load('ParameterStudies/Gallop_SH_SL_BASE_02_Velocity','configurations','types','indices','grids');
count = zeros(length(grids{1}),1);
%clf
figure(3)
hold on
box on
grid on

x = [];
y = [];
for i = 1:length(configurations)
    count(configurations{i}.indexVector(1))=count(configurations{i}.indexVector(1))+1;
    if(configurations{i}.nFailed < 5)
        if(configurations{i}.finalDatStruct.p(4) > 0.2 && (configurations{i}.costValue < 0.4))
            x = [x,grids{1}(configurations{i}.indexVector(1))];
            y = [y,configurations{i}.costValue];
%           plot(grids{1}(configurations{i}.indexVector(1)),configurations{i}.costValue,'*g');
        end
    end
end
%disp(count);

z = [x;y];
[tmp ind] = sort(z(1,:));
x = z(1,ind);
y = z(2,ind);
plot(x,y,'-g');
%% Play Muscod Result at Parameter Study Point
Veloc = 2.0;
SavedStatesFileName = 'GallopWithStates.mat';
PlayMuscodResults_ParamStudyPoint(SavedStatesFileName, Veloc)

