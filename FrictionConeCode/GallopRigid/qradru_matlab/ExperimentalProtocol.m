%% Computer dependent settings:
% Possibility one: Lab Computer
PathToMuscod  = '/home/yevyes/MUSCOD-II/MC2/Release/bin/muscod';
PathTo2Dqrad   = '/home/yevyes/yev_will/qradru_model/';


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Trotting
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

mkdir('BaseData');
datStruct = CreateDatStruct('Trot','SEA','SEA');
        
save('BaseData/Trot_SH_SL_BASE_00.mat', 'datStruct');
CreateDatFile('BaseData/Trot_SH_SL_BASE_00',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Trot_*_BASE_00.dat ', PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);

[MuscodResults_11(1), cmdout] = system([PathToMuscod,' Trot_SH_SL_BASE_00']);
cd(currentPath);

% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Trot_*_BASE_00.* ',pwd,'/BaseData/']);

PlotMuscodResults('BaseData/Trot_SH_SL_BASE_00',[1:6]);
PlotMuscodResults('BaseData/Trot_SH_SL_BASE_00',[10,14,18,22]);
%%

PlayMuscodResults('BaseData/Trot_SH_SL_BASE_00');
%%
load('BaseData/Trot_SH_SL_BASE_00.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/Trot_SH_SL_BASE_00', 30,41,8);
datStruct.header = {'// Updated from Trot_SH_SL_BASE_00'};
datStruct.p(5) = 1;
datStruct.p(4) = 0.7;
save('BaseData/Trot_SH_SL_BASE_01.mat', 'datStruct');
CreateDatFile('BaseData/Trot_SH_SL_BASE_01',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Trot_SH_SL_BASE_01.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Trot_SH_SL_BASE_01']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Trot_SH_SL_BASE_01.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

PlotMuscodResults('BaseData/Trot_SH_SL_BASE_01',[1:6]);

%%

PlayMuscodResults('BaseData/Trot_SH_SL_BASE_01');

%%
load('BaseData/Trot_SH_SL_BASE_01.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/Trot_SH_SL_BASE_01', 30,41,8);
datStruct.header = {'// Updated from Trot_SH_SL_BASE_01'};
datStruct.p(5) = 7;
datStruct.p(4) = 0.7;
datStruct.sd_fix_init(5) = 0;
save('BaseData/Trot_SH_SL_BASE_02.mat', 'datStruct');
CreateDatFile('BaseData/Trot_SH_SL_BASE_02',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Trot_SH_SL_BASE_02.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Trot_SH_SL_BASE_02']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Trot_SH_SL_BASE_02.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

PlotMuscodResults('BaseData/Trot_SH_SL_BASE_02',[1:6]);

%%

PlayMuscodResults('BaseData/Trot_SH_SL_BASE_02');
%%
load('BaseData/Trot_SH_SL_BASE_03.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/Trot_SH_SL_BASE_03', 30,41,8);
datStruct.header = {'// Updated from Trot_SH_SL_BASE_03'};
datStruct.p(5) = 7;
datStruct.p(4) = 0.6;
datStruct.sd_fix_init(5) = 0;
save('BaseData/Trot_SH_SL_BASE_04.mat', 'datStruct');
CreateDatFile('BaseData/Trot_SH_SL_BASE_04',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Trot_SH_SL_BASE_04.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Trot_SH_SL_BASE_04']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Trot_SH_SL_BASE_04.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

%PlotMuscodResults('BaseData/Trot_SH_SL_BASE_04',[1:6]);
%%

save('BaseData/Trot_SH_SL_BASE_04.mat','datStruct');
load('BaseData/Trot_SH_SL_BASE_04.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/Trot_SH_SL_BASE_04', 30,41,8);

%CreateParameterStudy('ParameterStudies/VelocityStudyTrotnew', datStruct, 'p',4,linspace(0.2,2.0,37)) ;
ProcessParameterStudy('ParameterStudies/VelocityStudyTrotnew', PathTo2Dqrad, 'muscod');

%%

load('ParameterStudies/VelocityStudyTrotnew','configurations','types','indices','grids');
count = zeros(length(grids{1}),1);
%clf
figure(3)
hold on
box on
grid on
for i = 1:length(configurations)
    count(configurations{i}.indexVector(1))=count(configurations{i}.indexVector(1))+1;
    plot(grids{1}(configurations{i}.indexVector(1)),configurations{i}.costValue,'*k');
end
disp(count);

%%
load('ParameterStudies/VelocityStudyTrot2','configurations','types','indices','grids');
count = zeros(length(grids{1}),1);
%clf
figure(4)
hold on
box on
grid on
for i = 1:length(configurations)
    count(configurations{i}.indexVector(1))=count(configurations{i}.indexVector(1))+1;
    plot(grids{1}(configurations{i}.indexVector(1)),configurations{i}.finalDatStruct.h(1),'*');
end
disp(count);
%%

load('ParameterStudies/VelocityStudyGallop2','configurations','types','indices','grids');
for i = 1:length(configurations)
result(i,1) = grids{1}(configurations{i}.indexVector(1));
result(i,2) = configurations{i}.costValue;
end

[~,IX] = sort(result(:,1));
for i = 1:length(configurations)
result2(i,1) = result(IX(i),1);
result2(i,2) = result(IX(i),2);
end

%%

figure(4)
plot(result2(:,1),result2(:,2));
grid on


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% WalkTrotting
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

mkdir('BaseData');
datStruct = CreateDatStruct('WalkTrot','SEA','SEA');
datStruct.sd_fix_init(5) = 0;
        
save('BaseData/WalkTrot_SH_SL_BASE_00.mat', 'datStruct');
CreateDatFile('BaseData/WalkTrot_SH_SL_BASE_00',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/WalkTrot_*_BASE_00.dat ', PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);

[MuscodResults_11(1), cmdout] = system([PathToMuscod,' WalkTrot_SH_SL_BASE_00']);
cd(currentPath);

% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/WalkTrot_*_BASE_00.* ',pwd,'/BaseData/']);

PlotMuscodResults('BaseData/WalkTrot_SH_SL_BASE_00',[1:6]);
%%

PlayMuscodResults('BaseData/WalkTrot_SH_SL_BASE_00');
%%
load('BaseData/WalkTrot_SH_SL_BASE_00.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/WalkTrot_SH_SL_BASE_00', 30,41,8);
datStruct.header = {'// Updated from WalkTrot_SH_SL_BASE_00'};
datStruct.p(5) = 7;
datStruct.p(4) = 0.1;
datStruct.sd_fix_init(5) = 0;
save('BaseData/WalkTrot_SH_SL_BASE_01.mat', 'datStruct');
CreateDatFile('BaseData/WalkTrot_SH_SL_BASE_01',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/WalkTrot_SH_SL_BASE_01.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' WalkTrot_SH_SL_BASE_01']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/WalkTrot_SH_SL_BASE_01.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

PlotMuscodResults('BaseData/WalkTrot_SH_SL_BASE_01',[1:6]);

%%
save('BaseData/WalkTrot_SH_SL_BASE_01.mat','datStruct');
load('BaseData/WalkTrot_SH_SL_BASE_01.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/WalkTrot_SH_SL_BASE_01', 30,41,8);

%CreateParameterStudy('ParameterStudies/VelocityStudyWalkTrotnew', datStruct, 'p',4,linspace(0.1,0.8,15)) ;
ProcessParameterStudy('ParameterStudies/VelocityStudyWalkTrotnew', PathTo2Dqrad, 'muscod');

%%

load('ParameterStudies/VelocityStudyWalkTrotnew','configurations','types','indices','grids');
count = zeros(length(grids{1}),1);
%clf
figure(3)
hold on
box on
grid on
for i = 1:length(configurations)
    count(configurations{i}.indexVector(1))=count(configurations{i}.indexVector(1))+1;
    plot(grids{1}(configurations{i}.indexVector(1)),configurations{i}.costValue,'*');
end
disp(count);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Bounding
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

mkdir('BaseData');
datStruct = CreateDatStruct('Bound','SEA','SEA');
        
save('BaseData/Bound_SH_SL_BASE_00.mat', 'datStruct');
CreateDatFile('BaseData/Bound_SH_SL_BASE_00',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Bound_SH_SL_BASE_00.dat ', PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);

[MuscodResults_11(1), cmdout] = system([PathToMuscod,' Bound_SH_SL_BASE_00']);
cd(currentPath);

% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Bound_SH_SL_BASE_00.* ',pwd,'/BaseData/']);

PlotMuscodResults('BaseData/Bound_SH_SL_BASE_00',[1:6]);
PlotMuscodResults('BaseData/Bound_SH_SL_BASE_00',[10,14,18,22]);
%%

PlayMuscodResults('BaseData/Bound_SH_SL_BASE_00');



%%
load('BaseData/Bound_SH_SL_BASE_00.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/Bound_SH_SL_BASE_00', 30,41,8);
datStruct.header = {'// Updated from Bound_SH_SL_BASE_00'};
datStruct.p(5) = 1;
datStruct.p(4) = 0.6;
datStruct.sd_fix_init(5) = 0;
save('BaseData/Bound_SH_SL_BASE_01.mat', 'datStruct');
CreateDatFile('BaseData/Bound_SH_SL_BASE_01',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Bound_SH_SL_BASE_01.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Bound_SH_SL_BASE_01']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Bound_SH_SL_BASE_01.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

PlotMuscodResults('BaseData/Bound_SH_SL_BASE_01',[1:6]);

%%

PlayMuscodResults('BaseData/Bound_SH_SL_BASE_01');

%%
load('BaseData/Bound_SH_SL_BASE_02.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/Bound_SH_SL_BASE_02', 30,41,8);
datStruct.header = {'// Updated from Bound_SH_SL_BASE_02'};
datStruct.p(5) = 7;
datStruct.p(4) = 0.6;
save('BaseData/Bound_SH_SL_BASE_03.mat', 'datStruct');
CreateDatFile('BaseData/Bound_SH_SL_BASE_03',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Bound_SH_SL_BASE_03.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Bound_SH_SL_BASE_03']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Bound_SH_SL_BASE_03.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

%PlotMuscodResults('BaseData/Bound_SH_SL_BASE_02',[1:6]);



%PlayMuscodResults('BaseData/Bound_SH_SL_BASE_02');




load('BaseData/Bound_SH_SL_BASE_03.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/Bound_SH_SL_BASE_03', 30,41,8);

CreateParameterStudy('ParameterStudies/VelocityStudyBoundnew', datStruct, 'p',4,linspace(0.2,1.6,29)) ;
ProcessParameterStudy('ParameterStudies/VelocityStudyBoundnew', PathTo2Dqrad, 'muscod');

%%

load('ParameterStudies/VelocityStudyBound','configurations','types','indices','grids');
count = zeros(length(grids{1}),1);
%clf
figure(3)
hold on
box on
grid on
for i = 1:length(configurations)
    count(configurations{i}.indexVector(1))=count(configurations{i}.indexVector(1))+1;
    plot(grids{1}(configurations{i}.indexVector(1)),configurations{i}.costValue,'*');
end
disp(count);
%%
datStruct = configurations{1,45}.startDatStruct;
datStruct = UpdateDatStruct(datStruct, 'BaseData/Bound_SH_SL_BASE_03', 30,41,8);

datStruct.options_itmax         = 5;
datStruct.p(4) = 1;
save('BaseData/Bound_SH_SL_BASE_04.mat', 'datStruct');
CreateDatFile('BaseData/Bound_SH_SL_BASE_04',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Bound_SH_SL_BASE_04.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Bound_SH_SL_BASE_04']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Bound_SH_SL_BASE_04.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

PlotMuscodResults('BaseData/Bound_SH_SL_BASE_04',[1:6]);
%%
PlayMuscodResults('BaseData/Bound_SH_SL_BASE_03');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Walking
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

mkdir('BaseData');
datStruct = CreateDatStruct('Walk','SEA','SEA');
        
save('BaseData/Walk_SH_SL_BASE_00.mat', 'datStruct');
CreateDatFile('BaseData/Walk_SH_SL_BASE_00',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Walk_SH_SL_BASE_00.dat ', PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);

[MuscodResults_11(1), cmdout] = system([PathToMuscod,' Walk_SH_SL_BASE_00']);
cd(currentPath);

% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Walk_SH_SL_BASE_00.* ',pwd,'/BaseData/']);

PlotMuscodResults('BaseData/Walk_SH_SL_BASE_00',[1:6]);
PlotMuscodResults('BaseData/Walk_SH_SL_BASE_00',[10,14,18,22]);
%%

PlayMuscodResults('BaseData/Walk_SH_SL_BASE_00');

%%
load('BaseData/Walk_SH_SL_BASE_00.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/Walk_SH_SL_BASE_00', 30,41,8);
datStruct.header = {'// Updated from Walk_SH_SL_BASE_00'};
datStruct.p(5) = 1;
datStruct.p(4) = 0.3;
datStruct.sd_fix_init(5) = 0;
save('BaseData/Walk_SH_SL_BASE_01.mat', 'datStruct');
CreateDatFile('BaseData/Walk_SH_SL_BASE_01',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Walk_SH_SL_BASE_01.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Walk_SH_SL_BASE_01']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Walk_SH_SL_BASE_01.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

PlotMuscodResults('BaseData/Walk_SH_SL_BASE_01',[1:6]);

%%

PlayMuscodResults('BaseData/Walk_SH_SL_BASE_01');
%%
load('BaseData/Walk_SH_SL_BASE_01.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/Walk_SH_SL_BASE_01', 30,41,8);
datStruct.header = {'// Updated from Walk_SH_SL_BASE_01'};
datStruct.p(5) = 7;
datStruct.p(4) = 0.35;
datStruct.sd_fix_init(5) = 0;
save('BaseData/Walk_SH_SL_BASE_02.mat', 'datStruct');
CreateDatFile('BaseData/Walk_SH_SL_BASE_02',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Walk_SH_SL_BASE_02.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Walk_SH_SL_BASE_02']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Walk_SH_SL_BASE_02.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

PlotMuscodResults('BaseData/Walk_SH_SL_BASE_02',[1:6]);

%%

PlayMuscodResults('BaseData/Walk_SH_SL_BASE_01');
%%
load('BaseData/Walk_SH_SL_BASE_02.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/Walk_SH_SL_BASE_02', 30,41,8);
datStruct.header = {'// Updated from Walk_SH_SL_BASE_02'};
datStruct.p(5) = 7;
datStruct.p(4) = 0.3;
datStruct.sd_fix_init(5) = 0;
save('BaseData/Walk_SH_SL_BASE_03.mat', 'datStruct');
CreateDatFile('BaseData/Walk_SH_SL_BASE_03',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Walk_SH_SL_BASE_03.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Walk_SH_SL_BASE_03']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Walk_SH_SL_BASE_03.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

PlotMuscodResults('BaseData/Walk_SH_SL_BASE_03',[1:6]);

%%

PlayMuscodResults('BaseData/Walk_SH_SL_BASE_03');
%%
load('BaseData/Walk_SH_SL_BASE_03.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/Walk_SH_SL_BASE_03', 30,41,8);
datStruct.header = {'// Updated from Walk_SH_SL_BASE_03'};
datStruct.p(5) = 7;
datStruct.p(4) = 0.3;
datStruct.p(3) = 2;
datStruct.sd_fix_init(5) = 0;
save('BaseData/Walk_SH_SL_BASE_04.mat', 'datStruct');
CreateDatFile('BaseData/Walk_SH_SL_BASE_04',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Walk_SH_SL_BASE_04.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Walk_SH_SL_BASE_04']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Walk_SH_SL_BASE_04.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

PlotMuscodResults('BaseData/Walk_SH_SL_BASE_04',[1:6]);

%%

PlayMuscodResults('BaseData/Walk_SH_SL_BASE_04');
%%
load('BaseData/Walk_SH_SL_BASE_04.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/Walk_SH_SL_BASE_04', 30,41,8);
datStruct.header = {'// Updated from Walk_SH_SL_BASE_04'};
datStruct.p(5) = 7;
datStruct.p(4) = 0.3;
datStruct.p(3) = 1;
datStruct.sd_fix_init(5) = 0;
datStruct.options_itmax         = 300;
save('BaseData/Walk_SH_SL_BASE_05.mat', 'datStruct');
CreateDatFile('BaseData/Walk_SH_SL_BASE_05',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Walk_SH_SL_BASE_05.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Walk_SH_SL_BASE_05']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Walk_SH_SL_BASE_05.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);


%%

load('BaseData/Walk_SH_SL_BASE_05.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/Walk_SH_SL_BASE_05', 30,41,8);

%CreateParameterStudy('ParameterStudies/VelocityStudyWalknew', datStruct, 'p',4,linspace(0.1,1.1,21)) ;
ProcessParameterStudy('ParameterStudies/VelocityStudyWalknew', PathTo2Dqrad, 'muscod');

%%

load('ParameterStudies/VelocityStudyWalknew','configurations','types','indices','grids');
count = zeros(length(grids{1}),1);
%clf
figure(3)
hold on
box on
grid on
for i = 1:length(configurations)
    count(configurations{i}.indexVector(1))=count(configurations{i}.indexVector(1))+1;
    plot(grids{1}(configurations{i}.indexVector(1)),configurations{i}.costValue,'*r');
end
disp(count);
%%

datStruct = configurations{1,41}.startDatStruct;


CreateParameterStudy('ParameterStudies/VelocityStudyWalk3', datStruct, 'p',4,linspace(0.6,1,17)) ;
ProcessParameterStudy('ParameterStudies/VelocityStudyWalk3', PathTo2Dqrad, 'muscod');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Gallopping
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

mkdir('BaseData');
datStruct = CreateDatStruct('Gallop','SEA','SEA');
        
save('BaseData/Gallop_SH_SL_BASE_00.mat', 'datStruct');
CreateDatFile('BaseData/Gallop_SH_SL_BASE_00',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Gallop_SH_SL_BASE_00.dat ', PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);

[MuscodResults_11(1), cmdout] = system([PathToMuscod,' Gallop_SH_SL_BASE_00']);
cd(currentPath);

% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Gallop_SH_SL_BASE_00.* ',pwd,'/BaseData/']);

PlotMuscodResults('BaseData/Gallop_SH_SL_BASE_00',[1:6]);
PlotMuscodResults('BaseData/Gallop_SH_SL_BASE_00',[10,14,18,22]);
%%

PlayMuscodResults('BaseData/Gallop_SH_SL_BASE_00');
%%
load('BaseData/Gallop_SH_SL_BASE_00.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/Gallop_SH_SL_BASE_00', 30,41,8);
datStruct.header = {'// Updated from Gallop_SH_SL_BASE_00'};
datStruct.p(5) = 7;
datStruct.p(4) = 0.3;
datStruct.sd_fix_init(5) = 0;
save('BaseData/Gallop_SH_SL_BASE_01.mat', 'datStruct');
CreateDatFile('BaseData/Gallop_SH_SL_BASE_01',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Gallop_SH_SL_BASE_01.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Gallop_SH_SL_BASE_01']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Gallop_SH_SL_BASE_01.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

PlotMuscodResults('BaseData/Gallop_SH_SL_BASE_01',[1:6]);

%%

PlayMuscodResults('BaseData/Gallop_SH_SL_BASE_01');

%%


load('BaseData/Gallop_SH_SL_BASE_01.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/Gallop_SH_SL_BASE_01', 30,41,8);
datStruct.header = {'// Updated from Gallop_SH_SL_BASE_01'};
datStruct.p(5) = 7;
datStruct.p(4) = 0.7;
save('BaseData/Gallop_SH_SL_BASE_02.mat', 'datStruct');
CreateDatFile('BaseData/Gallop_SH_SL_BASE_02',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Gallop_SH_SL_BASE_02.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Gallop_SH_SL_BASE_02']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Gallop_SH_SL_BASE_02.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

PlotMuscodResults('BaseData/Gallop_SH_SL_BASE_02',[1:6]);

%%

PlayMuscodResults('BaseData/Gallop_SH_SL_BASE_02');

%%


load('BaseData/Gallop_SH_SL_BASE_02.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/Gallop_SH_SL_BASE_02', 30,41,8);
datStruct.header = {'// Updated from Gallop_SH_SL_BASE_02'};
datStruct.p(5) = 7;
datStruct.p(4) = 1;
save('BaseData/Gallop_SH_SL_BASE_03.mat', 'datStruct');
CreateDatFile('BaseData/Gallop_SH_SL_BASE_03',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Gallop_SH_SL_BASE_03.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Gallop_SH_SL_BASE_03']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Gallop_SH_SL_BASE_03.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

PlotMuscodResults('BaseData/Gallop_SH_SL_BASE_03',[1:6]);

%%

PlayMuscodResults('BaseData/Gallop_SH_SL_BASE_03');

%%


load('BaseData/Gallop_SH_SL_BASE_03.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/Gallop_SH_SL_BASE_03', 30,41,8);
datStruct.header = {'// Updated from Gallop_SH_SL_BASE_03'};
datStruct.p(5) = 7;
datStruct.p(4) = 1.3;
datStruct.h_min  = [0.2;0.0;0.0001;0.0;0.0001;0.0001;0.0;0.0001;0.0001;0.0;0.0001;0.0001];
save('BaseData/Gallop_SH_SL_BASE_04.mat', 'datStruct');
CreateDatFile('BaseData/Gallop_SH_SL_BASE_04',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Gallop_SH_SL_BASE_04.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Gallop_SH_SL_BASE_04']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Gallop_SH_SL_BASE_04.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

PlotMuscodResults('BaseData/Gallop_SH_SL_BASE_04',[1:6]);

%%

PlayMuscodResults('BaseData/Gallop_SH_SL_BASE_04');
%%


load('BaseData/Gallop_SH_SL_BASE_04.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/Gallop_SH_SL_BASE_04', 30,41,8);
datStruct.header = {'// Updated from Gallop_SH_SL_BASE_04'};
datStruct.p(5) = 7;
datStruct.p(4) = 1.6;
datStruct.h_min  = [0.2;0.0;0.0001;0.0;0.0001;0.0001;0.0;0.0001;0.0001;0.0;0.0001;0.0001];
save('BaseData/Gallop_SH_SL_BASE_05.mat', 'datStruct');
CreateDatFile('BaseData/Gallop_SH_SL_BASE_05',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Gallop_SH_SL_BASE_05.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Gallop_SH_SL_BASE_05']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Gallop_SH_SL_BASE_05.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

PlotMuscodResults('BaseData/Gallop_SH_SL_BASE_05',[1:6]);

%%

PlayMuscodResults('BaseData/Gallop_SH_SL_BASE_05');
%%


load('BaseData/Gallop_SH_SL_BASE_05.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/Gallop_SH_SL_BASE_05', 30,41,8);
datStruct.header = {'// Updated from Gallop_SH_SL_BASE_05'};
datStruct.p(5) = 7;
datStruct.p(4) = 1.6;
datStruct.h_min  = [0.0001;0.0;0.0001;0.0;0.0001;0.0001;0.0;0.0001;0.0001;0.0;0.0001;0.0001];
save('BaseData/Gallop_SH_SL_BASE_06.mat', 'datStruct');
CreateDatFile('BaseData/Gallop_SH_SL_BASE_06',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Gallop_SH_SL_BASE_06.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Gallop_SH_SL_BASE_06']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Gallop_SH_SL_BASE_06.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

PlotMuscodResults('BaseData/Gallop_SH_SL_BASE_06',[1:6]);

%%

PlayMuscodResults('BaseData/Gallop_SH_SL_BASE_06');

%%


load('BaseData/Gallop_SH_SL_BASE_06.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/Gallop_SH_SL_BASE_06', 30,41,8);
datStruct.header = {'// Updated from Gallop_SH_SL_BASE_06'};
datStruct.p(5) = 7;
datStruct.p(4) = 1.6;
datStruct.sd_min(5) = -0.1;
datStruct.sd_max(5) = 0.1;
save('BaseData/Gallop_SH_SL_BASE_07.mat', 'datStruct');
CreateDatFile('BaseData/Gallop_SH_SL_BASE_07',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Gallop_SH_SL_BASE_07.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Gallop_SH_SL_BASE_07']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Gallop_SH_SL_BASE_07.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

PlotMuscodResults('BaseData/Gallop_SH_SL_BASE_07',[1:6]);

%%

PlayMuscodResults('BaseData/Gallop_SH_SL_BASE_07');

%%


load('BaseData/Gallop_SH_SL_BASE_07.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/Gallop_SH_SL_BASE_07', 30,41,8);
datStruct.header = {'// Updated from Gallop_SH_SL_BASE_07'};
datStruct.p(5) = 7;
datStruct.p(4) = 1.3;
datStruct.sd_min(5) = -0.1;
datStruct.sd_max(5) = 0.1;
save('BaseData/Gallop_SH_SL_BASE_08.mat', 'datStruct');
CreateDatFile('BaseData/Gallop_SH_SL_BASE_08',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Gallop_SH_SL_BASE_08.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Gallop_SH_SL_BASE_08']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Gallop_SH_SL_BASE_08.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

PlotMuscodResults('BaseData/Gallop_SH_SL_BASE_08',[1:6]);

%%

PlayMuscodResults('BaseData/Gallop_SH_SL_BASE_08');
%%


load('BaseData/Gallop_SH_SL_BASE_08.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/Gallop_SH_SL_BASE_08', 30,41,8);
datStruct.header = {'// Updated from Gallop_SH_SL_BASE_08'};
datStruct.p(5) = 7;
datStruct.p(4) = 1.3;
datStruct.sd_max(3) = 1.15;
datStruct.sd_min(5) = -0.07;
datStruct.sd_max(5) = 0.07;
save('BaseData/Gallop_SH_SL_BASE_09.mat', 'datStruct');
CreateDatFile('BaseData/Gallop_SH_SL_BASE_09',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Gallop_SH_SL_BASE_09.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Gallop_SH_SL_BASE_09']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Gallop_SH_SL_BASE_09.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

PlotMuscodResults('BaseData/Gallop_SH_SL_BASE_09',[1:6]);

%%

PlayMuscodResults('BaseData/Gallop_SH_SL_BASE_09');

%%


load('BaseData/Gallop_SH_SL_BASE_09.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/Gallop_SH_SL_BASE_09', 30,41,8);
datStruct.header = {'// Updated from Gallop_SH_SL_BASE_09'};
datStruct.p(5) = 7;
datStruct.p(4) = 1.0;
datStruct.sd_max(3) = 1.15;
datStruct.sd_min(5) = -0.07;
datStruct.sd_max(5) = 0.07;
save('BaseData/Gallop_SH_SL_BASE_10.mat', 'datStruct');
CreateDatFile('BaseData/Gallop_SH_SL_BASE_10',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Gallop_SH_SL_BASE_10.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Gallop_SH_SL_BASE_10']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Gallop_SH_SL_BASE_10.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

PlotMuscodResults('BaseData/Gallop_SH_SL_BASE_10',[1:6]);

%%

PlayMuscodResults('BaseData/Gallop_SH_SL_BASE_10');
%%


load('BaseData/Gallop_SH_SL_BASE_10.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/Gallop_SH_SL_BASE_10', 30,41,8);
datStruct.header = {'// Updated from Gallop_SH_SL_BASE_10'};
datStruct.p(5) = 7;
datStruct.p(4) = 1.0;
datStruct.sd_max(3) = 1.3;
datStruct.sd_min(5) = -0.2;
datStruct.sd_max(5) = 0.2;
save('BaseData/Gallop_SH_SL_BASE_11.mat', 'datStruct');
CreateDatFile('BaseData/Gallop_SH_SL_BASE_11',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Gallop_SH_SL_BASE_11.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Gallop_SH_SL_BASE_11']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Gallop_SH_SL_BASE_11.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

PlotMuscodResults('BaseData/Gallop_SH_SL_BASE_11',[1:6]);

%%

PlayMuscodResults('BaseData/Gallop_SH_SL_BASE_11');

%%

load('BaseData/Gallop_SH_SL_BASE_11.mat','datStruct');
datStruct.options_itmax         = 250;
datStruct = UpdateDatStruct(datStruct, 'BaseData/Gallop_SH_SL_BASE_11', 30,41,8);

%CreateParameterStudy('ParameterStudies/VelocityStudyGallop2', datStruct, 'p',4,linspace(0.5,1.5,21)) ;
ProcessParameterStudy('ParameterStudies/VelocityStudyGallop2', PathTo2Dqrad, 'muscod');

%%

load('ParameterStudies/VelocityStudyGallop2','configurations','types','indices','grids');
count = zeros(length(grids{1}),1);
%clf
figure(3)
hold on
box on
grid on
for i = 1:length(configurations)
    count(configurations{i}.indexVector(1))=count(configurations{i}.indexVector(1))+1;
    plot(grids{1}(configurations{i}.indexVector(1)),configurations{i}.costValue,'*');
end
disp(count);

%%
datStruct = configurations{1,78}.startDatStruct;

datStruct.options_itmax         = 20;
save('BaseData/Gallop_SH_SL_BASE_14.mat', 'datStruct');
CreateDatFile('BaseData/Gallop_SH_SL_BASE_14',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Gallop_SH_SL_BASE_14.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Gallop_SH_SL_BASE_14']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Gallop_SH_SL_BASE_14.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

PlotMuscodResults('BaseData/Gallop_SH_SL_BASE_14',[1:6]);
%%
PlayMuscodResults('BaseData/Gallop_SH_SL_BASE_14');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%   GalloppingTriple
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

load('BaseData/Gallop_SH_SL_BASE_10.mat','datStruct');

datStruct.h  = [0.2;0;0.2;0;0.2;0;0.2;0.2;0;0.2;0.2;0.2];
datStruct.nshoot = [3;1;3;1;3;1;3;3;1;3;3;3];
datStruct.h_sca  = [1;0;1;0;1;0;1;1;0;1;1;1];
datStruct.h_min  = [0.0001;0.0;0.0001;0.0;0.0001;0.0;0.0001;0.0001;0.0;0.0001;0.0001;0.0001];
datStruct.h_max  = [5.0;0.0;5.0;0.0;5.0;0.0;5.0;5.0;0.0;5.0;5.0;5.0];
datStruct.h_fix  = [0;1;0;1;0;1;0;0;1;0;0;0];

datStruct.p(5) = 0;
datStruct.p(4) = 1.0;

save('BaseData/GallopTriple_SH_SL_BASE_00.mat', 'datStruct');
CreateDatFile('BaseData/GallopTriple_SH_SL_BASE_00',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/GallopTriple_SH_SL_BASE_00.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' GallopTriple_SH_SL_BASE_00']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/GallopTriple_SH_SL_BASE_00.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

PlotMuscodResults('BaseData/GallopTriple_SH_SL_BASE_00',[1:6]);

%%

PlayMuscodResults('BaseData/GallopTriple_SH_SL_BASE_00');


%%


load('BaseData/GallopTriple_SH_SL_BASE_00.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/GallopTriple_SH_SL_BASE_00', 30,41,8);
datStruct.header = {'// Updated from GallopTriple_SH_SL_BASE_00'};
datStruct.p(5) = 7;
datStruct.p(4) = 1.0;
datStruct.sd_max(3) = 1.5;
datStruct.sd_min(5) = -0.3;
datStruct.sd_max(5) = 0.3;
save('BaseData/GallopTriple_SH_SL_BASE_01.mat', 'datStruct');
CreateDatFile('BaseData/GallopTriple_SH_SL_BASE_01',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/GallopTriple_SH_SL_BASE_01.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' GallopTriple_SH_SL_BASE_01']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/GallopTriple_SH_SL_BASE_01.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

PlotMuscodResults('BaseData/GallopTriple_SH_SL_BASE_01',[1:6]);

%%

PlayMuscodResults('BaseData/GallopTriple_SH_SL_BASE_01');

%%


load('BaseData/GallopTriple_SH_SL_BASE_01.mat','datStruct');
datStruct.options_itmax         = 300;
datStruct = UpdateDatStruct(datStruct, 'BaseData/GallopTriple_SH_SL_BASE_01', 30,41,8);

CreateParameterStudy('ParameterStudies/VelocityStudyGallopTri', datStruct, 'p',4,linspace(0.5,1.5,41)) ;
ProcessParameterStudy('ParameterStudies/VelocityStudyGallopTri', PathTo2Dqrad, 'muscod');


%%

load('ParameterStudies/VelocityStudyGallopTri','configurations','types','indices','grids');
count = zeros(length(grids{1}),1);
%clf
figure(3)
hold on
box on
grid on
for i = 1:length(configurations)
    count(configurations{i}.indexVector(1))=count(configurations{i}.indexVector(1))+1;
    plot(grids{1}(configurations{i}.indexVector(1)),configurations{i}.costValue,'*');
end
disp(count);

%%
datStruct = configurations{1,82}.startDatStruct;

datStruct.options_itmax         = 5;
save('BaseData/GallopTriple_SH_SL_BASE_02.mat', 'datStruct');
CreateDatFile('BaseData/GallopTriple_SH_SL_BASE_02',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/GallopTriple_SH_SL_BASE_02.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' GallopTriple_SH_SL_BASE_02']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/GallopTriple_SH_SL_BASE_02.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

PlotMuscodResults('BaseData/GallopTriple_SH_SL_BASE_02',[1:6]);
%%
PlayMuscodResults('BaseData/GallopTriple_SH_SL_BASE_02');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%   GalloppingBound
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


load('BaseData/Gallop_SH_SL_BASE_10.mat','datStruct');

datStruct.h  = [0.2;0;0.2;0;0.2;0.2;0.2;0;0.2;0;0.2;0.2];
datStruct.nshoot = [3;1;3;1;3;3;3;1;3;1;3;3];
datStruct.h_sca  = [1;0;1;0;1;1;1;0;1;0;1;1];
datStruct.h_min  = [0.0001;0.0;0.0001;0.0;0.0001;0.0001;0.0001;0.0;0.0001;0.0;0.0001;0.0001];
datStruct.h_max  = [5.0;0.0;5.0;0.0;5.0;5.0;5.0;0.0;5.0;0.0;5.0;5.0];
datStruct.h_fix  = [0;1;0;1;0;0;0;1;0;1;0;0];

datStruct.libmodel = 'libGalloppingBound';
datStruct.libind = {'ind_rkf45'; 'ind_strans'; 'ind_rkf45';'ind_strans'; 'ind_rkf45'; 'ind_rkf45';'ind_rkf45'; 'ind_strans'; 'ind_rkf45';'ind_strans'; 'ind_rkf45'; 'ind_rkf45'};
datStruct.h_name = {'flight'; 'touchdown collision of LH'; 'single stance LHlfrfrh';'touchdown collision of RH'; 'double stance LHlfrfRH'; 'single stance lhlfrfRH';
    'flight'; 'touchdown collision of LF'; 'single stance lhLFrfrh' ; 'touchdown collision of RF'; 'double stance lhLFRFrh'; 'single stance lhlfRFrh'};
datStruct.h_comment = {'flight'; 'touchdown collision of LH'; 'single stance LHlfrfrh';'touchdown collision of RH'; 'double stance LHlfrfRH'; 'single stance lhlfrfRH';
    'flight'; 'touchdown collision of LF'; 'single stance lhLFrfrh' ; 'touchdown collision of RF'; 'double stance lhLFRFrh'; 'single stance lhlfRFrh'};
datStruct.p(5) = 0;
datStruct.p(4) = 1.0;
datStruct.options_itmax         = 100;

save('BaseData/GallopBound_SH_SL_BASE_00.mat', 'datStruct');
CreateDatFile('BaseData/GallopBound_SH_SL_BASE_00',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/GallopBound_SH_SL_BASE_00.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' GallopBound_SH_SL_BASE_00']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/GallopBound_SH_SL_BASE_00.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

PlotMuscodResults('BaseData/GallopBound_SH_SL_BASE_00',[1:6]);

%%

PlayMuscodResults('BaseData/GallopBound_SH_SL_BASE_00');


%%


load('BaseData/GallopBound_SH_SL_BASE_00.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/GallopBound_SH_SL_BASE_00', 30,41,8);
datStruct.header = {'// Updated from GallopBound_SH_SL_BASE_00'};
datStruct.p(5) = 7;
datStruct.p(4) = 1.0;
datStruct.sd_max(3) = 1.5;
datStruct.sd_min(5) = -0.3;
datStruct.sd_max(5) = 0.3;
datStruct.options_itmax         = 100;
save('BaseData/GallopBound_SH_SL_BASE_01.mat', 'datStruct');
CreateDatFile('BaseData/GallopBound_SH_SL_BASE_01',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/GallopBound_SH_SL_BASE_01.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' GallopBound_SH_SL_BASE_01']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/GallopBound_SH_SL_BASE_01.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

PlotMuscodResults('BaseData/GallopBound_SH_SL_BASE_01',[1:6]);

%%

PlayMuscodResults('BaseData/GallopBound_SH_SL_BASE_01');


%%


load('BaseData/GallopBound_SH_SL_BASE_01.mat','datStruct');
datStruct.options_itmax         = 300;
datStruct = UpdateDatStruct(datStruct, 'BaseData/GallopBound_SH_SL_BASE_01', 30,41,8);

%CreateParameterStudy('ParameterStudies/VelocityStudyGallopBou', datStruct, 'p',4,linspace(0.5,1.5,21)) ;
ProcessParameterStudy('ParameterStudies/VelocityStudyGallopBou', PathTo2Dqrad, 'muscod');


%%

load('ParameterStudies/VelocityStudyGallopBou','configurations','types','indices','grids');
count = zeros(length(grids{1}),1);
%clf
figure(3)
hold on
box on
grid on
for i = 1:length(configurations)
    count(configurations{i}.indexVector(1))=count(configurations{i}.indexVector(1))+1;
    plot(grids{1}(configurations{i}.indexVector(1)),configurations{i}.costValue,'*');
end
disp(count);

%%
datStruct = configurations{1,51}.startDatStruct;

datStruct.options_itmax         = 20;
save('BaseData/GallopBound_SH_SL_BASE_03.mat', 'datStruct');
CreateDatFile('BaseData/GallopBound_SH_SL_BASE_03',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/GallopBound_SH_SL_BASE_03.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' GallopBound_SH_SL_BASE_03']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/GallopBound_SH_SL_BASE_03.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

PlotMuscodResults('BaseData/GallopBound_SH_SL_BASE_03',[1:6]);
%%
PlayMuscodResults('BaseData/GallopBound_SH_SL_BASE_03');












%%

load('BaseData/RUN_SH_SL_BASE_01.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/RUN_SH_SL_BASE_01', 30,25,4);

CreateParameterStudy('ParameterStudies/VelocityStudyRUN2', datStruct, 'p',4,linspace(0.3,1.0,29)) ;
ProcessParameterStudy('ParameterStudies/VelocityStudyRUN2', PathTo2DBiped, 'muscod');

%%

load('ParameterStudies/VelocityStudyRUN2','configurations','types','indices','grids');
count = zeros(length(grids{1}),1);
%clf
figure(2)
hold on
box on
grid on
for i = 1:length(configurations)
    count(configurations{i}.indexVector(1))=count(configurations{i}.indexVector(1))+1;
    plot(grids{1}(configurations{i}.indexVector(1)),configurations{i}.costValue,'*');
end
disp(count);

%%

load('ParameterStudies/VelocityStudyWALK4','configurations','types','indices','grids');
count = zeros(length(grids{1}),1);
%clf
figure(3)
hold on
box on
grid on
for i = 1:length(configurations)
    count(configurations{i}.indexVector(1))=count(configurations{i}.indexVector(1))+1;
    plot(grids{1}(configurations{i}.indexVector(1)),configurations{i}.finalDatStruct.h(3),'*');
end
disp(count);

%%

load('ParameterStudies/VelocityStudyRUN1','configurations','types','indices','grids');
count = zeros(length(grids{1}),1);
%clf
figure(4)
hold on
box on
grid on
for i = 1:length(configurations)
    count(configurations{i}.indexVector(1))=count(configurations{i}.indexVector(1))+1;
    plot(grids{1}(configurations{i}.indexVector(1)),configurations{i}.costValue,'*');
end
disp(count);
%%

load('ParameterStudies/VelocityStudyRUN1','configurations','types','indices','grids');
count = zeros(length(grids{1}),1);
%clf
figure(5)
hold on
box on
grid on
for i = 1:length(configurations)
    count(configurations{i}.indexVector(1))=count(configurations{i}.indexVector(1))+1;
    plot(grids{1}(configurations{i}.indexVector(1)),configurations{i}.finalDatStruct.h(1),'*');
end
disp(count);
%%
figure(6)
hold on
box on
grid on
for i=1:length(configurations)
    count(configurations{i}.indexVector(1))=count(configurations{i}.indexVector(1))+1;
    plot(i,grids{1}(configurations{i}.indexVector(1)),'*');
end

%%
figure(7)
hold on
box on
grid on
for i=1:length(configurations)
    count(configurations{i}.indexVector(1))=count(configurations{i}.indexVector(1))+1;
    plot(i,configurations{i}.costValue,'*');
end

%%
figure(9)
hold on
box on
grid on
for i=95:length(configurations)
    count(configurations{i}.indexVector(1))=count(configurations{i}.indexVector(1))+1;
    plot(i,configurations{i}.finalDatStruct.h(1),'*');
end
%%


load('BaseData/WALK_SH_SL_BASE_02.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/WALK_SH_SL_BASE_02', 30,25,4);
datStruct.header = {'// Updated from WALK_SH_SL_BASE_02'};
datStruct.p(5) = 1;
datStruct.p(4) = 0.3;
save('BaseData/WALK_SH_SL_BASE_03.mat', 'datStruct');
CreateDatFile('BaseData/WALK_SH_SL_BASE_03',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/WALK_*_BASE_03.dat ', PathTo2DBiped,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2DBiped);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' WALK_SH_SL_BASE_03']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2DBiped,'RES/WALK_*_BASE_03.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2DBiped,'DAT/*.* ']);
system(['rm ',PathTo2DBiped,'RES/*.* ']);

% Converged 4/26
PlotMuscodResults('BaseData/WALK_SH_SL_BASE_03',[10,14,18,22]);

PlotMuscodResults('BaseData/WALK_SH_SL_BASE_03',[1:6]);






