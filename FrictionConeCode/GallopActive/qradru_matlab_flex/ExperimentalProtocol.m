
%% Computer dependent settings:
% Possibility one: Lab Computer
PathToMuscod  = '/home/yevyes/MUSCOD-II/MC2/Release/bin/muscod';
PathTo2Dqrad   = '/home/yevyes/Yev_Will/qradru_model_flex/';
PathToMatlab = '/home/yevyes/Yev_Will/qradru_matlab_flex/';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Trotting
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

mkdir('BaseData');
datStruct = CreateDatStruct('Trot','SEA','SEA');
datStruct.p(5) = 0;
datStruct.p(4) = 0.7;
        
save('BaseData/Trot_SH_SL_BASE_00.mat', 'datStruct');
CreateDatFile('BaseData/Trot_SH_SL_BASE_00',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Trot_*_BASE_00.dat ', PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);

[MuscodResults_11(1), cmdout] = system([PathToMuscod,' Trot_SH_SL_BASE_00']);
disp(MuscodResults_11(1))
cd(currentPath);

% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Trot_*_BASE_00.* ',pwd,'/BaseData/']);

PlotMuscodResults('BaseData/Trot_SH_SL_BASE_00',[5 7]);
% PlotMuscodResults('BaseData/Trot_SH_SL_BASE_00',[10,14,18,22]);
%%

PlayMuscodResults('BaseData/Trot_SH_SL_BASE_00');
%%
load('BaseData/Trot_SH_SL_BASE_00.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/Trot_SH_SL_BASE_00', 30,45,9);
datStruct.header = {'// Updated from Trot_SH_SL_BASE_00'};
datStruct.p(5) = 7;
datStruct.p(4) = 0.5;
datStruct.p(3) = 1;
save('BaseData/Trot_SH_SL_BASE_01.mat', 'datStruct');
CreateDatFile('BaseData/Trot_SH_SL_BASE_01',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Trot_SH_SL_BASE_01.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Trot_SH_SL_BASE_01']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Trot_SH_SL_BASE_01.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

% Converged 4/26
PlotMuscodResults('BaseData/Trot_SH_SL_BASE_01',[10,14,18,22]);

PlotMuscodResults('BaseData/Trot_SH_SL_BASE_01',[7:10]);

%%

PlayMuscodResults('BaseData/Trot_SH_SL_BASE_01');
%%
load('BaseData/Trot_SH_SL_BASE_01.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/Trot_SH_SL_BASE_01', 30,45,9);

CreateParameterStudy('ParameterStudies/VelocityStudyTrotFlex1', datStruct, 'p',4,linspace(0.2,2.0,37)) ;
ProcessParameterStudy('ParameterStudies/VelocityStudyTrotFlex1', PathTo2Dqrad, 'muscod');

%%

load('ParameterStudies/VelocityStudyTrotFlex1','configurations','types','indices','grids');
count = zeros(length(grids{1}),1);
%clf
figure(3)
hold on
box on
grid on
for i = 1:length(configurations)
    count(configurations{i}.indexVector(1))=count(configurations{i}.indexVector(1))+1;
    plot(grids{1}(configurations{i}.indexVector(1)),configurations{i}.costValue,'*');
end
disp(count);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Bounding
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

mkdir('BaseData');
datStruct = CreateDatStruct('Bound','SEA','SEA');
        
save('BaseData/Bound_SH_SL_BASE_00.mat', 'datStruct');
CreateDatFile('BaseData/Bound_SH_SL_BASE_00',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Bound_SH_SL_BASE_00.dat ', PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);

[MuscodResults_11(1), cmdout] = system([PathToMuscod,' Bound_SH_SL_BASE_00']);
cd(currentPath);

% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Bound_SH_SL_BASE_00.* ',pwd,'/BaseData/']);

PlotMuscodResults('BaseData/Bound_SH_SL_BASE_00',[1:6]);
PlotMuscodResults('BaseData/Bound_SH_SL_BASE_00',[10,14,18,22]);
%%

PlayMuscodResults('BaseData/Bound_SH_SL_BASE_00');



%%
load('BaseData/Bound_SH_SL_BASE_00.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/Bound_SH_SL_BASE_00', 30,45,9);
datStruct.header = {'// Updated from Bound_SH_SL_BASE_00'};
datStruct.p(5) = 7;
datStruct.p(4) = 0.6;
datStruct.sd_fix_init(5) = 0;
save('BaseData/Bound_SH_SL_BASE_01.mat', 'datStruct');
CreateDatFile('BaseData/Bound_SH_SL_BASE_01',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Bound_SH_SL_BASE_01.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Bound_SH_SL_BASE_01']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Bound_SH_SL_BASE_01.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

% Converged 4/26
PlotMuscodResults('BaseData/Bound_SH_SL_BASE_01',[10,14,18,22]);

PlotMuscodResults('BaseData/Bound_SH_SL_BASE_01',[1:6]);

%%

PlayMuscodResults('BaseData/Bound_SH_SL_BASE_01');

%%
load('BaseData/Bound_SH_SL_BASE_01.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/Bound_SH_SL_BASE_01', 30,45,9);
datStruct.header = {'// Updated from Bound_SH_SL_BASE_01'};
datStruct.p(5) = 7;
datStruct.p(4) = 0.8;
save('BaseData/Bound_SH_SL_BASE_02.mat', 'datStruct');
CreateDatFile('BaseData/Bound_SH_SL_BASE_02',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Bound_SH_SL_BASE_02.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Bound_SH_SL_BASE_02']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Bound_SH_SL_BASE_02.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

% Converged 4/26
PlotMuscodResults('BaseData/Bound_SH_SL_BASE_02',[10,14,18,22]);

PlotMuscodResults('BaseData/Bound_SH_SL_BASE_02',[7:10]);

%%

PlayMuscodResults('BaseData/Bound_SH_SL_BASE_02');


%%
load('BaseData/Bound_SH_SL_BASE_00.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/Bound_SH_SL_BASE_00', 30,45,9);
datStruct.header = {'// Updated from Bound_SH_SL_BASE_00'};
datStruct.p(5) = 7;
datStruct.p(4) = 0.6;

save('BaseData/Bound_SH_SL_BASE_03.mat', 'datStruct');
CreateDatFile('BaseData/Bound_SH_SL_BASE_03',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Bound_SH_SL_BASE_03.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Bound_SH_SL_BASE_03']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Bound_SH_SL_BASE_03.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

% Converged 4/26
PlotMuscodResults('BaseData/Bound_SH_SL_BASE_03',[10,14,18,22]);

PlotMuscodResults('BaseData/Bound_SH_SL_BASE_03',[7:10]);

%%

PlayMuscodResults('BaseData/Bound_SH_SL_BASE_03');


%%
datStruct.nshoot = datStruct2.nshoot;
datStruct.h = datStruct2.h;
datStruct.sd = zeros(45,30);
datStruct.sd(1:6,:) = datStruct2.sd(1:6,:);
datStruct.sd(11:45,:) = datStruct2.sd(7:41,:);
datStruct.u = zeros(9,30);
datStruct.u(2:9,:) = datStruct2.u(1:8,:);


%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Walking
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

mkdir('BaseData');
datStruct = CreateDatStruct('Walk','SEA','SEA');
        
save('BaseData/Walk_SH_SL_BASE_00.mat', 'datStruct');
CreateDatFile('BaseData/Walk_SH_SL_BASE_00',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Walk_SH_SL_BASE_00.dat ', PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);

[MuscodResults_11(1), cmdout] = system([PathToMuscod,' Walk_SH_SL_BASE_00']);
cd(currentPath);

% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Walk_SH_SL_BASE_00.* ',pwd,'/BaseData/']);

PlotMuscodResults('BaseData/Walk_SH_SL_BASE_00',[1:6]);
PlotMuscodResults('BaseData/Walk_SH_SL_BASE_00',[10,14,18,22]);
%%

PlayMuscodResults('BaseData/Walk_SH_SL_BASE_00');

%%
load('BaseData/Walk_SH_SL_BASE_00.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/Walk_SH_SL_BASE_00', 30,45,9);
datStruct.header = {'// Updated from Walk_SH_SL_BASE_00'};
datStruct.p(5) = 7;
datStruct.p(4) = 0.3;
datStruct.sd_fix_init(5) = 0;
save('BaseData/Walk_SH_SL_BASE_01.mat', 'datStruct');
CreateDatFile('BaseData/Walk_SH_SL_BASE_01',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Walk_SH_SL_BASE_01.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Walk_SH_SL_BASE_01']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Walk_SH_SL_BASE_01.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

% Converged 4/26
PlotMuscodResults('BaseData/Walk_SH_SL_BASE_01',[10,14,18,22]);

PlotMuscodResults('BaseData/Walk_SH_SL_BASE_01',[1:6]);

%%

PlayMuscodResults('BaseData/Walk_SH_SL_BASE_01');
%%
load('BaseData/Walk_SH_SL_BASE_01.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/Walk_SH_SL_BASE_01', 30,41,8);
datStruct.header = {'// Updated from Walk_SH_SL_BASE_01'};
datStruct.p(5) = 7;
datStruct.p(4) = 0.35;
datStruct.sd_fix_init(5) = 0;
save('BaseData/Walk_SH_SL_BASE_02.mat', 'datStruct');
CreateDatFile('BaseData/Walk_SH_SL_BASE_02',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Walk_SH_SL_BASE_02.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Walk_SH_SL_BASE_02']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Walk_SH_SL_BASE_02.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

% Converged 4/26
PlotMuscodResults('BaseData/Walk_SH_SL_BASE_02',[10,14,18,22]);

PlotMuscodResults('BaseData/Walk_SH_SL_BASE_02',[1:6]);

%%

PlayMuscodResults('BaseData/Walk_SH_SL_BASE_01');
%%
load('BaseData/Walk_SH_SL_BASE_02.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/Walk_SH_SL_BASE_02', 30,41,8);
datStruct.header = {'// Updated from Walk_SH_SL_BASE_02'};
datStruct.p(5) = 7;
datStruct.p(4) = 0.3;
datStruct.sd_fix_init(5) = 0;
save('BaseData/Walk_SH_SL_BASE_03.mat', 'datStruct');
CreateDatFile('BaseData/Walk_SH_SL_BASE_03',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Walk_SH_SL_BASE_03.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Walk_SH_SL_BASE_03']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Walk_SH_SL_BASE_03.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

% Converged 4/26
PlotMuscodResults('BaseData/Walk_SH_SL_BASE_03',[10,14,18,22]);

PlotMuscodResults('BaseData/Walk_SH_SL_BASE_03',[1:6]);

%%

PlayMuscodResults('BaseData/Walk_SH_SL_BASE_03');
%%
load('BaseData/Walk_SH_SL_BASE_03.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/Walk_SH_SL_BASE_03', 30,41,8);
datStruct.header = {'// Updated from Walk_SH_SL_BASE_03'};
datStruct.p(5) = 7;
datStruct.p(4) = 0.3;
datStruct.p(3) = 2;
datStruct.sd_fix_init(5) = 0;
save('BaseData/Walk_SH_SL_BASE_04.mat', 'datStruct');
CreateDatFile('BaseData/Walk_SH_SL_BASE_04',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Walk_SH_SL_BASE_04.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Walk_SH_SL_BASE_04']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Walk_SH_SL_BASE_04.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

% Converged 4/26
PlotMuscodResults('BaseData/Walk_SH_SL_BASE_04',[10,14,18,22]);

PlotMuscodResults('BaseData/Walk_SH_SL_BASE_04',[1:6]);

%%

PlayMuscodResults('BaseData/Walk_SH_SL_BASE_04');
%%
load('BaseData/Walk_SH_SL_BASE_04.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/Walk_SH_SL_BASE_04', 30,41,8);
datStruct.header = {'// Updated from Walk_SH_SL_BASE_04'};
datStruct.p(5) = 7;
datStruct.p(4) = 0.3;
datStruct.p(3) = 1;
datStruct.sd_fix_init(5) = 0;
save('BaseData/Walk_SH_SL_BASE_05.mat', 'datStruct');
CreateDatFile('BaseData/Walk_SH_SL_BASE_05',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Walk_SH_SL_BASE_05.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Walk_SH_SL_BASE_05']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Walk_SH_SL_BASE_05.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

% Converged 4/26
PlotMuscodResults('BaseData/Walk_SH_SL_BASE_05',[10,14,18,22]);

PlotMuscodResults('BaseData/Walk_SH_SL_BASE_05',[1:6]);

%%

PlayMuscodResults_old('BaseData/Walk_SH_SL_BASE_05');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Gallopping
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

mkdir('BaseData');
datStruct = CreateDatStruct('Gallop','SEA','SEA');
        
save('BaseData/Gallop_SH_SL_BASE_00.mat', 'datStruct');
CreateDatFile('BaseData/Gallop_SH_SL_BASE_00',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Gallop_SH_SL_BASE_00.dat ', PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);

[MuscodResults_11(1), cmdout] = system([PathToMuscod,' Gallop_SH_SL_BASE_00']);
cd(currentPath);

% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Gallop_SH_SL_BASE_00.* ',pwd,'/BaseData/']);

PlotMuscodResults('BaseData/Gallop_SH_SL_BASE_00',[1:6]);
PlotMuscodResults('BaseData/Gallop_SH_SL_BASE_00',[10,14,18,22]);
%%

PlayMuscodResults('BaseData/Gallop_SH_SL_BASE_00');
%%
load('BaseData/Gallop_SH_SL_BASE_00.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/Gallop_SH_SL_BASE_00', 30,45,9);
datStruct.header = {'// Updated from Gallop_SH_SL_BASE_00'};
datStruct.p(5) = 7;
datStruct.p(4) = 1.4;
datStruct.sd_fix_init(5) = 0;
datStruct.sd_min(5) = -0.1;
datStruct.sd_max(5) = 0.1;
save('BaseData/Gallop_SH_SL_BASE_01.mat', 'datStruct');
CreateDatFile('BaseData/Gallop_SH_SL_BASE_01',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Gallop_SH_SL_BASE_01.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Gallop_SH_SL_BASE_01']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Gallop_SH_SL_BASE_01.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

% Converged 4/26
PlotMuscodResults('BaseData/Gallop_SH_SL_BASE_01',[10,14,18,22]);

PlotMuscodResults('BaseData/Gallop_SH_SL_BASE_01',[1:6]);

%%

PlayMuscodResults('BaseData/Gallop_SH_SL_BASE_01');

%%
load('BaseData/Gallop_SH_SL_BASE_01.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/Gallop_SH_SL_BASE_01', 30,45,9);
datStruct.header = {'// Updated from Gallop_SH_SL_BASE_01'};
datStruct.p(5) = 7;
datStruct.p(4) = 1.2;
datStruct.sd_fix_init(5) = 0;
datStruct.sd_min(5) = -0.1;
datStruct.sd_max(5) = 0.1;
save('BaseData/Gallop_SH_SL_BASE_02.mat', 'datStruct');
CreateDatFile('BaseData/Gallop_SH_SL_BASE_02',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Gallop_SH_SL_BASE_02.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Gallop_SH_SL_BASE_02']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Gallop_SH_SL_BASE_02.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

% Converged 4/26
PlotMuscodResults('BaseData/Gallop_SH_SL_BASE_02',[10,14,18,22]);

PlotMuscodResults('BaseData/Gallop_SH_SL_BASE_02',[1:6]);

%%

PlayMuscodResults('BaseData/Gallop_SH_SL_BASE_02');
%%
load('BaseData/Gallop_SH_SL_BASE_02.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/Gallop_SH_SL_BASE_02', 30,45,9);
datStruct.header = {'// Updated from Gallop_SH_SL_BASE_02'};
datStruct.p(5) = 7;
datStruct.p(4) = 1.2;
datStruct.sd_fix_init(5) = 0;
datStruct.sd_min(5) = -0.3;
datStruct.sd_max(5) = 0.3;
save('BaseData/Gallop_SH_SL_BASE_03.mat', 'datStruct');
CreateDatFile('BaseData/Gallop_SH_SL_BASE_03',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Gallop_SH_SL_BASE_03.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Gallop_SH_SL_BASE_03']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Gallop_SH_SL_BASE_03.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

% Converged 4/26
PlotMuscodResults('BaseData/Gallop_SH_SL_BASE_03',[10,14,18,22]);

PlotMuscodResults('BaseData/Gallop_SH_SL_BASE_03',[1:6]);

%%

PlayMuscodResults('BaseData/Gallop_SH_SL_BASE_03');

%%


load('BaseData/Gallop_SH_SL_BASE_03.mat','datStruct');
datStruct.options_itmax         = 300;
datStruct = UpdateDatStruct(datStruct, 'BaseData/Gallop_SH_SL_BASE_03', 30,45,9);

CreateParameterStudy('ParameterStudies/VelocityStudyGallop', datStruct, 'p',4,linspace(0.5,1.5,21)) ;
ProcessParameterStudy('ParameterStudies/VelocityStudyGallop', PathTo2Dqrad, 'muscod');


%%

load('ParameterStudies_Weitao/VelocityStudyGallop','configurations','types','indices','grids');
count = zeros(length(grids{1}),1);
%clf
figure(3)
hold on
box on
grid on
for i = 1:length(configurations)
    count(configurations{i}.indexVector(1))=count(configurations{i}.indexVector(1))+1;
    plot(grids{1}(configurations{i}.indexVector(1)),configurations{i}.costValue,'*');
end
disp(count);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%   GalloppingBound
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


load('BaseData/Gallop_SH_SL_BASE_03.mat','datStruct');

datStruct.h  = [0.2;0;0.2;0;0.2;0.2;0.2;0;0.2;0;0.2;0.2];
datStruct.nshoot = [3;1;3;1;3;3;3;1;3;1;3;3];
datStruct.h_sca  = [1;0;1;0;1;1;1;0;1;0;1;1];
datStruct.h_min  = [0.0001;0.0;0.0001;0.0;0.0001;0.0001;0.0001;0.0;0.0001;0.0;0.0001;0.0001];
datStruct.h_max  = [5.0;0.0;5.0;0.0;5.0;5.0;5.0;0.0;5.0;0.0;5.0;5.0];
datStruct.h_fix  = [0;1;0;1;0;0;0;1;0;1;0;0];

datStruct.libmodel = 'libGalloppingBound';
datStruct.libind = {'ind_rkf45'; 'ind_strans'; 'ind_rkf45';'ind_strans'; 'ind_rkf45'; 'ind_rkf45';'ind_rkf45'; 'ind_strans'; 'ind_rkf45';'ind_strans'; 'ind_rkf45'; 'ind_rkf45'};
datStruct.h_name = {'flight'; 'touchdown collision of LH'; 'single stance LHlfrfrh';'touchdown collision of RH'; 'double stance LHlfrfRH'; 'single stance lhlfrfRH';
    'flight'; 'touchdown collision of LF'; 'single stance lhLFrfrh' ; 'touchdown collision of RF'; 'double stance lhLFRFrh'; 'single stance lhlfRFrh'};
datStruct.h_comment = {'flight'; 'touchdown collision of LH'; 'single stance LHlfrfrh';'touchdown collision of RH'; 'double stance LHlfrfRH'; 'single stance lhlfrfRH';
    'flight'; 'touchdown collision of LF'; 'single stance lhLFrfrh' ; 'touchdown collision of RF'; 'double stance lhLFRFrh'; 'single stance lhlfRFrh'};
datStruct.p(5) = 0;
datStruct.p(4) = 1.0;
datStruct.options_itmax         = 100;

save('BaseData/GallopBound_SH_SL_BASE_00.mat', 'datStruct');
CreateDatFile('BaseData/GallopBound_SH_SL_BASE_00',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/GallopBound_SH_SL_BASE_00.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' GallopBound_SH_SL_BASE_00']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/GallopBound_SH_SL_BASE_00.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

PlotMuscodResults('BaseData/GallopBound_SH_SL_BASE_00',[1:6]);

%%

PlayMuscodResults('BaseData/GallopBound_SH_SL_BASE_00');


%%


load('BaseData/GallopBound_SH_SL_BASE_00.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/GallopBound_SH_SL_BASE_00', 30,45,9);
datStruct.header = {'// Updated from GallopBound_SH_SL_BASE_00'};
datStruct.p(5) = 7;
datStruct.p(4) = 1.1;
datStruct.sd_max(3) = 100;
datStruct.sd_min(5) = -1;
datStruct.sd_max(5) = 1;
datStruct.options_itmax         = 500;
save('BaseData/GallopBound_SH_SL_BASE_01.mat', 'datStruct');
CreateDatFile('BaseData/GallopBound_SH_SL_BASE_01',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/GallopBound_SH_SL_BASE_01.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' GallopBound_SH_SL_BASE_01']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/GallopBound_SH_SL_BASE_01.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

PlotMuscodResults('BaseData/GallopBound_SH_SL_BASE_01',[1:6]);

%%
PlayMuscodResults('BaseData/GallopBound_SH_SL_BASE_01');

%%


load('BaseData/GallopBound_SH_SL_BASE_01.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/GallopBound_SH_SL_BASE_01', 30,45,9);
datStruct.header = {'// Updated from GallopBound_SH_SL_BASE_01'};
datStruct.p(5) = 7;
datStruct.p(4) = 1.5;
datStruct.sd_max(3) = 100;
datStruct.sd_min(5) = -1;
datStruct.sd_max(5) = 1;
datStruct.options_itmax         = 500;
save('BaseData/GallopBound_SH_SL_BASE_02.mat', 'datStruct');
CreateDatFile('BaseData/GallopBound_SH_SL_BASE_02',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/GallopBound_SH_SL_BASE_02.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' GallopBound_SH_SL_BASE_02']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/GallopBound_SH_SL_BASE_02.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

PlotMuscodResults('BaseData/GallopBound_SH_SL_BASE_02',[7:10]);

%%

PlayMuscodResults('BaseData/GallopBound_SH_SL_BASE_02');

%%


load('BaseData/GallopBound_SH_SL_BASE_02.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/GallopBound_SH_SL_BASE_02', 30,45,9);
datStruct.header = {'// Updated from GallopBound_SH_SL_BASE_02'};
datStruct.p(5) = 7;
datStruct.p(4) = 1.5;
datStruct.sd_min(7) = 0;
datStruct.sd_max(7) = 1;
datStruct.h_min  = [0.1;0.0;0.0001;0.0;0.0001;0.0001;0.1;0.0;0.0001;0.0;0.0001;0.0001];
datStruct.options_itmax         = 250;
save('BaseData/GallopBound_SH_SL_BASE_03.mat', 'datStruct');
CreateDatFile('BaseData/GallopBound_SH_SL_BASE_03',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/GallopBound_SH_SL_BASE_03.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' GallopBound_SH_SL_BASE_03']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/GallopBound_SH_SL_BASE_03.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

PlotMuscodResults('BaseData/GallopBound_SH_SL_BASE_03',[7:10]);

%%

PlayMuscodResults('BaseData/GallopBound_SH_SL_BASE_03');

%%


load('BaseData/GallopBound_SH_SL_BASE_03.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/GallopBound_SH_SL_BASE_03', 30,45,9);
datStruct.header = {'// Updated from GallopBound_SH_SL_BASE_03'};
datStruct.p(5) = 7;
datStruct.p(4) = 1.5;
datStruct.sd_min(7) = 0.1;
datStruct.sd_max(7) = 1;
datStruct.h_min  = [0.1;0.0;0.0001;0.0;0.0001;0.0001;0.1;0.0;0.0001;0.0;0.0001;0.0001];
datStruct.options_itmax         = 250;
save('BaseData/GallopBound_SH_SL_BASE_04.mat', 'datStruct');
CreateDatFile('BaseData/GallopBound_SH_SL_BASE_04',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/GallopBound_SH_SL_BASE_04.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' GallopBound_SH_SL_BASE_04']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/GallopBound_SH_SL_BASE_04.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

PlotMuscodResults('BaseData/GallopBound_SH_SL_BASE_04',[7:10]);

%%

PlayMuscodResults('BaseData/GallopBound_SH_SL_BASE_04');

%%


load('BaseData/GallopBound_SH_SL_BASE_04.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/GallopBound_SH_SL_BASE_04', 30,45,9);
datStruct.header = {'// Updated from GallopBound_SH_SL_BASE_04'};
datStruct.p(5) = 7;
datStruct.p(4) = 1.5;
datStruct.sd_min(7) = 0.1;
datStruct.sd_max(7) = 1;
datStruct.h_min  = [0.1;0.0;0.0001;0.0;0.0001;0.0001;0.1;0.0;0.0001;0.0;0.0001;0.0001];
datStruct.h_max  = [5.0;0.0;0.3;0.0;5.0;0.3;5.0;0.0;0.4;0.0;5.0;0.4];
datStruct.options_itmax         = 250;
save('BaseData/GallopBound_SH_SL_BASE_05.mat', 'datStruct');
CreateDatFile('BaseData/GallopBound_SH_SL_BASE_05',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/GallopBound_SH_SL_BASE_05.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' GallopBound_SH_SL_BASE_05']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/GallopBound_SH_SL_BASE_05.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

PlotMuscodResults('BaseData/GallopBound_SH_SL_BASE_05',[7:10]);

%%

PlayMuscodResults('BaseData/GallopBound_SH_SL_BASE_05');

%%


load('BaseData/GallopBound_SH_SL_BASE_05.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/GallopBound_SH_SL_BASE_05', 30,45,9);
datStruct.header = {'// Updated from GallopBound_SH_SL_BASE_05'};
datStruct.p(5) = 7;
datStruct.p(4) = 1.5;
datStruct.sd_min(7) = 0.1;
datStruct.sd_max(7) = 1;
datStruct.h_min  = [0.1;0.0;0.0001;0.0;0.0001;0.0001;0.1;0.0;0.0001;0.0;0.0001;0.0001];
datStruct.h_max  = [5.0;0.0;0.2;0.0;5.0;0.2;5.0;0.0;0.2;0.0;5.0;0.2];
datStruct.options_itmax         = 250;
save('BaseData/GallopBound_SH_SL_BASE_06.mat', 'datStruct');
CreateDatFile('BaseData/GallopBound_SH_SL_BASE_06',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/GallopBound_SH_SL_BASE_06.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' GallopBound_SH_SL_BASE_06']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/GallopBound_SH_SL_BASE_06.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

PlotMuscodResults('BaseData/GallopBound_SH_SL_BASE_06',[7:10]);

%%

PlayMuscodResults('BaseData/GallopBound_SH_SL_BASE_06');

%%


load('BaseData/GallopBound_SH_SL_BASE_06.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/GallopBound_SH_SL_BASE_06', 30,45,9);
datStruct.header = {'// Updated from GallopBound_SH_SL_BASE_06'};
datStruct.p(5) = 7;
datStruct.p(4) = 2;
datStruct.sd_min(7) = -1;
datStruct.sd_max(7) = 1;
datStruct.h_min  = [0.0001;0.0;0.0001;0.0;0.0001;0.0001;0.0001;0.0;0.0001;0.0;0.0001;0.0001];
datStruct.h_max  = [5.0;0.0;5.0;0.0;5.0;5.0;5.0;0.0;5.0;0.0;5.0;5.0];
datStruct.options_itmax         = 200;
save('BaseData/GallopBound_SH_SL_BASE_07.mat', 'datStruct');
CreateDatFile('BaseData/GallopBound_SH_SL_BASE_07',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/GallopBound_SH_SL_BASE_07.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' GallopBound_SH_SL_BASE_07']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/GallopBound_SH_SL_BASE_07.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

PlotMuscodResults('BaseData/GallopBound_SH_SL_BASE_07',[7:10]);

%%

PlayMuscodResults('BaseData/GallopBound_SH_SL_BASE_07');

%%


load('BaseData/GallopBound_SH_SL_BASE_07.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/GallopBound_SH_SL_BASE_07', 30,45,9);
datStruct.header = {'// Updated from GallopBound_SH_SL_BASE_07'};
datStruct.p(5) = 7;
datStruct.p(4) = 2;
datStruct.sd_min(7) = -1;
datStruct.sd_max(7) = 1;
datStruct.h_min  = [0.0001;0.0;0.0001;0.0;0.0001;0.0001;0.0001;0.0;0.0001;0.0;0.0001;0.0001];
datStruct.h_max  = [5.0;0.0;5.0;0.0;5.0;5.0;5.0;0.0;5.0;0.0;5.0;5.0];
datStruct.options_itmax         = 200;
save('BaseData/GallopBound_SH_SL_BASE_08.mat', 'datStruct');
CreateDatFile('BaseData/GallopBound_SH_SL_BASE_08',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/GallopBound_SH_SL_BASE_08.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' GallopBound_SH_SL_BASE_08']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/GallopBound_SH_SL_BASE_08.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

PlotMuscodResults('BaseData/GallopBound_SH_SL_BASE_08',[7:10]);

%%

PlayMuscodResults('BaseData/GallopBound_SH_SL_BASE_08');


%%


load('BaseData/GallopBound_SH_SL_BASE_09.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/GallopBound_SH_SL_BASE_09', 30,45,9);
datStruct.header = {'// Updated from GallopBound_SH_SL_BASE_09'};
datStruct.p(5) = 7;
datStruct.p(4) = 2;
datStruct.sd_min(7) = -1;
datStruct.sd_max(7) = 1;
datStruct.h_min  = [0.0001;0.0;0.0001;0.0;0.0001;0.0001;0.0001;0.0;0.0001;0.0;0.0001;0.0001];
datStruct.h_max  = [5.0;0.0;0.2;0.0;5.0;0.2;5.0;0.0;0.4;0.0;5.0;0.4];
%datStruct.h_max  = [5.0;0.0;5.0;0.0;5.0;5.0;5.0;0.0;5.0;0.0;5.0;5.0];
datStruct.options_itmax         = 200;
save('BaseData/GallopBound_SH_SL_BASE_09.mat', 'datStruct');
CreateDatFile('BaseData/GallopBound_SH_SL_BASE_09',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/GallopBound_SH_SL_BASE_09.dat ', PathTo2Dqrad,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' GallopBound_SH_SL_BASE_09']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/GallopBound_SH_SL_BASE_09.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

PlotMuscodResults('BaseData/GallopBound_SH_SL_BASE_09',[7:10]);

%%

PlayMuscodResults('BaseData/GallopBound_SH_SL_BASE_09');






















%%

load('BaseData/RUN_SH_SL_BASE_01.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/RUN_SH_SL_BASE_01', 30,25,4);

CreateParameterStudy('ParameterStudies/VelocityStudyRUN2', datStruct, 'p',4,linspace(0.3,1.0,29)) ;
ProcessParameterStudy('ParameterStudies/VelocityStudyRUN2', PathTo2DBiped, 'muscod');

%%

load('ParameterStudies/VelocityStudyRUN2','configurations','types','indices','grids');
count = zeros(length(grids{1}),1);
%clf
figure(2)
hold on
box on
grid on
for i = 1:length(configurations)
    count(configurations{i}.indexVector(1))=count(configurations{i}.indexVector(1))+1;
    plot(grids{1}(configurations{i}.indexVector(1)),configurations{i}.costValue,'*');
end
disp(count);

%%

load('ParameterStudies/VelocityStudyWALK4','configurations','types','indices','grids');
count = zeros(length(grids{1}),1);
%clf
figure(3)
hold on
box on
grid on
for i = 1:length(configurations)
    count(configurations{i}.indexVector(1))=count(configurations{i}.indexVector(1))+1;
    plot(grids{1}(configurations{i}.indexVector(1)),configurations{i}.finalDatStruct.h(3),'*');
end
disp(count);

%%

load('ParameterStudies/VelocityStudyRUN1','configurations','types','indices','grids');
count = zeros(length(grids{1}),1);
%clf
figure(4)
hold on
box on
grid on
for i = 1:length(configurations)
    count(configurations{i}.indexVector(1))=count(configurations{i}.indexVector(1))+1;
    plot(grids{1}(configurations{i}.indexVector(1)),configurations{i}.costValue,'*');
end
disp(count);
%%

load('ParameterStudies/VelocityStudyRUN1','configurations','types','indices','grids');
count = zeros(length(grids{1}),1);
%clf
figure(5)
hold on
box on
grid on
for i = 1:length(configurations)
    count(configurations{i}.indexVector(1))=count(configurations{i}.indexVector(1))+1;
    plot(grids{1}(configurations{i}.indexVector(1)),configurations{i}.finalDatStruct.h(1),'*');
end
disp(count);
%%
figure(6)
hold on
box on
grid on
for i=1:length(configurations)
    count(configurations{i}.indexVector(1))=count(configurations{i}.indexVector(1))+1;
    plot(i,grids{1}(configurations{i}.indexVector(1)),'*');
end

%%
figure(7)
hold on
box on
grid on
for i=1:length(configurations)
    count(configurations{i}.indexVector(1))=count(configurations{i}.indexVector(1))+1;
    plot(i,configurations{i}.costValue,'*');
end

%%
figure(9)
hold on
box on
grid on
for i=95:length(configurations)
    count(configurations{i}.indexVector(1))=count(configurations{i}.indexVector(1))+1;
    plot(i,configurations{i}.finalDatStruct.h(1),'*');
end
%%


load('BaseData/WALK_SH_SL_BASE_02.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/WALK_SH_SL_BASE_02', 30,25,4);
datStruct.header = {'// Updated from WALK_SH_SL_BASE_02'};
datStruct.p(5) = 1;
datStruct.p(4) = 0.3;
save('BaseData/WALK_SH_SL_BASE_03.mat', 'datStruct');
CreateDatFile('BaseData/WALK_SH_SL_BASE_03',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/WALK_*_BASE_03.dat ', PathTo2DBiped,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2DBiped);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' WALK_SH_SL_BASE_03']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2DBiped,'RES/WALK_*_BASE_03.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2DBiped,'DAT/*.* ']);
system(['rm ',PathTo2DBiped,'RES/*.* ']);

% Converged 4/26
PlotMuscodResults('BaseData/WALK_SH_SL_BASE_03',[10,14,18,22]);

PlotMuscodResults('BaseData/WALK_SH_SL_BASE_03',[1:6]);












%%

mkdir('BaseData');
datStruct = CreateDatStruct('RUN','SEA','SEA');
save('BaseData/RUN_SH_SL_BASE_00.mat', 'datStruct');
CreateDatFile('BaseData/RUN_SH_SL_BASE_00',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/RUN_*_BASE_00.dat ', PathTo2DBiped,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2DBiped);

[MuscodResults_11(1), cmdout] = system([PathToMuscod,' RUN_SH_SL_BASE_00']);
cd(currentPath);

% Copy back to BaseData/
system(['cp ',PathTo2DBiped,'RES/RUN_*_BASE_00.* ',pwd,'/BaseData/']);


%%


load('BaseData/RUN_SH_SL_BASE_00.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/RUN_SH_SL_BASE_00', 30,25,4);
datStruct.header = {'// Updated from RUN_SH_SL_BASE_00'};
datStruct.p(5) = 1;
save('BaseData/RUN_SH_SL_BASE_01.mat', 'datStruct');
CreateDatFile('BaseData/RUN_SH_SL_BASE_01',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/RUN_*_BASE_01.dat ', PathTo2DBiped,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2DBiped);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' RUN_SH_SL_BASE_01']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2DBiped,'RES/RUN_*_BASE_01.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2DBiped,'DAT/*.* ']);
system(['rm ',PathTo2DBiped,'RES/*.* ']);

% Converged 4/26
PlotMuscodResults('BaseData/RUN_SH_SL_BASE_01',[10,14,18,22]);

PlotMuscodResults('BaseData/RUN_SH_SL_BASE_01',[1:6]);
%%

PlayMuscodResults('BaseData/RUN_SH_SL_BASE_01');

%%


load('BaseData/RUN_SH_SL_BASE_01.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/RUN_SH_SL_BASE_01', 30,25,4);
datStruct.header = {'// Updated from RUN_SH_SL_BASE_01'};
datStruct.p(5) = 1;
save('BaseData/RUN_SH_SL_BASE_02.mat', 'datStruct');
CreateDatFile('BaseData/RUN_SH_SL_BASE_02',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/RUN_*_BASE_02.dat ', PathTo2DBiped,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2DBiped);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' RUN_SH_SL_BASE_02']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2DBiped,'RES/RUN_*_BASE_02.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2DBiped,'DAT/*.* ']);
system(['rm ',PathTo2DBiped,'RES/*.* ']);

% Converged 4/26
PlotMuscodResults('BaseData/RUN_SH_SL_BASE_02',[10,14,18,22]);

PlotMuscodResults('BaseData/RUN_SH_SL_BASE_02',[1:6]);


%%


load('BaseData/RUN_SH_SL_BASE_01.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/RUN_SH_SL_BASE_02', 30,25,4);
datStruct.header = {'// Updated from RUN_SH_SL_BASE_02'};
datStruct.p(5) = 1;
save('BaseData/RUN_SH_SL_BASE_03.mat', 'datStruct');
CreateDatFile('BaseData/RUN_SH_SL_BASE_03',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/RUN_*_BASE_03.dat ', PathTo2DBiped,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2DBiped);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' RUN_SH_SL_BASE_03']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2DBiped,'RES/RUN_*_BASE_03.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2DBiped,'DAT/*.* ']);
system(['rm ',PathTo2DBiped,'RES/*.* ']);
% Converged 4/26
PlotMuscodResults('BaseData/RUN_SH_SL_BASE_03',[10,14,18,22]);

PlotMuscodResults('BaseData/RUN_SH_SL_BASE_03',[1:6]);
%%

load('BaseData/RUN_SH_SL_BASE_03_8.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/RUN_SH_SL_BASE_03_8', 30,25,4);
datStruct.header = {'// Updated from RUN_SH_SL_BASE_03_8'};
datStruct.p(5) = 1;
datStruct.p(4) = 0.7;
iii=7;
save(['BaseData/RUN_SH_SL_BASE_03_',num2str(iii),'.mat'], 'datStruct');
CreateDatFile(['BaseData/RUN_SH_SL_BASE_03_',num2str(iii)],datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/RUN_*_BASE_03_',num2str(iii),'.dat ', PathTo2DBiped,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2DBiped);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' RUN_SH_SL_BASE_03_',num2str(iii)]);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2DBiped,'RES/RUN_*_BASE_03_',num2str(iii),'.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2DBiped,'DAT/*.* ']);
system(['rm ',PathTo2DBiped,'RES/*.* ']);

% Converged 4/26
PlotMuscodResults(['BaseData/RUN_SH_SL_BASE_03_',num2str(iii)],[10,14,18,22]);

PlotMuscodResults(['BaseData/RUN_SH_SL_BASE_03_',num2str(iii)],[1:6]);


%% Parameter Study:
%CreateParameterStudy('ParameterStudies/VelocityStudyRunning', datStruct, 'p',4,linspace(0.1,2,39)) 
ProcessParameterStudy('ParameterStudies/VelocityStudyRunning', PathTo2DBiped, 'muscod');



%% STEP1
% This first set of datStructs and .dat files are the initial problem
% files. The initial parameters have been selected as simple as possible.
% Yet they will converge to solutions when used in muscod.
%  - No constraints are imposed at this stage.
%  - Transmission values are fixed.
mkdir('BaseData');
datStruct = CreateDatStruct('RUN','SEA','SEA');
save('BaseData/RUN_SH_SL_BASE_00.mat', 'datStruct');
CreateDatFile('BaseData/RUN_SH_SL_BASE_00',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/RUN_*_BASE_00.dat ', PathTo2DBiped,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2DBiped);
[MuscodResults_11(1), cmdout] = system([PathToMuscod,' RUN_PH_PL_BASE_00']);
[MuscodResults_11(2), cmdout] = system([PathToMuscod,' RUN_PH_SL_BASE_00']);
[MuscodResults_11(3), cmdout] = system([PathToMuscod,' RUN_SH_PL_BASE_00']);
[MuscodResults_11(4), cmdout] = system([PathToMuscod,' RUN_SH_SL_BASE_00']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2DBiped,'RES/RUN_*_BASE_00.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2DBiped,'DAT/*.* ']);
system(['rm ',PathTo2DBiped,'RES/*.* ']);
% Converged 4/21 and 6/24

%% STEP2:
% In a second step, the results obtained in STEP1 are resampled, and used
% as intial guesses for a series, in which the constraints are enforced,
% and transmission values can be changed.
load('BaseData/RUN_PH_PL_BASE_00.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/RUN_PH_PL_BASE_00', 30,25,4);
datStruct.header = {'// Updated from RUN_PH_PL_BASE_00'};
datStruct.p(5) = 6;
datStruct.p_fix(7) = 0;
datStruct.p_fix(9) = 0;
save('BaseData/RUN_PH_PL_BASE_01.mat', 'datStruct');
CreateDatFile('BaseData/RUN_PH_PL_BASE_01',datStruct);
% Converged 4/26
load('BaseData/RUN_PH_SL_BASE_00.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/RUN_PH_SL_BASE_00', 30,25,4);
datStruct.header = {'// Updated from RUN_PH_SL_BASE_00'};
datStruct.p(5) = 6;
datStruct.p_fix(7) = 0;
datStruct.p_fix(9) = 0;
datStruct.h(1) = 0.4;
save('BaseData/RUN_PH_SL_BASE_01.mat', 'datStruct');
CreateDatFile('BaseData/RUN_PH_SL_BASE_01',datStruct);
% failed in the first run, than changed h(1) to 0.4. Then it converged 4/26
load('BaseData/RUN_SH_PL_BASE_00.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/RUN_SH_PL_BASE_00', 30,25,4);
datStruct.header = {'// Updated from RUN_SH_PL_BASE_00'};
datStruct.p(5) = 6;
datStruct.p_fix(7) = 0;
datStruct.p_fix(9) = 0;
save('BaseData/RUN_SH_PL_BASE_01.mat', 'datStruct');
CreateDatFile('BaseData/RUN_SH_PL_BASE_01',datStruct);
% Converged4/26
load('BaseData/RUN_SH_SL_BASE_00.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/RUN_SH_SL_BASE_00', 30,25,4);
datStruct.header = {'// Updated from RUN_SH_SL_BASE_00'};
datStruct.p(5) = 6;
datStruct.p_fix(7) = 0;
datStruct.p_fix(9) = 0;
save('BaseData/RUN_SH_SL_BASE_01.mat', 'datStruct');
CreateDatFile('BaseData/RUN_SH_SL_BASE_01',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/RUN_*_BASE_01.dat ', PathTo2DBiped,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2DBiped);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' RUN_PH_PL_BASE_01']);
[MuscodResults_12(2), cmdout] = system([PathToMuscod,' RUN_PH_SL_BASE_01']);
[MuscodResults_12(3), cmdout] = system([PathToMuscod,' RUN_SH_PL_BASE_01']);
[MuscodResults_12(4), cmdout] = system([PathToMuscod,' RUN_SH_SL_BASE_01']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2DBiped,'RES/RUN_*_BASE_01.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2DBiped,'DAT/*.* ']);
system(['rm ',PathTo2DBiped,'RES/*.* ']);

% Converged 4/26
PlotMuscodResults('BaseData/RUN_PH_PL_BASE_01',[8,12,16,20]);
PlotMuscodResults('BaseData/RUN_PH_SL_BASE_01',[10,14,18,22]);
PlotMuscodResults('BaseData/RUN_SH_PL_BASE_01',[8,12,16,20]);
PlotMuscodResults('BaseData/RUN_SH_SL_BASE_01',[10,14,18,22]);

%% STEP3:
% Reprocess these files, resample them, and run them again to ensure that
% we're starting in a local minimum:
load('BaseData/RUN_PH_PL_BASE_01.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/RUN_PH_PL_BASE_01', 30,25,4);
datStruct.h(1) = 0.3;
save('BaseData/RUN_PH_PL_BASE_02.mat', 'datStruct');
CreateDatFile('BaseData/RUN_PH_PL_BASE_02',datStruct);
% failed in the first run, than changed h(1) to 0.3. Then it converged 4/26
load('BaseData/RUN_PH_SL_BASE_01.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/RUN_PH_SL_BASE_01', 30,25,4);
datStruct.h(1) = 0.55;
save('BaseData/RUN_PH_SL_BASE_02.mat', 'datStruct');
CreateDatFile('BaseData/RUN_PH_SL_BASE_02',datStruct);
% failed in the first run, than changed h(1) to 0.3. Then it converged 4/26
load('BaseData/RUN_SH_PL_BASE_01.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/RUN_SH_PL_BASE_01', 30,25,4);
save('BaseData/RUN_SH_PL_BASE_02.mat', 'datStruct');
CreateDatFile('BaseData/RUN_SH_PL_BASE_02',datStruct);
% Converged 4/26
load('BaseData/RUN_SH_SL_BASE_01.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/RUN_SH_SL_BASE_01', 30,25,4);
save('BaseData/RUN_SH_SL_BASE_02.mat', 'datStruct');
CreateDatFile('BaseData/RUN_SH_SL_BASE_02',datStruct);
% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/RUN_*_BASE_02.dat ', PathTo2DBiped,'DAT/']);
% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2DBiped);
[MuscodResults_13(1), cmdout] = system([PathToMuscod,' RUN_PH_PL_BASE_02']);
[MuscodResults_13(2), cmdout] = system([PathToMuscod,' RUN_PH_SL_BASE_02']);
[MuscodResults_13(3), cmdout] = system([PathToMuscod,' RUN_SH_PL_BASE_02']);
[MuscodResults_13(4), cmdout] = system([PathToMuscod,' RUN_SH_SL_BASE_02']);
cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2DBiped,'RES/RUN_*_BASE_02.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2DBiped,'DAT/*.* ']);
system(['rm ',PathTo2DBiped,'RES/*.* ']);
% Converged 4/26

%% STEP4: 
% Process a parameter study that varies the stiffness values:
load('BaseData/RUN_PH_PL_BASE_02.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/RUN_PH_PL_BASE_02', 30);
% ..., 'p',6,logspace( log10(0.001), log10(15),10), 'p',8,logspace( log10(0.001), log10(15),10)) 
%CreateParameterStudy('ParameterStudies/StiffnessStudy_PH_PL', datStruct, 'p',6,linspace(0.0001,15,10),'p',8,linspace(0.0001,15,10)) 
ProcessParameterStudy('ParameterStudies/StiffnessStudy_PH_PL', 'ParameterStudies/', 'muscod');

load('ParameterStudies/StiffnessStudy_PH_PL','configurations','types','indices','grids');
count = zeros(length(grids{1}),length(grids{2}));
clf
hold on
box on
grid on
for i = 1:length(configurations)
    count(configurations{i}.indexVector(1),configurations{i}.indexVector(2))=count(configurations{i}.indexVector(1),configurations{i}.indexVector(2))+1;
    if configurations{i}.nFailed <= 5
        plot3(grids{1}(configurations{i}.indexVector(1)), grids{2}(configurations{i}.indexVector(2)), configurations{i}.costValue,'*');
    end
end
%axis equal
disp(count);


%% STEP5: 
% Process a parameter study that varies the velocity values:
load('BaseData/RUN_PH_PL_BASE_02.mat','datStruct');
datStruct = UpdateDatStruct(datStruct, 'BaseData/RUN_PH_PL_BASE_02', 30);
CreateParameterStudy('VelocityStudy', datStruct, 'p',4,linspace( 0.05, 2,20)) 
ProcessParameterStudy('VelocityStudy', '.', 'muscod');

load('VelocityStudy','configurations','types','indices','grids');
count = zeros(length(grids{1}),1);
clf
hold on
box on
grid on
for i = 1:length(configurations)
    count(configurations{i}.indexVector(1))=count(configurations{i}.indexVector(1))+1;
    plot(grids{1}(configurations{i}.indexVector(1)),configurations{i}.costValue,'*');
end
disp(count);
