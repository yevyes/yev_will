% *************************************************************************
%
% function [f, v, c] = 
%  GetQuadrupedPatch(x, y, phi, alphaF, ualphaF, alphaB, ualphaB, lF, ulF, lB, ulB)
% Returns faces, vertices, and color-values representing a patch that shows
% the 3D view of a bounding quadruped with two degrees of freedom.
%
%  GetQuadrupedPatch(x, y, phi, alphaF, ualphaF, alphaB, ualphaB, lF, ulF,
%  lB, ulB), without return arguments shows the patch in a new figure
%
% Input:  - x, y, phi (position and orientation of the main body)
%         - alphaF(B), lF(B) (length and orientation of the front (back)
%             leg) 
%         - ualphaF(B), ulF(B) (deflection of the front (back) actuators
%             from their resting position)
%
% Output: - faces 'f', vertices 'v', and color values 'c' as used in the
%           'patch' function
%
% Created by C. David Remy on 03/14/2011
% MATLAB 2010a
%
% Documentation:
%  'A MATLAB Framework For Gait Creation', 2011, C. David Remy (1), Keith
%  Buffinton (2), and Roland Siegwart (1),  International Conference on
%  Intelligent Robots and Systems, September 25-30, San Francisco, USA 
%
% (1) Autonomous Systems Lab, Institute of Robotics and Intelligent Systems, 
%     Swiss Federal Institute of Technology (ETHZ) 
%     Tannenstr. 3 / CLA-E-32.1
%     8092 Zurich, Switzerland  
%     cremy@ethz.ch; rsiegwart@ethz.ch
%
% (2) Department of Mechanical Engineering, 
%     Bucknell University
%     701 Moore Avenue
%     Lewisburg, PA-17837, USA
%     buffintk@bucknell.edu
%
%   See also OUTPUTCLASS, PATCH.
%
function [f,v,c] = GetQuadrupedPatch(x, y, phi, theta, alphaLH, lLH, alphaLF, lLF, alphaRF, lRF, alphaRH, lRH, ualphaLH, ulLH, ualphaLF, ulLF, ualphaRF, ulRF, ualphaRH, ulRH)

    % The faces, vertices, and color values are only created once and then
    % stored in memory:
    persistent mainBodyVertices1 mainBodyVertices2 upperLegVertices lowerLegVertices upperSpringVertices lowerSpringVertices upperActuatorVertices lowerActuatorVertices
    persistent f11 f12 f2 f3 f4 f5 f6 f7
    persistent c11 c12 c2 c3 c4 c5 c6 c7
    if isempty(mainBodyVertices1) || isempty(mainBodyVertices2) || isempty(upperLegVertices) || isempty(lowerLegVertices) || isempty(upperSpringVertices) || isempty(lowerSpringVertices) || isempty(upperActuatorVertices) || isempty(lowerActuatorVertices)
        [f11,mainBodyVertices1,c11]     = GetMainBodyQuadrupedPatch1();
        [f12,mainBodyVertices2,c12]     = GetMainBodyQuadrupedPatch2();
        [f2,upperLegVertices,c2]      = GetUpperLegPatch();
        [f3,lowerLegVertices,c3]      = GetLowerLegPatch();
        [f4,upperSpringVertices,c4]   = GetUpperSpringPatch();
        [f5,lowerSpringVertices,c5]   = GetLowerSpringPatch();
        [f6,upperActuatorVertices,c6] = GetUpperActuatorPatch();
        [f7,lowerActuatorVertices,c7] = GetLowerActuatorPatch();
    end
    
    % NOTE: Everything is implicitly transformed from the [X Y Z] in which
    % the dynamics are stated into the [X -Z Y] system in which the
    % graphics are displayed, i.e. the translation of the main body is
    % [x,0,y] instead of [x,y,0]. The main body and leg angles are thus
    % inverted: 
    phi     = -phi;
    theta     = -theta;
    alphaLF  = -alphaLF;
    alphaLH  = -alphaLH;
    alphaRF  = -alphaRF;
    alphaRH  = -alphaRH;
    ualphaLF  = -ualphaLF;
    ualphaLH  = -ualphaLH;
    ualphaRF  = -ualphaRF;
    ualphaRH  = -ualphaRH;
           
    %% Transform the vertices, and 'add' up the final patch object
    %% Main body:
    v = TransformVertices(mainBodyVertices1, Body312dc([0,0,phi+theta]), [x,0,y]);
    f = f11;
    c = c11;
    v12 = TransformVertices(mainBodyVertices2, Body312dc([0,0,phi-theta]), [x,0,y]);
    f12 = f12;
    c12 = c12;
    [v,f,c]=AddPatchesWithColor(v,f,c,v12,f12,c12);
    %% Upper leg segments (rotate and shift side wards)
    v_ = TransformVertices(upperLegVertices,Body312dc([0,0,-alphaLH]),[0,0,0]); % rotate
    v2 = TransformVertices(v_,diag([-1,+1,1]), [-0.75,+0.6,0]); % shift within main body
    v2 = TransformVertices(v2,Body312dc([0,0,phi+theta]), [x,0,y]);   % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v2,f2,c2);
    v2 = TransformVertices(v_,diag([-1,-1,1]), [-0.75,-0.6,0]); % shift within main body
    v2 = TransformVertices(v2,Body312dc([0,0,phi+theta]), [x,0,y]);   % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v2,f2,c2);
    v_ = TransformVertices(upperLegVertices,Body312dc([0,0,alphaLF]),[0,0,0]); % rotate
    v2 = TransformVertices(v_,diag([+1,+1,1]), [+0.75,+0.6,0]); % shift within main body
    v2 = TransformVertices(v2,Body312dc([0,0,phi-theta]), [x,0,y]);   % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v2,f2,c2);
    v2 = TransformVertices(v_,diag([+1,-1,1]), [+0.75,-0.6,0]); % shift within main body
    v2 = TransformVertices(v2,Body312dc([0,0,phi-theta]), [x,0,y]);   % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v2,f2,c2);
    v_ = TransformVertices(upperLegVertices,Body312dc([0,0,alphaRF]),[0,0,0]); % rotate
    v2 = TransformVertices(v_,diag([+1,+1,1]), [+0.75,+0.6,0]); % shift within main body
    v2 = TransformVertices(v2,Body312dc([0,0,phi-theta]), [x,0,y]);   % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v2,f2,c2);
    v2 = TransformVertices(v_,diag([+1,-1,1]), [+0.75,-0.6,0]); % shift within main body
    v2 = TransformVertices(v2,Body312dc([0,0,phi-theta]), [x,0,y]);   % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v2,f2,c2);
    v_ = TransformVertices(upperLegVertices,Body312dc([0,0,-alphaRH]),[0,0,0]); % rotate
    v2 = TransformVertices(v_,diag([-1,+1,1]), [-0.75,+0.6,0]); % shift within main body
    v2 = TransformVertices(v2,Body312dc([0,0,phi+theta]), [x,0,y]);   % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v2,f2,c2);
    v2 = TransformVertices(v_,diag([-1,-1,1]), [-0.75,-0.6,0]); % shift within main body
    v2 = TransformVertices(v2,Body312dc([0,0,phi+theta]), [x,0,y]);   % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v2,f2,c2);
    %% Lower leg segments (move downwards, rotate and shift side wards)
    v_ = TransformVertices(lowerLegVertices,eye(3),[0,0,-lLF]);
    v_ = TransformVertices(v_,Body312dc([0,0,alphaLF]),[0,0,0]); % rotate
    v3 = TransformVertices(v_,diag([+1,+1,1]), [+0.75,+0.6,0]); % shift within main body
    v3 = TransformVertices(v3,Body312dc([0,0,phi-theta]), [x,0,y]);   % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v3,f3,c3);
    v3 = TransformVertices(v_,diag([+1,-1,1]), [+0.75,-0.6,0]); % shift within main body
    v3 = TransformVertices(v3,Body312dc([0,0,phi-theta]), [x,0,y]);   % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v3,f3,c3);
    v_ = TransformVertices(lowerLegVertices,eye(3),[0,0,-lRF]);
    v_ = TransformVertices(v_,Body312dc([0,0,alphaRF]),[0,0,0]); % rotate
    v3 = TransformVertices(v_,diag([+1,+1,1]), [+0.75,+0.6,0]); % shift within main body
    v3 = TransformVertices(v3,Body312dc([0,0,phi-theta]), [x,0,y]);   % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v3,f3,c3);
    v3 = TransformVertices(v_,diag([+1,-1,1]), [+0.75,-0.6,0]); % shift within main body
    v3 = TransformVertices(v3,Body312dc([0,0,phi-theta]), [x,0,y]);   % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v3,f3,c3);
    v_ = TransformVertices(lowerLegVertices,eye(3),[0,0,-lLH]);
    v_ = TransformVertices(v_,Body312dc([0,0,-alphaLH]),[0,0,0]); % rotate
    v3 = TransformVertices(v_,diag([-1,+1,1]), [-0.75,+0.6,0]);  % shift within main body
    v3 = TransformVertices(v3,Body312dc([0,0,phi+theta]), [x,0,y]);    % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v3,f3,c3);
    v3 = TransformVertices(v_,diag([-1,-1,1]), [-0.75,-0.6,0]);  % shift within main body
    v3 = TransformVertices(v3,Body312dc([0,0,phi+theta]), [x,0,y]);    % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v3,f3,c3);
    v_ = TransformVertices(lowerLegVertices,eye(3),[0,0,-lRH]);
    v_ = TransformVertices(v_,Body312dc([0,0,-alphaRH]),[0,0,0]); % rotate
    v3 = TransformVertices(v_,diag([-1,+1,1]), [-0.75,+0.6,0]);  % shift within main body
    v3 = TransformVertices(v3,Body312dc([0,0,phi+theta]), [x,0,y]);    % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v3,f3,c3);
    v3 = TransformVertices(v_,diag([-1,-1,1]), [-0.75,-0.6,0]);  % shift within main body
    v3 = TransformVertices(v3,Body312dc([0,0,phi+theta]), [x,0,y]);    % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v3,f3,c3);
    %% Lower actuators (move downwards, rotate and shift side wards)
    v_ = TransformVertices(lowerActuatorVertices,eye(3),[0,0,(-0.5 -ulLF)]);
    v_ = TransformVertices(v_,Body312dc([0,0,alphaLF]),[0,0,0]); % rotate
    v6 = TransformVertices(v_,diag([+1,+1,1]), [+0.75,+0.6,0]); % shift within main body
    v6 = TransformVertices(v6,Body312dc([0,0,phi-theta]), [x,0,y]);   % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v6,f6,c6);
    v6 = TransformVertices(v_,diag([+1,-1,1]), [+0.75,-0.6,0]); % shift within main body
    v6 = TransformVertices(v6,Body312dc([0,0,phi-theta]), [x,0,y]);   % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v6,f6,c6);              
    v_ = TransformVertices(lowerActuatorVertices,eye(3),[0,0,(-0.5 -ulRF)]);
    v_ = TransformVertices(v_,Body312dc([0,0,alphaRF]),[0,0,0]); % rotate
    v6 = TransformVertices(v_,diag([+1,+1,1]), [+0.75,+0.6,0]); % shift within main body
    v6 = TransformVertices(v6,Body312dc([0,0,phi-theta]), [x,0,y]);   % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v6,f6,c6);
    v6 = TransformVertices(v_,diag([+1,-1,1]), [+0.75,-0.6,0]); % shift within main body
    v6 = TransformVertices(v6,Body312dc([0,0,phi-theta]), [x,0,y]);   % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v6,f6,c6);              
    v_ = TransformVertices(lowerActuatorVertices,eye(3),[0,0,(-0.5 -ulLH)]);
    v_ = TransformVertices(v_,Body312dc([0,0,-alphaLH]),[0,0,0]); % rotate
    v6 = TransformVertices(v_,diag([-1,+1,1]), [-0.75,+0.6,0]);  % shift within main body
    v6 = TransformVertices(v6,Body312dc([0,0,phi+theta]), [x,0,y]);    % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v6,f6,c6);
    v6 = TransformVertices(v_,diag([-1,-1,1]), [-0.75,-0.6,0]);  % shift within main body
    v6 = TransformVertices(v6,Body312dc([0,0,phi+theta]), [x,0,y]);    % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v6,f6,c6);              
    [v,f,c]=AddPatchesWithColor(v,f,c,v6,f6,c6);              
    v_ = TransformVertices(lowerActuatorVertices,eye(3),[0,0,(-0.5 -ulRH)]);
    v_ = TransformVertices(v_,Body312dc([0,0,-alphaRH]),[0,0,0]); % rotate
    v6 = TransformVertices(v_,diag([-1,+1,1]), [-0.75,+0.6,0]);  % shift within main body
    v6 = TransformVertices(v6,Body312dc([0,0,phi+theta]), [x,0,y]);    % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v6,f6,c6);
    v6 = TransformVertices(v_,diag([-1,-1,1]), [-0.75,-0.6,0]);  % shift within main body
    v6 = TransformVertices(v6,Body312dc([0,0,phi+theta]), [x,0,y]);    % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v6,f6,c6);              
    %% Lower spring (scale, move downwards, rotate and shift side wards)
    delta_lLF = (1-(1+ulLF-lLF)/0.34);
    v_ = TransformVertices(lowerSpringVertices,diag([1,1,delta_lLF]),[0,0,(-lLF + 0.16)]);
    v_ = TransformVertices(v_,Body312dc([0,0,alphaLF]),[0,0,0]); % rotate
    v5 = TransformVertices(v_,diag([+1,+1,1]), [+0.75,+0.6,0]); % shift within main body
    v5 = TransformVertices(v5,Body312dc([0,0,phi-theta]), [x,0,y]);   % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v5,f5,c5);
    v5 = TransformVertices(v_,diag([+1,-1,1]), [+0.75,-0.6,0]); % shift within main body
    v5 = TransformVertices(v5,Body312dc([0,0,phi-theta]), [x,0,y]);   % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v5,f5,c5);
    delta_lRF = (1-(1+ulRF-lRF)/0.34);
    v_ = TransformVertices(lowerSpringVertices,diag([1,1,delta_lRF]),[0,0,(-lRF + 0.16)]);
    v_ = TransformVertices(v_,Body312dc([0,0,alphaRF]),[0,0,0]); % rotate
    v5 = TransformVertices(v_,diag([+1,+1,1]), [+0.75,+0.6,0]); % shift within main body
    v5 = TransformVertices(v5,Body312dc([0,0,phi-theta]), [x,0,y]);   % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v5,f5,c5);
    v5 = TransformVertices(v_,diag([+1,-1,1]), [+0.75,-0.6,0]); % shift within main body
    v5 = TransformVertices(v5,Body312dc([0,0,phi-theta]), [x,0,y]);   % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v5,f5,c5);
    delta_lLH = (1-(1+ulLH-lLH)/0.34);
    v_ = TransformVertices(lowerSpringVertices,diag([1,1,delta_lLH]),[0,0,(-lLH + 0.16)]);
    v_ = TransformVertices(v_,Body312dc([0,0,-alphaLH]),[0,0,0]); % rotate
    v5 = TransformVertices(v_,diag([-1,+1,1]), [-0.75,+0.6,0]);  % shift within main body
    v5 = TransformVertices(v5,Body312dc([0,0,phi+theta]), [x,0,y]);    % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v5,f5,c5);
    v5 = TransformVertices(v_,diag([-1,-1,1]), [-0.75,-0.6,0]);  % shift within main body
    v5 = TransformVertices(v5,Body312dc([0,0,phi+theta]), [x,0,y]);    % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v5,f5,c5);
    delta_lRH = (1-(1+ulRH-lRH)/0.34);
    v_ = TransformVertices(lowerSpringVertices,diag([1,1,delta_lRH]),[0,0,(-lRH + 0.16)]);
    v_ = TransformVertices(v_,Body312dc([0,0,-alphaRH]),[0,0,0]); % rotate
    v5 = TransformVertices(v_,diag([-1,+1,1]), [-0.75,+0.6,0]);  % shift within main body
    v5 = TransformVertices(v5,Body312dc([0,0,phi+theta]), [x,0,y]);    % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v5,f5,c5);
    v5 = TransformVertices(v_,diag([-1,-1,1]), [-0.75,-0.6,0]);  % shift within main body
    v5 = TransformVertices(v5,Body312dc([0,0,phi+theta]), [x,0,y]);    % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v5,f5,c5);
    %% Upper actuators (rotate and shift side wards)
    v_ = TransformVertices(upperActuatorVertices,Body312dc([0,0,ualphaLF]),[0,0,0]); % rotate
    v7 = TransformVertices(v_,diag([+1,+1,1]), [+0.75,+0.6,0]); % shift within main body
    v7 = TransformVertices(v7,Body312dc([0,0,phi-theta]), [x,0,y]);   % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v7,f7,c7);
    v7 = TransformVertices(v_,diag([+1,-1,1]), [+0.75,-0.6,0]); % shift within main body
    v7 = TransformVertices(v7,Body312dc([0,0,phi-theta]), [x,0,y]);   % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v7,f7,c7);              
    v_ = TransformVertices(upperActuatorVertices,Body312dc([0,0,ualphaRF]),[0,0,0]); % rotate
    v7 = TransformVertices(v_,diag([+1,+1,1]), [+0.75,+0.6,0]); % shift within main body
    v7 = TransformVertices(v7,Body312dc([0,0,phi-theta]), [x,0,y]);   % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v7,f7,c7);
    v7 = TransformVertices(v_,diag([+1,-1,1]), [+0.75,-0.6,0]); % shift within main body
    v7 = TransformVertices(v7,Body312dc([0,0,phi-theta]), [x,0,y]);   % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v7,f7,c7);              
    v_ = TransformVertices(upperActuatorVertices,Body312dc([0,0,-ualphaLH]),[0,0,0]); % rotate
    v7 = TransformVertices(v_,diag([-1,+1,1]), [-0.75,+0.6,0]); % shift within main body
    v7 = TransformVertices(v7,Body312dc([0,0,phi+theta]), [x,0,y]);   % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v7,f7,c7);
    v7 = TransformVertices(v_,diag([-1,-1,1]), [-0.75,-0.6,0]); % shift within main body
    v7 = TransformVertices(v7,Body312dc([0,0,phi+theta]), [x,0,y]);   % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v7,f7,c7);              
    v_ = TransformVertices(upperActuatorVertices,Body312dc([0,0,-ualphaRH]),[0,0,0]); % rotate
    v7 = TransformVertices(v_,diag([-1,+1,1]), [-0.75,+0.6,0]); % shift within main body
    v7 = TransformVertices(v7,Body312dc([0,0,phi+theta]), [x,0,y]);   % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v7,f7,c7);
    v7 = TransformVertices(v_,diag([-1,-1,1]), [-0.75,-0.6,0]); % shift within main body
    v7 = TransformVertices(v7,Body312dc([0,0,phi+theta]), [x,0,y]);   % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v7,f7,c7);              
    %% Upper springs (bent& compress, rotate and shift side wards)
    delta_alphaLF = atan2(0.26,0.1) + (ualphaLF-alphaLF);
    % Bend including compression:
    v_ = BendVertices(upperSpringVertices,0.25/(delta_alphaLF)*2*pi);
    v_ = TransformVertices(v_,Body312dc([0,0,ualphaLF]),[0,0,0]); % rotate
    v4 = TransformVertices(v_,diag([+1,+1,1]), [+0.75,+0.6,0]); % shift within main body
    v4 = TransformVertices(v4,Body312dc([0,0,phi-theta]), [x,0,y]);   % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v4,f4,c4);
    v4 = TransformVertices(v_,diag([+1,-1,1]), [+0.75,-0.6,0]); % shift within main body
    v4 = TransformVertices(v4,Body312dc([0,0,phi-theta]), [x,0,y]);   % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v4,f4,c4);
    delta_alphaRF = atan2(0.26,0.1) + (ualphaRF-alphaRF);
    % Bend including compression:
    v_ = BendVertices(upperSpringVertices,0.25/(delta_alphaRF)*2*pi);
    v_ = TransformVertices(v_,Body312dc([0,0,ualphaRF]),[0,0,0]); % rotate
    v4 = TransformVertices(v_,diag([+1,+1,1]), [+0.75,+0.6,0]); % shift within main body
    v4 = TransformVertices(v4,Body312dc([0,0,phi-theta]), [x,0,y]);   % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v4,f4,c4);
    v4 = TransformVertices(v_,diag([+1,-1,1]), [+0.75,-0.6,0]); % shift within main body
    v4 = TransformVertices(v4,Body312dc([0,0,phi-theta]), [x,0,y]);   % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v4,f4,c4);
    
    delta_alphaLH = atan2(0.26,0.1) - (ualphaLH-alphaLH);
    % Bend including compression:
    v_ = BendVertices(upperSpringVertices,0.25/(delta_alphaLH)*2*pi);
    v_ = TransformVertices(v_,Body312dc([0,0,-ualphaLH]),[0,0,0]); % rotate
    v4 = TransformVertices(v_,diag([-1,+1,1]), [-0.75,+0.6,0]); % shift within main body
    v4 = TransformVertices(v4,Body312dc([0,0,phi+theta]), [x,0,y]);   % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v4,f4,c4);
    v4 = TransformVertices(v_,diag([-1,-1,1]), [-0.75,-0.6,0]); % shift within main body
    v4 = TransformVertices(v4,Body312dc([0,0,phi+theta]), [x,0,y]);   % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v4,f4,c4);
    delta_alphaRH = atan2(0.26,0.1) - (ualphaRH-alphaRH);
    % Bend including compression:
    v_ = BendVertices(upperSpringVertices,0.25/(delta_alphaRH)*2*pi);
    v_ = TransformVertices(v_,Body312dc([0,0,-ualphaRH]),[0,0,0]); % rotate
    v4 = TransformVertices(v_,diag([-1,+1,1]), [-0.75,+0.6,0]); % shift within main body
    v4 = TransformVertices(v4,Body312dc([0,0,phi+theta]), [x,0,y]);   % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v4,f4,c4);
    v4 = TransformVertices(v_,diag([-1,-1,1]), [-0.75,-0.6,0]); % shift within main body
    v4 = TransformVertices(v4,Body312dc([0,0,phi+theta]), [x,0,y]);   % shift with main body
    [v,f,c]=AddPatchesWithColor(v,f,c,v4,f4,c4);
    
    if nargout == 0
        figure;
        p = patch('faces', f, 'vertices' ,v,'FaceVertexCData',c,'FaceColor','flat');
        view(3);
        axis equal;
        set(p, 'FaceLighting','phong');                                            % Set the renderer
        set(p, 'FaceColor','flat');                                                % Set the face coloring
        set(p, 'EdgeColor','none'); 
        camlight right
    end
end
% *************************************************************************
% *************************************************************************