
%% Computer dependent settings:
clc
clear

% Possibility one: Lab Computer
PathToMuscod  = '/home/yevyes/MUSCOD-II/MC2/Release/bin/muscod';
PathTo2Dqrad   = '/home/yevyes/Yev_Will/GallopActive/qradru_model_flex/';
PathToMatlab = '/home/yevyes/Yev_Will/GallopActive/qradru_matlab_flex/';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Gallopting
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Create intial datStruct with no constraints to converge on a
%gait

datStruct = CreateDatStruct('Gallop','SEA','SEA');
% load('datStruct_GallopInit.mat')
% datStruct.h = datStruct_GallopInit.h;
% datStruct.sd = datStruct_GallopInit.sd;
% datStruct.u = datStruct_GallopInit.u;
% datStruct.sd_fix(9) = 0;  % Active Case
% datStruct.sd_fix_init(9) = 0;   % Active Case


% load('datStruct_GallopInit2.mat')
% datStruct.p(14) = .7;
% datStruct.p_name{14} = 'mu';
% datStruct.p_comment{14} = 'friction';
% datStruct.p_sca(14) = 1;
% datStruct.p_min(14) = 0;z
% datStruct.p_max(14) = 100;
% datStruct.p_fix(14) = 1; 

datStruct.p(4) = .7; %enforce average velocity
datStruct.p(5) = 15; %enforce friction cone
datStruct.p(13) = 1; %Actuated Spine
datStruct.p(14) = 1; %static friction coefficient


% Make all changes to the datStruct above this line
save('BaseData/Gallop_SH_SL_BASE_00.mat', 'datStruct');
CreateDatFile('BaseData/Gallop_SH_SL_BASE_00',datStruct); %Save datStruct to be run in Muscod

% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Gallop_*_BASE_00.dat ', PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_11(1), cmdout] = system([PathToMuscod,' Gallop_SH_SL_BASE_00']);
disp(MuscodResults_11(1))

cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Gallop_*_BASE_00.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

%%
% PlotMuscodResults('BaseData/Gallop_SH_SL_BASE_00',[7,9]);
%%
% PlayMuscodResults('BaseData/Gallop_SH_SL_BASE_00');


%% Parameter Study for Gallopting
cd(PathToMatlab)
datStruct = UpdateDatStruct(datStruct,'BaseData/Gallop_SH_SL_BASE_00', 30,45,9);

% Make all changes to the datStruct above this line
% save('BaseData/Gallop_SH_SL_BASE_03.mat', 'datStruct');

minVel = 0.025;
maxVel = 4.0;
deltaVel = 0.025;
VelVec = minVel:deltaVel:maxVel;

CreateParameterStudy('ParameterStudies/Gallop_SH_SL_BASE_ActiveFricCone_Velocity',datStruct,'p',4,VelVec);
[CostMatval,forces, states, time, hval] = ProcessParameterStudy_ANNOTATED('ParameterStudies/Gallop_SH_SL_BASE_ActiveFricCone_Velocity', PathTo2Dqrad, PathToMuscod);
load('ParameterStudies/Gallop_SH_SL_BASE_ActiveFricCone_Velocity');
save('GallopFlexFrictionCone_Active');
%% Plot Parameter Study For Gallopting

% %load('ParameterStudies/Gallop_SH_SL_BASE_02_Velocity','configurations','types','indices','grids');
% count = zeros(length(grids{1}),1);
% figure(3);
% %clf
% hold on
% box on
% grid on
% x = [];
% y = [];
% for i = 1:length(configurations)
%     count(configurations{i}.indexVector(1))=count(configurations{i}.indexVector(1))+1;
%     if(configurations{i}.nFailed < 5)
%         if(configurations{i}.finalDatStruct.p(4) > 0.2 && (configurations{i}.costValue < 0.4));
%             %plot(grids{1}(configurations{i}.indexVector(1)),configurations{i}.costValue,'*r');
%             x = [x,grids{1}(configurations{i}.indexVector(1))];
%             y = [y,configurations{i}.costValue];
%         end
%       
%     end
% end
% z = [x;y];
% [tmp ind] = sort(z(1,:));
% x = z(1,ind);
% y = z(2,ind);
% plot(x,y,'--k');
% legend('Gallopting');
