/*
 * PB_Constraints.cpp
 *
 * Common Constraint code for a Prismatic Biped.
 *
 *  Created on: April 14, 2013
 *      Author: cdremy@umich.edu
 */

#include "PB_Constraints.h"
#include <stdio.h>
#include "model.hpp"

/*********************************
 * FlowMaps
 *********************************/
void ffcn_flowLHLFRFRH(double *t, double *xd, double *xa, double *u, double *p_free,
		double *rhs, double *rwh, long *iwh, InfoPtr *info) {
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(xd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	// Compute Flow Map:
	syst.FlowMap(rhs);
}
void ffcn_flowlhLFRFRH(double *t, double *xd, double *xa, double *u, double *p_free,
		double *rhs, double *rwh, long *iwh, InfoPtr *info) {
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(xd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	// Compute Flow Map:
	syst.FlowMap(rhs);
}
void ffcn_flowLHlfRFRH(double *t, double *xd, double *xa, double *u, double *p_free,
		double *rhs, double *rwh, long *iwh, InfoPtr *info) {
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(xd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	// Compute Flow Map:
	syst.FlowMap(rhs);
}
void ffcn_flowlhlfRFRH(double *t, double *xd, double *xa, double *u, double *p_free,
		double *rhs, double *rwh, long *iwh, InfoPtr *info) {
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(xd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	// Compute Flow Map:
	syst.FlowMap(rhs);
}
void ffcn_flowLHLFrfRH(double *t, double *xd, double *xa, double *u, double *p_free,
		double *rhs, double *rwh, long *iwh, InfoPtr *info) {
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(xd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	// Compute Flow Map:
	syst.FlowMap(rhs);
}
void ffcn_flowlhLFrfRH(double *t, double *xd, double *xa, double *u, double *p_free,
		double *rhs, double *rwh, long *iwh, InfoPtr *info) {
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(xd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	// Compute Flow Map:
	syst.FlowMap(rhs);
}
void ffcn_flowLHlfrfRH(double *t, double *xd, double *xa, double *u, double *p_free,
		double *rhs, double *rwh, long *iwh, InfoPtr *info) {
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(xd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	// Compute Flow Map:
	syst.FlowMap(rhs);
}
void ffcn_flowlhlfrfRH(double *t, double *xd, double *xa, double *u, double *p_free,
		double *rhs, double *rwh, long *iwh, InfoPtr *info) {
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(xd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	// Compute Flow Map:
	syst.FlowMap(rhs);
}
void ffcn_flowLHLFRFrh(double *t, double *xd, double *xa, double *u, double *p_free,
		double *rhs, double *rwh, long *iwh, InfoPtr *info) {
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(xd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	// Compute Flow Map:
	syst.FlowMap(rhs);
}
void ffcn_flowlhLFRFrh(double *t, double *xd, double *xa, double *u, double *p_free,
		double *rhs, double *rwh, long *iwh, InfoPtr *info) {
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(xd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	// Compute Flow Map:
	syst.FlowMap(rhs);
}
void ffcn_flowLHlfRFrh(double *t, double *xd, double *xa, double *u, double *p_free,
		double *rhs, double *rwh, long *iwh, InfoPtr *info) {
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(xd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	// Compute Flow Map:
	syst.FlowMap(rhs);
}
void ffcn_flowlhlfRFrh(double *t, double *xd, double *xa, double *u, double *p_free,
		double *rhs, double *rwh, long *iwh, InfoPtr *info) {
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(xd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	// Compute Flow Map:
	syst.FlowMap(rhs);
}
void ffcn_flowLHLFrfrh(double *t, double *xd, double *xa, double *u, double *p_free,
		double *rhs, double *rwh, long *iwh, InfoPtr *info) {
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(xd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	// Compute Flow Map:
	syst.FlowMap(rhs);
}
void ffcn_flowlhLFrfrh(double *t, double *xd, double *xa, double *u, double *p_free,
		double *rhs, double *rwh, long *iwh, InfoPtr *info) {
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(xd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	// Compute Flow Map:
	syst.FlowMap(rhs);
}
void ffcn_flowLHlfrfrh(double *t, double *xd, double *xa, double *u, double *p_free,
		double *rhs, double *rwh, long *iwh, InfoPtr *info) {
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(xd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	// Compute Flow Map:
	syst.FlowMap(rhs);
}
void ffcn_flowlhlfrfrh(double *t, double *xd, double *xa, double *u, double *p_free,
		double *rhs, double *rwh, long *iwh, InfoPtr *info) {
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(xd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	// Compute Flow Map:
	syst.FlowMap(rhs);
}

/*********************************
 * JumpMaps
 *********************************/
// The outcome of the collisions doesn't depend on the previous
// contact configuration, so we just need one function per touchdown event
// Touchdown collision of the right leg
void ffcn_collisionLHLFRFRH(double *t, double *xd, double *xa, double *u,
		double *p_free, double *rhs, double *rwh, long *iwh, InfoPtr *info) {
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(xd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	// Compute Jump Map:
	syst.JumpMap(rhs);
}
void ffcn_collisionlhLFRFRH(double *t, double *xd, double *xa, double *u,
		double *p_free, double *rhs, double *rwh, long *iwh, InfoPtr *info) {
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(xd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	// Compute Jump Map:
	syst.JumpMap(rhs);
}
void ffcn_collisionLHlfRFRH(double *t, double *xd, double *xa, double *u,
		double *p_free, double *rhs, double *rwh, long *iwh, InfoPtr *info) {
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(xd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	// Compute Jump Map:
	syst.JumpMap(rhs);
}
void ffcn_collisionlhlfRFRH(double *t, double *xd, double *xa, double *u,
		double *p_free, double *rhs, double *rwh, long *iwh, InfoPtr *info) {
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(xd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	// Compute Jump Map:
	syst.JumpMap(rhs);
}
void ffcn_collisionLHLFrfRH(double *t, double *xd, double *xa, double *u,
		double *p_free, double *rhs, double *rwh, long *iwh, InfoPtr *info) {
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(xd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	// Compute Jump Map:
	syst.JumpMap(rhs);
}
void ffcn_collisionlhLFrfRH(double *t, double *xd, double *xa, double *u,
		double *p_free, double *rhs, double *rwh, long *iwh, InfoPtr *info) {
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(xd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	// Compute Jump Map:
	syst.JumpMap(rhs);
}
void ffcn_collisionLHlfrfRH(double *t, double *xd, double *xa, double *u,
		double *p_free, double *rhs, double *rwh, long *iwh, InfoPtr *info) {
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(xd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	// Compute Jump Map:
	syst.JumpMap(rhs);
}
void ffcn_collisionlhlfrfRH(double *t, double *xd, double *xa, double *u,
		double *p_free, double *rhs, double *rwh, long *iwh, InfoPtr *info) {
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(xd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	// Compute Jump Map:
	syst.JumpMap(rhs);
}
void ffcn_collisionLHLFRFrh(double *t, double *xd, double *xa, double *u,
		double *p_free, double *rhs, double *rwh, long *iwh, InfoPtr *info) {
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(xd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	// Compute Jump Map:
	syst.JumpMap(rhs);
}
void ffcn_collisionlhLFRFrh(double *t, double *xd, double *xa, double *u,
		double *p_free, double *rhs, double *rwh, long *iwh, InfoPtr *info) {
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(xd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	// Compute Jump Map:
	syst.JumpMap(rhs);
}
void ffcn_collisionLHlfRFrh(double *t, double *xd, double *xa, double *u,
		double *p_free, double *rhs, double *rwh, long *iwh, InfoPtr *info) {
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(xd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	// Compute Jump Map:
	syst.JumpMap(rhs);
}
void ffcn_collisionlhlfRFrh(double *t, double *xd, double *xa, double *u,
		double *p_free, double *rhs, double *rwh, long *iwh, InfoPtr *info) {
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(xd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	// Compute Jump Map:
	syst.JumpMap(rhs);
}
void ffcn_collisionLHLFrfrh(double *t, double *xd, double *xa, double *u,
		double *p_free, double *rhs, double *rwh, long *iwh, InfoPtr *info) {
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(xd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	// Compute Jump Map:
	syst.JumpMap(rhs);
}
void ffcn_collisionlhLFrfrh(double *t, double *xd, double *xa, double *u,
		double *p_free, double *rhs, double *rwh, long *iwh, InfoPtr *info) {
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(xd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	// Compute Jump Map:
	syst.JumpMap(rhs);
}
void ffcn_collisionLHlfrfrh(double *t, double *xd, double *xa, double *u,
		double *p_free, double *rhs, double *rwh, long *iwh, InfoPtr *info) {
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(xd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	// Compute Jump Map:
	syst.JumpMap(rhs);
}

/*********************************
 * JumpSets
 *********************************/
// Since the detection of touchdown and liftoff depends on the current
// contact configuration, every function is provided with two versions:
// capital means, the leg is on the ground, small it is in the air
//************* liftoffLH **********************
void rdfcn_liftoffLH_LHLFRFRH(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[liftoffLH];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_liftoffLH_LHlfRFRH(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[liftoffLH];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_liftoffLH_LHLFrfRH(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[liftoffLH];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_liftoffLH_LHlfrfRH(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[liftoffLH];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_liftoffLH_LHLFRFrh(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[liftoffLH];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_liftoffLH_LHlfRFrh(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[liftoffLH];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_liftoffLH_LHLFrfrh(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[liftoffLH];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_liftoffLH_LHlfrfrh(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[liftoffLH];
	computeConstraints(p_free[const_sel_free], 1, res);
}
//************* liftoffLF **********************
void rdfcn_liftoffLF_LHLFRFRH(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[liftoffLF];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_liftoffLF_lhLFRFRH(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[liftoffLF];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_liftoffLF_LHLFrfRH(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[liftoffLF];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_liftoffLF_lhLFrfRH(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[liftoffLF];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_liftoffLF_LHLFRFrh(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[liftoffLF];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_liftoffLF_lhLFRFrh(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[liftoffLF];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_liftoffLF_LHLFrfrh(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[liftoffLF];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_liftoffLF_lhLFrfrh(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[liftoffLF];
	computeConstraints(p_free[const_sel_free], 1, res);
}

//************* liftoffRF **********************
void rdfcn_liftoffRF_LHLFRFRH(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[liftoffRF];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_liftoffRF_lhLFRFRH(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[liftoffRF];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_liftoffRF_LHlfRFRH(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[liftoffRF];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_liftoffRF_lhlfRFRH(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[liftoffRF];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_liftoffRF_LHLFRFrh(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[liftoffRF];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_liftoffRF_lhLFRFrh(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[liftoffRF];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_liftoffRF_LHlfRFrh(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[liftoffRF];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_liftoffRF_lhlfRFrh(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[liftoffRF];
	computeConstraints(p_free[const_sel_free], 1, res);
}
//************* liftoffRH **********************
void rdfcn_liftoffRH_LHLFRFRH(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[liftoffRH];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_liftoffRH_lhLFRFRH(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[liftoffRH];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_liftoffRH_LHlfRFRH(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[liftoffRH];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_liftoffRH_lhlfRFRH(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[liftoffRH];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_liftoffRH_LHLFrfRH(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[liftoffRH];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_liftoffRH_lhLFrfRH(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[liftoffRH];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_liftoffRH_LHlfrfRH(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[liftoffRH];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_liftoffRH_lhlfrfRH(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[liftoffRH];
	computeConstraints(p_free[const_sel_free], 1, res);
}
//************* touchdownlh **********************
void rdfcn_touchdownlh_lhlfrfrh(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[touchdownLH];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_touchdownlh_lhLHrfrh(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[touchdownLH];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_touchdownlh_lhlfRFrh(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[touchdownLH];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_touchdownlh_lhLHRFrh(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[touchdownLH];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_touchdownlh_lhlfrfRH(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[touchdownLH];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_touchdownlh_lhLHrfRH(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[touchdownLH];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_touchdownlh_lhlfRFRH(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[touchdownLH];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_touchdownlh_lhLHRFRH(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[touchdownLH];
	computeConstraints(p_free[const_sel_free], 1, res);
}
//************* touchdownlf **********************
void rdfcn_touchdownlf_lhlfrfrh(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[touchdownLF];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_touchdownlf_LHlfrfrh(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[touchdownLF];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_touchdownlf_lhlfRFrh(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[touchdownLF];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_touchdownlf_LHlfRFrh(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[touchdownLF];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_touchdownlf_lhlfrfRH(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[touchdownLF];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_touchdownlf_LHlfrfRH(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[touchdownLF];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_touchdownlf_lhlfRFRH(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[touchdownLF];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_touchdownlf_LHlfRFRH(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[touchdownLF];
	computeConstraints(p_free[const_sel_free], 1, res);
}
//************* touchdownrf **********************
void rdfcn_touchdownrf_lhlfrfrh(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[touchdownRF];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_touchdownrf_LHlfrfrh(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[touchdownRF];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_touchdownrf_lhLFrfrh(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[touchdownRF];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_touchdownrf_LHLFrfrh(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[touchdownRF];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_touchdownrf_lhlfrfRH(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[touchdownRF];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_touchdownrf_LHlfrfRH(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[touchdownRF];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_touchdownrf_lhLFrfRH(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[touchdownRF];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_touchdownrf_LHLFrfRH(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[touchdownRF];
	computeConstraints(p_free[const_sel_free], 1, res);
}
//************* touchdownrh **********************
void rdfcn_touchdownrh_lhlfrfrh(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[touchdownRH];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_touchdownrh_LHlfrfrh(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[touchdownRH];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_touchdownrh_lhLFrfrh(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[touchdownRH];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_touchdownrh_LHLFrfrh(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[touchdownRH];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_touchdownrh_lhlfRFrh(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[touchdownRH];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_touchdownrh_LHlfRFrh(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[touchdownRH];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_touchdownrh_lhLFRFrh(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[touchdownRH];
	computeConstraints(p_free[const_sel_free], 1, res);
}
void rdfcn_touchdownrh_LHLFRFrh(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[touchdownRH];
	computeConstraints(p_free[const_sel_free], 1, res);
}

// *********************** two legs change together ************************
void rdfcn_liftoffLHRF_LHlfRFrh(double *ts, double *sd, double *sa, double *u, double *p_free,
		double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[liftoffLH];
	res[1] = eventVal[liftoffRF];
	computeConstraints(p_free[const_sel_free], 2, res);
}
void rdfcn_liftoffLFRH_lhLFrfRH(double *ts, double *sd, double *sa, double *u, double *p_free,
		double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[liftoffLF];
	res[1] = eventVal[liftoffRH];
	computeConstraints(p_free[const_sel_free], 2, res);
}
void rdfcn_liftoffLHRH_LHlfrfRH(double *ts, double *sd, double *sa, double *u, double *p_free,
		double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[liftoffLH];
	res[1] = eventVal[liftoffRH];
	computeConstraints(p_free[const_sel_free], 2, res);
}
void rdfcn_liftoffLFRF_lhLFRFrh(double *ts, double *sd, double *sa, double *u, double *p_free,
		double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[liftoffLF];
	res[1] = eventVal[liftoffRF];
	computeConstraints(p_free[const_sel_free], 2, res);
}
void rdfcn_touchdownlfrh_lhlfrfrh(double *ts, double *sd, double *sa, double *u, double *p_free,
		double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[touchdownLF];
	res[1] = eventVal[touchdownRH];
	computeConstraints(p_free[const_sel_free], 2, res);
}
void rdfcn_touchdownlhrf_lhlfrfrh(double *ts, double *sd, double *sa, double *u, double *p_free,
		double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[touchdownLH];
	res[1] = eventVal[touchdownRF];
	computeConstraints(p_free[const_sel_free], 2, res);
}
void rdfcn_touchdownlfrf_lhlfrfrh(double *ts, double *sd, double *sa, double *u, double *p_free,
		double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[touchdownLF];
	res[1] = eventVal[touchdownRF];
	computeConstraints(p_free[const_sel_free], 2, res);
}
void rdfcn_touchdownlhrh_lhlfrfrh(double *ts, double *sd, double *sa, double *u, double *p_free,
		double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[touchdownLH];
	res[1] = eventVal[touchdownRH];
	computeConstraints(p_free[const_sel_free], 2, res);
}


void rdfcn_liftoffLFRH_LHLFRFRH(double *ts, double *sd, double *sa, double *u, double *p_free,
		double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = stance;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[liftoffLF];
	res[1] = eventVal[liftoffRH];
	computeConstraints(p_free[const_sel_free], 2, res);
}


void rdfcn_touchdownlfrh_LHlfRFrh(double *ts, double *sd, double *sa, double *u, double *p_free,
		double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = flight;
	// Update States:
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	double eventVal[NEV];
	syst.JumpSet(eventVal);
	res[0] = eventVal[touchdownLF];
	res[1] = eventVal[touchdownRH];
	computeConstraints(p_free[const_sel_free], 2, res);
}

/*********************************
 * Periodicity Constraints
 *********************************/
// Periodicity constraint at the beginning of the stride
void rcfcn_beginning(double *ts, double *sd, double *sa, double *u, double *p_free,
		double *pr, double *res, long *dpnd, InfoPtr *info) {
	// Sum all periodic values in positive direction
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, 0, 0);
		return;
	}
	res[dx_const]       = sd[dx];
	res[y_const]        = sd[y];
	res[dy_const]       = sd[dy];
	res[phi_const]      = sd[phi];
	res[dphi_const]     = sd[dphi];
	res[alphaLH_const]   = sd[alphaLH];
	res[dalphaLH_const]  = sd[dalphaLH];
	res[ualphaLH_const]  = sd[ualphaLH];
	res[dualphaLH_const] = sd[dualphaLH];
	res[alphaLF_const]   = sd[alphaLF];
	res[dalphaLF_const]  = sd[dalphaLF];
	res[ualphaLF_const]  = sd[ualphaLF];
	res[dualphaLF_const] = sd[dualphaLF];
	res[alphaRF_const]   = sd[alphaRF];
	res[dalphaRF_const]  = sd[dalphaRF];
	res[ualphaRF_const]  = sd[ualphaRF];
	res[dualphaRF_const] = sd[dualphaRF];
	res[alphaRH_const]   = sd[alphaRH];
	res[dalphaRH_const]  = sd[dalphaRH];
	res[ualphaRH_const]  = sd[ualphaRH];
	res[dualphaRH_const] = sd[dualphaRH];
	res[lLH_const]       = sd[lLH];
	res[dlLH_const]      = sd[dlLH];
	res[ulLH_const]      = sd[ulLH];
	res[dulLH_const]     = sd[dulLH];
	res[lLF_const]       = sd[lLF];
	res[dlLF_const]      = sd[dlLF];
	res[ulLF_const]      = sd[ulLF];
	res[dulLF_const]     = sd[dulLF];
	res[lRF_const]       = sd[lRF];
	res[dlRF_const]      = sd[dlRF];
	res[ulRF_const]      = sd[ulRF];
	res[dulRF_const]     = sd[dulRF];
	res[lRH_const]       = sd[lRH];
	res[dlRH_const]      = sd[dlRH];
	res[ulRH_const]      = sd[ulRH];
	res[dulRH_const]     = sd[dulRH];
	res[TalphaLH_const]  = u[TalphaLH];
	res[TalphaLF_const]  = u[TalphaLF];
	res[TalphaRF_const]  = u[TalphaRF];
	res[TalphaRH_const]  = u[TalphaRH];
	res[FlLH_const]      = u[FlLH];
	res[FlLF_const]      = u[FlLF];
	res[FlRF_const]      = u[FlRF];
	res[FlRH_const]      = u[FlRH];
}
// Periodicity constraint at the middle of the stride that switches left and right leg
void rcfcn_endSymmetric(double *ts, double *sd, double *sa, double *u,
		double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, 0, 0);
		return;
	}
	// Sum all periodic values in negative direction
	// in the constraints, we have to flip left and right:
	res[dx_const]       = -sd[dx];
	res[y_const]        = -sd[y];
	res[dy_const]       = -sd[dy];
	res[phi_const]      = -sd[phi];
	res[dphi_const]     = -sd[dphi];
	res[alphaLH_const]   = -sd[alphaRH];
	res[dalphaLH_const]  = -sd[dalphaRH];
	res[ualphaLH_const]  = -sd[ualphaRH];
	res[dualphaLH_const] = -sd[dualphaRH];
	res[alphaLF_const]   = -sd[alphaRF];
	res[dalphaLF_const]  = -sd[dalphaRF];
	res[ualphaLF_const]  = -sd[ualphaRF];
	res[dualphaLF_const] = -sd[dualphaRF];
	res[alphaRF_const]   = -sd[alphaLF];
	res[dalphaRF_const]  = -sd[dalphaLF];
	res[ualphaRF_const]  = -sd[ualphaLF];
	res[dualphaRF_const] = -sd[dualphaLF];
	res[alphaRH_const]   = -sd[alphaLH];
	res[dalphaRH_const]  = -sd[dalphaLH];
	res[ualphaRH_const]  = -sd[ualphaLH];
	res[dualphaRH_const] = -sd[dualphaLH];
	res[lLH_const]       = -sd[lRH];
	res[dlLH_const]      = -sd[dlRH];
	res[ulLH_const]      = -sd[ulRH];
	res[dulLH_const]     = -sd[dulRH];
	res[lLF_const]       = -sd[lRF];
	res[dlLF_const]      = -sd[dlRF];
	res[ulLF_const]      = -sd[ulRF];
	res[dulLF_const]     = -sd[dulRF];
	res[lRF_const]       = -sd[lLF];
	res[dlRF_const]      = -sd[dlLF];
	res[ulRF_const]      = -sd[ulLF];
	res[dulRF_const]     = -sd[dulLF];
	res[lRH_const]       = -sd[lLH];
	res[dlRH_const]      = -sd[dlLH];
	res[ulRH_const]      = -sd[ulLH];
	res[dulRH_const]     = -sd[dulLH];
	res[TalphaLH_const]  = -u[TalphaRH];
	res[TalphaLF_const]  = -u[TalphaRF];
	res[TalphaRF_const]  = -u[TalphaLF];
	res[TalphaRH_const]  = -u[TalphaLH];
	res[FlLH_const]      = -u[FlRH];
	res[FlLF_const]      = -u[FlRF];
	res[FlRF_const]      = -u[FlLF];
	res[FlRH_const]      = -u[FlLH];
}
// Periodicity constraint at the end of the stride
void rcfcn_endPeriodic(double *ts, double *sd, double *sa, double *u, double *p_free,
		double *pr, double *res, long *dpnd, InfoPtr *info) {
	// Sum all periodic values in negative direction
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, 0, 0);
		return;
	}
	res[dx_const]       = -sd[dx];
	res[y_const]        = -sd[y];
	res[dy_const]       = -sd[dy];
	res[phi_const]      = -sd[phi];
	res[dphi_const]     = -sd[dphi];
	res[alphaLH_const]   = -sd[alphaLH];
	res[dalphaLH_const]  = -sd[dalphaLH];
	res[ualphaLH_const]  = -sd[ualphaLH];
	res[dualphaLH_const] = -sd[dualphaLH];
	res[alphaLF_const]   = -sd[alphaLF];
	res[dalphaLF_const]  = -sd[dalphaLF];
	res[ualphaLF_const]  = -sd[ualphaLF];
	res[dualphaLF_const] = -sd[dualphaLF];
	res[alphaRF_const]   = -sd[alphaRF];
	res[dalphaRF_const]  = -sd[dalphaRF];
	res[ualphaRF_const]  = -sd[ualphaRF];
	res[dualphaRF_const] = -sd[dualphaRF];
	res[alphaRH_const]   = -sd[alphaRH];
	res[dalphaRH_const]  = -sd[dalphaRH];
	res[ualphaRH_const]  = -sd[ualphaRH];
	res[dualphaRH_const] = -sd[dualphaRH];
	res[lLH_const]       = -sd[lLH];
	res[dlLH_const]      = -sd[dlLH];
	res[ulLH_const]      = -sd[ulLH];
	res[dulLH_const]     = -sd[dulLH];
	res[lLF_const]       = -sd[lLF];
	res[dlLF_const]      = -sd[dlLF];
	res[ulLF_const]      = -sd[ulLF];
	res[dulLF_const]     = -sd[dulLF];
	res[lRF_const]       = -sd[lRF];
	res[dlRF_const]      = -sd[dlRF];
	res[ulRF_const]      = -sd[ulRF];
	res[dulRF_const]     = -sd[dulRF];
	res[lRH_const]       = -sd[lRH];
	res[dlRH_const]      = -sd[dlRH];
	res[ulRH_const]      = -sd[ulRH];
	res[dulRH_const]     = -sd[dulRH];
	res[TalphaLH_const]  = -u[TalphaLH];
	res[TalphaLF_const]  = -u[TalphaLF];
	res[TalphaRF_const]  = -u[TalphaRF];
	res[TalphaRH_const]  = -u[TalphaRH];
	res[FlLH_const]      = -u[FlLH];
	res[FlLF_const]      = -u[FlLF];
	res[FlRF_const]      = -u[FlRF];
	res[FlRH_const]      = -u[FlRH];
}

/*********************************
 * Other Constraints
 *********************************/
// Enforce the desired average speed:
void rdfcn_avgSpeed(double *ts, double *sd, double *sa, double *u, double *p_free,
		double *pr, double *res, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = RFCN_DPND(*ts, *sd, 0, 0, *p_free, 0);
		return;
	}
	res[0] = sd[x] - p_free[v_avg_free] * (*ts);
}

/*********************************
 * Interior point Constraints
 *********************************/
// Ground clearances:
// Since the computation of the constraints depends on the current contact configuration,
// four different configurations exist:
// capital means, the leg is on the ground, small it is in the air

void rdfcn_neConstraints_LHLFRFRH(double *ts, double *sd, double *sa, double *u,  double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info){
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = stance;
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	computeConstraints(p_free[const_sel_free], 0, res);
}
void rdfcn_neConstraints_lhLFRFRH(double *ts, double *sd, double *sa, double *u,  double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info){
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = stance;
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	computeConstraints(p_free[const_sel_free], 0, res);
}
void rdfcn_neConstraints_LHlfRFRH(double *ts, double *sd, double *sa, double *u,  double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info){
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = stance;
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	computeConstraints(p_free[const_sel_free], 0, res);
}
void rdfcn_neConstraints_lhlfRFRH(double *ts, double *sd, double *sa, double *u,  double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info){
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = stance;
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	computeConstraints(p_free[const_sel_free], 0, res);
}
void rdfcn_neConstraints_LHLFrfRH(double *ts, double *sd, double *sa, double *u,  double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info){
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = stance;
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	computeConstraints(p_free[const_sel_free], 0, res);
}
void rdfcn_neConstraints_lhLFrfRH(double *ts, double *sd, double *sa, double *u,  double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info){
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = stance;
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	computeConstraints(p_free[const_sel_free], 0, res);
}
void rdfcn_neConstraints_LHlfrfRH(double *ts, double *sd, double *sa, double *u,  double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info){
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = stance;
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	computeConstraints(p_free[const_sel_free], 0, res);
	//cout << "ConstraintFunctionDoubleStanceLHRH  "<< endl;
}
void rdfcn_neConstraints_lhlfrfRH(double *ts, double *sd, double *sa, double *u,  double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info){
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = stance;
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	computeConstraints(p_free[const_sel_free], 0, res);
}
void rdfcn_neConstraints_LHLFRFrh(double *ts, double *sd, double *sa, double *u,  double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info){
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = flight;
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	computeConstraints(p_free[const_sel_free], 0, res);
}
void rdfcn_neConstraints_lhLFRFrh(double *ts, double *sd, double *sa, double *u,  double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info){
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = flight;
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	computeConstraints(p_free[const_sel_free], 0, res);
}
void rdfcn_neConstraints_LHlfRFrh(double *ts, double *sd, double *sa, double *u,  double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info){
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = flight;
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	computeConstraints(p_free[const_sel_free], 0, res);
}
void rdfcn_neConstraints_lhlfRFrh(double *ts, double *sd, double *sa, double *u,  double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info){
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = stance;
	zVecPtr[phaseRH] = flight;
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	computeConstraints(p_free[const_sel_free], 0, res);
}
void rdfcn_neConstraints_LHLFrfrh(double *ts, double *sd, double *sa, double *u,  double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info){
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = flight;
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	computeConstraints(p_free[const_sel_free], 0, res);
}
void rdfcn_neConstraints_lhLFrfrh(double *ts, double *sd, double *sa, double *u,  double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info){
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = stance;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = flight;
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	computeConstraints(p_free[const_sel_free], 0, res);
}
void rdfcn_neConstraints_LHlfrfrh(double *ts, double *sd, double *sa, double *u,  double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info){
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = stance;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = flight;
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	computeConstraints(p_free[const_sel_free], 0, res);
}
void rdfcn_neConstraints_lhlfrfrh(double *ts, double *sd, double *sa, double *u,  double *p_free, double *pr, double *res, long *dpnd, InfoPtr *info){
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, *u, *p_free, 0);
		return;
	}
	// Set phases:
	double zVecPtr[NZ];
	zVecPtr[phaseLH] = flight;
	zVecPtr[phaseLF] = flight;
	zVecPtr[phaseRF] = flight;
	zVecPtr[phaseRH] = flight;
	syst.setContState(sd);
	syst.setDiscState(zVecPtr);
	double p[NP];
	convertParameters(p_free,p);
	syst.setSystParam(p);
	syst.setExctState(u);
	computeConstraints(p_free[const_sel_free], 0, res);
}




/*********************************
 * Objective Functions
 *********************************/
void mfcn_COT(double *ts, double *sd, double *sa, double *p_free, double *pr,
		double *mval, long *dpnd, InfoPtr *info) {
	if (*dpnd) {
		*dpnd = MFCN_DPND(0, *sd, 0, *p_free, 0);
		return;
	}
	switch(int(p_free[cost_fct_sel_free])){
	case 1: // Actuator Cost Of Transport
		if (sd[x] > 0.0001)
			*mval = sd[posActWork] / sd[x];
		else
			*mval = sd[posActWork];
		break;
	case 2: // Electrical Cost Of Transport
		if (sd[x] > 0.0001)
			*mval = sd[posElWork] / sd[x];
		else
			*mval = sd[posElWork];
		break;
	case 3: // Electrical Loss Cost Of Transport
		if (sd[x] > 0.0001)
			*mval = sd[totElLoss] / sd[x];
		else
			*mval = sd[totElLoss];
		break;
	default:
		cout << "Unknown parameter value for ""cost_fct_sel""." << endl;
		cout << "Must be 1 for posActWork" << endl;
		cout << "        2 for posElWork" << endl;
		cout << "        3 for totElLoss" << endl;
		break;
	}
}


/*********************************
 * Output Functions
 *********************************/
void figureview_output(long *imos, ///< index of model stage (I)
		long *imsn, ///< index of m.s. node on current model stage (I)
		double *ts, ///< time at m.s. node (I)
		double *te, ///< time at end of m.s. interval (I)
		double *sd, ///< differential states at m.s. node (I)
		double *sa, ///< algebraic states at m.s. node (I)
		double *u, ///< controls at m.s. node (I)
		double *udot, ///< control slopes at m.s. node (I)
		double *ue, ///< controls at end of m.s. interval (I)
		double *uedot, ///< control slopes at end of m.s. interval (I)
		double *p_free, ///< global model parameters (I)
		double *pr, ///< local i.p.c. parameters (I)
		double *ccxd, double *mul_ccxd, ///< multipliers of continuity conditions (I)
#if defined(PRSQP) || defined(EXTPRSQP)
		double *ares, double *mul_ares,
#endif
		double *rd, double *mul_rd, ///< multipliers of decoupled i.p.c. (I)
		double *rc, double *mul_rc, ///< multipliers of coupled i.p.c. (I)
		double *obj, double *rwh, ///< real work array (I)
		long *iwh ///< integer work array (I)
		) {
	FILE *fp;
	stringstream output_filename("");
	const char* fv_header_state_names = "# Time\n"
			"# LowerTrunk_TX\n"
			"# LowerTrunk_TY\n"
			"# LowerTrunk_RZ\n"
			"# UpperLeg_LH_RZ\n"
			"# UpperLeg_LH_TY\n"
			"# UpperLeg_LF_RZ\n"
			"# UpperLeg_LF_TY\n"
			"# UpperLeg_RF_RZ\n"
			"# UpperLeg_RF_TY\n"
			"# UpperLeg_RH_RZ\n"
			"# UpperLeg_RH_TY\n"
			"# LowerLeg_LH_RZ\n"
			"# LowerLeg_LH_TY\n"
			"# LowerLeg_LF_RZ\n"
			"# LowerLeg_LF_TY\n"
			"# LowerLeg_RF_RZ\n"
			"# LowerLeg_RF_TY\n"
			"# LowerLeg_RH_RZ\n"
			"# LowerLeg_RH_TY\n";
	// Filename
	output_filename << "./RES/" << figureview_file_name;

	/* Create and reset file when we're in the first MS-knot.
	 * otherwise just append data
	 */
	if ((*imsn == 0) && (*imos == 0)) {
		fp = fopen(output_filename.str().c_str(), "w");

		if (!fp) {
			fprintf(stderr, "Error: Could not open file '%s'!\n",
					output_filename.str().c_str());
			return;
		}

		// Write the header of the figureview file

		fprintf(
				fp,
				"# JAFV Format: DOF_0 DOF_1 ... DOF_N TAG0_x TAG0_y ... TAGN_z\n");
		fprintf(fp, "# Format for cal3dview:\n");

		fprintf(fp, "# MODEL hopper\n");
		fprintf(fp, "# SYMMETRIC_DATA\n");
		fprintf(fp, "# BEGIN_FORMAT\n");
		// the degrees of freedom
		fprintf(fp, "%s", fv_header_state_names);
		fprintf(fp, "# END_FORMAT\n");
		// More header information
		fprintf(
				fp,
				"#\n#\n# Prismatic biped hopper\n#\n# Variables per frame: \n#\n# %d\n#\n",
				1 + NQ + 2);
	} else {
		fp = fopen(output_filename.str().c_str(), "a");

		if (!fp) {
			fprintf(stderr, "Error: Could not open file '%s'!\n",
					output_filename.str().c_str());
			return;
		}
	}

	// Write a line of data:
	fprintf(fp, "%e\t", *ts);
	fprintf(fp, "%e\t", sd[x]);
	fprintf(fp, "%e\t", sd[y]);
	fprintf(fp, "%e\t", sd[phi]);
	fprintf(fp, "%e\t", sd[alphaLH]);
	fprintf(fp, "0\t");
	fprintf(fp, "%e\t", sd[alphaLF]);
	fprintf(fp, "0\t");
	fprintf(fp, "%e\t", sd[alphaRF]);
	fprintf(fp, "0\t");
	fprintf(fp, "%e\t", sd[alphaRH]);
	fprintf(fp, "0\t");
	fprintf(fp, "0\t");
	fprintf(fp, "%e\t", 1 - sd[lLH]);
	fprintf(fp, "0\t");
	fprintf(fp, "%e\t", 1 - sd[lLF]);
	fprintf(fp, "0\t");
	fprintf(fp, "%e\t", 1 - sd[lRF]);
	fprintf(fp, "0\t");
	fprintf(fp, "%e\t", 1 - sd[lRH]);
	fprintf(fp, "\n");
	// Close file
	fclose(fp);
}

void motion_output(long *imos, ///< index of model stage (I)
			long *imsn, ///< index of m.s. node on current model stage (I)
			double *ts, ///< time at m.s. node (I)
			double *te, ///< time at end of m.s. interval (I)
			double *sd, ///< differential states at m.s. node (I)
			double *sa, ///< algebraic states at m.s. node (I)
			double *u, ///< controls at m.s. node (I)
			double *udot, ///< control slopes at m.s. node (I)
			double *ue, ///< controls at end of m.s. interval (I)
			double *uedot, ///< control slopes at end of m.s. interval (I)
			double *p_free, ///< global model parameters (I)
			double *pr, ///< local i.p.c. parameters (I)
			double *ccxd, double *mul_ccxd, ///< multipliers of continuity conditions (I)
	#if defined(PRSQP) || defined(EXTPRSQP)
			double *ares, double *mul_ares,
	#endif
			double *rd, double *mul_rd, ///< multipliers of decoupled i.p.c. (I)
			double *rc, double *mul_rc, ///< multipliers of coupled i.p.c. (I)
			double *obj, double *rwh, ///< real work array (I)
			long *iwh ///< integer work array (I)
			) {
	FILE *fp;
	stringstream output_filename("");
	// Filename
	char problemname[50];
	get_pname(problemname);
	output_filename << "./RES/" << problemname <<".mot";
	if ((*imsn == 0) && (*imos == 0))
				fp = fopen(output_filename.str().c_str(), "w");
			else
				fp = fopen(output_filename.str().c_str(), "a");
			if (!fp) {
				fprintf(stderr, "Error: Could not open file '%s'!\n",
						output_filename.str().c_str());
				return;
			}
		/* Create and reset file when we're in the first MS-knot.
		 * otherwise just append data
		 */
		if ((*imsn == 0) && (*imos == 0)) {
			// write header:
			/// Get names of model quantities.
			char **xd_name[1];  /* differential state names (O) */
			char **xa_name[1];  /* algebraic state names (O) */
			char **u_name[1];   /* control names (O) */
			char **h_name[1];   /* model stage duration names (O) */
			char **p_name[1];   /* global model parameter names (O) */
			char *of_name[1];   /* objective name (O) */
			get_names(xd_name, xa_name, u_name, h_name, p_name, of_name);
			// Objective:
			fprintf(fp, "COT: %e\n", *obj);
			// Print parameters first:
			fprintf(fp, "//");
			for (int i = 0; i<NPFree; i++){
				fprintf(fp, "%s\t",p_name[0][i]);
			}
			fprintf(fp, "\n");
			for (int i = 0; i<NPFree; i++){
				fprintf(fp, "%e\t",p_free[i]);
			}
			fprintf(fp, "\n");
			// States and timing:
			fprintf(fp, "//imos\t");
			fprintf(fp, "imsn\t");
			fprintf(fp, "time\t");
			for (int i = 0; i<NY; i++){
				fprintf(fp, "%s\t",xd_name[0][i]);
			}
			for (int i = 0; i<NU; i++){
				fprintf(fp, "%s\t",u_name[0][i]);
			}
			fprintf(fp, "\n");
		}
		fprintf(fp, "%li\t", *imos);
		fprintf(fp, "%li\t", *imsn);
		fprintf(fp, "%e\t", *ts);
		for (int i = 0; i<NY; i++){
			fprintf(fp, "%e\t",sd[i]);
		}
		for (int i = 0; i<NU; i++){
			fprintf(fp, "%e\t",u[i]);
		}
		fprintf(fp, "\n");
		// Close file
		fclose(fp);
}

void plot_output(
  double *t,      ///< time (I)
  double *xd,     ///< differential states (I)
  double *xa,     ///< algebraic states (I)
  double *u,      ///< controls (I)
  double *p_free,      ///< global model parameters (I)
  double *rwh,    ///< real work array (I)
  long   *iwh,     ///< integer work array (I)
  InfoPtr *info
){
	FILE *fp;
	stringstream output_filename("");
	// Filename
	char problemname[50];
	get_pname(problemname);
	output_filename << "./RES/" << problemname <<".plt";
	if (*t == 0)
		fp = fopen(output_filename.str().c_str(), "w");
	else
		fp = fopen(output_filename.str().c_str(), "a");
	if (!fp) {
		fprintf(stderr, "Error: Could not open file '%s'!\n",
				output_filename.str().c_str());
		return;
	}
	if (*t == 0) {
		// write header:
		/// Get names of model quantities.
		char **xd_name[1];  /* differential state names (O) */
		char **xa_name[1];  /* algebraic state names (O) */
		char **u_name[1];   /* control names (O) */
		char **h_name[1];   /* model stage duration names (O) */
		char **p_name[1];   /* global model parameter names (O) */
		char *of_name[1];   /* objective name (O) */
		get_names(xd_name, xa_name, u_name, h_name, p_name, of_name);
		fprintf(fp, "//time\t");
		for (int i = 0; i<NY; i++){
			fprintf(fp, "%s\t",xd_name[0][i]);
		}
		for (int i = 0; i<NU; i++){
			fprintf(fp, "%s\t",u_name[0][i]);
		}
		fprintf(fp, "\n");
	}
	fprintf(fp, "%e\t", *t);
	for (int i = 0; i<NY; i++){
		fprintf(fp, "%e\t",xd[i]);
	}
	for (int i = 0; i<NU; i++){
		fprintf(fp, "%e\t",u[i]);
	}

	fprintf(fp, "\n");
	// Close file
	fclose(fp);
}


/*********************************
 * Misc Functions
 *********************************/
void convertParameters(double *p_free, double *p){
	/* this is a quite tricky function, since it maps a set of free
	 * parameters (which can be changed in MUSCOD) to the full parameter
	 * vector of the dynamic representation (or the other way around). We
	 * hence have to be very careful with the two different parameter enums
	 */
	p[hip_jnt_type] = p_free[hipJointType_free];
	p[leg_jnt_type] = p_free[legJointType_free];
	p[kalpha]       = p_free[kalpha_free];
	p[du_max_alpha] = p_free[du_max_alpha_free];
	p[kl]           = p_free[kl_free];
	p[du_max_l]     = p_free[du_max_l_free];
	p[sigma]	= p_free[sigma_free];

	p[balphaRat]    = 0.2;//p_free[balphaRat_free];
	p[rho_alpha]    = 0.5;//p_free[rho_alpha_free];
	p[blRat]        = 0.2;//p_free[blRat_free];
	p[rho_l]        = 0.5;//p_free[rho_l_free];
	p[g]     = 1.0;
	p[l_0]   = 1.0;
	p[m_0]   = 1.0;
	p[m1]    = 1.4;
	p[m2]    = 0.1;
	p[m3]    = 0.05;
	p[l1]    = 0.75;
	p[l2]    = 0.25;
	p[l3]    = 0.25;
	p[rFoot] = 0.05;
	p[j1_]   = 0.4;
	p[j2]    = 0.002;
	p[j3]    = 0.002;
	p[P_max] = 24.0;
	p[j_unsc]= 0.6;
	p[c_lim] = 0.07;
}

// Computes the inequality constraints on ground clearance (2),
// actuator torque(8) and actuator speed (8).  They can be turned on
// and off via the free parameter const_selFLAG, that takes binary flags
// as follows:
// 1 ground clearance constraint on
// 2 actuator torque constraint on
// 4 actuator velocity constraint on
// 'offset' defines, how many equality constraints are used in the res-vector
void computeConstraints(int const_selFLAG, int offset, double *res){
	double gcLH;
	double gcLF;
	double gcRF;
	double gcRH;
	double du_max[16];
	double F_max[16];
	syst.Constraints(&gcLH, &gcLF, &gcRF, &gcRH, du_max, F_max);
	// map to residuals:
	if (const_selFLAG & 1){
		res[0+offset] = gcLH;
		res[1+offset] = gcLF;
		res[2+offset] = gcRF;
		res[3+offset] = gcRH;
		cout << "GC active" << endl;

	} else {
		res[0+offset] = 1;
		res[1+offset] = 1;
		res[2+offset] = 1;
		res[3+offset] = 1;
		cout << "oh noooooo" << endl;
	}
	if (const_selFLAG & 2){
		for (int i = 0; i<16; i++){
			res[i+4+offset] = F_max[i];
		}
	} else {
		for (int i = 0; i<16; i++){
			res[i+4+offset] = 1;
		}

	}
	if (const_selFLAG & 4){
		for (int i = 0; i<16; i++){
			res[i+20+offset] = du_max[i];
		}
	} else {
		for (int i = 0; i<16; i++){
			res[i+20+offset] = 1;
		}
	}
}
