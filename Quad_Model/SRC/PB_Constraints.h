/*
 * PB_Constraints.h
 *
 * Common Constraint Header for a Prismatic Biped.
 *
 *  Created on: April 14, 2013
 *      Author: cdremy@umich.edu
 */

#include "PB_Dynamics.h"
#include "def_usrmod.hpp"

#ifndef PB_DDH_CONSTRAINTS_H_
#define PB_DDH_CONSTRAINTS_H_

// Create the instance of the model object
static PB_Dynamics syst;

/*********************************
 * FlowMaps
 *********************************/
// The Flow Maps are used as differential right hand side functions
// capital means on the ground, small means on the air. 2*2*2*2=16 different flowmap
void ffcn_flowLHLFRFRH(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void ffcn_flowlhLFRFRH(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void ffcn_flowLHlfRFRH(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void ffcn_flowlhlfRFRH(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void ffcn_flowLHLFrfRH(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void ffcn_flowlhLFrfRH(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void ffcn_flowLHlfrfRH(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void ffcn_flowlhlfrfRH(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void ffcn_flowLHLFRFrh(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void ffcn_flowlhLFRFrh(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void ffcn_flowLHlfRFrh(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void ffcn_flowlhlfRFrh(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void ffcn_flowLHLFrfrh(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void ffcn_flowlhLFrfrh(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void ffcn_flowLHlfrfrh(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void ffcn_flowlhlfrfrh(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);

/*********************************
 * JumpMaps
 *********************************/
// The Jump Maps are used as differential right hand side functions
void ffcn_collisionLHLFRFRH(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void ffcn_collisionlhLFRFRH(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void ffcn_collisionLHlfRFRH(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void ffcn_collisionlhlfRFRH(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void ffcn_collisionLHLFrfRH(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void ffcn_collisionlhLFrfRH(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void ffcn_collisionLHlfrfRH(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void ffcn_collisionlhlfrfRH(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void ffcn_collisionLHLFRFrh(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void ffcn_collisionlhLFRFrh(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void ffcn_collisionLHlfRFrh(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void ffcn_collisionlhlfRFrh(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void ffcn_collisionLHLFrfrh(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void ffcn_collisionlhLFrfrh(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void ffcn_collisionLHlfrfrh(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);

/*********************************
 * JumpSets
 *********************************/
// The Jummp Sets are used as start and end point constraints
// In addition to the constraints for lift-off and touchdown,
// constraints on ground clearance (2), actuator torque(8) and
// actuator speed (8) are enforced.  They can be turned on and
// off via the free parameter const_sel, that takes binary flags
// as follows:
// 1 ground clearance constraint on
// 2 actuator torque constraint on
// 4 actuator velocity constraint on
static int rdfcn_singleLeg_n  = 37;// 1+4+16+16=37
static int rdfcn_singleLeg_ne = 1;// 1
// Since the detection of touchdown and liftoff depends on the current
// contact configuration, every function is provided with two versions:
// capital means, the leg is on the ground, small it is in the air
void rdfcn_liftoffLH_LHLFRFRH(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_liftoffLH_LHlfRFRH(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_liftoffLH_LHLFrfRH(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_liftoffLH_LHlfrfRH(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_liftoffLH_LHLFRFrh(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_liftoffLH_LHlfRFrh(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_liftoffLH_LHLFrfrh(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_liftoffLH_LHlfrfrh(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);

void rdfcn_liftoffLF_LHLFRFRH(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_liftoffLF_lhLFRFRH(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_liftoffLF_LHLFrfRH(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_liftoffLF_lhLFrfRH(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_liftoffLF_LHLFRFrh(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_liftoffLF_lhLFRFrh(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_liftoffLF_LHLFrfrh(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_liftoffLF_lhLFrfrh(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);

void rdfcn_liftoffRF_LHLFRFRH(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_liftoffRF_lhLFRFRH(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_liftoffRF_LHlfRFRH(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_liftoffRF_lhlfRFRH(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_liftoffRF_LHLFRFrh(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_liftoffRF_lhLFRFrh(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_liftoffRF_LHlfRFrh(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_liftoffRF_lhlfRFrh(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);

void rdfcn_liftoffRH_LHLFRFRH(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_liftoffRH_lhLFRFRH(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_liftoffRH_LHlfRFRH(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_liftoffRH_lhlfRFRH(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_liftoffRH_LHLFrfRH(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_liftoffRH_lhLFrfRH(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_liftoffRH_LHlfrfRH(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_liftoffRH_lhlfrfRH(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);

void rdfcn_touchdownlh_lhlfrfrh(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_touchdownlh_lhLFrfrh(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_touchdownlh_lhlfRFrh(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_touchdownlh_lhLFRFrh(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_touchdownlh_lhlfrfRH(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_touchdownlh_lhLFrfRH(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_touchdownlh_lhlfRFRH(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_touchdownlh_lhLFRFRH(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);

void rdfcn_touchdownlf_lhlfrfrh(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_touchdownlf_LHlfrfrh(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_touchdownlf_lhlfRFrh(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_touchdownlf_LHlfRFrh(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_touchdownlf_lhlfrfRH(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_touchdownlf_LHlfrfRH(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_touchdownlf_lhlfRFRH(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_touchdownlf_LHlfRFRH(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);

void rdfcn_touchdownrf_lhlfrfrh(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_touchdownrf_LHlfrfrh(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_touchdownrf_lhLFrfrh(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_touchdownrf_LHLFrfrh(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_touchdownrf_lhlfrfRH(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_touchdownrf_LHlfrfRH(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_touchdownrf_lhLFrfRH(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_touchdownrf_LHLFrfRH(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);

void rdfcn_touchdownrh_lhlfrfrh(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_touchdownrh_LHlfrfrh(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_touchdownrh_lhLFrfrh(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_touchdownrh_LHLFrfrh(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_touchdownrh_lhlfRFrh(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_touchdownrh_LHlfRFrh(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_touchdownrh_lhLFRFrh(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_touchdownrh_LHLFRFrh(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);

static int rdfcn_bothLegs_n  = 38;
static int rdfcn_bothLegs_ne = 2;

void rdfcn_liftoffLHRF_LHlfRFrh(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_liftoffLFRH_lhLFrfRH(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_liftoffLHRH_LHlfrfRH(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_liftoffLFRF_lhLFRFrh(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_touchdownlfrh_lhlfrfrh(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_touchdownlhrf_lhlfrfrh(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_touchdownlfrf_lhlfrfrh(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_touchdownlhrh_lhlfrfrh(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);

void rdfcn_liftoffLFRH_LHLFRFRH(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
void rdfcn_touchdownlfrh_LHlfRFrh(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);

//void rdfcn_switchRIGHTLEFT(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
//void rdfcn_switchLEFTRIGHT(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);

/*********************************
 * Periodicity Constraints
 *********************************/
static int rcfcn  = NY + NU - 1;  /* Number of coupled constraints for periodicity */
static int rcfcne = NY + NU - 1;  /* Number of coupled equality constraints for periodicity  */
enum constNames { dx_const, y_const, dy_const, phi_const, dphi_const,
                  alphaLH_const, dalphaLH_const, ualphaLH_const, dualphaLH_const,
                  alphaLF_const, dalphaLF_const, ualphaLF_const, dualphaLF_const,
                  alphaRF_const, dalphaRF_const, ualphaRF_const, dualphaRF_const,
                  alphaRH_const, dalphaRH_const, ualphaRH_const, dualphaRH_const,
                  lLH_const, dlLH_const, ulLH_const, dulLH_const,
                  lLF_const, dlLF_const, ulLF_const, dulLF_const,
                  lRF_const, dlRF_const, ulRF_const, dulRF_const,
                  lRH_const, dlRH_const, ulRH_const, dulRH_const,
                  TalphaLH_const, TalphaLF_const, TalphaRF_const, TalphaRH_const,
                  FlLH_const, FlLF_const, FlRF_const, FlRH_const
};

// Periodicity constraint at the beginning of the stride
void rcfcn_beginning(double *ts, double *sd, double *sa, double *u, double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
// Periodicity constraint at the middle of the stride that switches left and right leg
void rcfcn_endSymmetric(double *ts, double *sd, double *sa, double *u, double *p, double *pr, double *res, long *dpnd, InfoPtr *info);
// Periodicity constraint at the end of the stride
void rcfcn_endPeriodic(double *ts, double *sd, double *sa, double *u, double *p, double *pr, double *res, long *dpnd, InfoPtr *info);

/*********************************
 * End point Constraints
 *********************************/
// Enforce the desired average speed:
static int rdfcn_avgSpeed_n  = 1;
static int rdfcn_avgSpeed_ne = 1;
void rdfcn_avgSpeed(double *ts, double *sd, double *sa, double *u,  double *p, double *pr, double *res, long *dpnd, InfoPtr *info);

/*********************************
 * Interior point Constraints
 *********************************/
// Constraints on ground clearance (2), actuator torque(8) and
// actuator speed (8) are enforced.  They can be turned on and
// off via the free parameter const_sel, that takes binary flags
// as follows:
// 1 ground clearance constraint on
// 2 actuator torque constraint on
// 4 actuator velocity constraint on
static int rdfcn_neConstraints_n  = 36;
static int rdfcn_neConstraints_ne = 0;

void rdfcn_neConstraints_LHLFRFRH(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void rdfcn_neConstraints_lhLFRFRH(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void rdfcn_neConstraints_LHlfRFRH(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void rdfcn_neConstraints_lhlfRFRH(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void rdfcn_neConstraints_LHLFrfRH(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void rdfcn_neConstraints_lhLFrfRH(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void rdfcn_neConstraints_LHlfrfRH(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void rdfcn_neConstraints_lhlfrfRH(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void rdfcn_neConstraints_LHLFRFrh(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void rdfcn_neConstraints_lhLFRFrh(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void rdfcn_neConstraints_LHlfRFrh(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void rdfcn_neConstraints_lhlfRFrh(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void rdfcn_neConstraints_LHLFrfrh(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void rdfcn_neConstraints_lhLFrfrh(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void rdfcn_neConstraints_LHlfrfrh(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);
void rdfcn_neConstraints_lhlfrfrh(double *t, double *xd, double *xa, double *u, double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info);

/*********************************
 * Objective Function
 *********************************/
void mfcn_COT(double *ts, double *sd, double *sa, double *u, double *p, double *mval, long *dpnd, InfoPtr *info);

/*********************************
 * Output Functions
 *********************************/
static const char *figureview_file_name = "PB_DDH_MOTION.fv";
void figureview_output
(
		long   *imos,      ///< index of model stage (I)
		long   *imsn,      ///< index of m.s. node on current model stage (I)
		double *ts,        ///< time at m.s. node (I)
		double *te,        ///< time at end of m.s. interval (I)
		double *sd,        ///< differential states at m.s. node (I)
		double *sa,        ///< algebraic states at m.s. node (I)
		double *u,         ///< controls at m.s. node (I)
		double *udot,      ///< control slopes at m.s. node (I)
		double *ue,        ///< controls at end of m.s. interval (I)
		double *uedot,     ///< control slopes at end of m.s. interval (I)
		double *p,         ///< global model parameters (I)
		double *pr,        ///< local i.p.c. parameters (I)
		double *ccxd,
		double *mul_ccxd,  ///< multipliers of continuity conditions (I)
#if defined(PRSQP) || defined(EXTPRSQP)
		double *ares,
		double *mul_ares,
#endif
		double *rd,
		double *mul_rd,    ///< multipliers of decoupled i.p.c. (I)
		double *rc,
		double *mul_rc,    ///< multipliers of coupled i.p.c. (I)
		double *obj,
		double *rwh,       ///< real work array (I)
		long   *iwh        ///< integer work array (I)
);
static const char *motion_file_name = "PB_MOTION.txt";
static const char *plot_file_name = "PB_PLOT.txt";
void motion_output
(
		long   *imos,      ///< index of model stage (I)
		long   *imsn,      ///< index of m.s. node on current model stage (I)
		double *ts,        ///< time at m.s. node (I)
		double *te,        ///< time at end of m.s. interval (I)
		double *sd,        ///< differential states at m.s. node (I)
		double *sa,        ///< algebraic states at m.s. node (I)
		double *u,         ///< controls at m.s. node (I)
		double *udot,      ///< control slopes at m.s. node (I)
		double *ue,        ///< controls at end of m.s. interval (I)
		double *uedot,     ///< control slopes at end of m.s. interval (I)
		double *p,         ///< global model parameters (I)
		double *pr,        ///< local i.p.c. parameters (I)
		double *ccxd,
		double *mul_ccxd,  ///< multipliers of continuity conditions (I)
#if defined(PRSQP) || defined(EXTPRSQP)
		double *ares,
		double *mul_ares,
#endif
		double *rd,
		double *mul_rd,    ///< multipliers of decoupled i.p.c. (I)
		double *rc,
		double *mul_rc,    ///< multipliers of coupled i.p.c. (I)
		double *obj,
		double *rwh,       ///< real work array (I)
		long   *iwh        ///< integer work array (I)
);
void plot_output(
  double *t,      ///< time (I)
  double *xd,     ///< differential states (I)
  double *xa,     ///< algebraic states (I)
  double *u,      ///< controls (I)
  double *p,      ///< global model parameters (I)
  double *rwh,    ///< real work array (I)
  long   *iwh,     ///< integer work array (I)
  InfoPtr *info
);

/*********************************
 * Misc Functionality
 *********************************/
// PFree
#define  NPFree 10 /* Number of adjustable system parameters */
enum pFreeNames { hipJointType_free, legJointType_free,
	              cost_fct_sel_free, v_avg_free,
	              const_sel_free,
				  kalpha_free, du_max_alpha_free,
		          kl_free, du_max_l_free, sigma_free
};
void convertParameters(double *p_free, double *p);
void computeConstraints(int const_selFLAG, int offset, double *res);

#endif /* PB_DDH_CONSTRAINTS_H_ */
