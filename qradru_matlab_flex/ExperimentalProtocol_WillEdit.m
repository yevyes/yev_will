
%% Computer dependent settings:
% Possibility one: Lab Computer
PathToMuscod  = '/home/yevyes/MUSCOD-II/MC2/Release/bin/muscod';
PathTo2Dqrad   = '/home/yevyes/Yev_Will/qradru_model_flex/';
PathToMatlab = '/home/yevyes/Yev_Will/qradru_matlab_flex/';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Trotting
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Create intial datStruct with no constraints to converge on a
%gait
cd(PathToMatlab);
datStruct = CreateDatStruct('Trot','SEA','SEA');
% Make all changes to the datStruct directly below this line

datStruct.p(4) = 0.4; %enforce average velocity
datStruct.sd_fix(9) = 1;
datStruct.sd_fix_init(9) = 1;
datStruct.sd(9) = 0;

datStruct.p(5) = 7;

% Make all changes to the datStruct above this line
save('BaseData/Trot_SH_SL_BASE_00.mat', 'datStruct');
CreateDatFile('BaseData/Trot_SH_SL_BASE_00',datStruct); %Save datStruct to be run in Muscod

% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Trot_*_BASE_00.dat ', PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_11(1), cmdout] = system([PathToMuscod,' Trot_SH_SL_BASE_00']);
disp(MuscodResults_11(1))

cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Trot_*_BASE_00.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

%%
PlotMuscodResults('BaseData/Trot_SH_SL_BASE_01',[7,9]);
%%
PlayMuscodResults('BaseData/Trot_SH_SL_BASE_001');
%% STEP 2
cd(PathToMatlab);
datStruct = UpdateDatStruct(datStruct,'BaseData/Trot_SH_SL_BASE_00', 30,45,9);

datStruct.p(4) = 1.0; %enforce ground clearance constraints

% Make all changes to the datStruct above this line
save('BaseData/Trot_SH_SL_BASE_01.mat', 'datStruct');
CreateDatFile('BaseData/Trot_SH_SL_BASE_01',datStruct);

% Copy to project directory to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Trot_*_BASE_01.dat ', PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Trot_SH_SL_BASE_01']);
disp(MuscodResults_12);

cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Trot_*_BASE_01.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

%% STEP 2
cd(PathToMatlab);
datStruct = UpdateDatStruct(datStruct,'BaseData/Trot_SH_SL_BASE_01', 30,45,9);
        
datStruct.p(5) = 7; %enforce ground clearance and motor torque constraints
datStruct.p(4) = 0.75;
datStruct.p(13) = 0;
% Make all changes to the datStruct above this line
save('BaseData/Trot_SH_SL_BASE_02.mat', 'datStruct');
CreateDatFile('BaseData/Trot_SH_SL_BASE_02',datStruct);

% Copy to project directory to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Trot_*_BASE_02.dat ', PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Trot_SH_SL_BASE_02']);
disp(MuscodResults_12);

cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Trot_*_BASE_02.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);
%% STEP 02 % Try again with Actuated Spine
cd(PathToMatlab);
datStruct = CreateDatStruct('Trot','SEA','SEA');
        
datStruct.p(5) = 7; %enforce ground clearance and motor torque constraints
datStruct.p(4) = 0.75;
datStruct.p(13) = 1; %Actuated Spine
datStruct.h_min = [0.001,0,0.001]; %try to seed with mode with long air phase

% Make all changes to the datStruct above this line
save('BaseData/Trot_SH_SL_BASE_002.mat', 'datStruct');
CreateDatFile('BaseData/Trot_SH_SL_BASE_002',datStruct);

% Copy to project directory to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Trot_*_BASE_002.dat ', PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Trot_SH_SL_BASE_002']);
disp(MuscodResults_12);

cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Trot_*_BASE_002.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);
%% STEP 02 % Try again with Actuated Spine
cd(PathToMatlab);
datStruct = CreateDatStruct('Trot','SEA','SEA');
        
datStruct.p(5) = 7; %enforce ground clearance and motor torque constraints
datStruct.p(4) = 0.4;
datStruct.p(13) = 1; %Actuated Spine

% Make all changes to the datStruct above this line
save('BaseData/Trot_SH_SL_BASE_001.mat', 'datStruct');
CreateDatFile('BaseData/Trot_SH_SL_BASE_001',datStruct);

% Copy to project directory to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Trot_*_BASE_001.dat ', PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Trot_SH_SL_BASE_001']);
disp(MuscodResults_12);

cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Trot_*_BASE_001.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);
%% Parameter Study for Trotting
cd(PathToMatlab)
load('BaseData/Trot_SH_SL_BASE_00.mat','datStruct');
datStruct.h_min  = [0.0001;0.0;0.0001];

minVel = 0.025;
maxVel = 0.7;
deltaVel = 0.025;
VelVec = minVel:deltaVel:maxVel;

CreateParameterStudy('ParameterStudies/Trot_SH_SL_BASE_001_Velocity',datStruct,'p',4,VelVec);
[CostMatval,forces, states, time, hval] = ProcessParameterStudy_ANNOTATED('ParameterStudies/Trot_SH_SL_BASE_001_Velocity', PathTo2Dqrad, PathToMuscod);
load('ParameterStudies/Trot_SH_SL_BASE_001_Velocity');
save('TrotFlexWithStates_Actuated_2');
%% Plot Parameter Study For Trotting

load('ParameterStudies/Trot_SH_SL_BASE_00_Velocity','configurations','types','indices','grids');
count = zeros(length(grids{1}),1);
figure(3);
%clf
hold on
box on
grid on
x = [];
y = [];
for i = 1:length(configurations)
    count(configurations{i}.indexVector(1))=count(configurations{i}.indexVector(1))+1;
    if(configurations{i}.nFailed < 5)
        if(configurations{i}.finalDatStruct.p(4) > 0.2 && (configurations{i}.costValue < 0.4));
            %plot(grids{1}(configurations{i}.indexVector(1)),configurations{i}.costValue,'*r');
            x = [x,grids{1}(configurations{i}.indexVector(1))];
            y = [y,configurations{i}.costValue];
        end
      
    end
end
z = [x;y];
[tmp ind] = sort(z(1,:));
x = z(1,ind);
y = z(2,ind);
%plot(x,y,'--k'); %passive spine
plot(x,y,':k','LineWidth',2); %actuated spine
legend('Trotting');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Combine Parameter Studies for Trotting
load('ParameterStudies/Trot_SH_SL_BASE_001_Velocity','configurations','types','indices','grids');
configurations1 = configurations;
load('ParameterStudies/Trot_SH_SL_BASE_002_Velocity','configurations','types','indices','grids');
for i = 1:length(configurations)
    if(configurations{i}.nFailed < 5)
        for j = 1:length(configurations1)
            if(configurations{i}.finalDatStruct.p(4) == configurations1{j}.finalDatStruct.p(4))
                if(configurations{i}.costValue >= configurations1{j}.costValue)
                    configurations{i}.costValue = configurations1{j}.costValue;
                end
            end
        end
    end
end

%% Walking
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%STEP 1
%Create intial datStruct with no constraints to converge on a
%gait
datStruct = CreateDatStruct('Walk','SEA','SEA');

datStruct.sd_fix_init(9) = 1;
datStruct.sd_fix(9) = 1;
datStruct.sd(9) = 0;
datStruct.p(5) = 1;

% Make all changes to the datStruct above this line
save('BaseData/Walk_SH_SL_BASE_00.mat', 'datStruct');
CreateDatFile('BaseData/Walk_SH_SL_BASE_00',datStruct); %Save datStruct to be run in Muscod

% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Walk_*_BASE_00.dat ', PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_11(1), cmdout] = system([PathToMuscod,' Walk_SH_SL_BASE_00']);
disp(MuscodResults_11(1))

cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Walk_*_BASE_00.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);
%%
PlotMuscodResults('BaseData/Walk_SH_SL_BASE_002',[7,9]);
%%
PlayMuscodResults('BaseData/Walk_SH_SL_BASE_002');
%% STEP 2
cd(PathToMatlab);
datStruct = UpdateDatStruct(datStruct,'BaseData/Walk_SH_SL_BASE_00', 30,45,9);
        

datStruct.p(5) = 1; %enforce ground clearance constraints

% Make all changes to the datStruct above this line
save('BaseData/Walk_SH_SL_BASE_01.mat', 'datStruct');
CreateDatFile('BaseData/Walk_SH_SL_BASE_01',datStruct);

% Copy to project directory to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Walk_*_BASE_01.dat ', PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Walk_SH_SL_BASE_01']);
disp(MuscodResults_12);

cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Walk_*_BASE_01.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);
%% STEP 3
cd(PathToMatlab);
datStruct = UpdateDatStruct(datStruct,'BaseData/Walk_SH_SL_BASE_01', 30,45,9);
        

datStruct.p(5) = 7; %enforce ground clearance and motor torque constraints

% Make all changes to the datStruct above this line
save('BaseData/Walk_SH_SL_BASE_02.mat', 'datStruct');
CreateDatFile('BaseData/Walk_SH_SL_BASE_02',datStruct);

% Copy to project directory to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Walk_*_BASE_02.dat ', PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Walk_SH_SL_BASE_02']);
disp(MuscodResults_12);

cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Walk_*_BASE_02.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

%% STEP 01
% Walking with an Actuated Spine
cd(PathToMatlab)
datStruct = UpdateDatStruct(datStruct,'BaseData/Walk_SH_SL_BASE_00', 30,45,9);

datStruct.p(13) = 1;
datStruct.p(5) = 7;


% Make all changes to the datStruct above this line
save('BaseData/Walk_SH_SL_BASE_002.mat', 'datStruct');
CreateDatFile('BaseData/Walk_SH_SL_BASE_002',datStruct);

% Copy to project directory to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Walk_*_BASE_002.dat ', PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_12(1), cmdout] = system([PathToMuscod,' Walk_SH_SL_BASE_002']);
disp(MuscodResults_12);

cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Walk_*_BASE_002.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

%% Parameter Study for Walking (finished)
cd(PathToMatlab)
load('BaseData/Walk_SH_SL_BASE_002.mat','datStruct');

minVel = 0.0;
maxVel = 2.0;
deltaVel = 0.025;
VelVec = minVel:deltaVel:maxVel;

CreateParameterStudy('ParameterStudies/Walk_SH_SL_BASE_002_Velocity',datStruct,'p',4,VelVec);
[CostMatval,forces, states, time, hval] = ProcessParameterStudy_ANNOTATED('ParameterStudies/Walk_SH_SL_BASE_002_Velocity', PathTo2Dqrad, PathToMuscod);

load('ParameterStudies/Walk_SH_SL_BASE_002_Velocity');
save('WalkFlexWithStates_Actuated');
%% Plot Parameter Study Walking

load('ParameterStudies/Walk_SH_SL_BASE_002_Velocity','configurations','types','indices','grids');
count = zeros(length(grids{1}),1);
%clf
figure(3)
hold on
box on
grid on
x = [];
y = [];
for i = 1:length(configurations)
    if(configurations{i}.nFailed < 5)
        if(configurations{i}.finalDatStruct.p(4) > 0.2 && (configurations{i}.costValue < 0.4))
            count(configurations{i}.indexVector(1))=count(configurations{i}.indexVector(1))+1;
            x = [x,grids{1}(configurations{i}.indexVector(1))];
            y = [y,configurations{i}.costValue];
            %plot(grids{1}(configurations{i}.indexVector(1)),configurations{i}.costValue,'*r');

        end
    end
end
z = [x;y];
[tmp ind] = sort(z(1,:));
x = z(1,ind);
y = z(2,ind);
%plot(x,y,'--','Color',[0.4,0.4,1]);
plot(x,y,':','Color',[0.4,0.4,1],'LineWidth',2);
legend('Trotting','Walking');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Galloping
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%STEP 1
%Create intial datStruct with no constraints to converge on a
%gait
datStruct = CreateDatStruct('Gallop','SEA','SEA');

datStruct.p(4) = 1.0;
datStruct.p(5) = 7;

datStruct.sd_fix_init(9) = 1; %fix utheta
datStruct.sd_fix(9) = 1;
datStruct.sd(9) = 0;

datStruct.p(12) = 0.001;

% Make all changes to the datStruct above this line
save('BaseData/Gallop_SH_SL_BASE_00.mat', 'datStruct');
CreateDatFile('BaseData/Gallop_SH_SL_BASE_00',datStruct); %Save datStruct to be run in Muscod

% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Gallop_*_BASE_00.dat ', PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_11(1), cmdout] = system([PathToMuscod,' Gallop_SH_SL_BASE_00']);
disp(MuscodResults_11(1))

cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Gallop_*_BASE_00.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);
%% STEP 2
cd(PathToMatlab);
datStruct = UpdateDatStruct(datStruct,'BaseData/Gallop_SH_SL_BASE_00', 30,45,9);

datStruct.p(4) = 1.75;

% Make all changes to the datStruct above this line
save('BaseData/Gallop_SH_SL_BASE_01.mat', 'datStruct');
CreateDatFile('BaseData/Gallop_SH_SL_BASE_01',datStruct);

% Copy to project directory to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Gallop_*_BASE_01.dat ', PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_13(1), cmdout] = system([PathToMuscod,' Gallop_SH_SL_BASE_01']);
disp(MuscodResults_13);

cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Gallop_*_BASE_01.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);
%% STEP 2
cd(PathToMatlab);
datStruct = UpdateDatStruct(datStruct,'BaseData/Gallop_SH_SL_BASE_01', 30,45,9);

datStruct.p(3) = 3;
datStruct.p(4) = 1.4;

% Make all changes to the datStruct above this line
save('BaseData/Gallop_SH_SL_BASE_02.mat', 'datStruct');
CreateDatFile('BaseData/Gallop_SH_SL_BASE_02',datStruct);

% Copy to project directory to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Gallop_*_BASE_02.dat ', PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_13(1), cmdout] = system([PathToMuscod,' Gallop_SH_SL_BASE_02']);
disp(MuscodResults_13);

cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Gallop_*_BASE_02.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);
%% STEP 1.0 Test Galloping with actuated spine
datStruct = CreateDatStruct('Gallop','SEA','SEA');


% Make all changes to the datStruct above this line
save('BaseData/Gallop_SH_SL_BASE_001.mat', 'datStruct');
CreateDatFile('BaseData/Gallop_SH_SL_BASE_001',datStruct); %Save datStruct to be run in Muscod

% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Gallop_*_BASE_001.dat ', PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_11(1), cmdout] = system([PathToMuscod,' Gallop_SH_SL_BASE_001']);
disp(MuscodResults_11(1))

cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Gallop_*_BASE_001.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);
%% STEP 2.0
cd(PathToMatlab);
datStruct = UpdateDatStruct(datStruct,'BaseData/Gallop_SH_SL_BASE_00', 30,45,9);

datStruct.p(13) = 1;


% Make all changes to the datStruct above this line
save('BaseData/Gallop_SH_SL_BASE_002.mat', 'datStruct');
CreateDatFile('BaseData/Gallop_SH_SL_BASE_002',datStruct);

% Copy to project directory to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Gallop_*_BASE_002.dat ', PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_13(1), cmdout] = system([PathToMuscod,' Gallop_SH_SL_BASE_002']);
disp(MuscodResults_13);

cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Gallop_*_BASE_002.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);
%% STEP 3.0
cd(PathToMatlab);
datStruct = UpdateDatStruct(datStruct,'BaseData/Gallop_SH_SL_BASE_00', 30,45,9);

datStruct.p(10) = 9.4;
datStruct.sd_min(3) = .7;
datStruct.sd_max(3) = 1.15;
datStruct.p(4) = 2.0;

% Make all changes to the datStruct above this line
save('BaseData/Gallop_SH_SL_BASE_003.mat', 'datStruct');
CreateDatFile('BaseData/Gallop_SH_SL_BASE_003',datStruct);

% Copy to project directory to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Gallop_*_BASE_003.dat ', PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_13(1), cmdout] = system([PathToMuscod,' Gallop_SH_SL_BASE_003']);
disp(MuscodResults_13);

cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Gallop_*_BASE_003.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

%%
PlotMuscodResults('BaseData/Gallop_SH_SL_BASE_00',[7,9]);
%%
PlayMuscodResults('BaseData/Gallop_SH_SL_BASE_002');
%% Parameter Study for Galloping (finished)
cd(PathToMatlab)
load('BaseData/Gallop_SH_SL_BASE_002.mat','datStruct');

minVel = 0.025;
maxVel = 4.0;
deltaVel = 0.025;
VelVec = minVel:deltaVel:maxVel;

CreateParameterStudy('ParameterStudies/Gallop_SH_SL_BASE_002_Velocity',datStruct,'p',4,VelVec);
[CostMatval,forces, states, time, hval] = ProcessParameterStudy_ANNOTATED('ParameterStudies/Gallop_SH_SL_BASE_002_Velocity', PathTo2Dqrad, PathToMuscod);

load('ParameterStudies/Gallop_SH_SL_BASE_002_Velocity');
save('GallopFlexWithStates_Actuated_Velocity');
%% Parameter Study for Galloping (Spine Stiffness)
% it seems stiffer spines decrease the COT
% Average Velocity: 1.75
cd(PathToMatlab)
load('BaseData/Gallop_SH_SL_BASE_01.mat','datStruct');

minktheta = 2;
maxktheta = 100;
deltak = 1;
KVec = minktheta:deltak:maxktheta;

CreateParameterStudy('ParameterStudies/Gallop_SH_SL_BASE_01_Ktheta',datStruct,'p',10,KVec);
[CostMatval,forces, states, time, hval] = ProcessParameterStudy_ANNOTATED('ParameterStudies/Gallop_SH_SL_BASE_01_Ktheta', PathTo2Dqrad, PathToMuscod);

load('ParameterStudies/Gallop_SH_SL_BASE_01_Ktheta');
save('GallopFlexWithStates_Ktheta');

%% Plot Parameter Study Galloping

load('ParameterStudies/Gallop_SH_SL_BASE_002_Velocity','configurations','types','indices','grids');
count = zeros(length(grids{1}),1);
%clf
figure(3)
hold on
box on
grid on
x = [];
y = [];
for i = 1:length(configurations)
    if(configurations{i}.nFailed < 5)
        if(configurations{i}.finalDatStruct.p(4) > 0.2 && (configurations{i}.costValue < 0.4))
            count(configurations{i}.indexVector(1))=count(configurations{i}.indexVector(1))+1;
            x = [x,grids{1}(configurations{i}.indexVector(1))];
            y = [y,configurations{i}.costValue];
            %plot(grids{1}(configurations{i}.indexVector(1)),configurations{i}.costValue,'*r');

        end
    end
end
z = [x;y];
[tmp ind] = sort(z(1,:));
x = z(1,ind);
y = z(2,ind);
%plot(x,y,'--','Color',[0.4,1,0.4]);
plot(x,y,':','Color',[0.4,1,0.4],'LineWidth',2);

legend('Trotting','Walking','Galloping');
%% Plot Parameter Study Galloping (Spine Stiffness)
%Average Velocity = 1.75
load('ParameterStudies/Gallop_SH_SL_BASE_01_Ktheta','configurations','types','indices','grids');
count = zeros(length(grids{1}),1);
%clf
figure(3)
hold on
box on
grid on
for i = 1:length(configurations)
    count(configurations{i}.indexVector(1))=count(configurations{i}.indexVector(1))+1;
    if(configurations{i}.nFailed < 5)
         plot(grids{1}(configurations{i}.indexVector(1)),configurations{i}.costValue,'*g');

    end
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Bounding
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

datStruct = CreateDatStruct('Bound','SEA','SEA');

datStruct.sd(9) = 0;
datStruct.sd_fix(9) = 1;
datStruct.sd_fix_init(9) = 1;


save('BaseData/Bound_SH_SL_BASE_00.mat', 'datStruct');
CreateDatFile('BaseData/Bound_SH_SL_BASE_00',datStruct);

% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Bound_SH_SL_BASE_00.dat ', PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);

[MuscodResults_11(1), cmdout] = system([PathToMuscod,' Bound_SH_SL_BASE_00']);
disp(MuscodResults_11);

cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Bound_*_BASE_00.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);


%% STEP 2
datStruct = UpdateDatStruct(datStruct,'BaseData/Bound_SH_SL_BASE_00',30,45,9);

datStruct.p(5) = 1; 

save('BaseData/Bound_SH_SL_BASE_01.mat', 'datStruct');
CreateDatFile('BaseData/Bound_SH_SL_BASE_01',datStruct);

% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Bound_SH_SL_BASE_01.dat ', PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);

[MuscodResults_11(1), cmdout] = system([PathToMuscod,' Bound_SH_SL_BASE_01']);
disp(MuscodResults_11);

cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Bound_*_BASE_01.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

%% STEP 3
datStruct = UpdateDatStruct(datStruct,'BaseData/Bound_SH_SL_BASE_01',30,45,9);

datStruct.p(5) = 7;

save('BaseData/Bound_SH_SL_BASE_02.mat', 'datStruct');
CreateDatFile('BaseData/Bound_SH_SL_BASE_02',datStruct);

% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Bound_SH_SL_BASE_02.dat ', PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);

[MuscodResults_11(1), cmdout] = system([PathToMuscod,' Bound_SH_SL_BASE_02']);
disp(MuscodResults_11);

cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Bound_*_BASE_02.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);
%% STEP 3
datStruct = UpdateDatStruct(datStruct,'BaseData/Bound_SH_SL_BASE_02',30,45,9);

datStruct.p(4) = 1; %high speed to emphasize the effect of the spine
datStruct.p(13) = 0;
save('BaseData/Bound_SH_SL_BASE_03.mat', 'datStruct');
CreateDatFile('BaseData/Bound_SH_SL_BASE_03',datStruct);

% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Bound_SH_SL_BASE_03.dat ', PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);

[MuscodResults_11(1), cmdout] = system([PathToMuscod,' Bound_SH_SL_BASE_03']);
disp(MuscodResults_11);

cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Bound_*_BASE_03.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);

%% Plot
PlotMuscodResults('BaseData/Bound_SH_SL_BASE_03',[7,9]);
%% Play Muscod Results
PlayMuscodResults('BaseData/Bound_SH_SL_BASE_02');
%% Parameter Study for Bounding (Finished)
cd(PathToMatlab)
load('BaseData/Bound_SH_SL_BASE_02.mat','datStruct');

minVel = 0.0;
maxVel = 3.0;
deltaVel = 0.025;
VelVec = minVel:deltaVel:maxVel;

CreateParameterStudy('ParameterStudies/Bound_SH_SL_BASE_02_Velocity',datStruct,'p',4,VelVec);
[CostMatval,forces, states, time, hval] = ProcessParameterStudy_ANNOTATED('ParameterStudies/Bound_SH_SL_BASE_02_Velocity', PathTo2Dqrad, PathToMuscod);

load('ParameterStudies/Bound_SH_SL_BASE_02_Velocity');
save('BoundFlexWithStates');
%% Plot Parameter Study Bounding

load('ParameterStudies/Bound_SH_SL_BASE_02_Velocity','configurations','types','indices','grids');
count = zeros(length(grids{1}),1);
%clf
figure(3)
hold on
box on
grid on
for i = 1:length(configurations)
    count(configurations{i}.indexVector(1))=count(configurations{i}.indexVector(1))+1;
    if(configurations{i}.nFailed < 5)
        plot(grids{1}(configurations{i}.indexVector(1)),configurations{i}.costValue,'*k');
    end
end

%% Find Configuration at speed
Vel1 = 0.6;
for i = 1:(size(states,2)-1)
    if(configurations{i}.finalDatStruct.p(4) == Vel1)
        disp('Found');
        index = configurations{i}.indexVector(1)
        %{
        configurations(i) = [];
        states(index) = [];
        time(index) = [];
        hval(index) = [];
        forces(index) = [];
        CostMatval(index) = [];
        %}
  
    end
    
end
%% Play Muscod Result at Parameter Study Point
Veloc = 2.05;
shouldplot = 0;
SavedStatesFileName = 'GallopFlexWithStates_Actuated_Velocity.mat';
PlayMuscodResults_ParamStudyPoint(SavedStatesFileName, Veloc, shouldplot)


