% Try: PlayMOTFile_SEH('Running')
function PlayMOTFile_SEH(fileName)
    %% Read file
    disp(['Processing: ',fileName,'.mot']);
    fileID = fopen([fileName,'.mot']);
    B = [];
    while true
        tline = fgetl(fileID);
        if ~ischar(tline)
            break
        end
        if tline(1)=='#'
%             disp(['Comment: ',tline]);
        else
            [A,count] = sscanf(tline,'%f');
            B = [B;A'];
        end
    end
    fclose(fileID);
    
    %% Extract trajectories
    % t, x, y, phi, alphaL, alphaR, ualphaL, ualphaR, lL, lR, ulL, ulR,
    % ddualphaL, ddualphaR, ddulL, ddulR
    tVec       = [B(1:end-1,1);B(1:end-1,1)+B(end,1)];
    xVec       = [B(1:end-1,2);B(1:end-1,2)+B(end,2)];
    yVec       = [B(1:end-1,3);B(1:end-1,3)];
    phiVec     = [B(1:end-1,4);B(1:end-1,4)];
    alphaLVec  = [B(1:end-1,5);B(1:end-1,6)];
    alphaRVec  = [B(1:end-1,6);B(1:end-1,5)];
    ualphaLVec = [B(1:end-1,7);B(1:end-1,8)];
    ualphaRVec = [B(1:end-1,8);B(1:end-1,7)];
    lLVec      = [B(1:end-1,9);B(1:end-1,10)];
    lRVec      = [B(1:end-1,10);B(1:end-1,9)];
    ulLVec     = [B(1:end-1,11);B(1:end-1,12)];
    ulRVec     = [B(1:end-1,12);B(1:end-1,11)];
    % Resample
    method = 'spline';
    % do 25 frames per unit of normalized time:
    n = round(tVec(end)*25);
    %n = 50;      % # of frames per step
    nStride = 1;  % The movie is composed of that many stride(s)
    
    % to create destinct values:
    tOld = tVec+linspace(0,1e-10,length(tVec))';
    tVec = linspace(0,2*B(end,1),n);
    tVec = tVec(1:end-1);
    xVec = interp1(tOld, xVec,tVec, method);
    yVec = interp1(tOld, yVec,tVec, method);
    phiVec = interp1(tOld, phiVec,tVec, method);
    alphaLVec = interp1(tOld, alphaLVec,tVec, method);
    alphaRVec = interp1(tOld, alphaRVec,tVec, method);
    ualphaLVec = interp1(tOld, ualphaLVec,tVec, method);
    ualphaRVec = interp1(tOld, ualphaRVec,tVec, method);
    lLVec = interp1(tOld, lLVec,tVec, method);
    lRVec = interp1(tOld, lRVec,tVec, method);
    ulLVec = interp1(tOld, ulLVec,tVec, method);
    ulRVec = interp1(tOld, ulRVec,tVec, method);
    
    %% Animate:
	% Set up the graphical output:
    graph3DOUTPUT = PrismaticBiped3DCLASS(false);
    xOffset = 0; % This shifts the model forward from step to step
    % If desired, every iteration a rendered picture is saved to disc.  This
    % can later be used to create a animation of the monopod.
    % (un)comment the following lines, if you (don't) want to save the
    % individual frames to disc:
    frameCount = 0; 
    %mkdir(fileName);
    [y, ~, contStateIndices] = ContStateDefinition();
    z = DiscStateDefinition();
    [u, ~, exctStateIndices] = ExctStateDefinition();
    %  For each step
    for i = 1:nStride
        for j = 1:n-1
            y(contStateIndices.x) = xVec(j) + xOffset;
            y(contStateIndices.y) = yVec(j);
            y(contStateIndices.phi) = phiVec(j);
            y(contStateIndices.alphaL) = alphaLVec(j);
            y(contStateIndices.alphaR) = alphaRVec(j);
            y(contStateIndices.lL) = lLVec(j);
            y(contStateIndices.lR) = lRVec(j);
            u(exctStateIndices.ualphaL) = ualphaLVec(j); 
            u(exctStateIndices.ualphaR) = ualphaRVec(j); 
            u(exctStateIndices.ulL) = ulLVec(j); 
            u(exctStateIndices.ulR) = ulRVec(j); 
            graph3DOUTPUT.update(y,z,[],u);
            % (un)comment the following line, if you (don't) want to save the
            % individual frames to disc:
            %fig = gcf; print(fig,'-r600','-djpeg',[fileName,'/Frame',num2str(frameCount,'%04d.jpg')],'-zbuffer'); frameCount = frameCount + 1;
        end
        % After each step the model is moved forward:
        xOffset = xOffset + xVec(end);
    end     
end