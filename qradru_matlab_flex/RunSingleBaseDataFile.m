function RunSingleBaseDataFile(filename,constraints,gaitType, hipJointType, legJointType)

cd(PathToMatlab)
datStruct = CreateDatStruct(gaitType, hipJointType, legJointType);

% Make all changes to the datStruct above this line
save(strcat('BaseData/',filename,'.mat'), 'datStruct');
CreateDatFile(strcat('BaseData/',filename),datStruct); %Save datStruct to be run in Muscod

% Copy to project director to process in Muscod
currentPath = pwd;
system(['cp ',pwd,'/BaseData/Trot_*_BASE_00.dat ', PathTo2Dqrad,'DAT/']);

% SWITCH TO PROJECT DIRECTORY AND RUN MUSCOD THERE
cd(PathTo2Dqrad);
[MuscodResults_11(1), cmdout] = system([PathToMuscod,strcat(' ',filename)' Trot_SH_SL_BASE_00']);
disp(MuscodResults_11(1))

cd(currentPath);
% Copy back to BaseData/
system(['cp ',PathTo2Dqrad,'RES/Trot_*_BASE_00.* ',pwd,'/BaseData/']);
% Clean up DAT and RES folders
system(['rm ',PathTo2Dqrad,'DAT/*.* ']);
system(['rm ',PathTo2Dqrad,'RES/*.* ']);