dJLFdtTIMESdqdt = MatrixXXd::Zero();	double t532 = yVec(alphaLF)+yVec(phi)-theta_Var;
	double t533 = sin(t532);
	double t534 = cos(t532);
	double t535 = yVec(dlLF)*t534;
	double t536 = yVec(lLF)*t533;
	double t537 = yVec(phi)-theta_Var;
	double t538 = cos(t537);
	double t539 = pVec(l1)*t538;
	double t540 = t536+t539;
	double t541 = yVec(dtheta)*t540;
	double t542 = t535+t541-yVec(dphi)*t540-yVec(dalphaLF)*yVec(lLF)*t533;
	double t543 = yVec(lLF)*t534;
	double t544 = sin(t537);
	double t548 = pVec(l1)*t544;
	double t545 = t543-t548;
	double t546 = yVec(dlLF)*t533;
	double t547 = yVec(dalphaLF)*yVec(lLF)*t534;
	double t549 = yVec(dphi)*t545;
	double t550 = t546+t547+t549-yVec(dtheta)*t545;
	double t551 = yVec(dalphaLF)+yVec(dphi)-yVec(dtheta);
	dJLFdtTIMESdqdt(0,0) = yVec(dphi)*t542-yVec(dtheta)*t542+yVec(dalphaLF)*(t535-yVec(dalphaLF)*yVec(lLF)*t533-yVec(dphi)*yVec(lLF)*t533+yVec(dtheta)*yVec(lLF)*t533)+yVec(dlLF)*t534*t551;
	dJLFdtTIMESdqdt(1,0) = yVec(dphi)*t550-yVec(dtheta)*t550+yVec(dalphaLF)*(t546+t547+yVec(dphi)*yVec(lLF)*t534-yVec(dtheta)*yVec(lLF)*t534)+yVec(dlLF)*t533*t551;
	