function logo = GetRAMLogoSurface(side)
    X = imread('ModelCover.jpg');
    X = flipdim(X,1);
    X = flipdim(X,2);
    % Scale to mainbody dimensions of [1.4 x 0.9 x 0.4]
    %y = [-1,-1;1,1]*0.45;
    if (side == 'l')
        x = [-1,1;-1,1]*0.7;
        y = [1,1;1,1]*0.451;
    else
        x = [1,-1;1,-1]*0.7;
        y = [-1,-1;-1,-1]*0.451;
    end
    z = [-1,-1;1,1]*0.2;
    logo = surface(x,y,z,X, ...
          'EdgeColor','none', ...
          'FaceColor','texturemap', ...
          'CDataMapping','direct');
end