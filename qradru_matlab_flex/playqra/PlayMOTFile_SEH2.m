% Try: PlayMOTFile_SEH('Running')
function PlayMOTFile_SEH2(fileName)
    %% Read file
    b=load([fileName,'.mat']);
    B=b.draw;
    
    %% Extract trajectories
    % t, x, y, phi, alphaL, alphaR, ualphaL, ualphaR, lL, lR, ulL, ulR,
    % ddualphaL, ddualphaR, ddulL, ddulR
    tVec       = [B(1:end-1,1);B(1:end-1,1)+B(end,1)];
    xVec       = [B(1:end-1,2);B(1:end-1,2)+B(end,2)];
    yVec       = [B(1:end-1,3);B(1:end-1,3)];
    phiVec     = [B(1:end-1,4);B(1:end-1,4)];
    alphaLHVec  = [B(1:end-1,5);B(1:end-1,5)];
    lLHVec      = [B(1:end-1,6);B(1:end-1,6)];
    alphaLFVec  = [B(1:end-1,7);B(1:end-1,7)];
    lLFVec      = [B(1:end-1,8);B(1:end-1,8)];
    alphaRFVec  = [B(1:end-1,9);B(1:end-1,9)];
    lRFVec      = [B(1:end-1,10);B(1:end-1,10)];
    alphaRHVec  = [B(1:end-1,11);B(1:end-1,11)];
    lRHVec      = [B(1:end-1,12);B(1:end-1,12)];
    ualphaLHVec = [B(1:end-1,13);B(1:end-1,13)];
    ulLHVec     = [B(1:end-1,14);B(1:end-1,14)];
    ualphaLFVec = [B(1:end-1,15);B(1:end-1,15)];
    ulLFVec     = [B(1:end-1,16);B(1:end-1,16)];
    ualphaRFVec = [B(1:end-1,17);B(1:end-1,17)];
    ulRFVec     = [B(1:end-1,18);B(1:end-1,18)];
    ualphaRHVec = [B(1:end-1,19);B(1:end-1,19)];
    ulRHVec     = [B(1:end-1,20);B(1:end-1,20)];
    % Resample
    method = 'spline';
    % do 25 frames per unit of normalized time:
    n = round(tVec(end)*25);
    %n = 50;      % # of frames per step
    nStride = 1;  % The movie is composed of that many stride(s)
    
    % to create destinct values:
    tOld = tVec+linspace(0,1e-10,length(tVec))';
    tVec = linspace(0,2*B(end,1),n);
    tVec = tVec(1:end-1);
    xVec = interp1(tOld, xVec,tVec, method);
    yVec = interp1(tOld, yVec,tVec, method);
    phiVec = interp1(tOld, phiVec,tVec, method);
    alphaLHVec = interp1(tOld, alphaLHVec,tVec, method);
    lLHVec = interp1(tOld, lLHVec,tVec, method);
    alphaLFVec = interp1(tOld, alphaLFVec,tVec, method);
    lLFVec = interp1(tOld, lLFVec,tVec, method);
    alphaRFVec = interp1(tOld, alphaRFVec,tVec, method);
    lRFVec = interp1(tOld, lRFVec,tVec, method);
    alphaRHVec = interp1(tOld, alphaRHVec,tVec, method);
    lRHVec = interp1(tOld, lRHVec,tVec, method);
    ualphaLHVec = interp1(tOld, ualphaLHVec,tVec, method);
    ulLHVec = interp1(tOld, ulLHVec,tVec, method);
    ualphaLFVec = interp1(tOld, ualphaLFVec,tVec, method);
    ulLFVec = interp1(tOld, ulLFVec,tVec, method);
    ualphaRFVec = interp1(tOld, ualphaRFVec,tVec, method);
    ulRFVec = interp1(tOld, ulRFVec,tVec, method);
    ualphaRHVec = interp1(tOld, ualphaRHVec,tVec, method);
    ulRHVec = interp1(tOld, ulRHVec,tVec, method);
    
    %% Animate:
	% Set up the graphical output:
    graph3DOUTPUT = Quadruped3DCLASS(false);
    xOffset = 0; % This shifts the model forward from step to step
    % If desired, every iteration a rendered picture is saved to disc.  This
    % can later be used to create a animation of the monopod.
    % (un)comment the following lines, if you (don't) want to save the
    % individual frames to disc:
    frameCount = 0; 
    %mkdir(fileName);
    [y, ~, contStateIndices] = ContStateDefinition();
    z = DiscStateDefinition();
    [u, ~, exctStateIndices] = ExctStateDefinition();
    %  For each step
    for i = 1:nStride
        for j = 1:n-1
            y(contStateIndices.x) = xVec(j) + xOffset;
            y(contStateIndices.y) = yVec(j);
            y(contStateIndices.phi) = phiVec(j);
            y(contStateIndices.alphaLH) = alphaLHVec(j);
            y(contStateIndices.lLH) = lLHVec(j);
            y(contStateIndices.alphaLF) = alphaLFVec(j);
            y(contStateIndices.lLF) = lLFVec(j);
            y(contStateIndices.alphaRF) = alphaRFVec(j);
            y(contStateIndices.lRF) = lRFVec(j);
            y(contStateIndices.alphaRH) = alphaRHVec(j);
            y(contStateIndices.lRH) = lRHVec(j);
            u(exctStateIndices.ualphaLH) = ualphaLHVec(j); 
            u(exctStateIndices.ulLH) = ulLHVec(j); 
            u(exctStateIndices.ualphaLF) = ualphaLFVec(j); 
            u(exctStateIndices.ulLF) = ulLFVec(j); 
            u(exctStateIndices.ualphaRF) = ualphaRFVec(j);
            u(exctStateIndices.ulRF) = ulRFVec(j);  
            u(exctStateIndices.ualphaRH) = ualphaRHVec(j); 
            u(exctStateIndices.ulRH) = ulRHVec(j); 
            graph3DOUTPUT.update(y,z,[],u);
            % (un)comment the following line, if you (don't) want to save the
            % individual frames to disc:
            %fig = gcf; print(fig,'-r600','-djpeg',[fileName,'/Frame',num2str(frameCount,'%04d.jpg')],'-zbuffer'); frameCount = frameCount + 1;
        end
        % After each step the model is moved forward:
        xOffset = xOffset + xVec(end);
    end     
end