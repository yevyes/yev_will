#include <cmath>
#include "def_usrmod.hpp"
#include <PB_Constraints.h>


#define  NMOS   12  /* Number of phases (MOdel Stages) */
/* There are three different stages for the symmetrically walking prismatic biped:
 * 0) flight 
 * 1) touchdown collision of LH
 * 2) single stance LHlfrfrh
 * 3) touchdown collision of RH
 * 4) double stance LHlfrfRH
 * 5) single stance lhlfrfRH
 * 6) touchdown collision of LF 
 * 7) double stance lhLFrfRH
 * 8) single stance lhLFrfrh
 * 9) touchdown collision of RF
 * 10) double stance lhLFRFrh
 * 11) single stance lhlfRFrh
 */
#define  NXA    0  /* Number of algebraic states */
#define  NPR    0  /* Number of local parameters */

/** \brief Entry point for the muscod application */
extern "C" void def_model(void);
void def_model(void)
{
	/* Define problem dimensions */
	def_mdims(NMOS, NPFree, rcfcn, rcfcne);
	/* Define the first phase */
	/* def_mstage(I, NXD, NXA, NU, mfcn, lfcn, jacmlo, jacmup, astruc, afcn, ffcn, gfcn, rwh, iwh)
	 * Call to define a model stage with index I, where
	 * NXD is the differential state dimension,
	 * NXA the algebraic state dimension, and
	 * NU is the control dimension.
	 * mfcn is a pointer to a Mayer term function (or NULL) to be evaluated at the end of the stage, and
	 * lfcn a pointer to a Lagrange term (or NULL).
	 * For documentation of the left-hand side matrix function afcn, and of the integers jacmlo,
	 * jacmup, and astruc that provide structural matrix information please consult the
	 * DAESOL-manual [BBS99]; setting the integers to zero is equivalent to not defining
	 * any structural information.
	 * ffcn is a pointer to the differential right hand side function,
	 * gfcn the pointer to the algebraic right hand side function (or NULL).
	 * rwh, iwh are real and integer work arrays which can be used to pass a common workspace to the stage functions.
	 *
	 */
	def_mstage( 0, // 0) flight 
				NY, NXA, NU,
				NULL, NULL,
				0, 0, 0, NULL, ffcn_flowlhlfrfrh, NULL,
				NULL, NULL
				);
	def_mstage( 1, // 1) touchdown collision of LH
				NY, NXA, NU,
				NULL, NULL,
				0, 0, 0, NULL, ffcn_collisionLHlfrfrh, NULL,
				NULL, NULL
				);
	def_mstage( 2, // 2) single stance LHlfrfrh
				NY, NXA, NU,
				mfcn_COT, NULL,
				0, 0, 0, NULL, ffcn_flowLHlfrfrh, NULL,
				0, NULL
				);
	def_mstage( 3, // 3) touchdown collision of RH
				NY, NXA, NU,
				NULL, NULL,
				0, 0, 0, NULL, ffcn_collisionLHlfrfRH, NULL,
				NULL, NULL
				);
	def_mstage( 4, // 4) double stance LHlfrfRH
				NY, NXA, NU,
				NULL, NULL,
				0, 0, 0, NULL, ffcn_flowLHlfrfRH, NULL,
				NULL, NULL
				);
	def_mstage( 5, // 5) single stance lhlfrfRH
				NY, NXA, NU,
				mfcn_COT, NULL,
				0, 0, 0, NULL, ffcn_flowlhlfrfRH, NULL,
				0, NULL
				);
	def_mstage( 6, // 6) touchdown collision of LF 
				NY, NXA, NU,
				NULL, NULL,
				0, 0, 0, NULL, ffcn_collisionlhLFrfRH, NULL,
				NULL, NULL
				);
	def_mstage( 7, // 7) double stance lhLFrfRH
				NY, NXA, NU,
				NULL, NULL,
				0, 0, 0, NULL, ffcn_flowlhLFrfRH, NULL,
				NULL, NULL
				);
	def_mstage( 8, // 8) single stance lhLFrfrh
				NY, NXA, NU,
				mfcn_COT, NULL,
				0, 0, 0, NULL, ffcn_flowlhLFrfrh, NULL,
				0, NULL
				);
	def_mstage( 9, // 9) touchdown collision of RF
				NY, NXA, NU,
				NULL, NULL,
				0, 0, 0, NULL, ffcn_collisionlhLFRFrh, NULL,
				NULL, NULL
				);
	def_mstage( 10, // 10) double stance lhLFRFrh
				NY, NXA, NU,
				NULL, NULL,
				0, 0, 0, NULL, ffcn_flowlhLFRFrh, NULL,
				NULL, NULL
				);
	def_mstage( 11, // 11) single stance lhlfRFrh
				NY, NXA, NU,
				mfcn_COT, NULL,
				0, 0, 0, NULL, ffcn_flowlhlfRFrh, NULL,
				0, NULL
				);
/* There are three different stages for the symmetrically walking prismatic biped:
 * 0) flight 
 * 1) touchdown collision of LH
 * 2) single stance LHlfrfrh
 * 3) touchdown collision of RH
 * 4) double stance LHlfrfRH
 * 5) single stance lhlfrfRH
 * 6) touchdown collision of LF 
 * 7) double stance lhLFrfRH
 * 8) single stance lhLFrfrh
 * 9) touchdown collision of RF
 * 10) double stance lhLFRFrh
 * 11) single stance lhlfRFrh
 */

	def_mpc(0, "Start Point", NPR, rdfcn_singleLeg_n, rdfcn_singleLeg_ne, rdfcn_liftoffRF_lhlfRFrh, rcfcn_beginning);
	def_mpc(0, "Interior Point", NPR, rdfcn_neConstraints_n, rdfcn_neConstraints_ne, rdfcn_neConstraints_lhlfrfrh, NULL);

	def_mpc(1,"Start Point", NPR, rdfcn_singleLeg_n, rdfcn_singleLeg_ne, rdfcn_touchdownlh_lhlfrfrh, NULL);
	def_mpc(1, "Interior Point", NPR, rdfcn_neConstraints_n, rdfcn_neConstraints_ne, rdfcn_neConstraints_lhlfrfrh, NULL);

	def_mpc(2, "Start Point", NPR, rdfcn_neConstraints_n, rdfcn_neConstraints_ne, rdfcn_neConstraints_LHlfrfrh, NULL);
	def_mpc(2, "Interior Point", NPR, rdfcn_neConstraints_n, rdfcn_neConstraints_ne, rdfcn_neConstraints_LHlfrfrh, NULL);

	def_mpc(3, "Start Point", NPR, rdfcn_singleLeg_n, rdfcn_singleLeg_ne, rdfcn_touchdownrh_LHlfrfrh, NULL);
	def_mpc(3, "Interior Point", NPR, rdfcn_neConstraints_n, rdfcn_neConstraints_ne, rdfcn_neConstraints_LHlfrfrh, NULL);

	def_mpc(4, "Start Point", NPR, rdfcn_neConstraints_n, rdfcn_neConstraints_ne, rdfcn_neConstraints_LHlfrfRH, NULL);
	def_mpc(4, "Interior Point", NPR, rdfcn_neConstraints_n, rdfcn_neConstraints_ne, rdfcn_neConstraints_LHlfrfRH, NULL);

	def_mpc(5, "Start Point", NPR, rdfcn_singleLeg_n, rdfcn_singleLeg_ne, rdfcn_liftoffLH_LHlfrfRH, NULL);
	def_mpc(5, "Interior Point", NPR, rdfcn_neConstraints_n, rdfcn_neConstraints_ne, rdfcn_neConstraints_lhlfrfRH, NULL);

	def_mpc(6, "Start Point", NPR, rdfcn_singleLeg_n, rdfcn_singleLeg_ne, rdfcn_touchdownlf_lhlfrfRH, NULL);
	def_mpc(6, "Interior Point", NPR, rdfcn_neConstraints_n, rdfcn_neConstraints_ne, rdfcn_neConstraints_lhlfrfRH, NULL);

	def_mpc(7,"Start Point", NPR, rdfcn_neConstraints_n, rdfcn_neConstraints_ne, rdfcn_neConstraints_lhLFrfRH, NULL);
	def_mpc(7, "Interior Point", NPR, rdfcn_neConstraints_n, rdfcn_neConstraints_ne, rdfcn_neConstraints_lhLFrfRH, NULL);

	def_mpc(8, "Start Point", NPR, rdfcn_singleLeg_n, rdfcn_singleLeg_ne, rdfcn_liftoffRH_lhLFrfRH, NULL);
	def_mpc(8, "Interior Point", NPR, rdfcn_neConstraints_n, rdfcn_neConstraints_ne, rdfcn_neConstraints_lhLFrfrh, NULL);

	def_mpc(9, "Start Point", NPR, rdfcn_singleLeg_n, rdfcn_singleLeg_ne, rdfcn_touchdownrf_lhLFrfrh, NULL);
	def_mpc(9, "Interior Point", NPR, rdfcn_neConstraints_n, rdfcn_neConstraints_ne, rdfcn_neConstraints_lhLFrfrh, NULL);

	def_mpc(10, "Start Point", NPR, rdfcn_neConstraints_n, rdfcn_neConstraints_ne, rdfcn_neConstraints_lhLFRFrh, NULL);
	def_mpc(10, "Interior Point", NPR, rdfcn_neConstraints_n, rdfcn_neConstraints_ne, rdfcn_neConstraints_lhLFRFrh, NULL);

	def_mpc(11, "Start Point", NPR, rdfcn_singleLeg_n, rdfcn_singleLeg_ne, rdfcn_liftoffLF_lhLFRFrh, NULL);
	def_mpc(11, "Interior Point", NPR, rdfcn_neConstraints_n, rdfcn_neConstraints_ne, rdfcn_neConstraints_lhlfRFrh, NULL);

	/* Define constraints at the end point of phase 2
	 * i.e., the coupled constraints for periodicity and the conditions for the average velocity	 */
	def_mpc(11, "End Point", NPR, rdfcn_avgSpeed_n, rdfcn_avgSpeed_ne, rdfcn_avgSpeed, rcfcn_endPeriodic);

	// Create output to a .mot and a .plt file
	def_mio (NULL , motion_output, plot_output);
}
