/*
 * PB_Dynamics.cpp
 *
 * Common Dynamic code for a Prismatic Biped.
 *
 *  Created on: April 13, 2013
 *      Author: cdremy@umich.edu
 */

#include "PB_Dynamics.h"
#include <algorithm>

PB_Dynamics::PB_Dynamics() {}  // Constructor (Empty)
PB_Dynamics::~PB_Dynamics() {} // Destructor (Empty)

/* Hybrid Dynamic Equations */
/* The FlowMap computes the right hand side of the governing differential
 * equations.  Prior to it's call, the values for continuous states y,
 * discrete states z, excitation states u, and system parameters p must
 * be set to the newest values.  The function updates the dynamics (i.e.,
 * computes mass matrix, differentiable forces, etc. automatically.  It
 * returns an array (size NY) of the right hand side.
 */

VectorXd PB_Dynamics::ComputeContact(){
	updateDynamics();

	VectorXd lambda(nContact*2, 1);
	if (nContact>0)
	{  // check if some ground contact is present
		MatrixXd projM(nContact*2, nContact*2);
		VectorXd projF(nContact*2, 1);
		projM = J * Mdecomp.solve(J.transpose());
		projF = J * Mdecomp.solve(tau + h);
		lambda = projM.partialPivLu().solve(-projF - dJdtTimesdqdt);
	} else {
		// Do nothing, lambda is an empty vector anyway
	}

	return lambda;
}

void PB_Dynamics::FlowMap(double* dydtPtr){
	updateDynamics();

	VectorYd dydt;

	/**********************
	 * Actuator dynamics:
	 **********************/
	// Map velocities to position derivatives:
	dydt(utheta)    = yVec(dutheta);
	dydt(ualphaLH)  = yVec(dualphaLH);
	dydt(ualphaLF)  = yVec(dualphaLF);
	dydt(ualphaRF)  = yVec(dualphaRF);
	dydt(ualphaRH)  = yVec(dualphaRH);
	dydt(ulLH)      = yVec(dulLH);
	dydt(ulLF)      = yVec(dulLF);
	dydt(ulRF)      = yVec(dulRF);
	dydt(ulRH)      = yVec(dulRH);
	
	//std::cout<<pVec(UthetaOn)<<endl;
	if(pVec(UthetaOn) == 1)
		dydt(dutheta) = (uVec(Ttheta) - tau(qtheta))/j_mot_theta;
	else if(pVec(UthetaOn) == 0)
		dydt(dutheta) = 0;
	else
		std::cout<<"Unknown Spine Actuator Setting"<<endl;



	if (pVec(hip_jnt_type) == PEA){ // No actuator dynamics in the parallel actuated joints
		dydt(dualphaLH) = 0;
		dydt(dualphaLF) = 0;
		dydt(dualphaRF) = 0;
		dydt(dualphaRH) = 0;
	} else { // Actuators are driven by the difference between motor and joint force
		dydt(dualphaLH) = (uVec(TalphaLH) - tau(qalphaLH))/j_mot_alpha;
		dydt(dualphaLF) = (uVec(TalphaLF) - tau(qalphaLF))/j_mot_alpha;
		dydt(dualphaRF) = (uVec(TalphaRF) - tau(qalphaRF))/j_mot_alpha;
		dydt(dualphaRH) = (uVec(TalphaRH) - tau(qalphaRH))/j_mot_alpha;
	}
	if (pVec(leg_jnt_type) == PEA){ // No actuator dynamics in the parallel actuated joints
		dydt(dulLH)     = 0;
		dydt(dulLF)     = 0;
		dydt(dulRF)     = 0;
		dydt(dulRH)     = 0;
	} else { // Actuators are driven by the difference between motor and joint forcempute con
		dydt(dulLH)     = (uVec(FlLH) - tau(qlLH))/j_mot_l;
		dydt(dulLF)     = (uVec(FlLF) - tau(qlLF))/j_mot_l;
		dydt(dulRF)     = (uVec(FlRF) - tau(qlRF))/j_mot_l;
		dydt(dulRH)     = (uVec(FlRH) - tau(qlRH))/j_mot_l;
	}

	/**********************
	 * Mechanical dynamics:
	 **********************/
	// Map velocities to position derivatives:
	dydt(x)      = yVec(dx);
	dydt(y)      = yVec(dy);
	dydt(phi)    = yVec(dphi);
	dydt(theta)  = yVec(dtheta);
	dydt(alphaLH) = yVec(dalphaLH);
	dydt(alphaLF) = yVec(dalphaLF);
	dydt(alphaRF) = yVec(dalphaRF);
	dydt(alphaRH) = yVec(dalphaRH);
	dydt(lLH)     = yVec(dlLH);
	dydt(lLF)     = yVec(dlLF);
	dydt(lRF)     = yVec(dlRF);
	dydt(lRH)     = yVec(dlRH);

	// Compute contact forces:
	VectorQd Jlambda;
	if (nContact>0) // check if some ground contact is present
	{
		MatrixXd projM(nContact*2, nContact*2);
		VectorXd projF(nContact*2, 1);
		VectorXd lambda(nContact*2, 1);
		projM = J * Mdecomp.solve(J.transpose());
		projF = J * Mdecomp.solve(tau + h);
		lambda = projM.partialPivLu().solve(-projF - dJdtTimesdqdt);
		
		//for (int n = 0; n<nContact*2; n++)		
		//	lambdaVal[n] = lambda[n];
		
		// Project these forces back into the generalized coordinate space
		Jlambda = J.transpose()*lambda;
	} else {
		Jlambda = VectorQd::Zero();
	}

	// Evaluate the EQM:
	VectorQd dd_q;
	dd_q = Mdecomp.solve(tau + h + Jlambda);

	// Map the generalized accelerations back into continuous state derivatives:
	dydt(dx)      = dd_q(qx);
	dydt(dy)      = dd_q(qy);
	dydt(dphi)    = dd_q(qphi);
	dydt(dtheta)  = dd_q(qtheta);
	dydt(dalphaLH) = dd_q(qalphaLH);
	dydt(dalphaLF) = dd_q(qalphaLF);
	dydt(dalphaRF) = dd_q(qalphaRF);
	dydt(dalphaRH) = dd_q(qalphaRH);
	dydt(dlLH)     = dd_q(qlLH);
	dydt(dlLF)     = dd_q(qlLF);
	dydt(dlRF)     = dd_q(qlRF);
	dydt(dlRH)     = dd_q(qlRH);

	/*****************
	 * Cost Functions:
	 *****************/
	// Compute the mechanical power performed by the actuator (that is actuator velocity times actuator torque/force):
	double P_act_theta, P_act_alphaLH, P_act_alphaLF, P_act_alphaRF, P_act_alphaRH, P_act_lLH, P_act_lLF, P_act_lRF, P_act_lRH;
	P_act_theta = yVec(dutheta)*uVec(Ttheta);
		P_act_alphaLH = yVec(dualphaLH)*uVec(TalphaLH);
		P_act_alphaLF = yVec(dualphaLF)*uVec(TalphaLF);
		P_act_alphaRF = yVec(dualphaRF)*uVec(TalphaRF);
		P_act_alphaRH = yVec(dualphaRH)*uVec(TalphaRH);
		P_act_lLH = yVec(dulLH)*uVec(FlLH);
		P_act_lLF = yVec(dulLF)*uVec(FlLF);
		P_act_lRF = yVec(dulRF)*uVec(FlRF);
		P_act_lRH = yVec(dulRH)*uVec(FlRH);
	// Compute the electrical losses:
	double P_loss_el_theta = uVec(Ttheta)*uVec(Ttheta) * pVec(du_max_theta)*pVec(du_max_theta) / P_max_theta;
	double P_loss_el_alphaLH = uVec(TalphaLH)*uVec(TalphaLH) * pVec(du_max_alpha)*pVec(du_max_alpha) / P_max_alpha;
	double P_loss_el_alphaLF = uVec(TalphaLF)*uVec(TalphaLF) * pVec(du_max_alpha)*pVec(du_max_alpha) / P_max_alpha;
	double P_loss_el_alphaRF = uVec(TalphaRF)*uVec(TalphaRF) * pVec(du_max_alpha)*pVec(du_max_alpha) / P_max_alpha;
	double P_loss_el_alphaRH = uVec(TalphaRH)*uVec(TalphaRH) * pVec(du_max_alpha)*pVec(du_max_alpha) / P_max_alpha;
	double P_loss_el_lLH = uVec(FlLH)*uVec(FlLH) * pVec(du_max_l)*pVec(du_max_l) / P_max_l;
	double P_loss_el_lLF = uVec(FlLF)*uVec(FlLF) * pVec(du_max_l)*pVec(du_max_l) / P_max_l;
	double P_loss_el_lRF = uVec(FlRF)*uVec(FlRF) * pVec(du_max_l)*pVec(du_max_l) / P_max_l;
	double P_loss_el_lRH = uVec(FlRH)*uVec(FlRH) * pVec(du_max_l)*pVec(du_max_l) / P_max_l;

	// Compute the accumulated positive actuator work (assuming that negative work at EACH actuator can not be recovered):
	dydt(posActWork)  = Logistic(P_act_theta, pVec(sigma)) +
			Logistic(P_act_alphaLH, pVec(sigma)) +
			Logistic(P_act_alphaLF, pVec(sigma)) +
                        Logistic(P_act_alphaRF, pVec(sigma)) +
                        Logistic(P_act_alphaRH, pVec(sigma)) +
                        Logistic(P_act_lLH, pVec(sigma)) +
                        Logistic(P_act_lLF, pVec(sigma)) +
                        Logistic(P_act_lRF, pVec(sigma)) +
                        Logistic(P_act_lRH, pVec(sigma));
    // Compute the total electrical Losses
	dydt(totElLoss)  = P_loss_el_theta + P_loss_el_alphaLH + P_loss_el_alphaLF + P_loss_el_alphaRF + P_loss_el_alphaRH + P_loss_el_lLH + P_loss_el_lLF + P_loss_el_lRF + P_loss_el_lRH;
	// Compute the positive electrical work (which allows using negative work in one actuator to perform positive work in another):
    dydt(posElWork)  = max(0.0, P_act_theta + P_act_alphaLH + P_act_alphaLF + P_act_alphaRF + P_act_alphaRH + P_act_lLH + P_act_lLF + P_act_lRF + P_act_lRH + double(dydt(totElLoss)));


	/*******************
	 * Write to pointer:
	 *******************/
    for (int i = 0; i<NY; i++)
    	dydtPtr[i] = dydt(i);
}
/* The JumpMap computes the changes in velocities, that are necessary to
 * bring the contact points to a velocity of zero.  Prior to it's call,
 * the values for continuous states y, discrete states z, excitation
 * states u, and system parameters p must be set to the newest values.
 * **NOTE** the values of z need to represent the phases AFTER the
 * collision.  The function updates the dynamics (i.e., computes mass
 * matrix, differentiable forces, etc. automatically.  It returns an
 * array (size NY) of the state after the collision
 */
void PB_Dynamics::JumpMap(double* yPlusPtr){
	updateDynamics();

	// As most things remain unchanged we copy the current states
	for (int i = 0; i<NY; i++)
		yPlusPtr[i] = yVec(i);

	MatrixXd projM(nContact*2, nContact*2);
	MatrixQd projMinv;
	VectorQd dqMINUS;
	VectorQd dqPLUS;
	// At touchdown, the contact point comes to a complete rest, a fully plastic collision is computed:
	// Velocities before collision:
	dqMINUS(qx)      = yVec(dx);
	dqMINUS(qy)      = yVec(dy);
	dqMINUS(qphi)    = yVec(dphi);
	dqMINUS(qtheta)  = yVec(dtheta);
	dqMINUS(qalphaLH) = yVec(dalphaLH);
	dqMINUS(qalphaLF) = yVec(dalphaLF);
	dqMINUS(qalphaRF) = yVec(dalphaRF);
	dqMINUS(qalphaRH) = yVec(dalphaRH);
	dqMINUS(qlLH)     = yVec(dlLH);
	dqMINUS(qlLF)     = yVec(dlLF);
	dqMINUS(qlRF)     = yVec(dlRF);
	dqMINUS(qlRH)     = yVec(dlRH);
	// Project EoM into the contact space:
	// Rest after contact: J*qPlus = 0
	// with: M*(qPlus-qMinus) = I_cont_q % with the contact impulse I_cont
	// qPlus = inv(M)*I_cont_q + qMinus
	// -> J*inv(M)*I_cont_q + qMinus = 0
	// -> J*inv(M)*J'*I_cont_x + qMinus = 0
	// -> I_cont_x = -inv(J*inv(M)*J')*qMinus
	// -> I_cont_q = -J'*inv(J*inv(M)*J')*J*qMinus
	// qPlus = -inv(M)*J'*inv(J*inv(M)*J')*J*qMinus + qMinus
	projM = J * Mdecomp.solve(J.transpose());
	projMinv = J.transpose() * projM.partialPivLu().solve(J);
	// Velocities after
	dqPLUS = Mdecomp.solve(M*dqMINUS - projMinv*dqMINUS);
	yPlusPtr[dx]       = dqPLUS(qx);
	yPlusPtr[dy]       = dqPLUS(qy);
	yPlusPtr[dphi]     = dqPLUS(qphi);
	yPlusPtr[dtheta]   = dqPLUS(qtheta);
	yPlusPtr[dalphaLH]  = dqPLUS(qalphaLH);
	yPlusPtr[dalphaLF]  = dqPLUS(qalphaLF);
	yPlusPtr[dalphaRF]  = dqPLUS(qalphaRF);
	yPlusPtr[dalphaRH]  = dqPLUS(qalphaRH);
	yPlusPtr[dlLH]      = dqPLUS(qlLH);
	yPlusPtr[dlLF]      = dqPLUS(qlLF);
	yPlusPtr[dlRF]      = dqPLUS(qlRF);
	yPlusPtr[dlRH]      = dqPLUS(qlRH);
}
/* The JumpSet tracks the various events that can happen throughout a
 * stride. Each event value will have a zero crossing in positive
 * direction, when the corresponding event happens  Prior to it's call,
 * the values for continuous states y, discrete states z, excitation
 * states u, and system parameters p must be set to the newest values.
 * **NOTE** the values of z need to represent the phases BEFORE the
 * event.  If one seeks to identify multiple events simultaneously,
 * all involved event values must be zero.  They are returned in an
 * array (size NEV).
 */
void PB_Dynamics::JumpSet(double* eventValPtr){
	updateDynamics();

	// Compute the contact forces:
	VectorXd lambda(nContact*2, 1);
	if (nContact>0)
	{  // check if some ground contact is present
		MatrixXd projM(nContact*2, nContact*2);
		VectorXd projF(nContact*2, 1);
		projM = J * Mdecomp.solve(J.transpose());
		projF = J * Mdecomp.solve(tau + h);
		lambda = projM.partialPivLu().solve(-projF - dJdtTimesdqdt);
	} else {
		// Do nothing, lambda is an empty vector anyway
	}
	int counter = 0;
	if (zVec(phaseLH) == stance) {	//  Event is detected if the vertical contact force of the left leg becomes negative.
		eventValPtr[liftoffLH] = -lambda (counter+1);
		counter = counter + 2;
	} else { // But only in flight
		eventValPtr[liftoffLH] = -1;
	}
	if (zVec(phaseLF) == stance) {	//  Event is detected if the vertical contact force of the left leg becomes negative.
		eventValPtr[liftoffLF] = -lambda (counter+1);
		counter = counter + 2;
	} else { // But only in flight
		eventValPtr[liftoffLF] = -1;
	}
	if (zVec(phaseRF) == stance) {	//  Event is detected if the vertical contact force of the right leg becomes negative.
		eventValPtr[liftoffRF] = -lambda (counter+1);
		counter = counter + 2;
	} else { // But only in flight
		eventValPtr[liftoffRF] = -1;
	}
	if (zVec(phaseRH) == stance) {	//  Event is detected if the vertical contact force of the right leg becomes negative.
		eventValPtr[liftoffRH] = -lambda (counter+1);
		counter = counter + 2;
	} else { // But only in flight
		eventValPtr[liftoffRH] = -1;
	}
	if (zVec(phaseLH) == flight) {	// Event is detected if the left foot goes below the ground during flight
		eventValPtr[touchdownLH] = -posLH(1);
	} else { // But only in flight
		eventValPtr[touchdownLH] = -1;
	}
	if (zVec(phaseLF) == flight) {	// Event is detected if the left foot goes below the ground during flight
		eventValPtr[touchdownLF] = -posLF(1);
	} else { // But only in flight
		eventValPtr[touchdownLF] = -1;
	}
	if (zVec(phaseRF) == flight) {	// Event is detected if the right foot goes below the ground during flight
		eventValPtr[touchdownRF] = -posRF(1);
	} else { // But only in flight
		eventValPtr[touchdownRF] = -1;
	}
	if (zVec(phaseRH) == flight) {	// Event is detected if the right foot goes below the ground during flight
		eventValPtr[touchdownRH] = -posRH(1);
	} else { // But only in flight
		eventValPtr[touchdownRH] = -1;
	}
	// Detect apex transit
	eventValPtr[apexTransit] = yVec(dy);

}

/* Compute a number of constraint values for
 * a) The position of the left and right foot above the ground
 * b) The velocity du of each actuator below du_max*(1-c_lim)
 * c) The force Fu of each actuator below F_max = c_lim*P_max/du_max
 * I.e., each constraint is met as soon it is larger than 0.
 * the vectors for the actuator speeds and velocities are given in the order:
 * [alphaL, alphaR, lL, lR]
 */
void PB_Dynamics::Constraints(double* gcLH, double* gcLF, double* gcRF, double* gcRH, double* du_max, double* F_max, double* Fric_max){
	updateDynamics();
	VectorXd lambdaTemp(nContact*2, 1);
	lambdaTemp = ComputeContact();

	// Ground clearance:
	if (zVec(phaseLH) == flight) {
		*gcLH = posLH(1);
	} else {
		*gcLH = 1; // if the foot is on the ground, the constraint is always met
	}

	if (zVec(phaseLF) == flight) {
		*gcLF = posLF(1);
	} else {
		*gcLF = 1; // sif the foot is on the ground, the constraint is always met
	}

	if (zVec(phaseRF) == flight) {
		*gcRF = posRF(1);
	} else {
		*gcRF = 1; // If the foot is on the ground, the constraint is always met
	}

	if (zVec(phaseRH) == flight) {
		*gcRH = posRH(1);
	} else {
		*gcRH = 1; // If the foot is on the ground, the constraint is always met
	}

		
	if (pVec(hip_jnt_type) == PEA){ // Actuator velocity is joint velocity
		// Compute how much the actual joint velocity is below its upper limit:
		du_max[0] = pVec(du_max_alpha)*(1-c_lim_alpha)-yVec(dalphaLH);
		du_max[1] = pVec(du_max_alpha)*(1-c_lim_alpha)-yVec(dalphaLF);
		du_max[2] = pVec(du_max_alpha)*(1-c_lim_alpha)-yVec(dalphaRF);
		du_max[3] = pVec(du_max_alpha)*(1-c_lim_alpha)-yVec(dalphaRH);
		// Compute how much the actual joint velocity is above its lower limit:
		du_max[8] = pVec(du_max_alpha)*(1-c_lim_alpha)+yVec(dalphaLH);
		du_max[9] = pVec(du_max_alpha)*(1-c_lim_alpha)+yVec(dalphaLF);
		du_max[10] = pVec(du_max_alpha)*(1-c_lim_alpha)+yVec(dalphaRF);
		du_max[11] = pVec(du_max_alpha)*(1-c_lim_alpha)+yVec(dalphaRH);
	} else { // Actuator velocity is different from joint velocity
		// Compute how much the actual actuator velocity is below its upper limit:
		du_max[0] = pVec(du_max_alpha)*(1-c_lim_alpha)-yVec(dualphaLH);
		du_max[1] = pVec(du_max_alpha)*(1-c_lim_alpha)-yVec(dualphaLF);
		du_max[2] = pVec(du_max_alpha)*(1-c_lim_alpha)-yVec(dualphaRF);
		du_max[3] = pVec(du_max_alpha)*(1-c_lim_alpha)-yVec(dualphaRH);
		// Compute how much the actual actuator velocity is above its lower limit:
		du_max[8] = pVec(du_max_alpha)*(1-c_lim_alpha)+yVec(dualphaLH);
		du_max[9] = pVec(du_max_alpha)*(1-c_lim_alpha)+yVec(dualphaLF);
		du_max[10] = pVec(du_max_alpha)*(1-c_lim_alpha)+yVec(dualphaRF);
		du_max[11] = pVec(du_max_alpha)*(1-c_lim_alpha)+yVec(dualphaRF);
	}
	if (pVec(leg_jnt_type) == PEA){ // Actuator velocity is joint velocity
		// Compute how much the actual joint velocity is below its upper limit:
		du_max[4] = pVec(du_max_l)*(1-c_lim_l)-yVec(dlLH);
		du_max[5] = pVec(du_max_l)*(1-c_lim_l)-yVec(dlLF);
		du_max[6] = pVec(du_max_l)*(1-c_lim_l)-yVec(dlRF);
		du_max[7] = pVec(du_max_l)*(1-c_lim_l)-yVec(dlRH);
		// Compute how much the actual joint velocity is above its lower limit:
		du_max[12] = pVec(du_max_l)*(1-c_lim_l)+yVec(dlLH);
		du_max[13] = pVec(du_max_l)*(1-c_lim_l)+yVec(dlLF);
		du_max[14] = pVec(du_max_l)*(1-c_lim_l)+yVec(dlRF);
		du_max[15] = pVec(du_max_l)*(1-c_lim_l)+yVec(dlRH);
	} else { // Actuator velocity is different from joint velocity
		// Compute how much the actual actuator velocity is below its upper limit:
		du_max[4] = pVec(du_max_l)*(1-c_lim_l)-yVec(dulLH);
		du_max[5] = pVec(du_max_l)*(1-c_lim_l)-yVec(dulLF);
		du_max[6] = pVec(du_max_l)*(1-c_lim_l)-yVec(dulRF);
		du_max[7] = pVec(du_max_l)*(1-c_lim_l)-yVec(dulRH);
		// Compute how much the actual actuator velocity is above its lower limit:
		du_max[12] = pVec(du_max_l)*(1-c_lim_l)+yVec(dulLH);
		du_max[13] = pVec(du_max_l)*(1-c_lim_l)+yVec(dulLF);
		du_max[14] = pVec(du_max_l)*(1-c_lim_l)+yVec(dulRF);
		du_max[15] = pVec(du_max_l)*(1-c_lim_l)+yVec(dulRH);
	}
		du_max[16] = pVec(du_max_theta)*(1-c_lim_theta)-yVec(dtheta);
		du_max[17] = pVec(du_max_theta)*(1-c_lim_theta)+yVec(dtheta);

	// Compute how much the actual actuator torque is below its upper limit:
	F_max[0] = c_lim_alpha*P_max_alpha/pVec(du_max_alpha) - uVec(TalphaLH);
	F_max[1] = c_lim_alpha*P_max_alpha/pVec(du_max_alpha) - uVec(TalphaLF);
	F_max[2] = c_lim_alpha*P_max_alpha/pVec(du_max_alpha) - uVec(TalphaRF);
	F_max[3] = c_lim_alpha*P_max_alpha/pVec(du_max_alpha) - uVec(TalphaRH);
	F_max[4] = c_lim_l*P_max_l/pVec(du_max_l) - uVec(FlLH);
	F_max[5] = c_lim_l*P_max_l/pVec(du_max_l) - uVec(FlLF);
	F_max[6] = c_lim_l*P_max_l/pVec(du_max_l) - uVec(FlRF);
	F_max[7] = c_lim_l*P_max_l/pVec(du_max_l) - uVec(FlRH);
	// Compute how much the actual actuator torque is above its lower limit:
	F_max[8] = c_lim_alpha*P_max_alpha/pVec(du_max_alpha) + uVec(TalphaLH);
	F_max[9] = c_lim_alpha*P_max_alpha/pVec(du_max_alpha) + uVec(TalphaLF);
	F_max[10] = c_lim_alpha*P_max_alpha/pVec(du_max_alpha) + uVec(TalphaRF);
	F_max[11] = c_lim_alpha*P_max_alpha/pVec(du_max_alpha) + uVec(TalphaRH);
	F_max[12] = c_lim_l*P_max_l/pVec(du_max_l) + uVec(FlLH);
	F_max[13] = c_lim_l*P_max_l/pVec(du_max_l) + uVec(FlLF);
	F_max[14] = c_lim_l*P_max_l/pVec(du_max_l) + uVec(FlRF);
	F_max[15] = c_lim_l*P_max_l/pVec(du_max_l) + uVec(FlRH);

	F_max[16] = c_lim_theta*P_max_theta/pVec(du_max_theta) - uVec(Ttheta);
	F_max[17] = c_lim_theta*P_max_theta/pVec(du_max_theta) + uVec(Ttheta);
	
	//cout << "F_max      " << F_max << endl;

	// Check friction cone
	int j = -2; 
/*
	if (ncontact == 0)
		Fric_max[0] = 1;
	else{
		for (int i = 0; i<nContact; i++){
			j = j+2;
			Fric_max[i] = lambda(j)-pVec(mu)*lambda(j+1);
		}
	}
*/
//	if (nContact == 0)
//		Fric_max[0] = 1;
//	else{
		//cout << "ncontact    " << nContact << endl;
		//cout << "lambda      " << lambda << endl;

/*
	VectorXd lambda(nContact*2, 1);
	if (nContact>0)
	{  // check if some ground contact is present
		MatrixXd projM(nContact*2, nContact*2);
		VectorXd projF(nContact*2, 1);
		projM = J * Mdecomp.solve(J.transpose());
		projF = J * Mdecomp.solve(tau + h);
		lambda = projM.partialPivLu().solve(-projF - dJdtTimesdqdt);
	} else {
		// Do nothing, lambda is an empty vector anyway
	}
*/
		
/*

	for (int i = 0; i<nContact; i++){
			//cout << "i      " << i << endl;		
			//cout << "ncontact    " << nContact << endl;

			j = j+2;

			//cout << "j      " << j << endl;
			//cout << "lambda(j)      " << lambda[j] << endl;
			//cout << "lambda(j+1)      " << lambda[j+1] << endl;

			Fric_max[i] = lambda[j]-pVec(mu)*lambda[j+1];
*/				
//		}
//	cout << "Fric_max (1)     " << *Fric_max[1] << endl;
	//return nContact	

	for (int i = 0; i<4; i++){
		//cout << "i " << i << endl;
		if (i<nContact){
			j = j+2;
			Fric_max[i] = pVec(mu)*abs(lambdaTemp[j+1])-abs(lambdaTemp[j]);
			//Fric_max[i] = 1 -abs(lambdaTemp[j]);		
			//cout << "Fric_max " << (pVec(mu)*abs(lambdaTemp[j+1])-abs(lambdaTemp[j])) << endl;
			}
		else {
			Fric_max[i] = 1;
			//cout << "Fric_max " << 1 << endl;
			}

		}

}

// Update current states
void PB_Dynamics::setContState(double* yPtr){yVec = Map<VectorYd>(yPtr);}
void PB_Dynamics::setDiscState(double* zPtr){zVec = Map<VectorZd>(zPtr);}
void PB_Dynamics::setSystParam(double* pPtr){pVec = Map<VectorPd>(pPtr);}
void PB_Dynamics::setExctState(double* uPtr){uVec = Map<VectorUd>(uPtr);}
// Retrieve the current states
void PB_Dynamics::getContState(double* yPtr){
	for (int i = 0; i<NY; i++)
		yPtr[i] = yVec(i);
}
void PB_Dynamics::getDiscState(double* zPtr){
	for (int i = 0; i<NZ; i++)
		zPtr[i] = zVec(i);
}
void PB_Dynamics::getSystParam(double* pPtr){
	for (int i = 0; i<NP; i++)
		pPtr[i] = pVec(i);
}
void PB_Dynamics::getExctState(double* uPtr){
	for (int i = 0; i<NU; i++)
		uPtr[i] = uVec(i);
}

// Prepare all the components for the equations of motion:
void PB_Dynamics::updateDynamics(){
	// Before this function is called, we assume that all states and parameters have been updated.
	// Update all parameters (inertia values, damping coefficients, ...)
	ComputeDependentParameters();
	// We compute new values for all the 'dynamics'-components:
	ComputeMassMatrix();  // M & Mdecomp
	ComputeDiffForces();  // h
	ComputeJointForces(); // f (this also computes T_spring_alpha and F_spring_l)
	nContact = 0;
	// And for the contact processing:
	if (zVec(phaseLH) == flight) {
		ComputeContactPointLH();     // posL
	} else {
		nContact++;
		ComputeContactJacobianLH();  //JL
		ComputeContactJacobianDtTIMESdqdtLH();  // dJLdtTimesdqdt
	}
	if (zVec(phaseLF) == flight) {
		ComputeContactPointLF();     // posL
	} else {
		nContact++;
		ComputeContactJacobianLF();  //JL
		ComputeContactJacobianDtTIMESdqdtLF();  // dJLdtTimesdqdt
	}
	if (zVec(phaseRF) == flight) {
		ComputeContactPointRF();     // posL
	} else {
		nContact++;
		ComputeContactJacobianRF();  //JR
		ComputeContactJacobianDtTIMESdqdtRF();  // dJRdtTimesdqdt
	}
	if (zVec(phaseRH) == flight) {
		ComputeContactPointRH();     // posL
	} else {
		nContact++;
		ComputeContactJacobianRH();  //JR
		ComputeContactJacobianDtTIMESdqdtRH();  // dJRdtTimesdqdt
	}
	//cout << "updateDynamics"  << nContact << endl;
	// And build the compound Jacobian (which starts with L, then R)
	J.resize(nContact*2, NQ);
	dJdtTimesdqdt.resize(nContact*2, 1);
	int counter = 0;
	if (zVec(phaseLH) == stance) {
		J.row(counter)   = JLH.row(0);
		J.row(counter+1) = JLH.row(1);
		dJdtTimesdqdt(counter)   = dJLHdtdqdt(0);
		dJdtTimesdqdt(counter+1) = dJLHdtdqdt(1);
		counter = counter + 2;
	}
	if (zVec(phaseLF) == stance) {
		J.row(counter)   = JLF.row(0);
		J.row(counter+1) = JLF.row(1);
		dJdtTimesdqdt(counter)   = dJLFdtdqdt(0);
		dJdtTimesdqdt(counter+1) = dJLFdtdqdt(1);
		counter = counter + 2;
	}
	if (zVec(phaseRF) == stance) {
		J.row(counter)   = JRF.row(0);
		J.row(counter+1) = JRF.row(1);
		dJdtTimesdqdt(counter)   = dJRFdtdqdt(0);
		dJdtTimesdqdt(counter+1) = dJRFdtdqdt(1);
		counter = counter + 2;
	}
	if (zVec(phaseRH) == stance) {
		J.row(counter)   = JRH.row(0);
		J.row(counter+1) = JRH.row(1);
		dJdtTimesdqdt(counter)   = dJRHdtdqdt(0);
		dJdtTimesdqdt(counter+1) = dJRHdtdqdt(1);
		counter = counter + 2;
	}
}

// Update dependent parameters
void PB_Dynamics::ComputeDependentParameters(){
	// Compute maximal power and the unscaled actuator inertia according to the scaling laws and
	// the values provided for rho_alpha and rho_l:
	P_max_theta  = pVec(P_max)  * pow(double(pVec(rho_theta)), +1.35);
	P_max_alpha  = pVec(P_max)  * pow(double(pVec(rho_alpha)), +1.35);
	P_max_l      = pVec(P_max)  * pow(double(pVec(rho_l)),     +1.35);
	// Percentage of the maximal power that can be used
		//Note: 100000 number sets theta force limit very high
			//Since it is a clutch in the passive case, it doesn't matter what the force is
	c_lim_theta  = pVec(c_lim)  * pow(double(pVec(rho_theta)), -0.37);
	c_lim_alpha  = pVec(c_lim)  * pow(double(pVec(rho_alpha)), -0.37);
	c_lim_l      = pVec(c_lim)  * pow(double(pVec(rho_l)),     -0.37);
	// Compute the actual actuator inertias as a function of the gear ratios (maximal speeds)
	double j_unsc_theta = pVec(j_unsc) * pow(double(pVec(rho_theta)), +1.26);
	double j_unsc_alpha = pVec(j_unsc) * pow(double(pVec(rho_alpha)), +1.26);
	double j_unsc_l     = pVec(j_unsc) * pow(double(pVec(rho_l)),     +1.26);
	j_mot_theta = j_unsc_theta/(pVec(du_max_theta)*pVec(du_max_theta));
	j_mot_alpha = j_unsc_alpha/(pVec(du_max_alpha)*pVec(du_max_alpha));
	j_mot_l     = j_unsc_l/(pVec(du_max_l)*pVec(du_max_l));
	// Compute the viscous damping coefficient of the springs, according to the desired damping ratio:
	double j_leg   = pVec(j3) + pow(double(pVec(l_0) - pVec(l3)),2)*pVec(m3) + pVec(j2) + pow(double(pVec(l2)),2)*pVec(m2); // total leg inertia wrt the hip
  	double j_half  = 0.123 + j_leg;
//	double j_half  = pVec(j1) + pow(pVec(l0),2) * pVec(m1) + pVec(j3) + (pow(pVec(l_0) - pVec(l3),2) + pow(pVec(l1),2))*pVec(m3) + pVec(j2) + (pow(pVec(l2),2)+ pow(pVec(l1),2))*pVec(m2);

	b_theta  = pVec(bthetaRat)*2*sqrt(double(pVec(ktheta)*j_half));
	b_alpha  = pVec(balphaRat)*2*sqrt(double(pVec(kalpha)*j_leg));
	b_l      = pVec(blRat)*2*sqrt(double(pVec(kl)*pVec(m3)));
	// To prevent instability damping values are forced to be non-negative:
	b_theta = max(0.0, b_theta);
	b_alpha = max(0.0, b_alpha);
	b_l     = max(0.0, b_l);
}

// Components for the Equations of Motion
void PB_Dynamics::ComputeMassMatrix(){
	M = MatrixQd::Zero();
	double t727 = yVec(alphaLH)+yVec(phi)+yVec(theta);
	double t728 = cos(t727);
	double t729 = yVec(alphaRH)+yVec(phi)+yVec(theta);
	double t730 = cos(t729);
	double t731 = yVec(phi)-yVec(theta);
	double t732 = sin(t731);
	double t733 = yVec(alphaLF)+yVec(phi)-yVec(theta);
	double t734 = cos(t733);
	double t735 = yVec(alphaRF)+yVec(phi)-yVec(theta);
	double t736 = cos(t735);
	double t737 = yVec(phi)+yVec(theta);
	double t738 = sin(t737);
	double t739 = pVec(l2)*pVec(m2)*t728;
	double t740 = pVec(l2)*pVec(m2)*t730;
	double t741 = yVec(lLH)*pVec(m3)*t728;
	double t742 = yVec(lRH)*pVec(m3)*t730;
	double t743 = pVec(l2)*pVec(m2)*t734;
	double t744 = pVec(l2)*pVec(m2)*t736;
	double t745 = yVec(lLF)*pVec(m3)*t734;
	double t746 = yVec(lRF)*pVec(m3)*t736;
	double t747 = pVec(l0)*pVec(m1)*t738;
	double t748 = pVec(l1)*pVec(m2)*t738*2.0;
	double t749 = pVec(l1)*pVec(m3)*t738*2.0;
	double t750 = pVec(l3)*pVec(m3)*t734;
	double t751 = pVec(l3)*pVec(m3)*t736;
	double t752 = pVec(m1)*2.0;
	double t753 = pVec(m2)*4.0;
	double t754 = pVec(m3)*4.0;
	double t755 = t752+t753+t754;
	double t756 = cos(t731);
	double t757 = sin(t727);
	double t758 = sin(t729);
	double t759 = sin(t733);
	double t760 = sin(t735);
	double t761 = cos(t737);
	double t762 = pVec(l0)*pVec(m1)*t756;
	double t763 = pVec(l1)*pVec(m2)*t756*2.0;
	double t764 = pVec(l1)*pVec(m3)*t756*2.0;
	double t765 = pVec(l2)*pVec(m2)*t757;
	double t766 = pVec(l2)*pVec(m2)*t758;
	double t767 = yVec(lLH)*pVec(m3)*t757;
	double t768 = yVec(lRH)*pVec(m3)*t758;
	double t769 = pVec(l2)*pVec(m2)*t759;
	double t770 = pVec(l2)*pVec(m2)*t760;
	double t771 = yVec(lLF)*pVec(m3)*t759;
	double t772 = yVec(lRF)*pVec(m3)*t760;
	double t773 = pVec(l3)*pVec(m3)*t759;
	double t774 = pVec(l3)*pVec(m3)*t760;
	double t775 = pVec(l0)*pVec(m1)*t732;
	double t776 = pVec(l1)*pVec(m2)*t732*2.0;
	double t777 = pVec(l1)*pVec(m3)*t732*2.0;
	double t778 = pVec(l1)*pVec(l1);
	double t779 = sin(yVec(alphaLF));
	double t780 = sin(yVec(alphaLH));
	double t781 = sin(yVec(alphaRF));
	double t782 = sin(yVec(alphaRH));
	double t783 = yVec(lLF)*yVec(lLF);
	double t784 = pVec(m3)*t783;
	double t785 = yVec(lLH)*yVec(lLH);
	double t786 = pVec(m3)*t785;
	double t787 = yVec(lRF)*yVec(lRF);
	double t788 = pVec(m3)*t787;
	double t789 = yVec(lRH)*yVec(lRH);
	double t790 = pVec(m3)*t789;
	double t791 = pVec(l1)*pVec(l2)*pVec(m2)*t779*2.0;
	double t792 = pVec(l1)*pVec(l3)*pVec(m3)*t780*2.0;
	double t793 = pVec(l1)*pVec(l2)*pVec(m2)*t781*2.0;
	double t794 = pVec(l1)*pVec(l3)*pVec(m3)*t782*2.0;
	double t795 = pVec(l1)*yVec(lLF)*pVec(m3)*t779*2.0;
	double t796 = pVec(l1)*yVec(lRF)*pVec(m3)*t781*2.0;
	double t797 = pVec(l2)*pVec(l2);
	double t798 = pVec(l3)*pVec(l3);
	double t799 = pVec(m2)*t797;
	double t800 = pVec(m3)*t798;
	double t801 = pVec(l3)*yVec(lLF)*pVec(m3)*2.0;
	double t802 = pVec(l3)*yVec(lRF)*pVec(m3)*2.0;
	double t835 = pVec(l3)*pVec(m3)*t728;
	double t853 = pVec(l3)*pVec(m3)*t730;
	double t803 = t739+t740+t741+t742-t743-t744-t745-t746+t747+t748+t749+t750+t751+t775+t776+t777-t835-t853;
	double t836 = pVec(l3)*pVec(m3)*t757;
	double t854 = pVec(l3)*pVec(m3)*t758;
	double t804 = -t762-t763-t764+t765+t766+t767+t768-t769-t770-t771-t772+t773+t774-t836-t854-pVec(l0)*pVec(m1)*t761-pVec(l1)*pVec(m2)*t761*2.0-pVec(l1)*pVec(m3)*t761*2.0;
	double t805 = pVec(l1)*pVec(l3)*pVec(m3)*t779*2.0;
	double t806 = pVec(l1)*pVec(l3)*pVec(m3)*t781*2.0;
	double t817 = pVec(l3)*yVec(lLH)*pVec(m3)*2.0;
	double t818 = pVec(l3)*yVec(lRH)*pVec(m3)*2.0;
	double t819 = pVec(l1)*pVec(l2)*pVec(m2)*t780*2.0;
	double t820 = pVec(l1)*pVec(l2)*pVec(m2)*t782*2.0;
	double t821 = pVec(l1)*yVec(lLH)*pVec(m3)*t780*2.0;
	double t822 = pVec(l1)*yVec(lRH)*pVec(m3)*t782*2.0;
	double t807 = -t784+t786-t788+t790-t791+t792-t793+t794-t795-t796+t801+t802+t805+t806-t817-t818-t819-t820-t821-t822;
	double t808 = pVec(j1_)*2.0;
	double t809 = pVec(j2)*4.0;
	double t810 = pVec(j3)*4.0;
	double t811 = pVec(l0)*pVec(l0);
	double t812 = pVec(m1)*t811*2.0;
	double t813 = pVec(m2)*t778*4.0;
	double t814 = pVec(m3)*t778*4.0;
	double t815 = pVec(m2)*t797*4.0;
	double t816 = pVec(m3)*t798*4.0;
	double t823 = pVec(l1)*pVec(l3)*pVec(m3)*t780;
	double t824 = cos(yVec(alphaLH));
	double t825 = pVec(l1)*pVec(m3)*t824;
	double t826 = pVec(l1)*pVec(l2)*pVec(m2)*t779;
	double t827 = pVec(l1)*yVec(lLF)*pVec(m3)*t779;
	double t828 = cos(yVec(alphaLF));
	double t829 = pVec(l1)*pVec(l2)*pVec(m2)*t781;
	double t830 = pVec(l1)*yVec(lRF)*pVec(m3)*t781;
	double t831 = cos(yVec(alphaRF));
	double t832 = pVec(l1)*pVec(l3)*pVec(m3)*t782;
	double t833 = cos(yVec(alphaRH));
	double t834 = pVec(l1)*pVec(m3)*t833;
	double t838 = pVec(l1)*pVec(l2)*pVec(m2)*t780;
	double t839 = pVec(l1)*yVec(lLH)*pVec(m3)*t780;
	double t837 = pVec(j2)+pVec(j3)+t786+t799+t800-t817+t823-t838-t839;
	double t840 = pVec(m3)*t757;
	double t841 = t743+t745-t750;
	double t842 = t769+t771-t773;
	double t843 = pVec(l1)*pVec(l3)*pVec(m3)*t779;
	double t844 = -pVec(j2)-pVec(j3)-t784-t799-t800+t801-t826-t827+t843;
	double t845 = pVec(m3)*t759;
	double t846 = pVec(l1)*pVec(m3)*t828;
	double t847 = t744+t746-t751;
	double t848 = t770+t772-t774;
	double t849 = pVec(l1)*pVec(l3)*pVec(m3)*t781;
	double t850 = -pVec(j2)-pVec(j3)-t788-t799-t800+t802-t829-t830+t849;
	double t851 = pVec(m3)*t760;
	double t852 = pVec(l1)*pVec(m3)*t831;
	double t856 = pVec(l1)*pVec(l2)*pVec(m2)*t782;
	double t857 = pVec(l1)*yVec(lRH)*pVec(m3)*t782;
	double t855 = pVec(j2)+pVec(j3)+t790+t799+t800-t818+t832-t856-t857;
	double t858 = pVec(m3)*t758;
	M(0,0) = t755;
	M(0,2) = t739+t740+t741+t742+t743+t744+t745+t746+t747+t748+t749-pVec(l0)*pVec(m1)*t732-pVec(l3)*pVec(m3)*t728-pVec(l1)*pVec(m2)*t732*2.0-pVec(l1)*pVec(m3)*t732*2.0-pVec(l3)*pVec(m3)*t730-pVec(l3)*pVec(m3)*t734-pVec(l3)*pVec(m3)*t736;
	M(0,3) = t803;
	M(0,4) = t739+t741-pVec(l3)*pVec(m3)*t728;
	M(0,5) = t840;
	M(0,6) = t841;
	M(0,7) = t845;
	M(0,8) = t847;
	M(0,9) = t851;
	M(0,10) = t740+t742-pVec(l3)*pVec(m3)*t730;
	M(0,11) = t858;
	M(1,1) = t755;
	M(1,2) = t762+t763+t764+t765+t766+t767+t768+t769+t770+t771+t772-pVec(l0)*pVec(m1)*t761-pVec(l3)*pVec(m3)*t757-pVec(l1)*pVec(m2)*t761*2.0-pVec(l3)*pVec(m3)*t758-pVec(l1)*pVec(m3)*t761*2.0-pVec(l3)*pVec(m3)*t759-pVec(l3)*pVec(m3)*t760;
	M(1,3) = t804;
	M(1,4) = t765+t767-pVec(l3)*pVec(m3)*t757;
	M(1,5) = -pVec(m3)*t728;
	M(1,6) = t842;
	M(1,7) = -pVec(m3)*t734;
	M(1,8) = t848;
	M(1,9) = -pVec(m3)*t736;
	M(1,10) = t766+t768-pVec(l3)*pVec(m3)*t758;
	M(1,11) = -pVec(m3)*t730;
	M(2,0) = t739+t740+t741+t742+t743+t744+t745+t746+t747+t748+t749-t750-t751-t775-t776-t777-pVec(l3)*pVec(m3)*t728-pVec(l3)*pVec(m3)*t730;
	M(2,1) = t762+t763+t764+t765+t766+t767+t768+t769+t770+t771+t772-t773-t774-pVec(l0)*pVec(m1)*t761-pVec(l3)*pVec(m3)*t757-pVec(l1)*pVec(m2)*t761*2.0-pVec(l3)*pVec(m3)*t758-pVec(l1)*pVec(m3)*t761*2.0;
	M(2,2) = t784+t786+t788+t790+t791+t792+t793+t794+t795+t796+t808+t809+t810+t812+t813+t814+t815+t816-pVec(l3)*yVec(lLF)*pVec(m3)*2.0-pVec(l3)*yVec(lLH)*pVec(m3)*2.0-pVec(l3)*yVec(lRF)*pVec(m3)*2.0-pVec(l3)*yVec(lRH)*pVec(m3)*2.0-pVec(l1)*pVec(l2)*pVec(m2)*t780*2.0-pVec(l1)*pVec(l3)*pVec(m3)*t779*2.0-pVec(l1)*pVec(l2)*pVec(m2)*t782*2.0-pVec(l1)*pVec(l3)*pVec(m3)*t781*2.0-pVec(l1)*yVec(lLH)*pVec(m3)*t780*2.0-pVec(l1)*yVec(lRH)*pVec(m3)*t782*2.0;
	M(2,3) = t807;
	M(2,4) = pVec(j2)+pVec(j3)+t786+t799+t800+t823-pVec(l3)*yVec(lLH)*pVec(m3)*2.0-pVec(l1)*pVec(l2)*pVec(m2)*t780-pVec(l1)*yVec(lLH)*pVec(m3)*t780;
	M(2,5) = t825;
	M(2,6) = pVec(j2)+pVec(j3)+t784+t799+t800-t801+t826+t827-pVec(l1)*pVec(l3)*pVec(m3)*t779;
	M(2,7) = -pVec(l1)*pVec(m3)*t828;
	M(2,8) = pVec(j2)+pVec(j3)+t788+t799+t800-t802+t829+t830-pVec(l1)*pVec(l3)*pVec(m3)*t781;
	M(2,9) = -pVec(l1)*pVec(m3)*t831;
	M(2,10) = pVec(j2)+pVec(j3)+t790+t799+t800+t832-pVec(l3)*yVec(lRH)*pVec(m3)*2.0-pVec(l1)*pVec(l2)*pVec(m2)*t782-pVec(l1)*yVec(lRH)*pVec(m3)*t782;
	M(2,11) = t834;
	M(3,0) = t803;
	M(3,1) = t804;
	M(3,2) = t807;
	M(3,3) = t784+t786+t788+t790+t791+t792+t793+t794+t795+t796-t801-t802-t805-t806+t808+t809+t810+t812+t813+t814+t815+t816-t817-t818-t819-t820-t821-t822;
	M(3,4) = t837;
	M(3,5) = t825;
	M(3,6) = t844;
	M(3,7) = t846;
	M(3,8) = t850;
	M(3,9) = t852;
	M(3,10) = t855;
	M(3,11) = t834;
	M(4,0) = t739+t741-t835;
	M(4,1) = t765+t767-t836;
	M(4,2) = t837;
	M(4,3) = t837;
	M(4,4) = pVec(j2)+pVec(j3)+t786+t799+t800-t817;
	M(5,0) = t840;
	M(5,1) = -pVec(m3)*t728;
	M(5,2) = t825;
	M(5,3) = t825;
	M(5,5) = pVec(m3);
	M(6,0) = t841;
	M(6,1) = t842;
	M(6,2) = pVec(j2)+pVec(j3)+t784+t799+t800-t801+t826+t827-t843;
	M(6,3) = t844;
	M(6,6) = pVec(j2)+pVec(j3)+t784+t799+t800-t801;
	M(7,0) = t845;
	M(7,1) = -pVec(m3)*t734;
	M(7,2) = -t846;
	M(7,3) = t846;
	M(7,7) = pVec(m3);
	M(8,0) = t847;
	M(8,1) = t848;
	M(8,2) = pVec(j2)+pVec(j3)+t788+t799+t800-t802+t829+t830-t849;
	M(8,3) = t850;
	M(8,8) = pVec(j2)+pVec(j3)+t788+t799+t800-t802;
	M(9,0) = t851;
	M(9,1) = -pVec(m3)*t736;
	M(9,2) = -t852;
	M(9,3) = t852;
	M(9,9) = pVec(m3);
	M(10,0) = t740+t742-t853;
	M(10,1) = t766+t768-t854;
	M(10,2) = t855;
	M(10,3) = t855;
	M(10,10) = pVec(j2)+pVec(j3)+t790+t799+t800-t818;
	M(11,0) = t858;
	M(11,1) = -pVec(m3)*t730;
	M(11,2) = t834;
	M(11,3) = t834;
	M(11,11) = pVec(m3);
	
	
	
	// Increase inertia matrix values for DOFs with parallel elastic actuation:
	// HIP
	if (pVec(hip_jnt_type) == PEA){
		M(qalphaLH,qalphaLH) = M(qalphaLH,qalphaLH) + j_mot_alpha;
		M(qalphaLF,qalphaLF) = M(qalphaLF,qalphaLF) + j_mot_alpha;
		M(qalphaRF,qalphaRF) = M(qalphaRF,qalphaRF) + j_mot_alpha;
		M(qalphaRH,qalphaRH) = M(qalphaRF,qalphaRH) + j_mot_alpha;
	}
	// LEG
	if (pVec(leg_jnt_type) == PEA){
		M(qlLH,qlLH) = M(qlLH,qlLH) + j_mot_l;
		M(qlLF,qlLF) = M(qlLF,qlLF) + j_mot_l;
		M(qlRF,qlRF) = M(qlRF,qlRF) + j_mot_l;
		M(qlRF,qlRH) = M(qlRH,qlRH) + j_mot_l;
	}
	// directly decompose the Mass-matrix:
	Mdecomp = M.ldlt();
}
void PB_Dynamics::ComputeDiffForces(){
	h = VectorQd::Zero();
	double t860 = yVec(alphaLF)+yVec(phi)-yVec(theta);
	double t861 = sin(t860);
	double t862 = yVec(alphaRF)+yVec(phi)-yVec(theta);
	double t863 = sin(t862);
	double t864 = cos(t860);
	double t865 = yVec(dlLF)*pVec(m3)*t864;
	double t866 = cos(t862);
	double t867 = yVec(dlRF)*pVec(m3)*t866;
	double t868 = yVec(dalphaLF)*pVec(l3)*pVec(m3)*t861;
	double t869 = yVec(dalphaRF)*pVec(l3)*pVec(m3)*t863;
	double t870 = yVec(dphi)*pVec(l3)*pVec(m3)*t861;
	double t871 = yVec(dphi)*pVec(l3)*pVec(m3)*t863;
	double t872 = yVec(dtheta)*pVec(l2)*pVec(m2)*t861;
	double t873 = yVec(dtheta)*pVec(l2)*pVec(m2)*t863;
	double t874 = yVec(dtheta)*yVec(lLF)*pVec(m3)*t861;
	double t875 = yVec(dtheta)*yVec(lRF)*pVec(m3)*t863;
	double t876 = yVec(phi)+yVec(theta);
	double t877 = cos(t876);
	double t878 = yVec(phi)-yVec(theta);
	double t879 = cos(t878);
	double t880 = yVec(alphaLH)+yVec(phi)+yVec(theta);
	double t881 = sin(t880);
	double t882 = yVec(alphaRH)+yVec(phi)+yVec(theta);
	double t883 = sin(t882);
	double t884 = cos(t880);
	double t885 = yVec(dlLH)*pVec(m3)*t884;
	double t886 = cos(t882);
	double t887 = yVec(dlRH)*pVec(m3)*t886;
	double t888 = yVec(dphi)*pVec(l0)*pVec(m1)*t877;
	double t889 = yVec(dphi)*pVec(l1)*pVec(m2)*t877*2.0;
	double t890 = yVec(dphi)*pVec(l1)*pVec(m3)*t877*2.0;
	double t891 = yVec(dtheta)*pVec(l0)*pVec(m1)*t877;
	double t892 = yVec(dtheta)*pVec(l1)*pVec(m2)*t877*2.0;
	double t893 = yVec(dtheta)*pVec(l1)*pVec(m3)*t877*2.0;
	double t894 = yVec(dtheta)*pVec(l0)*pVec(m1)*t879;
	double t895 = yVec(dtheta)*pVec(l1)*pVec(m2)*t879*2.0;
	double t896 = yVec(dtheta)*pVec(l1)*pVec(m3)*t879*2.0;
	double t897 = yVec(dalphaLH)*pVec(l3)*pVec(m3)*t881;
	double t898 = yVec(dalphaRH)*pVec(l3)*pVec(m3)*t883;
	double t899 = yVec(dphi)*pVec(l3)*pVec(m3)*t881;
	double t900 = yVec(dphi)*pVec(l3)*pVec(m3)*t883;
	double t901 = yVec(dtheta)*pVec(l3)*pVec(m3)*t881;
	double t902 = yVec(dtheta)*pVec(l3)*pVec(m3)*t883;
	double t903 = yVec(dlLH)*pVec(m3)*t881;
	double t904 = yVec(dlRH)*pVec(m3)*t883;
	double t905 = sin(t876);
	double t906 = yVec(dalphaLH)*pVec(l2)*pVec(m2)*t884;
	double t907 = yVec(dalphaRH)*pVec(l2)*pVec(m2)*t886;
	double t908 = yVec(dphi)*pVec(l2)*pVec(m2)*t884;
	double t909 = yVec(dphi)*pVec(l2)*pVec(m2)*t886;
	double t910 = yVec(dtheta)*pVec(l2)*pVec(m2)*t884;
	double t911 = yVec(dtheta)*pVec(l2)*pVec(m2)*t886;
	double t912 = yVec(dalphaLH)*yVec(lLH)*pVec(m3)*t884;
	double t913 = yVec(dalphaRH)*yVec(lRH)*pVec(m3)*t886;
	double t914 = yVec(dphi)*yVec(lLH)*pVec(m3)*t884;
	double t915 = yVec(dtheta)*yVec(lLH)*pVec(m3)*t884;
	double t916 = yVec(dphi)*yVec(lRH)*pVec(m3)*t886;
	double t917 = yVec(dtheta)*yVec(lRH)*pVec(m3)*t886;
	double t918 = sin(t878);
	double t919 = yVec(dlLF)*pVec(m3)*t861;
	double t920 = yVec(dlRF)*pVec(m3)*t863;
	double t921 = yVec(dalphaLF)*pVec(l2)*pVec(m2)*t864;
	double t922 = yVec(dalphaRF)*pVec(l2)*pVec(m2)*t866;
	double t923 = yVec(dphi)*pVec(l2)*pVec(m2)*t864;
	double t924 = yVec(dphi)*pVec(l2)*pVec(m2)*t866;
	double t925 = yVec(dtheta)*pVec(l3)*pVec(m3)*t864;
	double t926 = yVec(dtheta)*pVec(l3)*pVec(m3)*t866;
	double t927 = yVec(dalphaLF)*yVec(lLF)*pVec(m3)*t864;
	double t928 = yVec(dalphaRF)*yVec(lRF)*pVec(m3)*t866;
	double t929 = yVec(dphi)*yVec(lLF)*pVec(m3)*t864;
	double t930 = yVec(dphi)*yVec(lRF)*pVec(m3)*t866;
	double t931 = yVec(dphi)*pVec(l0)*pVec(m1)*t905;
	double t932 = yVec(dphi)*pVec(l1)*pVec(m2)*t905*2.0;
	double t933 = yVec(dphi)*pVec(l1)*pVec(m3)*t905*2.0;
	double t934 = yVec(dtheta)*pVec(l0)*pVec(m1)*t905;
	double t935 = yVec(dtheta)*pVec(l1)*pVec(m2)*t905*2.0;
	double t936 = yVec(dtheta)*pVec(l1)*pVec(m3)*t905*2.0;
	double t937 = yVec(dtheta)*pVec(l0)*pVec(m1)*t918;
	double t938 = yVec(dtheta)*pVec(l1)*pVec(m2)*t918*2.0;
	double t939 = yVec(dtheta)*pVec(l1)*pVec(m3)*t918*2.0;
	double t940 = yVec(dalphaLF)*pVec(l3)*pVec(m3)*t864;
	double t941 = yVec(dphi)*pVec(l3)*pVec(m3)*t864;
	double t942 = yVec(dtheta)*pVec(l2)*pVec(m2)*t864;
	double t943 = yVec(dtheta)*yVec(lLF)*pVec(m3)*t864;
	double t944 = yVec(dalphaRF)*pVec(l3)*pVec(m3)*t866;
	double t945 = yVec(dphi)*pVec(l3)*pVec(m3)*t866;
	double t946 = yVec(dtheta)*pVec(l2)*pVec(m2)*t866;
	double t947 = yVec(dtheta)*yVec(lRF)*pVec(m3)*t866;
	double t948 = cos(yVec(alphaLF));
	double t949 = cos(yVec(alphaRF));
	double t950 = sin(yVec(alphaLF));
	double t951 = yVec(dtheta)*pVec(l3)*pVec(m3)*2.0;
	double t952 = sin(yVec(alphaRF));
	double t953 = sin(yVec(alphaLH));
	double t954 = yVec(dphi)*pVec(l3)*pVec(m3)*2.0;
	double t955 = sin(yVec(alphaRH));
	double t956 = yVec(dy)*pVec(l2)*pVec(m2)*t864;
	double t957 = yVec(dy)*pVec(l2)*pVec(m2)*t866;
	double t958 = yVec(dy)*yVec(lLF)*pVec(m3)*t864;
	double t959 = yVec(dy)*yVec(lRF)*pVec(m3)*t866;
	double t960 = yVec(dx)*pVec(l3)*pVec(m3)*t861;
	double t961 = yVec(dx)*pVec(l3)*pVec(m3)*t863;
	double t962 = yVec(dx)*pVec(l0)*pVec(m1)*t877;
	double t963 = yVec(dx)*pVec(l1)*pVec(m2)*t877*2.0;
	double t964 = yVec(dx)*pVec(l1)*pVec(m3)*t877*2.0;
	double t965 = yVec(dy)*pVec(l0)*pVec(m1)*t905;
	double t966 = yVec(dy)*pVec(l1)*pVec(m2)*t905*2.0;
	double t967 = yVec(dy)*pVec(l1)*pVec(m3)*t905*2.0;
	double t968 = yVec(dy)*pVec(l2)*pVec(m2)*t884;
	double t969 = yVec(dy)*pVec(l2)*pVec(m2)*t886;
	double t970 = yVec(dy)*yVec(lLH)*pVec(m3)*t884;
	double t971 = yVec(dy)*yVec(lRH)*pVec(m3)*t886;
	double t972 = yVec(dx)*pVec(l3)*pVec(m3)*t881;
	double t973 = yVec(dx)*pVec(l3)*pVec(m3)*t883;
	double t974 = cos(yVec(alphaLH));
	double t975 = cos(yVec(alphaRH));
	double t976 = yVec(dy)*pVec(l3)*pVec(m3)*t864;
	double t977 = yVec(dx)*pVec(l2)*pVec(m2)*t861;
	double t978 = yVec(dx)*yVec(lLF)*pVec(m3)*t861;
	double t979 = yVec(dlLF)*pVec(l1)*pVec(m3)*t950;
	double t980 = yVec(dalphaLF)*pVec(l1)*pVec(l2)*pVec(m2)*t948;
	double t981 = yVec(dphi)*pVec(l1)*pVec(l2)*pVec(m2)*t948*2.0;
	double t982 = yVec(dtheta)*pVec(l1)*pVec(l3)*pVec(m3)*t948*2.0;
	double t983 = yVec(dalphaLF)*pVec(l1)*yVec(lLF)*pVec(m3)*t948;
	double t984 = yVec(dphi)*pVec(l1)*yVec(lLF)*pVec(m3)*t948*2.0;
	double t985 = yVec(dy)*pVec(l3)*pVec(m3)*t866;
	double t986 = yVec(dx)*pVec(l2)*pVec(m2)*t863;
	double t987 = yVec(dx)*yVec(lRF)*pVec(m3)*t863;
	double t988 = yVec(dlRF)*pVec(l1)*pVec(m3)*t952;
	double t989 = yVec(dalphaRF)*pVec(l1)*pVec(l2)*pVec(m2)*t949;
	double t990 = yVec(dphi)*pVec(l1)*pVec(l2)*pVec(m2)*t949*2.0;
	double t991 = yVec(dtheta)*pVec(l1)*pVec(l3)*pVec(m3)*t949*2.0;
	double t992 = yVec(dalphaRF)*pVec(l1)*yVec(lRF)*pVec(m3)*t949;
	double t993 = yVec(dphi)*pVec(l1)*yVec(lRF)*pVec(m3)*t949*2.0;
	double t994 = yVec(dx)*pVec(m3)*t864;
	double t995 = yVec(dalphaLF)*yVec(lLF)*pVec(m3)*2.0;
	double t996 = yVec(dphi)*yVec(lLF)*pVec(m3)*2.0;
	double t997 = yVec(dy)*pVec(m3)*t861;
	double t998 = yVec(dalphaLF)*pVec(l1)*pVec(m3)*t950;
	double t999 = yVec(dphi)*pVec(l1)*pVec(m3)*t950*2.0;
	double t1000 = yVec(dx)*pVec(m3)*t866;
	double t1001 = yVec(dalphaRF)*yVec(lRF)*pVec(m3)*2.0;
	double t1002 = yVec(dphi)*yVec(lRF)*pVec(m3)*2.0;
	double t1003 = yVec(dy)*pVec(m3)*t863;
	double t1004 = yVec(dalphaRF)*pVec(l1)*pVec(m3)*t952;
	double t1005 = yVec(dphi)*pVec(l1)*pVec(m3)*t952*2.0;
	double t1006 = pVec(l0)*pVec(m1)*t879;
	double t1007 = pVec(l1)*pVec(m2)*t879*2.0;
	double t1008 = pVec(l1)*pVec(m3)*t879*2.0;
	double t1009 = pVec(l2)*pVec(m2)*t881;
	double t1010 = pVec(l2)*pVec(m2)*t883;
	double t1011 = yVec(lLH)*pVec(m3)*t881;
	double t1012 = yVec(lRH)*pVec(m3)*t883;
	double t1013 = pVec(l2)*pVec(m2)*t861;
	double t1014 = pVec(l2)*pVec(m2)*t863;
	double t1015 = yVec(lLF)*pVec(m3)*t861;
	double t1016 = yVec(lRF)*pVec(m3)*t863;
	double t1017 = yVec(dalphaLH)*pVec(l3)*pVec(m3)*2.0;
	double t1018 = yVec(dalphaLH)*pVec(l1)*pVec(m3)*t953;
	double t1019 = yVec(dphi)*pVec(l1)*pVec(m3)*t953*2.0;
	double t1020 = yVec(dtheta)*pVec(l1)*pVec(m3)*t953*2.0;
	double t1123 = yVec(dx)*pVec(m3)*t884;
	double t1124 = yVec(dy)*pVec(m3)*t881;
	double t1125 = yVec(dalphaLH)*yVec(lLH)*pVec(m3)*2.0;
	double t1126 = yVec(dphi)*yVec(lLH)*pVec(m3)*2.0;
	double t1127 = yVec(dtheta)*yVec(lLH)*pVec(m3)*2.0;
	double t1021 = t951+t954+t1017+t1018+t1019+t1020-t1123-t1124-t1125-t1126-t1127;
	double t1022 = yVec(dlLH)*t1021;
	double t1023 = yVec(dalphaRH)*pVec(l3)*pVec(m3)*2.0;
	double t1024 = yVec(dalphaRH)*pVec(l1)*pVec(m3)*t955;
	double t1025 = yVec(dphi)*pVec(l1)*pVec(m3)*t955*2.0;
	double t1026 = yVec(dtheta)*pVec(l1)*pVec(m3)*t955*2.0;
	double t1163 = yVec(dx)*pVec(m3)*t886;
	double t1164 = yVec(dy)*pVec(m3)*t883;
	double t1165 = yVec(dalphaRH)*yVec(lRH)*pVec(m3)*2.0;
	double t1166 = yVec(dphi)*yVec(lRH)*pVec(m3)*2.0;
	double t1167 = yVec(dtheta)*yVec(lRH)*pVec(m3)*2.0;
	double t1027 = t951+t954+t1023+t1024+t1025+t1026-t1163-t1164-t1165-t1166-t1167;
	double t1028 = yVec(dlRH)*t1027;
	double t1029 = yVec(dx)*pVec(l0)*pVec(m1)*t879;
	double t1030 = yVec(dx)*pVec(l1)*pVec(m2)*t879*2.0;
	double t1031 = yVec(dx)*pVec(l1)*pVec(m3)*t879*2.0;
	double t1032 = yVec(dy)*pVec(l3)*pVec(m3)*t884;
	double t1033 = yVec(dy)*pVec(l3)*pVec(m3)*t886;
	double t1034 = yVec(dy)*pVec(l0)*pVec(m1)*t918;
	double t1035 = yVec(dy)*pVec(l1)*pVec(m2)*t918*2.0;
	double t1036 = yVec(dy)*pVec(l1)*pVec(m3)*t918*2.0;
	double t1037 = yVec(dx)*pVec(l2)*pVec(m2)*t881;
	double t1038 = yVec(dx)*pVec(l2)*pVec(m2)*t883;
	double t1039 = yVec(dx)*yVec(lLH)*pVec(m3)*t881;
	double t1040 = yVec(dx)*yVec(lRH)*pVec(m3)*t883;
	double t1041 = yVec(dlLH)*pVec(l1)*pVec(m3)*t953;
	double t1042 = yVec(dalphaLH)*pVec(l1)*pVec(l2)*pVec(m2)*t974;
	double t1043 = yVec(dphi)*pVec(l1)*pVec(l2)*pVec(m2)*t974*2.0;
	double t1044 = yVec(dtheta)*pVec(l1)*pVec(l2)*pVec(m2)*t974*2.0;
	double t1045 = yVec(dalphaLH)*pVec(l1)*yVec(lLH)*pVec(m3)*t974;
	double t1046 = yVec(dphi)*pVec(l1)*yVec(lLH)*pVec(m3)*t974*2.0;
	double t1047 = yVec(dtheta)*pVec(l1)*yVec(lLH)*pVec(m3)*t974*2.0;
	double t1048 = -t968-t970-t972+t1032+t1037+t1039+t1041+t1042+t1043+t1044+t1045+t1046+t1047-yVec(dalphaLH)*pVec(l1)*pVec(l3)*pVec(m3)*t974-yVec(dphi)*pVec(l1)*pVec(l3)*pVec(m3)*t974*2.0-yVec(dtheta)*pVec(l1)*pVec(l3)*pVec(m3)*t974*2.0;
	double t1049 = yVec(dalphaLH)*t1048;
	double t1050 = yVec(dlRH)*pVec(l1)*pVec(m3)*t955;
	double t1051 = yVec(dalphaRH)*pVec(l1)*pVec(l2)*pVec(m2)*t975;
	double t1052 = yVec(dphi)*pVec(l1)*pVec(l2)*pVec(m2)*t975*2.0;
	double t1053 = yVec(dtheta)*pVec(l1)*pVec(l2)*pVec(m2)*t975*2.0;
	double t1054 = yVec(dalphaRH)*pVec(l1)*yVec(lRH)*pVec(m3)*t975;
	double t1055 = yVec(dphi)*pVec(l1)*yVec(lRH)*pVec(m3)*t975*2.0;
	double t1056 = yVec(dtheta)*pVec(l1)*yVec(lRH)*pVec(m3)*t975*2.0;
	double t1057 = -t969-t971-t973+t1033+t1038+t1040+t1050+t1051+t1052+t1053+t1054+t1055+t1056-yVec(dalphaRH)*pVec(l1)*pVec(l3)*pVec(m3)*t975-yVec(dphi)*pVec(l1)*pVec(l3)*pVec(m3)*t975*2.0-yVec(dtheta)*pVec(l1)*pVec(l3)*pVec(m3)*t975*2.0;
	double t1058 = yVec(dalphaRH)*t1057;
	double t1059 = yVec(dlLF)*yVec(dy)*pVec(m3)*t861;
	double t1060 = yVec(dlRF)*yVec(dy)*pVec(m3)*t863;
	double t1061 = yVec(dlLH)*yVec(dx)*pVec(m3)*t884;
	double t1062 = yVec(dlRH)*yVec(dx)*pVec(m3)*t886;
	double t1063 = yVec(dlLH)*yVec(dy)*pVec(m3)*t881;
	double t1064 = yVec(dlRH)*yVec(dy)*pVec(m3)*t883;
	double t1065 = yVec(dlLF)*yVec(dx)*pVec(m3)*t864;
	double t1066 = yVec(dlRF)*yVec(dx)*pVec(m3)*t866;
	double t1067 = yVec(dtheta)*yVec(dx)*pVec(l0)*pVec(m1)*t879;
	double t1068 = yVec(dtheta)*yVec(dx)*pVec(l1)*pVec(m2)*t879*2.0;
	double t1069 = yVec(dtheta)*yVec(dx)*pVec(l1)*pVec(m3)*t879*2.0;
	double t1070 = yVec(dalphaLH)*yVec(dy)*pVec(l2)*pVec(m2)*t884;
	double t1071 = yVec(dalphaRH)*yVec(dy)*pVec(l2)*pVec(m2)*t886;
	double t1072 = yVec(dphi)*yVec(dy)*pVec(l2)*pVec(m2)*t884;
	double t1073 = yVec(dphi)*yVec(dy)*pVec(l2)*pVec(m2)*t886;
	double t1074 = yVec(dtheta)*yVec(dy)*pVec(l2)*pVec(m2)*t884;
	double t1075 = yVec(dtheta)*yVec(dy)*pVec(l2)*pVec(m2)*t886;
	double t1076 = yVec(dalphaLH)*yVec(dy)*yVec(lLH)*pVec(m3)*t884;
	double t1077 = yVec(dalphaRH)*yVec(dy)*yVec(lRH)*pVec(m3)*t886;
	double t1078 = yVec(dphi)*yVec(dy)*yVec(lLH)*pVec(m3)*t884;
	double t1079 = yVec(dtheta)*yVec(dy)*yVec(lLH)*pVec(m3)*t884;
	double t1080 = yVec(dphi)*yVec(dy)*yVec(lRH)*pVec(m3)*t886;
	double t1081 = yVec(dtheta)*yVec(dy)*yVec(lRH)*pVec(m3)*t886;
	double t1082 = yVec(dtheta)*yVec(dy)*pVec(l0)*pVec(m1)*t918;
	double t1083 = yVec(dtheta)*yVec(dy)*pVec(l1)*pVec(m2)*t918*2.0;
	double t1084 = yVec(dtheta)*yVec(dy)*pVec(l1)*pVec(m3)*t918*2.0;
	double t1085 = yVec(dalphaLH)*yVec(dx)*pVec(l3)*pVec(m3)*t881;
	double t1086 = yVec(dalphaRH)*yVec(dx)*pVec(l3)*pVec(m3)*t883;
	double t1087 = yVec(dphi)*yVec(dx)*pVec(l3)*pVec(m3)*t881;
	double t1088 = yVec(dphi)*yVec(dx)*pVec(l3)*pVec(m3)*t883;
	double t1089 = yVec(dtheta)*yVec(dx)*pVec(l3)*pVec(m3)*t881;
	double t1090 = yVec(dtheta)*yVec(dx)*pVec(l3)*pVec(m3)*t883;
	double t1091 = yVec(dalphaLF)*yVec(dy)*pVec(l2)*pVec(m2)*t864;
	double t1092 = yVec(dalphaRF)*yVec(dy)*pVec(l2)*pVec(m2)*t866;
	double t1093 = yVec(dphi)*yVec(dy)*pVec(l2)*pVec(m2)*t864;
	double t1094 = yVec(dphi)*yVec(dy)*pVec(l2)*pVec(m2)*t866;
	double t1095 = yVec(dtheta)*yVec(dy)*pVec(l3)*pVec(m3)*t864;
	double t1096 = yVec(dtheta)*yVec(dy)*pVec(l3)*pVec(m3)*t866;
	double t1097 = yVec(dalphaLF)*yVec(dy)*yVec(lLF)*pVec(m3)*t864;
	double t1098 = yVec(dalphaRF)*yVec(dy)*yVec(lRF)*pVec(m3)*t866;
	double t1099 = yVec(dphi)*yVec(dy)*yVec(lLF)*pVec(m3)*t864;
	double t1100 = yVec(dphi)*yVec(dy)*yVec(lRF)*pVec(m3)*t866;
	double t1101 = yVec(dalphaLF)*yVec(dx)*pVec(l3)*pVec(m3)*t861;
	double t1102 = yVec(dalphaRF)*yVec(dx)*pVec(l3)*pVec(m3)*t863;
	double t1103 = yVec(dphi)*yVec(dx)*pVec(l3)*pVec(m3)*t861;
	double t1104 = yVec(dphi)*yVec(dx)*pVec(l3)*pVec(m3)*t863;
	double t1105 = yVec(dtheta)*yVec(dx)*pVec(l2)*pVec(m2)*t861;
	double t1106 = yVec(dtheta)*yVec(dx)*pVec(l2)*pVec(m2)*t863;
	double t1107 = yVec(dtheta)*yVec(dx)*yVec(lLF)*pVec(m3)*t861;
	double t1108 = yVec(dtheta)*yVec(dx)*yVec(lRF)*pVec(m3)*t863;
	double t1109 = yVec(dphi)*yVec(dx)*pVec(l0)*pVec(m1)*t877;
	double t1110 = yVec(dphi)*yVec(dx)*pVec(l1)*pVec(m2)*t877*2.0;
	double t1111 = yVec(dphi)*yVec(dx)*pVec(l1)*pVec(m3)*t877*2.0;
	double t1112 = yVec(dtheta)*yVec(dx)*pVec(l0)*pVec(m1)*t877;
	double t1113 = yVec(dtheta)*yVec(dx)*pVec(l1)*pVec(m2)*t877*2.0;
	double t1114 = yVec(dtheta)*yVec(dx)*pVec(l1)*pVec(m3)*t877*2.0;
	double t1115 = yVec(dphi)*yVec(dy)*pVec(l0)*pVec(m1)*t905;
	double t1116 = yVec(dphi)*yVec(dy)*pVec(l1)*pVec(m2)*t905*2.0;
	double t1117 = yVec(dphi)*yVec(dy)*pVec(l1)*pVec(m3)*t905*2.0;
	double t1118 = yVec(dtheta)*yVec(dy)*pVec(l0)*pVec(m1)*t905;
	double t1119 = yVec(dtheta)*yVec(dy)*pVec(l1)*pVec(m2)*t905*2.0;
	double t1120 = yVec(dtheta)*yVec(dy)*pVec(l1)*pVec(m3)*t905*2.0;
	double t1121 = pVec(l3)*pVec(m3)*t881;
	double t1122 = t968+t970+t972-t1032-t1037-t1039;
	double t1128 = yVec(dphi)*yVec(dphi);
	double t1129 = yVec(dtheta)*yVec(dtheta);
	double t1130 = yVec(dphi)*pVec(l1)*pVec(m3)*t953;
	double t1131 = yVec(dtheta)*pVec(l1)*pVec(m3)*t953;
	double t1132 = t1123+t1124;
	double t1133 = yVec(dalphaLH)*yVec(dalphaLH);
	double t1134 = t956+t958+t960-t976-t977-t978;
	double t1135 = yVec(dalphaLF)*yVec(dy)*pVec(l3)*pVec(m3)*t864;
	double t1136 = yVec(dphi)*yVec(dy)*pVec(l3)*pVec(m3)*t864;
	double t1137 = yVec(dtheta)*yVec(dy)*pVec(l2)*pVec(m2)*t864;
	double t1138 = yVec(dtheta)*yVec(dy)*yVec(lLF)*pVec(m3)*t864;
	double t1139 = yVec(dalphaLF)*yVec(dx)*pVec(l2)*pVec(m2)*t861;
	double t1140 = yVec(dphi)*yVec(dx)*pVec(l2)*pVec(m2)*t861;
	double t1141 = yVec(dtheta)*yVec(dx)*pVec(l3)*pVec(m3)*t861;
	double t1142 = yVec(dalphaLF)*yVec(dx)*yVec(lLF)*pVec(m3)*t861;
	double t1143 = yVec(dphi)*yVec(dx)*yVec(lLF)*pVec(m3)*t861;
	double t1144 = yVec(dphi)*pVec(l1)*pVec(m3)*t950;
	double t1145 = t994+t997;
	double t1146 = yVec(dalphaLF)*yVec(dalphaLF);
	double t1147 = t957+t959+t961-t985-t986-t987;
	double t1148 = yVec(dalphaRF)*yVec(dy)*pVec(l3)*pVec(m3)*t866;
	double t1149 = yVec(dphi)*yVec(dy)*pVec(l3)*pVec(m3)*t866;
	double t1150 = yVec(dtheta)*yVec(dy)*pVec(l2)*pVec(m2)*t866;
	double t1151 = yVec(dtheta)*yVec(dy)*yVec(lRF)*pVec(m3)*t866;
	double t1152 = yVec(dalphaRF)*yVec(dx)*pVec(l2)*pVec(m2)*t863;
	double t1153 = yVec(dphi)*yVec(dx)*pVec(l2)*pVec(m2)*t863;
	double t1154 = yVec(dtheta)*yVec(dx)*pVec(l3)*pVec(m3)*t863;
	double t1155 = yVec(dalphaRF)*yVec(dx)*yVec(lRF)*pVec(m3)*t863;
	double t1156 = yVec(dphi)*yVec(dx)*yVec(lRF)*pVec(m3)*t863;
	double t1157 = yVec(dphi)*pVec(l1)*pVec(m3)*t952;
	double t1158 = t1000+t1003;
	double t1159 = yVec(dalphaRF)*yVec(dalphaRF);
	double t1160 = yVec(dphi)*yVec(dtheta)*pVec(l3)*pVec(m3)*2.0;
	double t1161 = pVec(l3)*pVec(m3)*t883;
	double t1162 = t969+t971+t973-t1033-t1038-t1040;
	double t1168 = yVec(dphi)*pVec(l1)*pVec(m3)*t955;
	double t1169 = yVec(dtheta)*pVec(l1)*pVec(m3)*t955;
	double t1170 = t1163+t1164;
	double t1171 = yVec(dalphaRH)*yVec(dalphaRH);
	h(0,0) = -yVec(dphi)*(t865+t867+t868+t869+t870+t871+t872+t873+t874+t875+t885+t887+t888+t889+t890+t891+t892+t893+t894+t895+t896+t897+t898+t899+t900+t901+t902-yVec(dalphaLF)*pVec(l2)*pVec(m2)*t861-yVec(dalphaRF)*pVec(l2)*pVec(m2)*t863-yVec(dalphaLH)*pVec(l2)*pVec(m2)*t881-yVec(dalphaRH)*pVec(l2)*pVec(m2)*t883-yVec(dphi)*pVec(l2)*pVec(m2)*t861-yVec(dphi)*pVec(l2)*pVec(m2)*t863-yVec(dphi)*pVec(l0)*pVec(m1)*t879-yVec(dphi)*pVec(l1)*pVec(m2)*t879*2.0-yVec(dphi)*pVec(l1)*pVec(m3)*t879*2.0-yVec(dphi)*pVec(l2)*pVec(m2)*t881-yVec(dphi)*pVec(l2)*pVec(m2)*t883-yVec(dtheta)*pVec(l3)*pVec(m3)*t861-yVec(dtheta)*pVec(l3)*pVec(m3)*t863-yVec(dtheta)*pVec(l2)*pVec(m2)*t881-yVec(dtheta)*pVec(l2)*pVec(m2)*t883-yVec(dalphaLF)*yVec(lLF)*pVec(m3)*t861-yVec(dalphaLH)*yVec(lLH)*pVec(m3)*t881-yVec(dalphaRF)*yVec(lRF)*pVec(m3)*t863-yVec(dalphaRH)*yVec(lRH)*pVec(m3)*t883-yVec(dphi)*yVec(lLF)*pVec(m3)*t861-yVec(dphi)*yVec(lLH)*pVec(m3)*t881-yVec(dtheta)*yVec(lLH)*pVec(m3)*t881-yVec(dphi)*yVec(lRF)*pVec(m3)*t863-yVec(dphi)*yVec(lRH)*pVec(m3)*t883-yVec(dtheta)*yVec(lRH)*pVec(m3)*t883)-yVec(dlLF)*(yVec(dalphaLF)*pVec(m3)*t864+yVec(dphi)*pVec(m3)*t864-yVec(dtheta)*pVec(m3)*t864)-yVec(dlLH)*(yVec(dalphaLH)*pVec(m3)*t884+yVec(dphi)*pVec(m3)*t884+yVec(dtheta)*pVec(m3)*t884)-yVec(dlRF)*(yVec(dalphaRF)*pVec(m3)*t866+yVec(dphi)*pVec(m3)*t866-yVec(dtheta)*pVec(m3)*t866)-yVec(dlRH)*(yVec(dalphaRH)*pVec(m3)*t886+yVec(dphi)*pVec(m3)*t886+yVec(dtheta)*pVec(m3)*t886)-yVec(dalphaLF)*(t865+t868+t870+t872+t874-yVec(dalphaLF)*pVec(l2)*pVec(m2)*t861-yVec(dphi)*pVec(l2)*pVec(m2)*t861-yVec(dtheta)*pVec(l3)*pVec(m3)*t861-yVec(dalphaLF)*yVec(lLF)*pVec(m3)*t861-yVec(dphi)*yVec(lLF)*pVec(m3)*t861)-yVec(dalphaRF)*(t867+t869+t871+t873+t875-yVec(dalphaRF)*pVec(l2)*pVec(m2)*t863-yVec(dphi)*pVec(l2)*pVec(m2)*t863-yVec(dtheta)*pVec(l3)*pVec(m3)*t863-yVec(dalphaRF)*yVec(lRF)*pVec(m3)*t863-yVec(dphi)*yVec(lRF)*pVec(m3)*t863)+yVec(dalphaLH)*(-t885-t897-t899-t901+yVec(dalphaLH)*pVec(l2)*pVec(m2)*t881+yVec(dphi)*pVec(l2)*pVec(m2)*t881+yVec(dtheta)*pVec(l2)*pVec(m2)*t881+yVec(dalphaLH)*yVec(lLH)*pVec(m3)*t881+yVec(dphi)*yVec(lLH)*pVec(m3)*t881+yVec(dtheta)*yVec(lLH)*pVec(m3)*t881)+yVec(dalphaRH)*(-t887-t898-t900-t902+yVec(dalphaRH)*pVec(l2)*pVec(m2)*t883+yVec(dphi)*pVec(l2)*pVec(m2)*t883+yVec(dtheta)*pVec(l2)*pVec(m2)*t883+yVec(dalphaRH)*yVec(lRH)*pVec(m3)*t883+yVec(dphi)*yVec(lRH)*pVec(m3)*t883+yVec(dtheta)*yVec(lRH)*pVec(m3)*t883)-yVec(dtheta)*(-t865-t867-t868-t869-t870-t871-t872-t873-t874-t875+t885+t887+t888+t889+t890+t891+t892+t893-t894-t895-t896+t897+t898+t899+t900+t901+t902+yVec(dalphaLF)*pVec(l2)*pVec(m2)*t861+yVec(dalphaRF)*pVec(l2)*pVec(m2)*t863-yVec(dalphaLH)*pVec(l2)*pVec(m2)*t881-yVec(dalphaRH)*pVec(l2)*pVec(m2)*t883+yVec(dphi)*pVec(l2)*pVec(m2)*t861+yVec(dphi)*pVec(l2)*pVec(m2)*t863+yVec(dphi)*pVec(l0)*pVec(m1)*t879+yVec(dphi)*pVec(l1)*pVec(m2)*t879*2.0+yVec(dphi)*pVec(l1)*pVec(m3)*t879*2.0-yVec(dphi)*pVec(l2)*pVec(m2)*t881-yVec(dphi)*pVec(l2)*pVec(m2)*t883+yVec(dtheta)*pVec(l3)*pVec(m3)*t861+yVec(dtheta)*pVec(l3)*pVec(m3)*t863-yVec(dtheta)*pVec(l2)*pVec(m2)*t881-yVec(dtheta)*pVec(l2)*pVec(m2)*t883+yVec(dalphaLF)*yVec(lLF)*pVec(m3)*t861-yVec(dalphaLH)*yVec(lLH)*pVec(m3)*t881+yVec(dalphaRF)*yVec(lRF)*pVec(m3)*t863-yVec(dalphaRH)*yVec(lRH)*pVec(m3)*t883+yVec(dphi)*yVec(lLF)*pVec(m3)*t861-yVec(dphi)*yVec(lLH)*pVec(m3)*t881-yVec(dtheta)*yVec(lLH)*pVec(m3)*t881+yVec(dphi)*yVec(lRF)*pVec(m3)*t863-yVec(dphi)*yVec(lRH)*pVec(m3)*t883-yVec(dtheta)*yVec(lRH)*pVec(m3)*t883);
	h(1,0) = -yVec(dlLF)*(yVec(dalphaLF)*pVec(m3)*t861+yVec(dphi)*pVec(m3)*t861-yVec(dtheta)*pVec(m3)*t861)-yVec(dlLH)*(yVec(dalphaLH)*pVec(m3)*t881+yVec(dphi)*pVec(m3)*t881+yVec(dtheta)*pVec(m3)*t881)-yVec(dlRF)*(yVec(dalphaRF)*pVec(m3)*t863+yVec(dphi)*pVec(m3)*t863-yVec(dtheta)*pVec(m3)*t863)-yVec(dlRH)*(yVec(dalphaRH)*pVec(m3)*t883+yVec(dphi)*pVec(m3)*t883+yVec(dtheta)*pVec(m3)*t883)-yVec(dphi)*(t903+t904+t906+t907+t908+t909+t910+t911+t912+t913+t914+t915+t916+t917+t919+t920+t921+t922+t923+t924+t925+t926+t927+t928+t929+t930+t931+t932+t933+t934+t935+t936+t937+t938+t939-yVec(dalphaLF)*pVec(l3)*pVec(m3)*t864-yVec(dalphaRF)*pVec(l3)*pVec(m3)*t866-yVec(dalphaLH)*pVec(l3)*pVec(m3)*t884-yVec(dalphaRH)*pVec(l3)*pVec(m3)*t886-yVec(dphi)*pVec(l3)*pVec(m3)*t864-yVec(dphi)*pVec(l3)*pVec(m3)*t866-yVec(dphi)*pVec(l3)*pVec(m3)*t884-yVec(dphi)*pVec(l3)*pVec(m3)*t886-yVec(dphi)*pVec(l0)*pVec(m1)*t918-yVec(dphi)*pVec(l1)*pVec(m2)*t918*2.0-yVec(dphi)*pVec(l1)*pVec(m3)*t918*2.0-yVec(dtheta)*pVec(l2)*pVec(m2)*t864-yVec(dtheta)*pVec(l2)*pVec(m2)*t866-yVec(dtheta)*pVec(l3)*pVec(m3)*t884-yVec(dtheta)*pVec(l3)*pVec(m3)*t886-yVec(dtheta)*yVec(lLF)*pVec(m3)*t864-yVec(dtheta)*yVec(lRF)*pVec(m3)*t866)-yVec(dalphaLF)*(t919+t921+t923+t925+t927+t929-t940-t941-t942-t943)-yVec(dalphaRF)*(t920+t922+t924+t926+t928+t930-t944-t945-t946-t947)-yVec(dalphaLH)*(t903+t906+t908+t910+t912+t914+t915-yVec(dalphaLH)*pVec(l3)*pVec(m3)*t884-yVec(dphi)*pVec(l3)*pVec(m3)*t884-yVec(dtheta)*pVec(l3)*pVec(m3)*t884)-yVec(dalphaRH)*(t904+t907+t909+t911+t913+t916+t917-yVec(dalphaRH)*pVec(l3)*pVec(m3)*t886-yVec(dphi)*pVec(l3)*pVec(m3)*t886-yVec(dtheta)*pVec(l3)*pVec(m3)*t886)-pVec(g)*(pVec(m1)*2.0+pVec(m2)*4.0+pVec(m3)*4.0)-yVec(dtheta)*(t903+t904+t906+t907+t908+t909+t910+t911+t912+t913+t914+t915+t916+t917-t919-t920-t921-t922-t923-t924-t925-t926-t927-t928-t929-t930+t931+t932+t933+t934+t935+t936-t937-t938-t939+t940+t941+t942+t943+t944+t945+t946+t947-yVec(dalphaLH)*pVec(l3)*pVec(m3)*t884-yVec(dalphaRH)*pVec(l3)*pVec(m3)*t886-yVec(dphi)*pVec(l3)*pVec(m3)*t884-yVec(dphi)*pVec(l3)*pVec(m3)*t886+yVec(dphi)*pVec(l0)*pVec(m1)*t918+yVec(dphi)*pVec(l1)*pVec(m2)*t918*2.0+yVec(dphi)*pVec(l1)*pVec(m3)*t918*2.0-yVec(dtheta)*pVec(l3)*pVec(m3)*t884-yVec(dtheta)*pVec(l3)*pVec(m3)*t886);
	h(2,0) = t1022+t1028+t1049+t1058+t1059+t1060+t1061+t1062+t1063+t1064+t1065+t1066+t1067+t1068+t1069+t1070+t1071+t1072+t1073+t1074+t1075+t1076+t1077+t1078+t1079+t1080+t1081+t1082+t1083+t1084+t1085+t1086+t1087+t1088+t1089+t1090+t1091+t1092+t1093+t1094+t1095+t1096+t1097+t1098+t1099+t1100+t1101+t1102+t1103+t1104+t1105+t1106+t1107+t1108+t1109+t1110+t1111+t1112+t1113+t1114+t1115+t1116+t1117+t1118+t1119+t1120-yVec(dlLF)*(t951+t994+t995+t996+t997+t998+t999-yVec(dalphaLF)*pVec(l3)*pVec(m3)*2.0-yVec(dphi)*pVec(l3)*pVec(m3)*2.0-yVec(dtheta)*yVec(lLF)*pVec(m3)*2.0-yVec(dtheta)*pVec(l1)*pVec(m3)*t950*2.0)-yVec(dlRF)*(t951+t1000+t1001+t1002+t1003+t1004+t1005-yVec(dalphaRF)*pVec(l3)*pVec(m3)*2.0-yVec(dphi)*pVec(l3)*pVec(m3)*2.0-yVec(dtheta)*yVec(lRF)*pVec(m3)*2.0-yVec(dtheta)*pVec(l1)*pVec(m3)*t952*2.0)-yVec(dtheta)*(-t956-t957-t958-t959-t960-t961+t962+t963+t964+t965+t966+t967+t968+t969+t970+t971+t972+t973+t976+t977+t978+t985+t986+t987+t1029+t1030+t1031+t1034+t1035+t1036-yVec(dx)*pVec(l2)*pVec(m2)*t881-yVec(dx)*pVec(l2)*pVec(m2)*t883-yVec(dy)*pVec(l3)*pVec(m3)*t884-yVec(dy)*pVec(l3)*pVec(m3)*t886-yVec(dx)*yVec(lLH)*pVec(m3)*t881-yVec(dx)*yVec(lRH)*pVec(m3)*t883)-yVec(dalphaLF)*(t956+t958+t960+t979+t980+t981+t982+t983+t984-yVec(dx)*pVec(l2)*pVec(m2)*t861-yVec(dy)*pVec(l3)*pVec(m3)*t864-yVec(dx)*yVec(lLF)*pVec(m3)*t861-yVec(dalphaLF)*pVec(l1)*pVec(l3)*pVec(m3)*t948-yVec(dphi)*pVec(l1)*pVec(l3)*pVec(m3)*t948*2.0-yVec(dtheta)*pVec(l1)*pVec(l2)*pVec(m2)*t948*2.0-yVec(dtheta)*pVec(l1)*yVec(lLF)*pVec(m3)*t948*2.0)-yVec(dalphaRF)*(t957+t959+t961+t988+t989+t990+t991+t992+t993-yVec(dx)*pVec(l2)*pVec(m2)*t863-yVec(dy)*pVec(l3)*pVec(m3)*t866-yVec(dx)*yVec(lRF)*pVec(m3)*t863-yVec(dalphaRF)*pVec(l1)*pVec(l3)*pVec(m3)*t949-yVec(dphi)*pVec(l1)*pVec(l3)*pVec(m3)*t949*2.0-yVec(dtheta)*pVec(l1)*pVec(l2)*pVec(m2)*t949*2.0-yVec(dtheta)*pVec(l1)*yVec(lRF)*pVec(m3)*t949*2.0)-yVec(dphi)*(t956+t957+t958+t959+t960+t961+t962+t963+t964+t965+t966+t967+t968+t969+t970+t971+t972+t973-yVec(dx)*pVec(l2)*pVec(m2)*t861-yVec(dx)*pVec(l2)*pVec(m2)*t863-yVec(dx)*pVec(l0)*pVec(m1)*t879-yVec(dx)*pVec(l1)*pVec(m2)*t879*2.0-yVec(dx)*pVec(l1)*pVec(m3)*t879*2.0-yVec(dx)*pVec(l2)*pVec(m2)*t881-yVec(dx)*pVec(l2)*pVec(m2)*t883-yVec(dy)*pVec(l3)*pVec(m3)*t864-yVec(dy)*pVec(l3)*pVec(m3)*t866-yVec(dy)*pVec(l3)*pVec(m3)*t884-yVec(dy)*pVec(l3)*pVec(m3)*t886-yVec(dy)*pVec(l0)*pVec(m1)*t918-yVec(dy)*pVec(l1)*pVec(m2)*t918*2.0-yVec(dy)*pVec(l1)*pVec(m3)*t918*2.0-yVec(dx)*yVec(lLF)*pVec(m3)*t861-yVec(dx)*yVec(lLH)*pVec(m3)*t881-yVec(dx)*yVec(lRF)*pVec(m3)*t863-yVec(dx)*yVec(lRH)*pVec(m3)*t883)-pVec(g)*(t1006+t1007+t1008+t1009+t1010+t1011+t1012+t1013+t1014+t1015+t1016-pVec(l3)*pVec(m3)*t861-pVec(l3)*pVec(m3)*t863-pVec(l0)*pVec(m1)*t877-pVec(l1)*pVec(m2)*t877*2.0-pVec(l1)*pVec(m3)*t877*2.0-pVec(l3)*pVec(m3)*t881-pVec(l3)*pVec(m3)*t883)-yVec(dalphaLF)*yVec(dx)*pVec(l2)*pVec(m2)*t861-yVec(dalphaRF)*yVec(dx)*pVec(l2)*pVec(m2)*t863-yVec(dalphaLH)*yVec(dx)*pVec(l2)*pVec(m2)*t881-yVec(dalphaRH)*yVec(dx)*pVec(l2)*pVec(m2)*t883-yVec(dalphaLF)*yVec(dy)*pVec(l3)*pVec(m3)*t864-yVec(dalphaRF)*yVec(dy)*pVec(l3)*pVec(m3)*t866-yVec(dalphaLH)*yVec(dy)*pVec(l3)*pVec(m3)*t884-yVec(dalphaRH)*yVec(dy)*pVec(l3)*pVec(m3)*t886-yVec(dphi)*yVec(dx)*pVec(l2)*pVec(m2)*t861-yVec(dphi)*yVec(dx)*pVec(l2)*pVec(m2)*t863-yVec(dphi)*yVec(dx)*pVec(l0)*pVec(m1)*t879-yVec(dphi)*yVec(dx)*pVec(l1)*pVec(m2)*t879*2.0-yVec(dphi)*yVec(dx)*pVec(l1)*pVec(m3)*t879*2.0-yVec(dphi)*yVec(dx)*pVec(l2)*pVec(m2)*t881-yVec(dphi)*yVec(dx)*pVec(l2)*pVec(m2)*t883-yVec(dphi)*yVec(dy)*pVec(l3)*pVec(m3)*t864-yVec(dphi)*yVec(dy)*pVec(l3)*pVec(m3)*t866-yVec(dphi)*yVec(dy)*pVec(l3)*pVec(m3)*t884-yVec(dphi)*yVec(dy)*pVec(l3)*pVec(m3)*t886-yVec(dphi)*yVec(dy)*pVec(l0)*pVec(m1)*t918-yVec(dphi)*yVec(dy)*pVec(l1)*pVec(m2)*t918*2.0-yVec(dphi)*yVec(dy)*pVec(l1)*pVec(m3)*t918*2.0-yVec(dtheta)*yVec(dx)*pVec(l3)*pVec(m3)*t861-yVec(dtheta)*yVec(dx)*pVec(l3)*pVec(m3)*t863-yVec(dtheta)*yVec(dx)*pVec(l2)*pVec(m2)*t881-yVec(dtheta)*yVec(dx)*pVec(l2)*pVec(m2)*t883-yVec(dtheta)*yVec(dy)*pVec(l2)*pVec(m2)*t864-yVec(dtheta)*yVec(dy)*pVec(l2)*pVec(m2)*t866-yVec(dtheta)*yVec(dy)*pVec(l3)*pVec(m3)*t884-yVec(dtheta)*yVec(dy)*pVec(l3)*pVec(m3)*t886-yVec(dalphaLF)*yVec(dx)*yVec(lLF)*pVec(m3)*t861-yVec(dalphaLH)*yVec(dx)*yVec(lLH)*pVec(m3)*t881-yVec(dalphaRF)*yVec(dx)*yVec(lRF)*pVec(m3)*t863-yVec(dalphaRH)*yVec(dx)*yVec(lRH)*pVec(m3)*t883-yVec(dphi)*yVec(dx)*yVec(lLF)*pVec(m3)*t861-yVec(dphi)*yVec(dx)*yVec(lLH)*pVec(m3)*t881-yVec(dtheta)*yVec(dx)*yVec(lLH)*pVec(m3)*t881-yVec(dtheta)*yVec(dy)*yVec(lLF)*pVec(m3)*t864-yVec(dphi)*yVec(dx)*yVec(lRF)*pVec(m3)*t863-yVec(dphi)*yVec(dx)*yVec(lRH)*pVec(m3)*t883-yVec(dtheta)*yVec(dx)*yVec(lRH)*pVec(m3)*t883-yVec(dtheta)*yVec(dy)*yVec(lRF)*pVec(m3)*t866;
	h(3,0) = t1022+t1028+t1049+t1058-t1059-t1060+t1061+t1062+t1063+t1064-t1065-t1066-t1067-t1068-t1069+t1070+t1071+t1072+t1073+t1074+t1075+t1076+t1077+t1078+t1079+t1080+t1081-t1082-t1083-t1084+t1085+t1086+t1087+t1088+t1089+t1090-t1091-t1092-t1093-t1094-t1095-t1096-t1097-t1098-t1099-t1100-t1101-t1102-t1103-t1104-t1105-t1106-t1107-t1108+t1109+t1110+t1111+t1112+t1113+t1114+t1115+t1116+t1117+t1118+t1119+t1120+t1135+t1136+t1137+t1138+t1139+t1140+t1141+t1142+t1143+t1148+t1149+t1150+t1151+t1152+t1153+t1154+t1155+t1156-yVec(dtheta)*(t956+t957+t958+t959+t960+t961+t962+t963+t964+t965+t966+t967+t968+t969+t970+t971+t972+t973-t976-t977-t978-t985-t986-t987-t1029-t1030-t1031-t1032-t1033-t1034-t1035-t1036-t1037-t1038-t1039-t1040)+yVec(dlLF)*(t951-t954+t994+t995+t996+t997+t998+t999-yVec(dalphaLF)*pVec(l3)*pVec(m3)*2.0-yVec(dtheta)*yVec(lLF)*pVec(m3)*2.0-yVec(dtheta)*pVec(l1)*pVec(m3)*t950*2.0)+yVec(dlRF)*(t951-t954+t1000+t1001+t1002+t1003+t1004+t1005-yVec(dalphaRF)*pVec(l3)*pVec(m3)*2.0-yVec(dtheta)*yVec(lRF)*pVec(m3)*2.0-yVec(dtheta)*pVec(l1)*pVec(m3)*t952*2.0)+yVec(dalphaLF)*(t956+t958+t960-t976-t977-t978+t979+t980+t981+t982+t983+t984-yVec(dalphaLF)*pVec(l1)*pVec(l3)*pVec(m3)*t948-yVec(dphi)*pVec(l1)*pVec(l3)*pVec(m3)*t948*2.0-yVec(dtheta)*pVec(l1)*pVec(l2)*pVec(m2)*t948*2.0-yVec(dtheta)*pVec(l1)*yVec(lLF)*pVec(m3)*t948*2.0)+yVec(dalphaRF)*(t957+t959+t961-t985-t986-t987+t988+t989+t990+t991+t992+t993-yVec(dalphaRF)*pVec(l1)*pVec(l3)*pVec(m3)*t949-yVec(dphi)*pVec(l1)*pVec(l3)*pVec(m3)*t949*2.0-yVec(dtheta)*pVec(l1)*pVec(l2)*pVec(m2)*t949*2.0-yVec(dtheta)*pVec(l1)*yVec(lRF)*pVec(m3)*t949*2.0)+pVec(g)*(t1006+t1007+t1008-t1009-t1010-t1011-t1012+t1013+t1014+t1015+t1016+t1121+t1161-pVec(l3)*pVec(m3)*t861-pVec(l3)*pVec(m3)*t863+pVec(l0)*pVec(m1)*t877+pVec(l1)*pVec(m2)*t877*2.0+pVec(l1)*pVec(m3)*t877*2.0)-yVec(dphi)*(-t956-t957-t958-t959-t960-t961+t962+t963+t964+t965+t966+t967+t968+t969+t970+t971+t972+t973+t976+t977+t978+t985+t986+t987+t1029+t1030+t1031-t1032-t1033+t1034+t1035+t1036-t1037-t1038-t1039-t1040)-yVec(dalphaLH)*yVec(dx)*pVec(l2)*pVec(m2)*t881-yVec(dalphaRH)*yVec(dx)*pVec(l2)*pVec(m2)*t883-yVec(dalphaLH)*yVec(dy)*pVec(l3)*pVec(m3)*t884-yVec(dalphaRH)*yVec(dy)*pVec(l3)*pVec(m3)*t886+yVec(dphi)*yVec(dx)*pVec(l0)*pVec(m1)*t879+yVec(dphi)*yVec(dx)*pVec(l1)*pVec(m2)*t879*2.0+yVec(dphi)*yVec(dx)*pVec(l1)*pVec(m3)*t879*2.0-yVec(dphi)*yVec(dx)*pVec(l2)*pVec(m2)*t881-yVec(dphi)*yVec(dx)*pVec(l2)*pVec(m2)*t883-yVec(dphi)*yVec(dy)*pVec(l3)*pVec(m3)*t884-yVec(dphi)*yVec(dy)*pVec(l3)*pVec(m3)*t886+yVec(dphi)*yVec(dy)*pVec(l0)*pVec(m1)*t918+yVec(dphi)*yVec(dy)*pVec(l1)*pVec(m2)*t918*2.0+yVec(dphi)*yVec(dy)*pVec(l1)*pVec(m3)*t918*2.0-yVec(dtheta)*yVec(dx)*pVec(l2)*pVec(m2)*t881-yVec(dtheta)*yVec(dx)*pVec(l2)*pVec(m2)*t883-yVec(dtheta)*yVec(dy)*pVec(l3)*pVec(m3)*t884-yVec(dtheta)*yVec(dy)*pVec(l3)*pVec(m3)*t886-yVec(dalphaLH)*yVec(dx)*yVec(lLH)*pVec(m3)*t881-yVec(dalphaRH)*yVec(dx)*yVec(lRH)*pVec(m3)*t883-yVec(dphi)*yVec(dx)*yVec(lLH)*pVec(m3)*t881-yVec(dtheta)*yVec(dx)*yVec(lLH)*pVec(m3)*t881-yVec(dphi)*yVec(dx)*yVec(lRH)*pVec(m3)*t883-yVec(dtheta)*yVec(dx)*yVec(lRH)*pVec(m3)*t883;
	h(4,0) = t1061+t1063+t1070+t1072+t1074+t1076+t1078+t1079+t1085+t1087+t1089-yVec(dphi)*t1122-yVec(dtheta)*t1122+yVec(dlLH)*(t951+t954+t1017-t1123-t1124-t1125-t1126-t1127+t1130+t1131)-pVec(g)*(t1009+t1011-t1121)+yVec(dalphaLH)*(-t968-t970-t972+t1032+t1037+t1039+yVec(dphi)*pVec(l1)*pVec(l2)*pVec(m2)*t974-yVec(dphi)*pVec(l1)*pVec(l3)*pVec(m3)*t974+yVec(dtheta)*pVec(l1)*pVec(l2)*pVec(m2)*t974-yVec(dtheta)*pVec(l1)*pVec(l3)*pVec(m3)*t974+yVec(dphi)*pVec(l1)*yVec(lLH)*pVec(m3)*t974+yVec(dtheta)*pVec(l1)*yVec(lLH)*pVec(m3)*t974)-yVec(dalphaLH)*yVec(dx)*pVec(l2)*pVec(m2)*t881-yVec(dalphaLH)*yVec(dy)*pVec(l3)*pVec(m3)*t884-yVec(dlLH)*yVec(dphi)*pVec(l1)*pVec(m3)*t953-yVec(dlLH)*yVec(dtheta)*pVec(l1)*pVec(m3)*t953-yVec(dphi)*yVec(dx)*pVec(l2)*pVec(m2)*t881-yVec(dphi)*yVec(dy)*pVec(l3)*pVec(m3)*t884-yVec(dtheta)*yVec(dx)*pVec(l2)*pVec(m2)*t881-yVec(dtheta)*yVec(dy)*pVec(l3)*pVec(m3)*t884-yVec(dalphaLH)*yVec(dx)*yVec(lLH)*pVec(m3)*t881-yVec(dphi)*yVec(dx)*yVec(lLH)*pVec(m3)*t881-yVec(dtheta)*yVec(dx)*yVec(lLH)*pVec(m3)*t881-pVec(l1)*pVec(l2)*pVec(m2)*t974*t1128-pVec(l1)*pVec(l2)*pVec(m2)*t974*t1129+pVec(l1)*pVec(l3)*pVec(m3)*t974*t1128+pVec(l1)*pVec(l3)*pVec(m3)*t974*t1129-pVec(l1)*yVec(lLH)*pVec(m3)*t974*t1128-pVec(l1)*yVec(lLH)*pVec(m3)*t974*t1129-yVec(dalphaLH)*yVec(dphi)*pVec(l1)*pVec(l2)*pVec(m2)*t974+yVec(dalphaLH)*yVec(dphi)*pVec(l1)*pVec(l3)*pVec(m3)*t974-yVec(dalphaLH)*yVec(dtheta)*pVec(l1)*pVec(l2)*pVec(m2)*t974+yVec(dalphaLH)*yVec(dtheta)*pVec(l1)*pVec(l3)*pVec(m3)*t974-yVec(dphi)*yVec(dtheta)*pVec(l1)*pVec(l2)*pVec(m2)*t974*2.0+yVec(dphi)*yVec(dtheta)*pVec(l1)*pVec(l3)*pVec(m3)*t974*2.0-yVec(dalphaLH)*yVec(dphi)*pVec(l1)*yVec(lLH)*pVec(m3)*t974-yVec(dalphaLH)*yVec(dtheta)*pVec(l1)*yVec(lLH)*pVec(m3)*t974-yVec(dphi)*yVec(dtheta)*pVec(l1)*yVec(lLH)*pVec(m3)*t974*2.0;
	h(5,0) = -yVec(dphi)*t1132-yVec(dtheta)*t1132-yVec(dalphaLH)*(t1123+t1124-t1130-t1131)+pVec(g)*pVec(m3)*t884-pVec(l3)*pVec(m3)*t1128-pVec(l3)*pVec(m3)*t1129-pVec(l3)*pVec(m3)*t1133+yVec(lLH)*pVec(m3)*t1128+yVec(lLH)*pVec(m3)*t1129+yVec(lLH)*pVec(m3)*t1133-yVec(dalphaLH)*yVec(dphi)*pVec(l3)*pVec(m3)*2.0-yVec(dalphaLH)*yVec(dtheta)*pVec(l3)*pVec(m3)*2.0-yVec(dphi)*yVec(dtheta)*pVec(l3)*pVec(m3)*2.0+yVec(dalphaLH)*yVec(dphi)*yVec(lLH)*pVec(m3)*2.0+yVec(dalphaLH)*yVec(dtheta)*yVec(lLH)*pVec(m3)*2.0+yVec(dphi)*yVec(dtheta)*yVec(lLH)*pVec(m3)*2.0+yVec(dalphaLH)*yVec(dx)*pVec(m3)*t884+yVec(dalphaLH)*yVec(dy)*pVec(m3)*t881+yVec(dphi)*yVec(dx)*pVec(m3)*t884+yVec(dphi)*yVec(dy)*pVec(m3)*t881+yVec(dtheta)*yVec(dx)*pVec(m3)*t884+yVec(dtheta)*yVec(dy)*pVec(m3)*t881-pVec(l1)*pVec(m3)*t953*t1128-pVec(l1)*pVec(m3)*t953*t1129-yVec(dalphaLH)*yVec(dphi)*pVec(l1)*pVec(m3)*t953-yVec(dalphaLH)*yVec(dtheta)*pVec(l1)*pVec(m3)*t953-yVec(dphi)*yVec(dtheta)*pVec(l1)*pVec(m3)*t953*2.0;
	h(6,0) = t1059+t1065+t1091+t1093+t1095+t1097+t1099+t1101+t1103+t1105+t1107-t1135-t1136-t1137-t1138-t1139-t1140-t1141-t1142-t1143-yVec(dphi)*t1134+yVec(dtheta)*t1134-pVec(g)*(t1013+t1015-pVec(l3)*pVec(m3)*t861)-yVec(dalphaLF)*(t956+t958+t960-t976-t977-t978+yVec(dphi)*pVec(l1)*pVec(l2)*pVec(m2)*t948-yVec(dphi)*pVec(l1)*pVec(l3)*pVec(m3)*t948-yVec(dtheta)*pVec(l1)*pVec(l2)*pVec(m2)*t948+yVec(dtheta)*pVec(l1)*pVec(l3)*pVec(m3)*t948+yVec(dphi)*pVec(l1)*yVec(lLF)*pVec(m3)*t948-yVec(dtheta)*pVec(l1)*yVec(lLF)*pVec(m3)*t948)-yVec(dlLF)*(t951-t954+t994+t995+t996+t997+t1144-yVec(dalphaLF)*pVec(l3)*pVec(m3)*2.0-yVec(dtheta)*yVec(lLF)*pVec(m3)*2.0-yVec(dtheta)*pVec(l1)*pVec(m3)*t950)+yVec(dlLF)*yVec(dphi)*pVec(l1)*pVec(m3)*t950-yVec(dlLF)*yVec(dtheta)*pVec(l1)*pVec(m3)*t950+pVec(l1)*pVec(l2)*pVec(m2)*t948*t1128+pVec(l1)*pVec(l2)*pVec(m2)*t948*t1129-pVec(l1)*pVec(l3)*pVec(m3)*t948*t1128-pVec(l1)*pVec(l3)*pVec(m3)*t948*t1129+pVec(l1)*yVec(lLF)*pVec(m3)*t948*t1128+pVec(l1)*yVec(lLF)*pVec(m3)*t948*t1129+yVec(dalphaLF)*yVec(dphi)*pVec(l1)*pVec(l2)*pVec(m2)*t948-yVec(dalphaLF)*yVec(dphi)*pVec(l1)*pVec(l3)*pVec(m3)*t948-yVec(dalphaLF)*yVec(dtheta)*pVec(l1)*pVec(l2)*pVec(m2)*t948+yVec(dalphaLF)*yVec(dtheta)*pVec(l1)*pVec(l3)*pVec(m3)*t948-yVec(dphi)*yVec(dtheta)*pVec(l1)*pVec(l2)*pVec(m2)*t948*2.0+yVec(dphi)*yVec(dtheta)*pVec(l1)*pVec(l3)*pVec(m3)*t948*2.0+yVec(dalphaLF)*yVec(dphi)*pVec(l1)*yVec(lLF)*pVec(m3)*t948-yVec(dalphaLF)*yVec(dtheta)*pVec(l1)*yVec(lLF)*pVec(m3)*t948-yVec(dphi)*yVec(dtheta)*pVec(l1)*yVec(lLF)*pVec(m3)*t948*2.0;
	h(7,0) = t1160-yVec(dphi)*t1145+yVec(dtheta)*t1145-yVec(dalphaLF)*(t994+t997+t1144-yVec(dtheta)*pVec(l1)*pVec(m3)*t950)+pVec(g)*pVec(m3)*t864-pVec(l3)*pVec(m3)*t1128-pVec(l3)*pVec(m3)*t1129-pVec(l3)*pVec(m3)*t1146+yVec(lLF)*pVec(m3)*t1128+yVec(lLF)*pVec(m3)*t1129+yVec(lLF)*pVec(m3)*t1146-yVec(dalphaLF)*yVec(dphi)*pVec(l3)*pVec(m3)*2.0+yVec(dalphaLF)*yVec(dtheta)*pVec(l3)*pVec(m3)*2.0+yVec(dalphaLF)*yVec(dphi)*yVec(lLF)*pVec(m3)*2.0-yVec(dalphaLF)*yVec(dtheta)*yVec(lLF)*pVec(m3)*2.0-yVec(dphi)*yVec(dtheta)*yVec(lLF)*pVec(m3)*2.0+yVec(dalphaLF)*yVec(dx)*pVec(m3)*t864+yVec(dalphaLF)*yVec(dy)*pVec(m3)*t861+yVec(dphi)*yVec(dx)*pVec(m3)*t864+yVec(dphi)*yVec(dy)*pVec(m3)*t861-yVec(dtheta)*yVec(dx)*pVec(m3)*t864-yVec(dtheta)*yVec(dy)*pVec(m3)*t861+pVec(l1)*pVec(m3)*t950*t1128+pVec(l1)*pVec(m3)*t950*t1129+yVec(dalphaLF)*yVec(dphi)*pVec(l1)*pVec(m3)*t950-yVec(dalphaLF)*yVec(dtheta)*pVec(l1)*pVec(m3)*t950-yVec(dphi)*yVec(dtheta)*pVec(l1)*pVec(m3)*t950*2.0;
	h(8,0) = t1060+t1066+t1092+t1094+t1096+t1098+t1100+t1102+t1104+t1106+t1108-t1148-t1149-t1150-t1151-t1152-t1153-t1154-t1155-t1156-yVec(dphi)*t1147+yVec(dtheta)*t1147-pVec(g)*(t1014+t1016-pVec(l3)*pVec(m3)*t863)-yVec(dalphaRF)*(t957+t959+t961-t985-t986-t987+yVec(dphi)*pVec(l1)*pVec(l2)*pVec(m2)*t949-yVec(dphi)*pVec(l1)*pVec(l3)*pVec(m3)*t949-yVec(dtheta)*pVec(l1)*pVec(l2)*pVec(m2)*t949+yVec(dtheta)*pVec(l1)*pVec(l3)*pVec(m3)*t949+yVec(dphi)*pVec(l1)*yVec(lRF)*pVec(m3)*t949-yVec(dtheta)*pVec(l1)*yVec(lRF)*pVec(m3)*t949)-yVec(dlRF)*(t951-t954+t1000+t1001+t1002+t1003+t1157-yVec(dalphaRF)*pVec(l3)*pVec(m3)*2.0-yVec(dtheta)*yVec(lRF)*pVec(m3)*2.0-yVec(dtheta)*pVec(l1)*pVec(m3)*t952)+yVec(dlRF)*yVec(dphi)*pVec(l1)*pVec(m3)*t952-yVec(dlRF)*yVec(dtheta)*pVec(l1)*pVec(m3)*t952+pVec(l1)*pVec(l2)*pVec(m2)*t949*t1128+pVec(l1)*pVec(l2)*pVec(m2)*t949*t1129-pVec(l1)*pVec(l3)*pVec(m3)*t949*t1128-pVec(l1)*pVec(l3)*pVec(m3)*t949*t1129+pVec(l1)*yVec(lRF)*pVec(m3)*t949*t1128+pVec(l1)*yVec(lRF)*pVec(m3)*t949*t1129+yVec(dalphaRF)*yVec(dphi)*pVec(l1)*pVec(l2)*pVec(m2)*t949-yVec(dalphaRF)*yVec(dphi)*pVec(l1)*pVec(l3)*pVec(m3)*t949-yVec(dalphaRF)*yVec(dtheta)*pVec(l1)*pVec(l2)*pVec(m2)*t949+yVec(dalphaRF)*yVec(dtheta)*pVec(l1)*pVec(l3)*pVec(m3)*t949-yVec(dphi)*yVec(dtheta)*pVec(l1)*pVec(l2)*pVec(m2)*t949*2.0+yVec(dphi)*yVec(dtheta)*pVec(l1)*pVec(l3)*pVec(m3)*t949*2.0+yVec(dalphaRF)*yVec(dphi)*pVec(l1)*yVec(lRF)*pVec(m3)*t949-yVec(dalphaRF)*yVec(dtheta)*pVec(l1)*yVec(lRF)*pVec(m3)*t949-yVec(dphi)*yVec(dtheta)*pVec(l1)*yVec(lRF)*pVec(m3)*t949*2.0;
	h(9,0) = t1160-yVec(dphi)*t1158+yVec(dtheta)*t1158-yVec(dalphaRF)*(t1000+t1003+t1157-yVec(dtheta)*pVec(l1)*pVec(m3)*t952)+pVec(g)*pVec(m3)*t866-pVec(l3)*pVec(m3)*t1128-pVec(l3)*pVec(m3)*t1129-pVec(l3)*pVec(m3)*t1159+yVec(lRF)*pVec(m3)*t1128+yVec(lRF)*pVec(m3)*t1129+yVec(lRF)*pVec(m3)*t1159-yVec(dalphaRF)*yVec(dphi)*pVec(l3)*pVec(m3)*2.0+yVec(dalphaRF)*yVec(dtheta)*pVec(l3)*pVec(m3)*2.0+yVec(dalphaRF)*yVec(dphi)*yVec(lRF)*pVec(m3)*2.0-yVec(dalphaRF)*yVec(dtheta)*yVec(lRF)*pVec(m3)*2.0-yVec(dphi)*yVec(dtheta)*yVec(lRF)*pVec(m3)*2.0+yVec(dalphaRF)*yVec(dx)*pVec(m3)*t866+yVec(dalphaRF)*yVec(dy)*pVec(m3)*t863+yVec(dphi)*yVec(dx)*pVec(m3)*t866+yVec(dphi)*yVec(dy)*pVec(m3)*t863-yVec(dtheta)*yVec(dx)*pVec(m3)*t866-yVec(dtheta)*yVec(dy)*pVec(m3)*t863+pVec(l1)*pVec(m3)*t952*t1128+pVec(l1)*pVec(m3)*t952*t1129+yVec(dalphaRF)*yVec(dphi)*pVec(l1)*pVec(m3)*t952-yVec(dalphaRF)*yVec(dtheta)*pVec(l1)*pVec(m3)*t952-yVec(dphi)*yVec(dtheta)*pVec(l1)*pVec(m3)*t952*2.0;
	h(10,0) = t1062+t1064+t1071+t1073+t1075+t1077+t1080+t1081+t1086+t1088+t1090-yVec(dphi)*t1162-yVec(dtheta)*t1162+yVec(dlRH)*(t951+t954+t1023-t1163-t1164-t1165-t1166-t1167+t1168+t1169)-pVec(g)*(t1010+t1012-t1161)+yVec(dalphaRH)*(-t969-t971-t973+t1033+t1038+t1040+yVec(dphi)*pVec(l1)*pVec(l2)*pVec(m2)*t975-yVec(dphi)*pVec(l1)*pVec(l3)*pVec(m3)*t975+yVec(dtheta)*pVec(l1)*pVec(l2)*pVec(m2)*t975-yVec(dtheta)*pVec(l1)*pVec(l3)*pVec(m3)*t975+yVec(dphi)*pVec(l1)*yVec(lRH)*pVec(m3)*t975+yVec(dtheta)*pVec(l1)*yVec(lRH)*pVec(m3)*t975)-yVec(dalphaRH)*yVec(dx)*pVec(l2)*pVec(m2)*t883-yVec(dalphaRH)*yVec(dy)*pVec(l3)*pVec(m3)*t886-yVec(dlRH)*yVec(dphi)*pVec(l1)*pVec(m3)*t955-yVec(dlRH)*yVec(dtheta)*pVec(l1)*pVec(m3)*t955-yVec(dphi)*yVec(dx)*pVec(l2)*pVec(m2)*t883-yVec(dphi)*yVec(dy)*pVec(l3)*pVec(m3)*t886-yVec(dtheta)*yVec(dx)*pVec(l2)*pVec(m2)*t883-yVec(dtheta)*yVec(dy)*pVec(l3)*pVec(m3)*t886-yVec(dalphaRH)*yVec(dx)*yVec(lRH)*pVec(m3)*t883-yVec(dphi)*yVec(dx)*yVec(lRH)*pVec(m3)*t883-yVec(dtheta)*yVec(dx)*yVec(lRH)*pVec(m3)*t883-pVec(l1)*pVec(l2)*pVec(m2)*t975*t1128-pVec(l1)*pVec(l2)*pVec(m2)*t975*t1129+pVec(l1)*pVec(l3)*pVec(m3)*t975*t1128+pVec(l1)*pVec(l3)*pVec(m3)*t975*t1129-pVec(l1)*yVec(lRH)*pVec(m3)*t975*t1128-pVec(l1)*yVec(lRH)*pVec(m3)*t975*t1129-yVec(dalphaRH)*yVec(dphi)*pVec(l1)*pVec(l2)*pVec(m2)*t975+yVec(dalphaRH)*yVec(dphi)*pVec(l1)*pVec(l3)*pVec(m3)*t975-yVec(dalphaRH)*yVec(dtheta)*pVec(l1)*pVec(l2)*pVec(m2)*t975+yVec(dalphaRH)*yVec(dtheta)*pVec(l1)*pVec(l3)*pVec(m3)*t975-yVec(dphi)*yVec(dtheta)*pVec(l1)*pVec(l2)*pVec(m2)*t975*2.0+yVec(dphi)*yVec(dtheta)*pVec(l1)*pVec(l3)*pVec(m3)*t975*2.0-yVec(dalphaRH)*yVec(dphi)*pVec(l1)*yVec(lRH)*pVec(m3)*t975-yVec(dalphaRH)*yVec(dtheta)*pVec(l1)*yVec(lRH)*pVec(m3)*t975-yVec(dphi)*yVec(dtheta)*pVec(l1)*yVec(lRH)*pVec(m3)*t975*2.0;
	h(11,0) = -t1160-yVec(dphi)*t1170-yVec(dtheta)*t1170-yVec(dalphaRH)*(t1163+t1164-t1168-t1169)+pVec(g)*pVec(m3)*t886-pVec(l3)*pVec(m3)*t1128-pVec(l3)*pVec(m3)*t1129-pVec(l3)*pVec(m3)*t1171+yVec(lRH)*pVec(m3)*t1128+yVec(lRH)*pVec(m3)*t1129+yVec(lRH)*pVec(m3)*t1171-yVec(dalphaRH)*yVec(dphi)*pVec(l3)*pVec(m3)*2.0-yVec(dalphaRH)*yVec(dtheta)*pVec(l3)*pVec(m3)*2.0+yVec(dalphaRH)*yVec(dphi)*yVec(lRH)*pVec(m3)*2.0+yVec(dalphaRH)*yVec(dtheta)*yVec(lRH)*pVec(m3)*2.0+yVec(dphi)*yVec(dtheta)*yVec(lRH)*pVec(m3)*2.0+yVec(dalphaRH)*yVec(dx)*pVec(m3)*t886+yVec(dalphaRH)*yVec(dy)*pVec(m3)*t883+yVec(dphi)*yVec(dx)*pVec(m3)*t886+yVec(dphi)*yVec(dy)*pVec(m3)*t883+yVec(dtheta)*yVec(dx)*pVec(m3)*t886+yVec(dtheta)*yVec(dy)*pVec(m3)*t883-pVec(l1)*pVec(m3)*t955*t1128-pVec(l1)*pVec(m3)*t955*t1129-yVec(dalphaRH)*yVec(dphi)*pVec(l1)*pVec(m3)*t955-yVec(dalphaRH)*yVec(dtheta)*pVec(l1)*pVec(m3)*t955-yVec(dphi)*yVec(dtheta)*pVec(l1)*pVec(m3)*t955*2.0;
	
	}

void  PB_Dynamics::ComputeContactPointLH(){
	posLH =  Vector2d::Zero();
	double t1173 = yVec(phi)+yVec(theta);
	double t1174 = yVec(alphaLH)+yVec(phi)+yVec(theta);
	posLH(0,0) = yVec(x)-pVec(l1)*cos(t1173)+yVec(lLH)*sin(t1174);
	posLH(1,0) = -pVec(rFoot)+yVec(y)-yVec(lLH)*cos(t1174)-pVec(l1)*sin(t1173);
}

void  PB_Dynamics::ComputeContactPointLF(){
	posLF =  Vector2d::Zero();
	double t1176 = yVec(alphaLF)+yVec(phi)-yVec(theta);
	double t1177 = yVec(phi)-yVec(theta);
	posLF(0,0) = yVec(x)+pVec(l1)*cos(t1177)+yVec(lLF)*sin(t1176);
	posLF(1,0) = -pVec(rFoot)+yVec(y)-yVec(lLF)*cos(t1176)+pVec(l1)*sin(t1177);
}

void  PB_Dynamics::ComputeContactPointRF(){
	posRF =  Vector2d::Zero();
	double t1179 = yVec(alphaRF)+yVec(phi)-yVec(theta);
	double t1180 = yVec(phi)-yVec(theta);
	posRF(0,0) = yVec(x)+pVec(l1)*cos(t1180)+yVec(lRF)*sin(t1179);
	posRF(1,0) = -pVec(rFoot)+yVec(y)-yVec(lRF)*cos(t1179)+pVec(l1)*sin(t1180);
}

void  PB_Dynamics::ComputeContactPointRH(){
	posRH =  Vector2d::Zero();
	double t1182 = yVec(phi)+yVec(theta);
	double t1183 = yVec(alphaRH)+yVec(phi)+yVec(theta);
	posRH(0,0) = yVec(x)-pVec(l1)*cos(t1182)+yVec(lRH)*sin(t1183);
	posRH(1,0) = -pVec(rFoot)+yVec(y)-yVec(lRH)*cos(t1183)-pVec(l1)*sin(t1182);
}

void PB_Dynamics::ComputeContactJacobianLH(){
	JLH =  Matrix2Qd::Zero();
	double t460 = pVec(rFoot)*(1.0/2.0);
	double t461 = yVec(phi)+yVec(theta);
	double t462 = sin(t461);
	double t463 = pVec(l1)*t462;
	double t464 = yVec(alphaLH)+yVec(phi)+yVec(theta);
	double t465 = cos(t464);
	double t466 = yVec(lLH)*t465;
	double t467 = t460+t463+t466;
	double t468 = sin(t464);
	double t469 = cos(t461);
	double t470 = yVec(lLH)*t468;
	double t471 = t470-pVec(l1)*t469;
	JLH(0,0) = 1.0;
	JLH(0,2) = t467;
	JLH(0,3) = t467;
	JLH(0,4) = pVec(rFoot)+t466;
	JLH(0,5) = t468;
	JLH(1,1) = 1.0;
	JLH(1,2) = t471;
	JLH(1,3) = t471;
	JLH(1,4) = t470;
	JLH(1,5) = -t465;
	
}

void PB_Dynamics::ComputeContactJacobianLF(){
	JLF =  Matrix2Qd::Zero();
	double t473 = pVec(rFoot)*(1.0/2.0);
	double t474 = yVec(alphaLF)+yVec(phi)-yVec(theta);
	double t475 = cos(t474);
	double t476 = yVec(lLF)*t475;
	double t477 = yVec(phi)-yVec(theta);
	double t478 = sin(t477);
	double t479 = sin(t474);
	double t480 = yVec(lLF)*t479;
	double t481 = cos(t477);
	double t482 = pVec(l1)*t481;
	JLF(0,0) = 1.0;
	JLF(0,2) = t473+t476-pVec(l1)*t478;
	JLF(0,3) = -t473-t476+pVec(l1)*t478;
	JLF(0,6) = pVec(rFoot)+t476;
	JLF(0,7) = t479;
	JLF(1,1) = 1.0;
	JLF(1,2) = t480+t482;
	JLF(1,3) = -t480-t482;
	JLF(1,6) = t480;
	JLF(1,7) = -t475;
	
}
void PB_Dynamics::ComputeContactJacobianRF(){
	JRF =  Matrix2Qd::Zero();
	double t484 = pVec(rFoot)*(1.0/2.0);
	double t485 = yVec(alphaRF)+yVec(phi)-yVec(theta);
	double t486 = cos(t485);
	double t487 = yVec(lRF)*t486;
	double t488 = yVec(phi)-yVec(theta);
	double t489 = sin(t488);
	double t490 = sin(t485);
	double t491 = yVec(lRF)*t490;
	double t492 = cos(t488);
	double t493 = pVec(l1)*t492;
	JRF(0,0) = 1.0;
	JRF(0,2) = t484+t487-pVec(l1)*t489;
	JRF(0,3) = -t484-t487+pVec(l1)*t489;
	JRF(0,8) = pVec(rFoot)+t487;
	JRF(0,9) = t490;
	JRF(1,1) = 1.0;
	JRF(1,2) = t491+t493;
	JRF(1,3) = -t491-t493;
	JRF(1,8) = t491;
	JRF(1,9) = -t486;
	
}
void PB_Dynamics::ComputeContactJacobianRH(){
	JRH =  Matrix2Qd::Zero();
	double t495 = pVec(rFoot)*(1.0/2.0);
	double t496 = yVec(phi)+yVec(theta);
	double t497 = sin(t496);
	double t498 = pVec(l1)*t497;
	double t499 = yVec(alphaRH)+yVec(phi)+yVec(theta);
	double t500 = cos(t499);
	double t501 = yVec(lRH)*t500;
	double t502 = t495+t498+t501;
	double t503 = sin(t499);
	double t504 = cos(t496);
	double t505 = yVec(lRH)*t503;
	double t506 = t505-pVec(l1)*t504;
	JRH(0,0) = 1.0;
	JRH(0,2) = t502;
	JRH(0,3) = t502;
	JRH(0,10) = pVec(rFoot)+t501;
	JRH(0,11) = t503;
	JRH(1,1) = 1.0;
	JRH(1,2) = t506;
	JRH(1,3) = t506;
	JRH(1,10) = t505;
	JRH(1,11) = -t500;
	
	
}
void  PB_Dynamics::ComputeContactJacobianDtTIMESdqdtLH(){
	dJLHdtdqdt =  Vector2d::Zero();
	double t508 = yVec(phi)+yVec(theta);
	double t509 = cos(t508);
	double t510 = pVec(l1)*t509;
	double t511 = yVec(alphaLH)+yVec(phi)+yVec(theta);
	double t512 = sin(t511);
	double t514 = yVec(lLH)*t512;
	double t513 = t510-t514;
	double t515 = yVec(dphi)*t513;
	double t516 = yVec(dtheta)*t513;
	double t517 = cos(t511);
	double t518 = yVec(dlLH)*t517;
	double t520 = yVec(dalphaLH)*yVec(lLH)*t512;
	double t519 = t515+t516+t518-t520;
	double t521 = sin(t508);
	double t522 = pVec(l1)*t521;
	double t523 = yVec(lLH)*t517;
	double t524 = t522+t523;
	double t525 = yVec(dphi)*t524;
	double t526 = yVec(dtheta)*t524;
	double t527 = yVec(dlLH)*t512;
	double t528 = yVec(dalphaLH)*yVec(lLH)*t517;
	double t529 = t525+t526+t527+t528;
	double t530 = yVec(dalphaLH)+yVec(dphi)+yVec(dtheta);
	dJLHdtdqdt(0,0) = yVec(dphi)*t519+yVec(dtheta)*t519-yVec(dalphaLH)*(-t518+t520+yVec(dphi)*yVec(lLH)*t512+yVec(dtheta)*yVec(lLH)*t512)+yVec(dlLH)*t517*t530;
	dJLHdtdqdt(1,0) = yVec(dphi)*t529+yVec(dtheta)*t529+yVec(dalphaLH)*(t527+t528+yVec(dphi)*yVec(lLH)*t517+yVec(dtheta)*yVec(lLH)*t517)+yVec(dlLH)*t512*t530;
	
}
void  PB_Dynamics::ComputeContactJacobianDtTIMESdqdtLF(){
	dJLFdtdqdt =  Vector2d::Zero();
	double t532 = yVec(alphaLF)+yVec(phi)-yVec(theta);
	double t533 = sin(t532);
	double t534 = cos(t532);
	double t535 = yVec(dlLF)*t534;
	double t536 = yVec(lLF)*t533;
	double t537 = yVec(phi)-yVec(theta);
	double t538 = cos(t537);
	double t539 = pVec(l1)*t538;
	double t540 = t536+t539;
	double t541 = yVec(dtheta)*t540;
	double t542 = t535+t541-yVec(dphi)*t540-yVec(dalphaLF)*yVec(lLF)*t533;
	double t543 = yVec(lLF)*t534;
	double t544 = sin(t537);
	double t548 = pVec(l1)*t544;
	double t545 = t543-t548;
	double t546 = yVec(dlLF)*t533;
	double t547 = yVec(dalphaLF)*yVec(lLF)*t534;
	double t549 = yVec(dphi)*t545;
	double t550 = t546+t547+t549-yVec(dtheta)*t545;
	double t551 = yVec(dalphaLF)+yVec(dphi)-yVec(dtheta);
	dJLFdtdqdt(0,0) = yVec(dphi)*t542-yVec(dtheta)*t542+yVec(dalphaLF)*(t535-yVec(dalphaLF)*yVec(lLF)*t533-yVec(dphi)*yVec(lLF)*t533+yVec(dtheta)*yVec(lLF)*t533)+yVec(dlLF)*t534*t551;
	dJLFdtdqdt(1,0) = yVec(dphi)*t550-yVec(dtheta)*t550+yVec(dalphaLF)*(t546+t547+yVec(dphi)*yVec(lLF)*t534-yVec(dtheta)*yVec(lLF)*t534)+yVec(dlLF)*t533*t551;
	}

void  PB_Dynamics::ComputeContactJacobianDtTIMESdqdtRF(){
	dJRFdtdqdt =  Vector2d::Zero();
	double t553 = yVec(alphaRF)+yVec(phi)-yVec(theta);
	double t554 = sin(t553);
	double t555 = cos(t553);
	double t556 = yVec(dlRF)*t555;
	double t557 = yVec(lRF)*t554;
	double t558 = yVec(phi)-yVec(theta);
	double t559 = cos(t558);
	double t560 = pVec(l1)*t559;
	double t561 = t557+t560;
	double t562 = yVec(dtheta)*t561;
	double t563 = t556+t562-yVec(dphi)*t561-yVec(dalphaRF)*yVec(lRF)*t554;
	double t564 = yVec(lRF)*t555;
	double t565 = sin(t558);
	double t569 = pVec(l1)*t565;
	double t566 = t564-t569;
	double t567 = yVec(dlRF)*t554;
	double t568 = yVec(dalphaRF)*yVec(lRF)*t555;
	double t570 = yVec(dphi)*t566;
	double t571 = t567+t568+t570-yVec(dtheta)*t566;
	double t572 = yVec(dalphaRF)+yVec(dphi)-yVec(dtheta);
	dJRFdtdqdt(0,0) = yVec(dphi)*t563-yVec(dtheta)*t563+yVec(dalphaRF)*(t556-yVec(dalphaRF)*yVec(lRF)*t554-yVec(dphi)*yVec(lRF)*t554+yVec(dtheta)*yVec(lRF)*t554)+yVec(dlRF)*t555*t572;
	dJRFdtdqdt(1,0) = yVec(dphi)*t571-yVec(dtheta)*t571+yVec(dalphaRF)*(t567+t568+yVec(dphi)*yVec(lRF)*t555-yVec(dtheta)*yVec(lRF)*t555)+yVec(dlRF)*t554*t572;
	
}
void  PB_Dynamics::ComputeContactJacobianDtTIMESdqdtRH(){
	dJRHdtdqdt =  Vector2d::Zero();
	double t574 = yVec(phi)+yVec(theta);
	double t575 = cos(t574);
	double t576 = pVec(l1)*t575;
	double t577 = yVec(alphaRH)+yVec(phi)+yVec(theta);
	double t578 = sin(t577);
	double t580 = yVec(lRH)*t578;
	double t579 = t576-t580;
	double t581 = yVec(dphi)*t579;
	double t582 = yVec(dtheta)*t579;
	double t583 = cos(t577);
	double t584 = yVec(dlRH)*t583;
	double t586 = yVec(dalphaRH)*yVec(lRH)*t578;
	double t585 = t581+t582+t584-t586;
	double t587 = sin(t574);
	double t588 = pVec(l1)*t587;
	double t589 = yVec(lRH)*t583;
	double t590 = t588+t589;
	double t591 = yVec(dphi)*t590;
	double t592 = yVec(dtheta)*t590;
	double t593 = yVec(dlRH)*t578;
	double t594 = yVec(dalphaRH)*yVec(lRH)*t583;
	double t595 = t591+t592+t593+t594;
	double t596 = yVec(dalphaRH)+yVec(dphi)+yVec(dtheta);
	dJRHdtdqdt(0,0) = yVec(dphi)*t585+yVec(dtheta)*t585-yVec(dalphaRH)*(-t584+t586+yVec(dphi)*yVec(lRH)*t578+yVec(dtheta)*yVec(lRH)*t578)+yVec(dlRH)*t583*t596;
	dJRHdtdqdt(1,0) = yVec(dphi)*t595+yVec(dtheta)*t595+yVec(dalphaRH)*(t593+t594+yVec(dphi)*yVec(lRH)*t583+yVec(dtheta)*yVec(lRH)*t583)+yVec(dlRH)*t578*t596;
}
void PB_Dynamics::ComputeJointForces(){
	// Joint forces:
	tau = VectorQd::Zero();
	// Compute spring and damping forces:
	double T_spring_theta, T_spring_alphaLH, T_spring_alphaLF, T_spring_alphaRF, T_spring_alphaRH,
	       F_spring_lLH, F_spring_lLF, F_spring_lRF, F_spring_lRH;

	T_spring_theta = pVec(ktheta)*(yVec(utheta)  - yVec(theta)) +
				              b_theta *(yVec(dutheta) - yVec(dtheta));
	tau(qtheta) = T_spring_theta + 0;
	// HIP
	if (pVec(hip_jnt_type) == PEA){ // Parallel spring in the hip
		T_spring_alphaLH = pVec(kalpha)*(0 - yVec(alphaLH)) +
				              b_alpha *(0 - yVec(dalphaLH));
		T_spring_alphaLF = pVec(kalpha)*(0 - yVec(alphaLF)) +
				              b_alpha *(0 - yVec(dalphaLF));
		T_spring_alphaRF = pVec(kalpha)*(0 - yVec(alphaRF)) +
				              b_alpha *(0 - yVec(dalphaRF));
		T_spring_alphaRH = pVec(kalpha)*(0 - yVec(alphaRH)) +
				              b_alpha *(0 - yVec(dalphaRH));
		tau(qalphaLH) = T_spring_alphaLH + uVec(TalphaLH);
		tau(qalphaLF) = T_spring_alphaLF + uVec(TalphaLF);
		tau(qalphaRF) = T_spring_alphaRF + uVec(TalphaRF);
		tau(qalphaRH) = T_spring_alphaRH + uVec(TalphaRH);
	} else { // Serial spring in the hip
		T_spring_alphaLH = pVec(kalpha)*(yVec(ualphaLH)  - yVec(alphaLH)) +
				              b_alpha *(yVec(dualphaLH) - yVec(dalphaLH));
		T_spring_alphaLF = pVec(kalpha)*(yVec(ualphaLF)  - yVec(alphaLF)) +
				              b_alpha *(yVec(dualphaLF) - yVec(dalphaLF));
		T_spring_alphaRF = pVec(kalpha)*(yVec(ualphaRF)  - yVec(alphaRF)) +
				              b_alpha *(yVec(dualphaRF) - yVec(dalphaRF));
		T_spring_alphaRH = pVec(kalpha)*(yVec(ualphaRH)  - yVec(alphaRH)) +
				              b_alpha *(yVec(dualphaRH) - yVec(dalphaRH));
		tau(qalphaLH) = T_spring_alphaLH + 0;
		tau(qalphaLF) = T_spring_alphaLF + 0;
		tau(qalphaRF) = T_spring_alphaRF + 0;
		tau(qalphaRH) = T_spring_alphaRH + 0;
	}
	// LEG
	if (pVec(leg_jnt_type) == PEA){ // Parallel spring in the leg
		F_spring_lLH = pVec(kl)*(pVec(l_0) + 0 - yVec(lLH)) +
			              b_l *(          + 0 - yVec(dlLH));
		F_spring_lLF = pVec(kl)*(pVec(l_0) + 0 - yVec(lLF)) +
			              b_l *(          + 0 - yVec(dlLF));
		F_spring_lRF = pVec(kl)*(pVec(l_0) + 0 - yVec(lRF)) +
			              b_l *(          + 0 - yVec(dlRF));
		F_spring_lRH = pVec(kl)*(pVec(l_0) + 0 - yVec(lRH)) +
			              b_l *(          + 0 - yVec(dlRH));
		tau(qlLH) = F_spring_lLH + uVec(FlLH);
		tau(qlLF) = F_spring_lLF + uVec(FlLF);
		tau(qlRF) = F_spring_lRF + uVec(FlRF);
		tau(qlRH) = F_spring_lRH + uVec(FlRH);
	} else { // Serial spring in the leg
		F_spring_lLH = pVec(kl)*(pVec(l_0) + yVec(ulLH)  - yVec(lLH)) +
			              b_l *(          + yVec(dulLH) - yVec(dlLH));
		F_spring_lLF = pVec(kl)*(pVec(l_0) + yVec(ulLF)  - yVec(lLF)) +
			              b_l *(          + yVec(dulLF) - yVec(dlLF));
		F_spring_lRF = pVec(kl)*(pVec(l_0) + yVec(ulRF)  - yVec(lRF)) +
			              b_l *(          + yVec(dulRF) - yVec(dlRF));
		F_spring_lRH = pVec(kl)*(pVec(l_0) + yVec(ulRH)  - yVec(lRH)) +
			              b_l *(          + yVec(dulRH) - yVec(dlRH));
		tau(qlLH) = F_spring_lLH + 0;
		tau(qlLF) = F_spring_lLF + 0;
		tau(qlRF) = F_spring_lRF + 0;
		tau(qlRH) = F_spring_lRH + 0;
	}
}

double PB_Dynamics::Logistic(double x, double sigma){
	double y = x/sigma;
	
	if (abs(y) > 50){ // In linear domain
		return max(0.0,x);
	} else { // In logistic domain
		return sigma*log(1+exp(y));
	}
}

