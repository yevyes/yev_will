JLH = MatrixXXd::Zero();	double t230 = yVec(alphaLH)+yVec(phi);
	double t231 = cos(t230);
	double t232 = yVec(lLH)*t231;
	double t233 = sin(t230);
	double t234 = yVec(lLH)*t233;
	JLH(0,0) = 1.0;
	JLH(0,2) = pVec(rFoot)+t232+pVec(l1)*sin(yVec(phi));
	JLH(0,3) = pVec(rFoot)+t232;
	JLH(0,4) = t233;
	JLH(1,1) = 1.0;
	JLH(1,2) = t234-pVec(l1)*cos(yVec(phi));
	JLH(1,3) = t234;
	JLH(1,4) = -t231;
	