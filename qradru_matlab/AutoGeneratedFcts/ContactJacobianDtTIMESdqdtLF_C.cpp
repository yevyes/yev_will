dJLFdtTIMESdqdt = MatrixXXd::Zero();	double t262 = yVec(alphaLF)+yVec(phi);
	double t263 = sin(t262);
	double t264 = cos(t262);
	double t265 = yVec(dalphaLF)*yVec(lLF)*t263;
	double t266 = yVec(dlLF)*t263;
	double t267 = yVec(dalphaLF)*yVec(lLF)*t264;
	double t268 = yVec(dalphaLF)+yVec(dphi);
	dJLFdtTIMESdqdt(0,0) = -yVec(dalphaLF)*(t265-yVec(dlLF)*t264+yVec(dphi)*yVec(lLF)*t263)-yVec(dphi)*(t265+yVec(dphi)*(yVec(lLF)*t263+pVec(l1)*cos(yVec(phi)))-yVec(dlLF)*t264)+yVec(dlLF)*t264*t268;
	dJLFdtTIMESdqdt(1,0) = yVec(dphi)*(t266+t267+yVec(dphi)*(yVec(lLF)*t264-pVec(l1)*sin(yVec(phi))))+yVec(dalphaLF)*(t266+t267+yVec(dphi)*yVec(lLF)*t264)+yVec(dlLF)*t263*t268;
	