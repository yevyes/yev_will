dJLHdtTIMESdqdt = MatrixXXd::Zero();	double t254 = yVec(alphaLH)+yVec(phi);
	double t255 = sin(t254);
	double t256 = cos(t254);
	double t257 = yVec(dalphaLH)*yVec(lLH)*t255;
	double t258 = yVec(dlLH)*t255;
	double t259 = yVec(dalphaLH)*yVec(lLH)*t256;
	double t260 = yVec(dalphaLH)+yVec(dphi);
	dJLHdtTIMESdqdt(0,0) = -yVec(dalphaLH)*(t257-yVec(dlLH)*t256+yVec(dphi)*yVec(lLH)*t255)-yVec(dphi)*(t257+yVec(dphi)*(yVec(lLH)*t255-pVec(l1)*cos(yVec(phi)))-yVec(dlLH)*t256)+yVec(dlLH)*t256*t260;
	dJLHdtTIMESdqdt(1,0) = yVec(dphi)*(t258+t259+yVec(dphi)*(yVec(lLH)*t256+pVec(l1)*sin(yVec(phi))))+yVec(dalphaLH)*(t258+t259+yVec(dphi)*yVec(lLH)*t256)+yVec(dlLH)*t255*t260;
	