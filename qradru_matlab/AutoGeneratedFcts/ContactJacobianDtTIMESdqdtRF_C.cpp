dJRFdtTIMESdqdt = MatrixXXd::Zero();	double t270 = yVec(alphaRF)+yVec(phi);
	double t271 = sin(t270);
	double t272 = cos(t270);
	double t273 = yVec(dalphaRF)*yVec(lRF)*t271;
	double t274 = yVec(dlRF)*t271;
	double t275 = yVec(dalphaRF)*yVec(lRF)*t272;
	double t276 = yVec(dalphaRF)+yVec(dphi);
	dJRFdtTIMESdqdt(0,0) = -yVec(dalphaRF)*(t273-yVec(dlRF)*t272+yVec(dphi)*yVec(lRF)*t271)-yVec(dphi)*(t273+yVec(dphi)*(yVec(lRF)*t271+pVec(l1)*cos(yVec(phi)))-yVec(dlRF)*t272)+yVec(dlRF)*t272*t276;
	dJRFdtTIMESdqdt(1,0) = yVec(dphi)*(t274+t275+yVec(dphi)*(yVec(lRF)*t272-pVec(l1)*sin(yVec(phi))))+yVec(dalphaRF)*(t274+t275+yVec(dphi)*yVec(lRF)*t272)+yVec(dlRF)*t271*t276;
	