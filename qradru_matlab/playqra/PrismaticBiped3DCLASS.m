% *************************************************************************
% classdef PrismaticBiped3DCLASS(full3D) < OutputCLASS
%
% Three dimensional graphics of a prismatic monopod with series elastic
% actuation.  In the graphical representation, the robot is drawn with two
% legs which move together.  Since the motion is restricted to be planar,
% this is identical to a monopod.
%
% The system is initialized with a Boolean flag that indicates whether the
% view is fully three dimensional, or rather flat from the side.  The
% motion of the model is, however, in both cases 2D. 
%
%
% Properties: - NONE
% Methods:    - NONE
%
%
% Created by C. David Remy on 04/09/2011
% MATLAB 2010a
%
% Documentation:
%  'A MATLAB Framework For Gait Creation', 2011, C. David Remy (1), Keith
%  Buffinton (2), and Roland Siegwart (1),  International Conference on
%  Intelligent Robots and Systems, September 25-30, San Francisco, USA 
%
% (1) Autonomous Systems Lab, Institute of Robotics and Intelligent Systems, 
%     Swiss Federal Institute of Technology (ETHZ) 
%     Tannenstr. 3 / CLA-E-32.1
%     8092 Zurich, Switzerland  
%     cremy@ethz.ch; rsiegwart@ethz.ch
%
% (2) Department of Mechanical Engineering, 
%     Bucknell University
%     701 Moore Avenue
%     Lewisburg, PA-17837, USA
%     buffintk@bucknell.edu
%
%   See also OUTPUTCLASS.
%
classdef PrismaticBiped3DCLASS < OutputCLASS 
    properties (SetAccess = 'private', GetAccess = 'private')
        fig; % The output window
        % Patch objects used in the 3D representation
        HopperPatch;
        FloorPatch;
        % Surface objects for the logos
        logoL;
        logoR;
        % Vertices to which the transformation will be applied
        FloorVertices;
        % View Flag
        full3D
    end
    methods
        function obj = PrismaticBiped3DCLASS(full3D)
            obj.slowDown = 1; % Run this in real time.
            obj.rate     = 0.04;   % with 25 fps
            obj.full3D   = full3D;
            % Initialize the 3D graphics
            obj.fig = figure();
            clf(obj.fig);
            % Set some window properties
            set(obj.fig,'Name','3D-Output of a prismatic monopod');  % Window title
            set(obj.fig,'Color','w');         % Background color
            
            %% Create the 3D Objects:  
            % Create the floor:
            if full3D
                [f,v,c] = Get3DGroundPatch();
                p = patch('faces', f, 'vertices', v, 'FaceVertexCData', c, 'FaceColor', 'flat');
            	set(p, 'FaceAlpha',0.7);    % Set to partly transparent
            else
                [f,v,c] = Get2DGroundPatch();
                p = patch('faces', f, 'vertices', v, 'FaceVertexCData', c, 'FaceColor', 'flat');
            end
            set(p, 'FaceLighting','phong'); % Set the renderer
            set(p, 'FaceColor','flat');     % Set the face coloring
            set(p, 'EdgeColor','none');     % Don't show edges
            % Store theses objects for later use (the ground must be
            % shifted later, if the hopper moves)
            obj.FloorPatch    = p;
            obj.FloorVertices = v;
            % Create the hopper (with some arbitrary initial condition)
            [f,v,c] = GetBiped2DOFPatch(0, 1.2, 0, 0.1, 0, -0.1, 0, 1, 0, 1, 0);
            p = patch('faces', f, 'vertices', v, 'FaceVertexCData', c, 'FaceColor', 'flat');
            set(p, 'FaceLighting','phong');  % Set the renderer
            set(p, 'FaceColor','flat');      % Set the face coloring
            set(p, 'EdgeColor','none');      % Don't show edges
            obj.HopperPatch = p;
            % Add the logo:
            obj.logoL = GetRAMLogoSurface('l');
            obj.logoR = GetRAMLogoSurface('r');
            set(obj.logoL,'XData',[-0.7,+0.7;-0.7,+0.7]+0,'YData',[+0.45,+0.45;+0.45,+0.45],'ZData',[-0.2,-0.2;0.2,0.2]+1.2);
            set(obj.logoR,'XData',[+0.7,-0.7;+0.7,-0.7]+0,'YData',[-0.45,-0.45;-0.45,-0.45],'ZData',[-0.2,-0.2;0.2,0.2]+1.2);
            %% set up view:
            axis off
            box off
            axis equal
            if full3D
                camproj('perspective');
                camtarget([0, 0, +0.6])
                campos ([5, -10, +2.6]);
                camup ([0,0,1]);
                camva(10)
            else
                camproj('orthographic');
                camtarget([0, 0, +0.6])
                campos ([0, -10, +0.6]);
                camup ([0,0,1]);
                camva(10)
            end
            %% Create illumination:
            light('Position',[5 -10 12 ],'Style','local');   % parallel light, coming in the direction given in 'Position'
        end
        function obj = update(obj, y, ~, ~, u)
            persistent contStateIndices exctStateIndices
            if isempty(contStateIndices) || isempty(exctStateIndices)
                [~, ~, contStateIndices] = ContStateDefinition();
                [~, ~, exctStateIndices] = ExctStateDefinition();
            end
            % The floor is shifted to multiples of its pattern, so it's
            % not noticeable in the final graphics: 
            v = TransformVertices(obj.FloorVertices,...
                                  diag([1,1,1]),...
                                  [floor(y(contStateIndices.x)/2)*2,0,-0.05]);
            set(obj.FloorPatch,'Vertices',v); % Main body    
            % The hopper:
            [~, v] = GetBiped2DOFPatch(y(contStateIndices.x),...
                                       y(contStateIndices.y),...
                                       y(contStateIndices.phi),...
                                       y(contStateIndices.alphaL),...
                                       u(exctStateIndices.ualphaL),...
                                       y(contStateIndices.alphaR),...
                                       u(exctStateIndices.ualphaR),...
                                       y(contStateIndices.lL),...
                                       u(exctStateIndices.ulL),...
                                       y(contStateIndices.lR),...
                                       u(exctStateIndices.ulR));
            set(obj.HopperPatch,'Vertices',v); 
            % Logo:
            logoVertices = [-0.70,+0.70,-0.70,+0.70,+0.70,-0.70,+0.70,-0.70;
                            +0.451,+0.451,+0.451,+0.451,-0.451,-0.451,-0.451,-0.451;
                            -0.20,-0.20,+0.20,+0.20,-0.20,-0.20,+0.20,+0.20]';
            v = TransformVertices(logoVertices, Body312dc([0,0,-y(contStateIndices.phi)]), [y(contStateIndices.x),0,y(contStateIndices.y)])';
            set(obj.logoL,'XData',[v(1,1),v(1,2);v(1,3),v(1,4)],'YData',[v(2,1),v(2,2);v(2,3),v(2,4)],'ZData',[v(3,1),v(3,2);v(3,3),v(3,4)]);
            set(obj.logoR,'XData',[v(1,5),v(1,6);v(1,7),v(1,8)],'YData',[v(2,5),v(2,6);v(2,7),v(2,8)],'ZData',[v(3,5),v(3,6);v(3,7),v(3,8)]);
            
            % Set camera:
            if obj.full3D
                camtarget([y(contStateIndices.x), 0, +0.6])
                campos ([5, -10, +2.6]);
            else
                camtarget([y(contStateIndices.x), 0, +0.6])
                campos ([y(contStateIndices.x), -10, +0.6]);
            end
            drawnow();
        end
    end
end