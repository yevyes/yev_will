% Creates a data struct, as it is used in the function CreateDatFile. Each
% data struct create by this function is guranteed to converge to a
% solution in MUSCOD.
% gaitType can be of the following:
%   'RUN', 'WALK', 'SKIP', 'HOP', 'RUWA'
% hipJointType and legJointType are either
%   'PEA' or 'SEA'
%
% Possible uses:
% CreateDatFile('Data/RUN_PH_PL_BASE',CreateDatStruct('RUN','PEA','PEA'))
% CreateDatFile('Data/RUN_PH_SL_BASE',CreateDatStruct('RUN','PEA','SEA'))
% CreateDatFile('Data/RUN_SH_PL_BASE',CreateDatStruct('RUN','SEA','PEA'))
% CreateDatFile('Data/RUN_SH_SL_BASE',CreateDatStruct('RUN','SEA','SEA'))
function datStruct = CreateDatStruct(gaitType, hipJointType, legJointType)
    % First, define all properties that only depend on the gait type:
    switch gaitType
        case 'Trot'
            sd = zeros(41,1);
            sd(2)  = 0.5;   % dx
            sd(3)  = 1;   % y
            sd(4)  = 0.5; % dy
            %sd(23) = 1;   % lRH
            sd(27) = 1;   % lRF 
            %sd(31) = 1;   % lLF
            sd(35) = 1;   % lLH
            u  = zeros(8,1);
            h  = [1;0;1.4];
            nshoot = [5;1;15];
            p(3) = 1; % cost function 
            p(5) = 0; % limits (none)
            p(6:9) = [5;4;10;.5];
            p(10) = 0.001;
            typeString = 'Trotting';
            %datStruct.libmodel = 'libTrotting';
            datStruct.libmodel = 'libTrotting';
            datStruct.libind = {'ind_rkf45'; 'ind_strans'; 'ind_rkf45'};
            datStruct.h_name = {'flight'; 'touchdown LHRF'; 'stance LHRF'};
            datStruct.h_comment = {'Flight Phase'; 'touchdown LHRF'; 'stance LHRF'};
            datStruct.nshoot = nshoot;
            datStruct.h      = h;
            datStruct.h_sca  = [1.0;0;1.0];
            datStruct.h_min  = [0.0001;0.0;0.0001];
            datStruct.h_max  = [5.0;0.0;5.0];
            datStruct.h_fix  = [0;1;0];
            p(4) = 0.7; % set speed;
                        
        case 'WalkTrot'
            sd = zeros(41,1);
            sd(2)  = 0.1;   % dx
            sd(3)  = 1;   % y
            sd(4)  = 0.5; % dy
            sd(23) = 1;   % lLH
            sd(27) = 1;   % lLF
            sd(31) = 1;   % lRF
            sd(35) = 1;   % lRH
            u  = [0;0;0;0;1;0;1;0];
            h  = [1.5;0;0.5];
            nshoot = [15;1;5];
            p(3) = 1; % cost function 
            p(5) = 0; % limits (none)
            p(6:9) = [5;4;10;0.5];
            typeString = 'WalkTrotting';
            datStruct.libmodel = 'libWalkTrotting';
            datStruct.libind = {'ind_rkf45'; 'ind_strans'; 'ind_rkf45'};
            datStruct.h_name = {'stance LHRF'; 'touchdown LFRH'; 'stance LHLFRFRH'};
            datStruct.h_comment = {'stance LHRF'; 'touchdown LFRH'; 'stance LHLFRFRH'};
            datStruct.nshoot = nshoot;
            datStruct.h      = h;
            datStruct.h_sca  = [1.0;0;1.0];
            datStruct.h_min  = [0.0001;0.0;0.0001];
            datStruct.h_max  = [5.0;0.0;5.0];
            datStruct.h_fix  = [0;1;0];
            p(4) = 0.1; % set speed;
            
        case 'Bound'
            sd = zeros(41,1);
            sd(2)  = 0.5;   % dx
            sd(3)  = 1;   % y
            sd(4)  = 0.5; % dy
            sd(7)  = -0.2;   % alphaLH
            sd(9)  = -0.2;   % ualphaLH
            sd(11)  = 0.1;   % alphaLF
            sd(13)  = 0.1;   % ualphaLF
            sd(15)   = 0.1;   % alphaRF
            sd(17)   = 0.1;   % ualphaRF
            sd(19)  = -0.2;   % alphaRH
            sd(21)  = -0.2;   % ualphaRH
            sd(23) = 1;   % lLH
            sd(27) = 1;   % lLF
            sd(31) = 1;   % lRF
            sd(35) = 1;   % lRH
            u  = zeros(8,1);
            h  = [1;0;1;1;0;1];
            nshoot = [5;1;5;5;1;5];
            p(3) = 1; % cost function 
            p(5) = 0; % limits (none)
            p(6:9) = [5;4;10;0.5];
            typeString = 'Bounding';
            datStruct.libmodel = 'libBounding';
            datStruct.libind = {'ind_rkf45'; 'ind_strans'; 'ind_rkf45';'ind_rkf45'; 'ind_strans'; 'ind_rkf45'};
            datStruct.h_name = {'flight'; 'touchdown LFRF'; 'stance LFRF';'flight'; 'touchdown LHRH'; 'stance LHRH'};
            datStruct.h_comment = {'flight'; 'touchdown LFRF'; 'stance LFRF';'flight'; 'touchdown LHRH'; 'stance LHRH'};
            datStruct.nshoot = nshoot;
            datStruct.h      = h;
            datStruct.h_sca  = [1.0;0;1.0;1;0;1];
            datStruct.h_min  = [0.0001;0.0;0.0001;0.0001;0.0;0.0001];
 %           datStruct.h_min  = [0.2;0.0;0.2;0.2;0.0;0.2];
            datStruct.h_max  = [5.0;0.0;5.0;5.0;0.0;5.0];
            datStruct.h_fix  = [0;1;0;0;1;0];
            p(4) = 0.7; % set speed;
            
        case 'Walk'
            sd = zeros(41,1);
            sd(2)  = 0.3;   % dx
            sd(3)  = 1;   % y
            sd(4)  = 0.1; % dy
%             sd(7)  = -0.2;   % alphaLH
%             sd(9)  = -0.2;   % ualphaLH
%             sd(11)  = 0.1;   % alphaLF
%             sd(13)  = 0.1;   % ualphaLF
%             sd(15)   = 0.1;   % alphaRF
%             sd(17)   = 0.1;   % ualphaRF
%             sd(19)  = -0.2;   % alphaRH
%             sd(21)  = -0.2;   % ualphaRH
            sd(23) = 1;   % lLH
            sd(27) = 0.9;   % lLF
            sd(31) = 1;   % lRF
            sd(35) = 0.9;   % lRH
            u  = [0;0;0;0;1;0;1;0];
            h  = [1;0;1;1;0;1];
            nshoot = [5;1;5;5;1;5];
            p(3) = 1; % cost function 
            p(5) = 0; % limits (none)
            p(6:9) = [5;4;10;0.5];
            p(10) = 0.001;
            typeString = 'Walking';
            datStruct.libmodel = 'libWalking';
            datStruct.libind = {'ind_rkf45'; 'ind_strans'; 'ind_rkf45';'ind_rkf45'; 'ind_strans'; 'ind_rkf45'};
            datStruct.h_name = {'double stance LHlfRFrh'; 'touchdown LF'; 'triple stance LHLFRFrh';'double stance LHLFrfrh'; 'touchdown RH'; 'triple stance LHLFrfRH'};
            datStruct.h_comment = {'double stance LHlfRFrh'; 'touchdown LF'; 'triple stance LHLFRFrh';'double stance LHLFrfrh'; 'touchdown RH'; 'triple stance LHLFrfRH'};
            datStruct.nshoot = nshoot;
            datStruct.h      = h;
            datStruct.h_sca  = [1.0;0;1.0;1;0;1];
            datStruct.h_min  = [0.1;0.0;0.1;0.1;0.0;0.1];
 %           datStruct.h_min  = [0.2;0.0;0.2;0.2;0.0;0.2];
            datStruct.h_max  = [5.0;0.0;5.0;5.0;0.0;5.0];
            datStruct.h_fix  = [0;1;0;0;1;0];
            p(4) = 0.3; % set speed;
            
        case 'Gallop'
            sd = zeros(41,1);
            sd(2)  = 0.75;   % dx
            sd(3)  = 1.0;   % y
            sd(4)  = 0.1; % dy
            sd(7)  = 0.0;   % alphaLH
            sd(9)  = 0.0;   % ualphaLH
            sd(11)  = 0.0;   % alphaLF
            sd(13)  = 0.0;   % ualphaLF
            sd(15)  = -0.0;   % alphaRF
            sd(17)  = -0.0;   % ualphaRF
            sd(19)  = 0.0;   % alphaRH
            sd(21)  = 0.0;   % ualphaRH
            sd(23) = 1;   % lLH
            sd(27) = 1;   % lLF
            sd(31) = 1;   % lRF
            sd(35) = 1;   % lRH
            u  = [0;0;0;0;0.0;0.0;0.0;0.0];
            h  = [0.2;0;0.2;0;0.2;0.2;0;0.2;0.2;0;0.2;0.2];
            nshoot = [3;1;3;1;3;3;1;3;3;1;3;3];
%            nshoot = [5;1;5;1;5;5;1;5;5;1;5;5];
            p(3) = 1; % cost function 
            p(5) = 0; % limits (none)
            p(6:9) = [5;4;10;0.5];
            p(10) = 0.001;
            typeString = 'Gallopping';
            datStruct.libmodel = 'libGallopping';
            datStruct.libind = {'ind_rkf45'; 'ind_strans'; 'ind_rkf45';'ind_strans'; 'ind_rkf45'; 'ind_rkf45';'ind_strans'; 'ind_rkf45'; 'ind_rkf45';'ind_strans'; 'ind_rkf45'; 'ind_rkf45'};
            datStruct.h_name = {'flight'; 'touchdown collision of LH'; 'single stance LHlfrfrh';'touchdown collision of RH'; 'double stance LHlfrfRH'; 'single stance lhlfrfRH';
                                'touchdown collision of LF'; 'double stance lhLFrfRH'; 'single stance lhLFrfrh' ; 'touchdown collision of RF'; 'double stance lhLFRFrh'; 'single stance lhlfRFrh'};
            datStruct.h_comment = {'flight'; 'touchdown collision of LH'; 'single stance LHlfrfrh';'touchdown collision of RH'; 'double stance LHlfrfRH'; 'single stance lhlfrfRH';
                                'touchdown collision of LF'; 'double stance lhLFrfRH'; 'single stance lhLFrfrh' ; 'touchdown collision of RF'; 'double stance lhLFRFrh'; 'single stance lhlfRFrh'};
            datStruct.nshoot = nshoot;
            datStruct.h      = h;
            datStruct.h_sca  = [1;0;1;0;1;1;0;1;1;0;1;1];
            datStruct.h_min  = [0.0001;0.0;0.0001;0.0;0.0001;0.0001;0.0;0.0001;0.0001;0.0;0.0001;0.0001];
 %           datStruct.h_min  = [0.2;0.0;0.2;0.2;0.0;0.2];
            datStruct.h_max  = [5.0;0.0;5.0;0.0;5.0;5.0;0.0;5.0;5.0;0.0;5.0;5.0];
            datStruct.h_fix  = [0;1;0;1;0;0;1;0;0;1;0;0];
            p(4) = 0.75; % set speed;
            p(10) = .001; % Logistic Function Tuner
        case 'GallopTriple'
            sd = zeros(41,1);
            sd(2)  = 0.1;   % dx
            sd(3)  = 1.0;   % y
            sd(4)  = 0.1; % dy
            sd(7)  = 0.0;   % alphaLH
            sd(9)  = 0.0;   % ualphaLH
            sd(11)  = 0.0;   % alphaLF
            sd(13)  = 0.0;   % ualphaLF
            sd(15)  = -0.0;   % alphaRF
            sd(17)  = -0.0;   % ualphaRF
            sd(19)  = 0.0;   % alphaRH
            sd(21)  = 0.0;   % ualphaRH
            sd(23) = 1;   % lLH
            sd(27) = 1;   % lLF
            sd(31) = 1;   % lRF
            sd(35) = 1;   % lRH
            u  = [0;0;0;0;0.0;0.0;0.0;0.0];
            h  = [0.2;0;0.2;0;0.2;0;0.2;0.2;0;0.2;0.2;0.2];
            nshoot = [3;1;3;1;3;1;3;3;1;3;3;3];
            p(3) = 1; % cost function 
            p(5) = 0; % limits (none)
            p(6:9) = [5;4;10;0.5];
            typeString = 'GalloppingTriple';
            datStruct.libmodel = 'libGalloppingTriple';
            datStruct.libind = {'ind_rkf45'; 'ind_strans'; 'ind_rkf45';'ind_strans'; 'ind_rkf45'; 'ind_strans';'ind_rkf45'; 'ind_rkf45'; 'ind_strans'; 'ind_rkf45'; 'ind_rkf45'; 'ind_rkf45'};
            datStruct.h_name = {'flight'; 'touchdown collision of LH'; 'single stance LHlfrfrh';'touchdown collision of RH'; 'double stance LHlfrfRH'; 'touchdown collision of LF ';
                                'triple stance LHLFrfRH'; 'double stance lhLFrfRH'; 'touchdown collision of RF' ; 'triple stance lhLFRFRH'; 'double stance lhLFRFrh'; 'single stance lhlfRFrh'};
            datStruct.h_comment = {'flight'; 'touchdown collision of LH'; 'single stance LHlfrfrh';'touchdown collision of RH'; 'double stance LHlfrfRH'; 'touchdown collision of LF ';
                                'triple stance LHLFrfRH'; 'double stance lhLFrfRH'; 'touchdown collision of RF' ; 'triple stance lhLFRFRH'; 'double stance lhLFRFrh'; 'single stance lhlfRFrh'};
            datStruct.nshoot = nshoot;
            datStruct.h      = h;
            datStruct.h_sca  = [1;0;1;0;1;0;1;1;0;1;1;1];
            datStruct.h_min  = [0.0001;0.0;0.0001;0.0;0.0001;0.0;0.0001;0.0001;0.0;0.0001;0.0001;0.0001];
 %           datStruct.h_min  = [0.2;0.0;0.2;0.2;0.0;0.2];
            datStruct.h_max  = [5.0;0.0;5.0;0.0;5.0;0.0;5.0;5.0;0.0;5.0;5.0;5.0];
            datStruct.h_fix  = [0;1;0;1;0;1;0;0;1;0;0;0];
            p(4) = 0.1; % set speed;
            
        case 'GallopBound'
            sd = zeros(41,1);
            sd(2)  = 0.1;   % dx
            sd(3)  = 1.0;   % y
            sd(4)  = 0.1; % dy
            sd(7)  = 0.0;   % alphaLH
            sd(9)  = 0.0;   % ualphaLH
            sd(11)  = 0.0;   % alphaLF
            sd(13)  = 0.0;   % ualphaLF
            sd(15)  = -0.0;   % alphaRF
            sd(17)  = -0.0;   % ualphaRF
            sd(19)  = 0.0;   % alphaRH
            sd(21)  = 0.0;   % ualphaRH
            sd(23) = 1;   % lLH
            sd(27) = 1;   % lLF
            sd(31) = 1;   % lRF
            sd(35) = 1;   % lRH
            u  = [0;0;0;0;0.0;0.0;0.0;0.0];
            h  = [0.2;0;0.2;0;0.2;0.2;0;0.2;0.2;0;0.2;0.2];
            nshoot = [3;1;3;1;3;3;3;1;3;1;3;3];
            p(3) = 1; % cost function 
            p(5) = 0; % limits (none)
            p(6:9) = [5;4;10;0.5];
            typeString = 'GalloppingBound';
            datStruct.libmodel = 'libGalloppingBound';
            datStruct.libind = {'ind_rkf45'; 'ind_strans'; 'ind_rkf45';'ind_strans'; 'ind_rkf45'; 'ind_rkf45';'ind_rkf45'; 'ind_strans'; 'ind_rkf45';'ind_strans'; 'ind_rkf45'; 'ind_rkf45'};
            datStruct.h_name = {'flight'; 'touchdown collision of LH'; 'single stance LHlfrfrh';'touchdown collision of RH'; 'double stance LHlfrfRH'; 'single stance lhlfrfRH';
                                'flight'; 'touchdown collision of LF'; 'single stance lhLFrfrh' ; 'touchdown collision of RF'; 'double stance lhLFRFrh'; 'single stance lhlfRFrh'};
            datStruct.h_comment = {'flight'; 'touchdown collision of LH'; 'single stance LHlfrfrh';'touchdown collision of RH'; 'double stance LHlfrfRH'; 'single stance lhlfrfRH';
                                'flight'; 'touchdown collision of LF'; 'single stance lhLFrfrh' ; 'touchdown collision of RF'; 'double stance lhLFRFrh'; 'single stance lhlfRFrh'};
            datStruct.nshoot = nshoot;
            datStruct.h      = h;
            datStruct.h_sca  = [1;0;1;0;1;1;1;0;1;0;1;1];
            datStruct.h_min  = [0.0001;0.0;0.0001;0.0;0.0001;0.0001;0.0001;0.0;0.0001;0.0;0.0001;0.0001];
            datStruct.h_max  = [5.0;0.0;5.0;0.0;5.0;5.0;5.0;0.0;5.0;0.0;5.0;5.0];
            datStruct.h_fix  = [0;1;0;1;0;0;0;1;0;1;0;0];
            p(4) = 1; % set speed;
        otherwise
            error('Unknown gait type')
    end
    switch hipJointType
        case 'PEA'
            p(1) = 1;
            hipString = 'parallel';
        case 'SEA'
            p(1) = 0;
            hipString = 'series';
        otherwise
            error('Unknown hip joint type')
    end
    switch legJointType
        case 'PEA'
            p(2) = 1;
            legString = 'parallel';
        case 'SEA'
            p(2) = 0;
            legString = 'series';
        otherwise
            error('Unknown hip joint type')
    end
    
    % Parameterized properties:
    datStruct.header = {'/***********************************************';
                          '*';
                          ['*  Gait optimization for a prismatic biped in a symmetric ',typeString];
                          ['*  gait with ',hipString,' elastic hips and ',legString,' elastic legs'];
                          '*';
                          '*';
                          ['*    0: ',num2str(p(1),'% 8.5f'),'   // hip_jnt_type [0; 1]              Actuator selection hip (0 = series elastic; 1 = parallel elastic)'];
                          ['*    1: ',num2str(p(2),'% 8.5f'),'   // leg_jnt_type [0; 1]              Actuator selection leg (0 = series elastic; 1 = parallel elastic)'];
                          ['*    2: ',num2str(p(3),'% 8.5f'),'   // cost_fct_sel [1; 2; 3]           Cost function selector (0 = posMeWorkCOT; 1 = posActWorkCOT; 2 = posElWorkCOT; 3 = totElLossCOT)'];
                          ['*    3: ',num2str(p(4),'% 8.5f'),'   // v_avg        [sqrt(l_0*g)]       Enforced average velocity'];
                          ['*    4: ',num2str(p(5),'% 8.5f'),'   // const_sel    [binary flags]      1: GroundClearance 2: TorqueLimits 4: SpeedLimits'];
                          ['*    5: ',num2str(p(6),'% 8.5f'),'   // kalpha       [m_0*g*l_0/rad]     Rotational spring stiffness in the hip joint'];
                          ['*    6: ',num2str(p(7),'% 8.5f'),'   // du_max_alpha [rad/sqrt(l_0/g)]   Maximal velocity of the leg extension actuator'];
                          ['*    7: ',num2str(p(8),'% 8.5f'),'   // kl           [m_0*g/l_0]         Linear spring stiffness in the prismatic joint'];
                          ['*    8: ',num2str(p(9),'% 8.5f'),'   // du_max_l     [sqrt(g*l_0)]       Maximal velocity of the leg extension actuator'];
                          ['*    9: ',num2str(p(10),'% 8.5f'),'  // sigma        [0:1]               Logistic Function Tuner'];
                          '*';
                          '***********************************************/'};

    % Common properties:
    datStruct.xd_name = {'x'; 'd x'; 'y'; 'd y'; 'phi'; 'd phi';
                        'alpha LH'; 'd alpha LH'; 'u alpha LH'; 'du alpha LH';
                        'alpha LF'; 'd alpha LF'; 'u alpha LF'; 'du alpha LF';
                        'alpha RF'; 'd alpha RF'; 'u alpha RF'; 'du alpha RF';
                        'alpha RH'; 'd alpha RH'; 'u alpha RH'; 'du alpha RH';
                        'l LH'; 'd l LH'; 'u l LH'; 'du l LH';
                        'l LF'; 'd l LF'; 'u l LF'; 'du l LF';
                        'l RF'; 'd l RF'; 'u l RF'; 'du l RF';
                        'l RH'; 'd l RH'; 'u l RH'; 'du l RH';
                        'Act Work'; 'El Work'; 'El Loss'};
    datStruct.xd_comment = {'x -> must initially remain 0'; 
                             'dx'; 
                             'y'; 
                             'dy'; 
                             'phi'; 
                             'dphi'; 
                             'alphaLH'; 
                             'dalphaLH'; 
                             'ualphaLH'; 
                             'du alphaLH'; 
                             'alphaLF'; 
                             'dalphaLF'; 
                             'ualphaLF'; 
                             'du alphaLF'; 
                             'alphaRF'; 
                             'dalphaRF'; 
                             'ualphaRF'; 
                             'dualphaRF'; 
                             'alphaRH'; 
                             'dalphaRH'; 
                             'ualphaRH'; 
                             'dualphaRH'; 
                             'lLH'; 
                             'dlLH'; 
                             'ulLH';
                             'dulLH';
                             'lLF'; 
                             'dlLF'; 
                             'ulLF';
                             'dulLF';
                             'lRF'; 
                             'dlRF';
                             'ulRF';
                             'dulRF';
                             'lRH'; 
                             'dlRH';
                             'ulRH';
                             'dulRH';
                             'posActWork -> must initially remain 0'; 
                             'posElWork -> must initially remain 0'; 
                             'totElLoss -> must initially remain 0'};

    % If a joint is using parallel actuation no motion in the actuator is
    % necessary 
    if(strcmp(hipJointType, 'PEA'))
        for i = [9,10,13,14]
            datStruct.xd_comment{i} = [datStruct.xd_comment{i},' -> PEA, no actuator motion'];
        end
    end
    if(strcmp(legJointType, 'PEA'))
        for i = [17,18,21,22]
            datStruct.xd_comment{i} = [datStruct.xd_comment{i},' -> PEA, no actuator motion'];
        end
    end
    datStruct.sd = sd;
    datStruct.sd_sca      = ones(41,1);
    % States are unlimited apart from the actuator states (which are bound
    % by +/- 90deg and +/-0.15
    datStruct.sd_min      = [-100; -100; -100; -100; -100; -100;
                            -100; -100; -pi/4; -100;
                            -100; -100; -pi/4; -100;
                            -100; -100; -pi/4; -100;
                            -100; -100; -pi/4; -100;
                            -100; -100; -0.15; -100;
                            -100; -100; -0.15; -100;
                            -100; -100; -0.15; -100;
                            -100; -100; -0.15; -100;
                            -100; -100; -100];
   datStruct.sd_max      = -datStruct.sd_min;
%     datStruct.sd_max      = [100; 100; 100; 100; 100; 100;
%                             0; 100; pi/4; 100;
%                             100; 100; pi/4; 100;
%                             100; 100; pi/4; 100;
%                             0; 100; pi/4; 100;
%                             100; 100; 0.15; 100;
%                             100; 100; 0.15; 100;
%                             100; 100; 0.15; 100;
%                             100; 100; 0.15; 100;
%                             100; 100; 100];
    % initially only the x-position and the cost functions can not be
    % changed:
    datStruct.sd_fix_init = [1;0;0;0;0;0;
                             0;0;0;0;
                             0;0;0;0;
                             0;0;0;0;
                             0;0;0;0;
                             0;0;0;0;
                             0;0;0;0;
                             0;0;0;0;
                             0;0;0;0;
                             1;1;1];

    % Everything can change thereafter:
    datStruct.sd_fix      = zeros(41,1);
    % Change this, if a joint is using parallel actuation (then no motion
    % in the actuator is necessary)
    if(strcmp(hipJointType, 'PEA'))
        datStruct.sd_fix_init([9,10,13,14]) = 1;
        datStruct.sd_fix([9,10,13,14]) = 1;
        datStruct.sd([9,10,13,14],:) = 0;
    end
    if(strcmp(legJointType, 'PEA'))
        datStruct.sd_fix_init([17,18,21,22]) = 1;
        datStruct.sd_fix([17,18,21,22]) = 1;
        datStruct.sd([17,18,21,22],:) = 0;
    end
    
    datStruct.p_name = {'hip jnt type'; 'leg jnt type'; 'cost fct sel'; 'v avg'; 'const_sel'; 'k alpha'; 'du max alpha'; 'k l'; 'du max l'; 'sigma'};
    datStruct.p_comment = {'hip_jnt_type [0; 1]            Actuator selection hip (0 = series elastic; 1 = parallel elastic)'; 
                            'leg_jnt_type [0; 1]            Actuator selection leg (0 = series elastic; 1 = parallel elastic)'; 
                            'cost_fct_sel [1; 2; 3]         Cost function selector (1 = ActWorkCOT; 2 = ElWorkCOT; 3 = ElLossCOT)'; 
                            'v_avg        [sqrt(l_0*g)]     Enforced average velocity'; 
                            'const_sel    [binary flags]    1: GroundClearance 2: TorqueLimits 4: SpeedLimits';
                            'kalpha       [m_0*g*l_0/rad]   Rotational spring stiffness in the hip joint'; 
                            'du_max_alpha [rad/sqrt(l_0/g)] Maximal velocity of the leg extension actuator'; 
                            'kl           [m_0*g/l_0]       Linear spring stiffness in the prismatic joint'; 
                            'du_max_l     [sqrt(g*l_0)]     Maximal velocity of the leg '
                            'sigma        [0:1]             Logistic Function Sensitivity '
                            };
    datStruct.p      = p;
    datStruct.p_sca  = [1; 1; 1; 1.0; 1;     1.0;     1.0;     1.0;    1.0; 0.001
        ];
    datStruct.p_min  = [0; 0; 1; 0.0; 0;  0.0001;  0.0001;  0.0001; 0.0001; 0
        ];
    datStruct.p_max  = [1; 1; 3; 5.0; 7;    15.0;    10.0;    15.0;   10.0; 1
        ];
    % Only the actuator parameters can be changed.
    datStruct.p_fix  = [1; 1; 1; 1;   1;       1;       1;       1;      1; 1
        ];

    datStruct.u_name = {'T alpha LH'; 'T alpha LF'; 'T alpha RF'; 'T alpha RH'; 'F l LH';  'F l LF'; 'F l RF'; 'F l RH'};
    datStruct.u_comment = {'TalphaLH'; 
                            'TalphaLF'; 
                            'TalphaRF'; 
                            'TalphaRH'; 
                            'FlLH'; 
                            'FlLF'; 
                            'FlRF';
                            'FlRH'};
    datStruct.u_type = [0; 0; 0; 0; 0; 0; 0; 0]; % piecewise constant
%     datStruct.u_type = [1; 1; 1; 1; 1; 1; 1; 1]; % piecewise linear
    datStruct.u      = u;
    datStruct.u_sca  = [1.0; 1.0; 1.0; 1.0; 1.0; 1.0; 1.0; 1.0];
    % This limit should never go into effect, since the actuators are
    % limited within the dynamic functions. Yet, it helps stabilize the
    % optimization
    datStruct.u_min  = [-25.0; -25.0; -25.0; -25.0; -25.0; -25.0; -25.0; -25.0];
    datStruct.u_max  = -datStruct.u_min;
    datStruct.u_fix  = [0; 0; 0; 0; 0; 0; 0; 0];

    datStruct.rd_scaStart = 1.0;
    datStruct.rd_scaEnd   = 1.0;
    datStruct.rc_sca      = 1.0;

    datStruct.of_name = 'COT';
    datStruct.of_sca  = 0.5;
    datStruct.of_min  = 0.0;
    datStruct.of_max  = 1.0;
    datStruct.nhist   = 100;

    datStruct.libhessian = 'hess_update';
    datStruct.libsolve   = 'solve_slse';
    datStruct.libcond    = 'cond_std';
    datStruct.libtchk    = 'tchk';
    datStruct.libmssqp   = 'mssqp_standard';
    datStruct.libeval    = 'eval_ind';
    datStruct.libqps     = 'qps_qpopt';
    datStruct.libplot    = 'plot_pgplot';

    datStruct.options_acc           = 1e-6;
    datStruct.options_ftol          = -1.0;
    datStruct.options_itol          = -1.0;
    datStruct.options_rfac          = 0.0;
    datStruct.options_levmar        = 0.0;
    datStruct.options_qp_featol     = 1.0e-8;
    datStruct.options_qp_relax      = 1.1;
    datStruct.options_nhtopy        = 0;
    datStruct.options_frstart       = 0;
    datStruct.options_frmax         = 0;
    datStruct.options_itmax         = 400;
    datStruct.options_plevel_screen = 0;
    datStruct.options_plevel_file   = 1;
    datStruct.options_plevel_matlab = 0;
    datStruct.options_bflag         = -1;
    datStruct.options_qp_itmax      = 10000;
    datStruct.options_qp_expand     = 99999999;
    datStruct.options_sflag         = 0;
    datStruct.options_options_wflag = 0;
    datStruct.options_cflag         = 0;
    datStruct.options_output_ps     = 0;
    datStruct.options_output_gif    = 0;
end