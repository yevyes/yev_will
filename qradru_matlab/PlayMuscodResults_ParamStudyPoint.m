function PlayMuscodResults_ParamStudyPoint(ParamStudyName, Veloc)
    %% Init:
    % Reset the MATLAB search path to its default value:
    path(pathdef);
    % Set the path to include all library functions:
    path(path,'/home/yevyes/Yev_Will/qradru_matlab_flex/playqra2');
    load(ParamStudyName)

    %% Read file
    [t, y, u, yName, uName] = ReadPLTfile('BaseData/Trot_SH_SL_BASE_02',41,8);

    %% Read file
   %[t, y, u, yName, uName] = ReadPLTfile('BaseData/WALK_PH_PL_BASE_00',25,4);

   tol = 0.001; % Tolerance to find velocity
   
   for i = 1:length(configurations)
       if configurations{i}.nFailed < 5
           if (abs(configurations{i}.finalDatStruct.p(4) - Veloc) < tol)
               ind1 = configurations{i}.indexVector(1);
               %ind2 = configurations{i}.indexVector(2);
               p = configurations{i}.finalDatStruct.p;
               CostFuncVal = configurations{i}.costValue;
               disp(['i value is: ' num2str(i)])
               disp(['cost value: ' num2str(CostFuncVal)])
           end
       end%             u(exctStateIndices.ulL) = ulLVec(j);
%             u(exctStateIndices.ulR) = ulRVec(j); 
   end
   
   clear t y u
   
   t = time{ind1};
   totaltime = t(end)
   u = forces{ind1};
   y = states{ind1};
   h = hval{ind1}
   
%    forcesParam = u;
%    statesParam = y;
%    timeParam = t;
%    hParam = h;
%    
% Plot motions
   
% %     stateIndices = 1:21;
%     stateIndices = [1 3 5 7 8 11 12 15 19];
%    
%     figure(1)
%     clf
%     hold on
%     grid on
%     box on
%     plot(t,u)
%     % Show limits on actuator torques:
%     legend(uName{:});
%     
%     figure(2)
%     clf
%     hold on
%     grid on
%     box on
%     plot(t,y(:,stateIndices))
%     legend(yName{stateIndices});
%     
%     
%     figure(3)
    
    %% Extract trajectories
    tVec       = [t(1:end-1,1);t(1:end-1,1)+t(end,1)];
    xVec       = [y(1:end-1,1);y(1:end-1,1)+y(end,1)];
    yVec       = [y(1:end-1,3);y(1:end-1,3)];
    phiVec     = [y(1:end-1,5);y(1:end-1,5)];
    alphaLHVec  = [y(1:end-1,7);y(1:end-1,19)];
    ualphaLHVec = [y(1:end-1,9);y(1:end-1,21)];
    alphaLFVec  = [y(1:end-1,11);y(1:end-1,15)];
    ualphaLFVec = [y(1:end-1,13);y(1:end-1,17)];
    alphaRFVec  = [y(1:end-1,15);y(1:end-1,11)];
    ualphaRFVec = [y(1:end-1,17);y(1:end-1,13)];
    alphaRHVec  = [y(1:end-1,19);y(1:end-1,7)];
    ualphaRHVec = [y(1:end-1,21);y(1:end-1,9)];
    lLHVec      = [y(1:end-1,23);y(1:end-1,35)];
    ulLHVec     = [y(1:end-1,25);y(1:end-1,37)];
    lLFVec      = [y(1:end-1,27);y(1:end-1,31)];
    ulLFVec     = [y(1:end-1,29);y(1:end-1,33)];
    lRFVec      = [y(1:end-1,31);y(1:end-1,27)];
    ulRFVec     = [y(1:end-1,33);y(1:end-1,29)];
    lRHVec      = [y(1:end-1,35);y(1:end-1,23)];
    ulRHVec     = [y(1:end-1,37);y(1:end-1,25)];
    % Resample
    method = 'linear';
    n = 50;      % # of frames per step
    % to create destinct values:
    tOld       = tVec+linspace(0,1e-10,length(tVec))';
    tVec       = linspace(0,2*t(end,1),n);
    tVec       = tVec(1:end-1);
    xVec       = interp1(tOld, xVec,tVec, method);
    yVec       = interp1(tOld, yVec,tVec, method);
    phiVec     = interp1(tOld, phiVec,tVec, method);
    alphaLHVec  = interp1(tOld, alphaLHVec,tVec, method);
    ualphaLHVec = interp1(tOld, ualphaLHVec,tVec, method);
    alphaLFVec  = interp1(tOld, alphaLFVec,tVec, method);
    ualphaLFVec = interp1(tOld, ualphaLFVec,tVec, method);
    alphaRFVec  = interp1(tOld, alphaRFVec,tVec, method);
    ualphaRFVec = interp1(tOld, ualphaRFVec,tVec, method);
    alphaRHVec  = interp1(tOld, alphaRHVec,tVec, method);
    ualphaRHVec = interp1(tOld, ualphaRHVec,tVec, method);
    lLHVec      = interp1(tOld, lLHVec,tVec, method);
    ulLHVec     = interp1(tOld, ulLHVec,tVec, method);
    lLFVec      = interp1(tOld, lLFVec,tVec, method);
    ulLFVec     = interp1(tOld, ulLFVec,tVec, method);
    lRFVec      = interp1(tOld, lRFVec,tVec, method);
    ulRFVec     = interp1(tOld, ulRFVec,tVec, method);
    lRHVec      = interp1(tOld, lRHVec,tVec, method);
    ulRHVec     = interp1(tOld, ulRHVec,tVec, method);
%     % Plot
%     figure(3)
%     clf
%     hold on
%     plot(tVec,xVec,'r');
%     plot(tVec,yVec,'b');
%     plot(tVec,phiVec,'g');
%     plot(tVec,alphaLVec,'y');
%     plot(tVec,ualphaLVec,'y:');
%     plot(tVec,alphaRVec,'c');
%     plot(tVec,ualphaRVec,'c:');
%     plot(tVec,lLVec,'k');
%     plot(tVec,ulLVec,'k:');
%     plot(tVec,lRVec,'m');
%     plot(tVec,ulRVec,'m:');
     %% Animate:
	% Set up the graphical output:
    graph3DOUTPUT = Quadruped3DCLASS(false);
    xOffset = 0; % This shifts the model forward from step to step
    % If desired, every iteration a rendered picture is saved to disc.  This
    % can later be used to create a animation of the monopod.
    % (un)comment the following lines, if you (don't) want to save the
    % individual frames to disc:
    %frameCount = 0; 
    %mkdir MovieFramesPrismaticBipedRunWalk;
    [y, ~, contStateIndices] = ContStateDefinition();
    z = DiscStateDefinition();
    [u, ~, exctStateIndices] = ExctStateDefinition();
    %  For each step
    for i = 1:2 % Movie is composed of 2*2 strides
        for j = 1:n-1
            y(contStateIndices.x) = xVec(j) + xOffset;
            y(contStateIndices.y) = yVec(j);
            y(contStateIndices.phi) = phiVec(j);
            y(contStateIndices.alphaLH) = alphaLHVec(j);
            y(contStateIndices.alphaLF) = alphaLFVec(j);
            y(contStateIndices.alphaRF) = alphaRFVec(j);
            y(contStateIndices.alphaRH) = alphaRHVec(j);
            y(contStateIndices.lLH) = lLHVec(j);
            y(contStateIndices.lLF) = lLFVec(j);
            y(contStateIndices.lRF) = lRFVec(j);
            y(contStateIndices.lRH) = lRHVec(j);
            u(exctStateIndices.ualphaLH) = ualphaLHVec(j); 
            u(exctStateIndices.ualphaLF) = ualphaLFVec(j); 
            u(exctStateIndices.ualphaRF) = ualphaRFVec(j); 
            u(exctStateIndices.ualphaRH) = ualphaRHVec(j); 
            u(exctStateIndices.ulLH) = ulLHVec(j); 
            u(exctStateIndices.ulLF) = ulLFVec(j); 
            u(exctStateIndices.ulRF) = ulRFVec(j); 
            u(exctStateIndices.ulRH) = ulRHVec(j); 
            graph3DOUTPUT.update(y,z,[],u);
            % (un)comment the following line, if you (don't) want to save the
            % individual frames to disc:
           % fig = gcf; print(fig,'-r600','-djpeg',['MovieFramesPrismaticBipedRunWalk/Frame',num2str(frameCount,'%04d.jpg')],'-zbuffer'); frameCount = frameCount + 1;
        end
        % After each step the model is moved forward:
        xOffset = xOffset + xVec(end);
    end     
end